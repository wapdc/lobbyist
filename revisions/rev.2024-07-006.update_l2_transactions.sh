#!/usr/bin/env bash
set -e

$PSQL_CMD <<EOF
  ALTER TABLE l2_expense
  RENAME COLUMN stat TO state;
EOF
