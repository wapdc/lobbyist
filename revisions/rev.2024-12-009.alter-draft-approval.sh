set -e
$PSQL_CMD <<EOF

alter table private.draft_document
    alter column approval_target_id type int using approval_target_id::int;
EOF
