#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/update_legacy_report_data.sql
$PSQL_CMD -f db/functions/l1_legacy_contract_update.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_contract.sql

$PSQL_CMD <<EOF
SELECT l1_legacy_contract_update(lobbyist_contract_id) from lobbyist_contract
EOF
