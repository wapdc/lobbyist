#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/views/od_l3_reporting_history.sql
$PSQL_CMD -f db/functions/update_l3_legacy_report_data.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_l3.sql
$PSQL_CMD -f db/fixes/fix_l3_legacy_report_data.sql
