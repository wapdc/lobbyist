#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/views/od_l2_imaged_documents_and_reports.sql
$PSQL_CMD -f db/views/od_l2_imaged_documents_and_reports_config.sql