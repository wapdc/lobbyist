#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/sync/lobbyist_sync_firm.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_employer.sql
