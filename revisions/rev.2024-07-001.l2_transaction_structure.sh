#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/l2_transaction.sql
$PSQL_CMD -f db/l2_expense.sql
$PSQL_CMD -f db/l2_entertainment.sql
$PSQL_CMD -f db/l2_contribution.sql
