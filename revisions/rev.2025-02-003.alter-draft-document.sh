set -e
$PSQL_CMD <<EOF
drop index private.target_type_idx;
drop index private.target_id_idx;
create index approval_target_id_idx
    on private.draft_document (approval_target_id);
EOF
