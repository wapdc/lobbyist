#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
alter table public.l3_compensation
  add if not exists official_name text;
EOF
