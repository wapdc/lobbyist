#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/l2_legacy_transactions.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_accesshub.sql
$PSQL_CMD -f db/functions/l2_save_transactions.sql
