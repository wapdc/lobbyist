#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/views/od_lobbyist_registration_summary.sql
$PSQL_CMD -f db/views/od_lobbyist_registration_summary_config.sql
