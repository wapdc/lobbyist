#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/l3_legacy_transactions.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_l3.sql