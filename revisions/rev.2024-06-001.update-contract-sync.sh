#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/sync/lobbyist_sync_draft_contract.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_contract.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_accesshub.sql
