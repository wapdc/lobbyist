#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
ALTER TABLE lobbyist_firm
    ALTER COLUMN lobbyist_firm_id SET DEFAULT nextval('lobbyist_report_seq');

ALTER TABLE lobbyist_firm_submission
    ALTER COLUMN submission_id SET DEFAULT nextval('lobbyist_submission_seq');

ALTER TABLE lobbyist_client
    ALTER COLUMN lobbyist_client_id SET DEFAULT nextval('lobbyist_report_seq');

ALTER TABLE lobbyist_client_submission
    ALTER COLUMN submission_id SET DEFAULT nextval('lobbyist_submission_seq');

ALTER TABLE lobbyist_contract
    ALTER COLUMN lobbyist_contract_id SET DEFAULT nextval('lobbyist_report_seq');

ALTER TABLE lobbyist_contract_submission
    ALTER COLUMN submission_id SET DEFAULT nextval('lobbyist_submission_seq');
EOF
