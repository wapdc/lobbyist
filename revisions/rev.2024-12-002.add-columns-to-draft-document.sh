#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF

ALTER TABLE private.draft_document
    ADD approval_target_type text, ADD approval_target_id text;
CREATE UNIQUE INDEX target_type_idx ON private.draft_document (approval_target_id);
CREATE UNIQUE INDEX target_id_idx ON private.draft_document (approval_target_id);

EOF