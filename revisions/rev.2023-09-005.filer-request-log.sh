#!/usr/bin/env bash
set -e

psql wapdc -f db/filer_request_log.sql

$PSQL_CMD <<EOF
DO \$\$
BEGIN

-- Rename column only if it exists
IF EXISTS (
    SELECT 1
    FROM information_schema.columns
    WHERE table_name='filer_request'
    AND column_name='filer_request'
) THEN
    ALTER TABLE filer_request RENAME COLUMN filer_request TO id;
END IF;

-- Add agent_user_name only if it doesn't exist
IF NOT EXISTS (
    SELECT 1
    FROM information_schema.columns
    WHERE table_name='filer_request'
    AND column_name='agent_user_name'
) THEN
    ALTER TABLE filer_request ADD COLUMN agent_user_name text;
END IF;

-- Add action_date only if it doesn't exist
IF NOT EXISTS (
    SELECT 1
    FROM information_schema.columns
    WHERE table_name='filer_request'
    AND column_name='action_date'
) THEN
    ALTER TABLE filer_request ADD COLUMN action_date date;
END IF;

-- Add target_type only if it doesn't exist
IF NOT EXISTS (
    SELECT 1
    FROM information_schema.columns
    WHERE table_name='filer_request'
    AND column_name='target_type'
) THEN
    ALTER TABLE filer_request ADD COLUMN target_type text;
END IF;

END
\$\$;
EOF
