#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
alter table public.lobbyist_reporting_periods
    add if not exists exempt  boolean default false not null;
EOF
$PSQL_CMD wapdc -f  db/functions/sync/lobbyist_sync_contract.sql
$PSQL_CMD wapdc <<EOF
select lobbyist_sync_contract(s.user_data) from lobbyist_contract_submission s
where user_data->'field_exemption_periods'->'und' is not null and superseded_id is null;
EOF
