#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF

alter table lobbyist_contract
rename column subcontractor_id to contractor_id;
alter table lobbyist_contract drop constraint lobbyist_contract_subcontractor_id_fkey;
alter table lobbyist_contract add constraint lobbyist_contract_contractor_id_fkey foreign key (contractor_id)
references lobbyist_firm(lobbyist_firm_id) on delete cascade;

EOF