#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
alter table l3_transaction
    add if not exists official_type text;

alter table l3_transaction
    add if not exists official_name text;

alter table l3_transaction
    add if not exists is_family boolean default false;

alter table l3_transaction
    drop if exists title;
EOF

$PSQL_CMD -f db/functions/l3_save_transactions.sql;

$PSQL_CMD -f db/functions/l3_legacy_transactions.sql;