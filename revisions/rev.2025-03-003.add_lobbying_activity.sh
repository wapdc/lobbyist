#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/tables/l2_lobbying_activity.sql
$PSQL_CMD -f db/functions/l2_save_lobbying_activity.sql
$PSQL_CMD -f db/functions/l2_save_transactions.sql
$PSQL_CMD -f db/fixes/populate_l2_lobbying_activity.sql
