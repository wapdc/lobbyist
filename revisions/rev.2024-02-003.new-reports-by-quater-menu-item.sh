#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF

INSERT INTO menu_links (
    category,
    menu,
    title,
    abstract,
    url,
    permission
)
VALUES (
           'Lobbying',
           'staff',
           'New reports by quarter',
           'See how many reports of what kind have been filed over the history of the lobbyist system',
           '/lobbyist/admin/new-reports-by-quarter',
           'access wapdc data'
       )

EOF