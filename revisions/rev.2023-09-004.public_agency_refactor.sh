#!/usr/bin/env bash
set -e

$PSQL_CMD <<EOF
alter table lobbyist_public_agency
    drop column l5filer_id;
EOF
