#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
alter table lobbyist_firm_profile drop column if exists drupal_nid;
alter table lobbyist_firm_profile_submission add column if not exists drupal_vid int;
alter table lobbyist_client_profile drop column if exists drupal_nid;
alter table lobbyist_client_profile_submission add column if not exists drupal_vid int;
alter table public.lobbyist_firm_profile
    rename column address1 to address;
alter table public.lobbyist_client_profile
    rename column address1 to address;
alter table public.lobbyist_firm_profile drop column if exists address2;
alter table public.lobbyist_client_profile drop column if exists address2;
EOF
