#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/update_legacy_report_data.sql
$PSQL_CMD -f db/fixes/fix_legacy_report_data.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_firm.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_l2.sql
$PSQL_CMD -f db/views/od_l2_reporting_history.sql
$PSQL_CMD -f db/views/od_l3_reporting_history.sql
$PSQL_CMD -f db/views/od_lobbyist_client_history.sql
$PSQL_CMD -f db/views/od_lobbyist_firm_history.sql
$PSQL_CMD -f db/views/od_lobbyist_contract_history.sql
$PSQL_CMD -f db/views/od_lobbyist_reporting_history.sql
$PSQL_CMD -f db/views/od_lobbyist_reporting_history_config.sql
