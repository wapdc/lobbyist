#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
UPDATE public.menu_links
SET abstract = 'Register as local or state governmental agency that directly lobbies at the state level and file required reports (L5).'
WHERE link_id = 15;
EOF
