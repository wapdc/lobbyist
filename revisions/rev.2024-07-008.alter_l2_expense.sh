#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
alter table l2_expense add column date date;
EOF