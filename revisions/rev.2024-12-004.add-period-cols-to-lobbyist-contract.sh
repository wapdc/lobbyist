#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
ALTER TABLE lobbyist_contract
    ADD if not exists period_start date, ADD if not exists period_end date;
create index if not exists lobbyist_contract_period_start_index
    on lobbyist_contract (period_start);
EOF
