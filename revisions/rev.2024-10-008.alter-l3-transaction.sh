#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
alter table public.l3_transaction
    add if not exists name text;
alter table public.l3_transaction
    add if not exists title text;
EOF
