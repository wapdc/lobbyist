#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
DROP FUNCTION IF EXISTS l7_submit
EOF

$PSQL_CMD -f db/functions/l7_submit.sql