#!/usr/bin/env bash
set -e
$PSQL_CMD wapdc <<EOF
alter table public.l3_compensation
    add if not exists official_type text;

alter table public.l3_compensation
    add if not exists is_family boolean default false;

alter table public.l3_compensation
    drop if exists relationship;

alter table public.l3_compensation
    drop if exists relation;
EOF

$PSQL_CMD -f db/functions/l3_legacy_transactions.sql;

$PSQL_CMD -f db/functions/l3_save_transactions.sql;



