#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/sync/lobbyist_sync_employer.sql
$PSQL_CMD <<EOF
with client_submission_fix as (select lcs.lobbyist_client_id,
                                      lcs.submission_id,
                                      row_number()
                                      over (partition by lc.lobbyist_client_id order by submitted_at) as row_num
                               from lobbyist_client lc
                                        join lobbyist_client_submission lcs
                                             on lcs.lobbyist_client_id = lc.lobbyist_client_id)

update lobbyist_client
set current_submission_id = f.submission_id
from (
select * from client_submission_fix
         where row_num = 1) f
where lobbyist_client.lobbyist_client_id = f.lobbyist_client_id
  and lobbyist_client.current_submission_id is null;
EOF