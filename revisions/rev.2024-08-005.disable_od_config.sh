#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/views/od_lobbyist_agent_config.sql
$PSQL_CMD -f db/views/od_lobbyist_compensation_and_expenses_config.sql