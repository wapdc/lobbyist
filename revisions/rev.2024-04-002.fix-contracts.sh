#!/usr/bin/env bash
set -e
# make sure that we are running current sync code.
cd db/functions/sync
$PSQL_CMD -f install_sync.sql
# Change to fixes directory
cd ../../fixes
$PSQL_CMD -f correct_lobbyist_contracts.sql



