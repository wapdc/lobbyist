#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
update menu_links set url = replace(url, '/lobbyist/', '/lobbyist/#/') where url ilike '/lobbyist/%'
EOF
$PSQL_CMD -f db/views/od_l7_imaged_documents_and_reports.sql
$PSQL_CMD -f db/views/od_l2_imaged_documents_and_reports.sql
$PSQL_CMD -f db/views/od_l7.sql
