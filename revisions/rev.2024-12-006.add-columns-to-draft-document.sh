set -e
$PSQL_CMD <<EOF

ALTER TABLE private.draft_document
    ADD if not exists period_start date, ADD if not exists period_end date;

EOF