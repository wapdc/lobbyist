#!/usr/bin/env bash
set -e

if [ "$STAGE" != live ]
then
$PSQL_CMD -f db/fixes/lobbyist_firm_migration.sql
fi