#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/fixes/update_l2_ad_type.sql
$PSQL_CMD -f db/functions/l2_legacy_transactions.sql
#install views and config
$PSQL_CMD -f db/views/od_l2_reporting_history.sql
$PSQL_CMD -f db/views/od_l3_reporting_history.sql
$PSQL_CMD -f db/views/od_lobbyist_contract_history.sql
$PSQL_CMD -f db/views/od_lobbyist_client_history.sql
$PSQL_CMD -f db/views/od_lobbyist_firm_history.sql
$PSQL_CMD -f db/views/od_lobbyist_agent.sql
$PSQL_CMD -f db/views/od_lobbyist_agent_config.sql
$PSQL_CMD -f db/views/od_lobbyist_reporting_history.sql
$PSQL_CMD -f db/views/od_lobbyist_reporting_history_config.sql
$PSQL_CMD -f db/views/od_lobbyist_compensation_and_expenses.sql
$PSQL_CMD -f db/views/od_lobbyist_compensation_and_expenses_config.sql
$PSQL_CMD -f db/views/od_lobbyist_summary.sql
$PSQL_CMD -f db/views/od_lobbyist_summary_config.sql
