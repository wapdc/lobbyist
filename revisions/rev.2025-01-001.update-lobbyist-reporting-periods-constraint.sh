set -e
$PSQL_CMD <<EOF

alter table lobbyist_reporting_periods drop constraint lobbyist_reporting_periods_pkey;
alter table lobbyist_reporting_periods add primary key (period_start, period_end, lobbyist_contract_id);
EOF

$PSQL_CMD -f db/functions/sync/lobbyist_sync_contract.sql
