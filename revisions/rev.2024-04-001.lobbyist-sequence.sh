#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/sequences/lobbyist_report_seq.sql
$PSQL_CMD -f db/sequences/lobbyist_submission_seq.sql
$PSQL_CMD <<EOF
ALTER TABLE l2
    ALTER COLUMN report_id SET DEFAULT nextval('lobbyist_report_seq');

ALTER TABLE l2_submission
    ALTER COLUMN submission_id SET DEFAULT nextval('lobbyist_submission_seq');

EOF