set -e
$PSQL_CMD -f db/views/od_lobbyist_compensation_and_expenses.sql
$PSQL_CMD <<EOF
update l2 set period_start=period_start where report_id in (select l2.report_id from l2 join l2_transaction t on t.report_id=l2.report_id
    join l2_submission s  on s.submission_id=l2.current_submission_id
    where t.transaction_type='sub_lobbyist_payment'
    and s.version='2.0');
EOF
