#!/usr/bin/env bash
set -e

psql wapdc -f db/functions/lobbyist_submit_filer_request.sql
$PSQL_CMD <<EOF
INSERT INTO wapdc_settings(property, stage, value) VALUES ('lobbyist_aws_gateway', 'dev', 'http://127.0.0.1:3010');
EOF