#!/usr/bin/env bash
set -e

cd db/functions/sync
$PSQL_CMD -f install_sync.sql
