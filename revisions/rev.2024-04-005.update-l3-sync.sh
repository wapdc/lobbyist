#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/sync/lobbyist_sync_l3.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_draft_l2.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_draft_l3.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_accesshub.sql
