#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/views/od_lobbyist_contract_history.sql
$PSQL_CMD -f db/views/od_lobbyist_reporting_history.sql
$PSQL_CMD -f db/functions/update_l1_legacy_report_data.sql
$PSQL_CMD -f db/functions/sync/lobbyist_sync_contract.sql
$PSQL_CMD -f db/fixes/fix_l1_legacy_report_data.sql
