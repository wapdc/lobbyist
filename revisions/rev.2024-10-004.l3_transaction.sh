#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/l3_transaction.sql
$PSQL_CMD -f db/l3_compensation.sql
$PSQL_CMD -f db/l3_contribution.sql
$PSQL_CMD -f db/l3_lobbyist.sql
$PSQL_CMD -f db/l3_independent_expenditure.sql