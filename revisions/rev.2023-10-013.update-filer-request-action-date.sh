#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
alter table filer_request
alter column action_date set default now();

EOF
