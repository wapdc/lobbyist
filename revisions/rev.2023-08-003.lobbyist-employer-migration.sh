#!/usr/bin/env bash
set -e

if [ "$STAGE" != live ]
then
$PSQL_CMD -f db/fixes/lobbyist_employer_migration.sql
$PSQL_CMD -f db/fixes/lobbyist_firm_migration.sql
fi
$PSQL_CMD <<EOF
alter table lobbyist_employer
    alter column lobbyist_employer_id drop default;

drop sequence lobbyist_employer_lobbyist_employer_id_seq;
alter table lobbyist_firm
    alter column lobbyist_firm_id drop default;

drop sequence lobbyist_firm_lobbyist_firm_id_seq;
EOF
