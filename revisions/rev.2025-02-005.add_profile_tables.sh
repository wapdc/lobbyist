#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/tables/lobbyist_client_profile.sql
$PSQL_CMD -f db/tables/lobbyist_client_profile_submission.sql
$PSQL_CMD -f db/tables/lobbyist_firm_profile.sql
$PSQL_CMD -f db/tables/lobbyist_firm_profile_submission.sql
$PSQL_CMD -f db/tables/lobbyist_client_funders.sql
