
#!/usr/bin/env bash
set -e

## the firm and employer tables were moved to core
#psql wapdc -f db/tables/lobbyist_employer.sql
#psql wapdc -f db/tables/lobbyist_firm.sql

psql wapdc -f db/tmp_lobbyist_entity_crosswalk.sql