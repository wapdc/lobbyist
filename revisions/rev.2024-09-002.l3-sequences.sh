#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
ALTER TABLE l3
    ALTER COLUMN report_id SET DEFAULT nextval('lobbyist_report_seq');

ALTER TABLE l3_submission
    ALTER COLUMN submission_id SET DEFAULT nextval('lobbyist_submission_seq');
EOF
