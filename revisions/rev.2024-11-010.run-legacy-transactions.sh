#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/l3_legacy_transactions.sql
$PSQL_CMD <<EOF
SELECT l3_legacy_transactions(s.user_data) from l3 l
    join l3_submission s on s.submission_id = l.current_submission_id
where s.version = '1.0'
EOF
