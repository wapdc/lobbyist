#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/l3_legacy_transactions.sql
$PSQL_CMD -f db/functions/l3_save_transactions.sql
$PSQL_CMD -f db/views/od_lobbyist_employer_summary.sql
$PSQL_CMD -f db/views/od_lobbyist_employer_summary_config.sql