#!/usr/bin/env bash
set -e
$PSQL_CMD -f db/functions/accesshub_url.sql
$PSQL_CMD -f db/views/od_lobbyist_agent.sql
$PSQL_CMD -f db/views/od_lobbyist_agent_config.sql