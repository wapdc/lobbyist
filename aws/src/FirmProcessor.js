import * as rds from "@wapdc/common/wapdc-db"
import {legacyProcessor} from "./LegacyProcessor.js";

class FirmProcessor {
  async getFirms() {
    return await rds.query(`
    SELECT * FROM lobbyist_firm ORDER BY name`);
  }

  async getFirm(firm_id) {
    return await rds.fetch(`
        SELECT f.*
        FROM lobbyist_firm f
        WHERE f.lobbyist_firm_id = $1
    `, [firm_id])
  }

  async getFirmUsers() {
    return await rds.query(`
      SELECT
        f.lobbyist_firm_id AS "firm_id",
        f.name AS "firm_name",
        f.email AS "firm_email",
        f.phone AS "firm_phone",
        u.user_name AS "user_name",
         u.accesshub_uid AS "accesshub_id"
       /** u.verification_payload->>'email' AS "user_email" */
      FROM pdc_user u
        JOIN pdc_user_authorization ua
          ON u.uid = ua.uid
          AND u.realm = ua.realm
        JOIN lobbyist_firm f
          ON ua.target_id = f.lobbyist_firm_id
          AND ua.target_type = 'lobbyist_firm';
    `);
  }

  async getLobbyistFirmRegistration(firm_id, submission_id) {
    let result = await rds.fetch(`
        select s.*
        from lobbyist_firm_submission s
        where s.submission_id = $1 and s.lobbyist_firm_id=$2
    `, [submission_id, firm_id]);

    result.user_data = legacyProcessor.preProcessLegacyRecord(result.user_data);
    return result;
  }

  async getProfiles(firmId) {
    return await rds.query(`select p.*,s.*, extract(year from p.period_start) || '-' || extract(year from p.period_end) as biennium
      from lobbyist_firm_profile p 
          join lobbyist_firm_profile_submission s on p.current_submission_id=s.submission_id
      where p.lobbyist_firm_id = $1
          order by period_end desc
      `, [firmId])
  }

  /**
   * Retrieve a specific firm profile submission for a firm.
   * @param firmId Id of the firm for the profile being retrieved
   * @param submissionId Id of the profile being retrieved.
   * @return {Promise<*|null>}
   */
  async getFirmProfileSubmission(firmId, submissionId) {
    return await rds.fetch(`select p.period_start, p.period_end, p.profile_id, s.* from lobbyist_firm_profile_submission s 
      join lobbyist_firm_profile p on p.profile_id=s.profile_id 
       where p.lobbyist_firm_id = $1 and s.submission_id=$2`, [firmId, submissionId])
  }

  async getFirmProfileHistory(firmId, submissionId) {
    return await rds.query(`select s.* from lobbyist_firm_profile_submission s join lobbyist_firm_profile p 
      on p.profile_id=s.profile_id where p.lobbyist_firm_id = $1 and p.profile_id = $2
        order by submitted_at desc, submission_id desc
     `, [firmId, submissionId])
  }


  async getFirmSubmissionHistory(firm_id)  {
    return rds.query(`
      SELECT s.* FROM lobbyist_firm_submission s
      WHERE s.lobbyist_firm_id = $1
    `, [firm_id])
  }

  async getFirmAgents(firm_id) {
    return await rds.query(`
      SELECT a.* FROM lobbyist_agent a           
      WHERE a.lobbyist_firm_id = $1
        AND now() between a.starting and coalesce(a.ending, now())
    `, [firm_id]);
  }

  async getL2Periods(firm_id, hasDraftAccess) {
    return await rds.query(`
        select 
          lr.period_start,
          l2.report_id, 
          l2s.submission_id,
          l2s.submitted_at,
          rp.due_date,
          dd.draft_id,
          case 
            when l2.report_id is not null and dd.draft_id is not null and $2::bool then 'Amending'
            when l2.report_id is not null then 'Filed'
            when coalesce(dd.draft_id, dl.draft_id) is not null and $2::bool then 'Started'
            when lr.exempt then 'Exempt' 
            when now() > due_date  + interval '1 day' then 'Overdue' end as status,
          case when dl.draft_id is not null then true end as legacy_draft,
          dl.user_data->>'nid' as drupal_nid
          from
            (select lrp.lobbyist_firm_id, 
                    lrp.period_start,
                    bool_and(lrp.exempt) as exempt 
             from lobbyist_reporting_periods lrp 
                 join lobbyist_contract lc on lc.lobbyist_contract_id=lrp.lobbyist_contract_id
                 
             where $1::int in (lc.lobbyist_firm_id, lc.lobbyist_contract_id)
               and lrp.period_start <= now()
               and lrp.period_start >= '2017-10-01'
             group by lrp.lobbyist_firm_id, lrp.period_start, lrp.period_end                                                                    
             ) lr
            join reporting_period rp on rp.report_type='L2' and rp.start_date=lr.period_start     
            left join l2 on l2.lobbyist_firm_id=lr.lobbyist_firm_id and l2.period_start=lr.period_start
            left join l2_submission l2s on l2s.submission_id=l2.current_submission_id
            left join private.draft_document dd on dd.report_type = 'L2' and dd.target_type='lobbyist_firm'
              and dd.target_id= lr.lobbyist_firm_id 
             and dd.report_key=cast(lr.period_start as text)
            left join private.draft_document dl on dl.report_type = 'L2-Legacy' and dl.target_type='lobbyist_firm'
                and dl.target_id= lr.lobbyist_firm_id
                and dl.report_key=cast(lr.period_start as text)
            order by lr.period_start desc
              
    `, [firm_id, hasDraftAccess]);
  }

  async getFirmClients(firm_id) {
    return await rds.query(`
        select lcr.*
                from (select lc.lobbyist_firm_id,
                             lc.lobbyist_contract_id as lobbyist_contract_id,
                             lc.current_submission_id,
                             lc.period_start,
                             lc.period_end,
                             c.name,
                             c.lobbyist_client_id,
                             x.years_filed
                      from lobbyist_contract lc
                               join lobbyist_client c on lc.lobbyist_client_id = c.lobbyist_client_id
                               left join (select json_agg(contract_year) as years_filed,
                                                 lobbyist_client_id,
                                                 lobbyist_firm_id
                                          from (select distinct(date_part('year', period_start)) as contract_year,
                                                               lrp.lobbyist_client_id,
                                                               lrp.lobbyist_firm_id
                                                from lobbyist_reporting_periods lrp
                                                where $1 = lrp.lobbyist_firm_id) x
                                          group by lobbyist_client_id, lobbyist_firm_id) x
                                         on x.lobbyist_client_id = c.lobbyist_client_id
                      where lc.lobbyist_firm_id = $1 and lc.contractor_id is null) lcr
    `, [firm_id]);
  }

    async getEmploymentContracts(firm_id) {
        return await rds.query(`
        with sub as (select lc.lobbyist_firm_id,
                            lc.lobbyist_contract_id as lobbyist_contract_id,
                            lc.contractor_id,
                            lc.current_submission_id,
                            c.name,
                            c.lobbyist_client_id, 
                            lf.name                 as subcontractor_name,
                            lrp.period_end,
                            row_number() over (PARTITION BY lc.lobbyist_contract_id order by lrp.period_end desc) as employment_status
                     from lobbyist_contract lc
                              join lobbyist_client c on lc.lobbyist_client_id = c.lobbyist_client_id
                              join lobbyist_firm lf on lc.contractor_id = lf.lobbyist_firm_id
                              left join lobbyist_reporting_periods lrp on lrp.lobbyist_contract_id = lc.lobbyist_contract_id
                     where lc.lobbyist_firm_id = $1 and lc.contractor_id is not null)
        select * from sub where sub.employment_status = 1
    `, [firm_id]);
    }

    async getSubcontractedLobbyists(firm_id) {
        return await rds.query(`
        with sub as (select lc.lobbyist_firm_id,
                            lc.lobbyist_contract_id as lobbyist_contract_id,
                            lc.contractor_id,
                            lc.current_submission_id,
                            c.name,
                            c.lobbyist_client_id,
                            lf.name                 as subcontracted_lobbyist_name,
                            lrp.period_end,
                            row_number() over (PARTITION BY lc.lobbyist_contract_id order by lrp.period_end desc) as employment_status
                     from lobbyist_contract lc
                              join lobbyist_client c on lc.lobbyist_client_id = c.lobbyist_client_id
                              join lobbyist_firm lf on lc.lobbyist_firm_id = lf.lobbyist_firm_id
                              left join lobbyist_reporting_periods lrp on lrp.lobbyist_contract_id = lc.lobbyist_contract_id
                     where lc.contractor_id = $1)
        select * from sub where sub.employment_status = 1
    `, [firm_id]);
    }

  async getL2(firm_id, report_id) {
    let l2 = await rds.fetch(`
      select s.*, l2.*, dd.draft_id,
            (select json_agg(json_build_object('tid', a.tid, 'name', a.name)) from accesshub_taxonomy_term_data a) as taxonomy,
            (select json_agg(json_build_object('id', lc.lobbyist_contract_id, 'name', lf.name))
                    from lobbyist_contract lc 
                    left join lobbyist_firm lf on lf.lobbyist_firm_id = lc.lobbyist_firm_id
                    where lc.contractor_id = $1) as subcontracts,
            (select clients::jsonb || contracts::jsonb
             from (select json_agg(json_build_object('id', lc.lobbyist_contract_id, 'name', 
                                                     case when lc.contractor_id is null then l.name else concat(l.name, ' (', f.name, ')') end)) as contracts, 
                    json_agg(json_build_object('id', lc.lobbyist_client_id, 'name', l.name)) as clients
             from lobbyist_contract lc 
              left join lobbyist_client l on lc.lobbyist_client_id = l.lobbyist_client_id
              left join lobbyist_firm f on f.lobbyist_firm_id = lc.contractor_id
             where lc.lobbyist_firm_id = $1) c) as contract_lookup
             from l2_submission s
               left join l2 on l2.report_id = s.report_id
               left join private.draft_document dd on dd.target_id = $1 and dd.report_type = 'L2' and dd.report_key = cast(l2.period_start as text)
             where s.submission_id = $2
    `, [firm_id, report_id]);
    if (l2 && l2[0]) {
      l2 = l2[0];
    }
    if(l2 && l2.version < '2') {
      l2.user_data = legacyProcessor.preProcessLegacyRecord(l2.user_data);
    }
    return l2;
  }

  async getContracts () {
    return rds.query(`
      SELECT c.name AS employer,
         c.lobbyist_client_id AS client_id,
         f.name AS lobbyist,
         f.lobbyist_firm_id AS lobbyist_id,
         lcs.submission_id,
         lcs.lobbyist_contract_id AS contract_id,
         lcs.submitted_at, cr.periods
      FROM lobbyist_contract lc
      JOIN lobbyist_client c
      ON c.lobbyist_client_id=lc.lobbyist_client_id
      JOIN lobbyist_contract_submission lcs ON lc.current_submission_id = lcs.submission_id
      JOIN (SELECT lobbyist_contract_id, json_agg(json_build_object('period_start', period_start)
          ORDER BY period_start) periods FROM lobbyist_reporting_periods r
          GROUP BY lobbyist_contract_id) cr
          ON cr.lobbyist_contract_id=lc.lobbyist_contract_id
      JOIN lobbyist_firm f ON f.lobbyist_firm_id = lc.lobbyist_firm_id
    `)
  }

  getLegacySubContractsToApprove(firm_id) {
    return rds.query(`
      SELECT dd.report_key as drupal_nid,
             coalesce(lc.name || ' (' || lf.name || ')', user_data->>'title') as title,
             dd.updated_at
        from private.draft_document dd
          left join lobbyist_firm lf on lf.lobbyist_firm_id=cast(user_data->'field_firm_that_is_employing_you'->'und'->0->>'target_id' as int)
          left join lobbyist_client lc on lc.lobbyist_client_id=cast(user_data->'field_employer_entity_reference'->'und'->0->>'target_id' as int)
        WHERE dd.report_type='L-Contract-Legacy' and dd.target_type='lobbyist_firm'
          and dd.target_id=$1
      UNION
      select sub.user_data->>'nid' as drupal_nid,
             lcc.name || ' (' || lf.name || ')' as title,
             to_timestamp(CAST(sub.user_data->>'revision_timestamp' as numeric)) as updated_at
      from lobbyist_contract lc
             join lobbyist_contract_submission sub on sub.submission_id = lc.current_submission_id
             join lobbyist_firm lf on lf.lobbyist_firm_id = lc.lobbyist_firm_id
             join lobbyist_client lcc on lcc.lobbyist_client_id=lc.lobbyist_client_id
      where sub.user_data->>'field_authorization_date' = '[]' and lc.contractor_id = $1
    `, [firm_id])
  }

  getSubContractsToApprove(firm_id) {
    return rds.query(`
        SELECT lf.name as firm, dd.draft_id, dd.updated_at,
               case when dd.user_data->>'revision_message' is not null and dd.approval_target_id is not null and (dd.user_data->>'certification' is null or dd.user_data->>'certification' = '{}' )
                        then 'Returned to lobbyist for approval'
                    when dd.user_data->>'certification' is not null and dd.user_data->'contractor'->>'contractor_id' is not null
                        then 'Awaiting for contractor approval'
                    when dd.user_data->>'certification' is not null and dd.approval_target_id is not null
                        then 'Awaiting for lobbyist approval'
                    else 'Draft'
                   end as status
        FROM private.draft_document dd
             left join lobbyist_firm lf on lf.lobbyist_firm_id = dd.target_id and dd.target_type='lobbyist_firm'
             WHERE dd.approval_target_type='lobbyist_firm'
               and dd.approval_target_id = $1
               AND dd.report_type='L1'
      `, [firm_id])
  }

  getPendingL1Contracts(firm_id) {
    return rds.query(`
      SELECT dd.draft_id,
        lf.name as firm,
        dd.period_start,
        dd.period_end,
        dd.updated_at,
        dd.user_data->'client'->>'name' as client,
        case when dd.user_data->>'revision_message' is not null 
                      and dd.approval_target_id is not null  
                      and dd.user_data->'certification'->>'signature' is null 
                      and dd.user_data->'approval'->>'signature' is null 
                     then 'Returned to lobbyist for certification'
             when dd.user_data->'certification'->>'signature' is null and 
                  dd.user_data->'approval'->>'signature' is null 
                   and dd.approval_target_id is not null
                     then 'Awaiting both lobbyist and client/employer certification'
             when dd.user_data->'approval'->>'signature' is null and dd.user_data->'contractor'->>'contractor_id' is not null
                 then 'Awaiting contractor certification'
             when dd.user_data->'approval'->>'signature' is null and dd.approval_target_id is not null
                 then 'Awaiting client/employer certification'
             when dd.user_data->'certification'->>'signature' is null and dd.approval_target_id is not null
                 then 'Awaiting lobbyist certification'
             else 'Draft'
        end as status,
        cast (dd.user_data->'client'->>'lobbyist_client_id' as int) as lobbyist_client_id,
        dd.user_data->'contractor'->>'name' as contractor,
        cast (dd.user_data->'contractor'->>'contractor_id' as int) as contractor_id
        FROM private.draft_document dd
            join lobbyist_firm lf on lf.lobbyist_firm_id = dd.target_id and dd.target_type='lobbyist_firm'
            left join lobbyist_client lc on lc.lobbyist_client_id = dd.target_id and dd.target_type = 'lobbyist_client'
       WHERE dd.report_type = 'L1'
         and lf.lobbyist_firm_id = $1`, [firm_id])
  }

  getPendingLegacyContracts(firm_id) {
    return rds.query(`
      SELECT dd.report_key as drupal_nid,
             dd.draft_id,
             COALESCE(lf.name, user_data->>'title') as title,
             COALESCE(lc.name, lf2.name) as client_name,
             dd.updated_at
        from private.draft_document dd
        left join lobbyist_firm lf on lf.lobbyist_firm_id=cast(user_data->'field_firm_entity_reference'->'und'->0->>'target_id' as int)
        left join lobbyist_client lc on lc.lobbyist_client_id = dd.target_id and dd.target_type='lobbyist_client'
        left join lobbyist_firm lf2 on lf2.lobbyist_firm_id = dd.target_id and dd.target_type='lobbyist_firm'
        WHERE dd.report_type='L-Contract-Legacy' and dd.updated_at >= now() - interval '1 year'
          and lf.lobbyist_firm_id = $1
      UNION
      select sub.user_data->>'nid' as drupal_nid,
             null as draft_id,
             lcc.name || ' (' || lf.name || ')' as title,
             coalesce(lf2.name, lcc.name) as client_name,
             to_timestamp(CAST(sub.user_data->>'revision_timestamp' as numeric)) as updated_at
      from lobbyist_contract lc
               join lobbyist_contract_submission sub on sub.submission_id = lc.current_submission_id
               join lobbyist_firm lf on lf.lobbyist_firm_id = lc.lobbyist_firm_id
               join lobbyist_client lcc on lcc.lobbyist_client_id = lc.lobbyist_client_id
               left join lobbyist_firm lf2 on lf2.lobbyist_firm_id = lc.contractor_id
      where sub.user_data->>'field_authorization_date' = '[]' 
        and to_timestamp(CAST(sub.user_data->>'revision_timestamp' as numeric)) >= now() - interval '1 year' 
        and lc.lobbyist_firm_id = $1
    `, [firm_id])
  }

  /*
    Note: be careful to not block people that ended an agent so that they could submit an L1.
    Scenarios for agent certification:
    1. Evaluate agents who started before/during biennium and do not have an end date.
    2. Evaluate agents that have a start_date during the biennium.
    3. For the cases when the agents end during the biennium we don't evaluate any agents with an end_date
   */
  getAgents(firm_id) {
    return rds.query(
        `select name, training_certified::date 
                from lobbyist_agent 
                where ending is null
                  and lobbyist_firm_id = $1`
        , [firm_id])
  }
}

const firmProcessor = new FirmProcessor()
export {firmProcessor}