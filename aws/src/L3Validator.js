import Moment from "moment"

function validDate(date, period_start, period_end) {
  if (!date) {
    return false
  }
  period_start = new Moment(period_start, 'YYYY-MM-DD')
  period_end = new Moment(period_end, 'YYYY-MM-DD')
  date = Moment(date, 'YYYY-MM-DD')
  if (!date.isValid() || !period_start.isValid() || !period_end.isValid()) {
    return false
  }
  return (date >= period_start && date <= period_end)
}
function validateLobbyingExpenses(l3) {
  const validations = []

  function validBoolean(value) {
    return value !== undefined && value !== null && typeof value === 'boolean'
  }
  if (!validBoolean(l3.has_entertainment_expenditures)) {
    validations.push({'l3.lobbying_expenses.has_entertainment_expenditures.incomplete': 'incomplete'})
  }
  if (l3.entertainment_expenditures && l3.entertainment_expenditures.length > 0) {
    l3.entertainment_expenditures.forEach((expenditure, i) => {

      if (!expenditure.amount  || parseInt(expenditure.amount) <= 0) {
        validations.push({[`l3.entertainment_expenditures.${i}.amount.invalid`]: 'invalid'})
      }

      if (!expenditure.date) {
        validations.push({[`l3.entertainment_expenditures.${i}.date.required`]: 'required'})
      }

      else if (!validDate(expenditure.date, l3.period_start, l3.period_end)) {
           validations.push({[`l3.entertainment_expenditures.${i}.date.invalid`]: `invalid`})
      }

      if (!expenditure.name || typeof expenditure.name !== 'string' || expenditure.name.trim() === '') {
        validations.push({[`l3.entertainment_expenditures.${i}.name.required`]: 'required'})
      }

      if (!expenditure.relationship || typeof expenditure.relationship !== 'string' || expenditure.relationship.trim() === '') {
        validations.push({[`l3.entertainment_expenditures.${i}.relationship.required`]: 'required'})
      }

      if (['State Officials family member', 'Legislators family member', 'State Employees family member'].includes(expenditure.relationship)) {
        if (!expenditure.family_member_name || typeof expenditure.family_member_name !== 'string' || expenditure.family_member_name.trim() === '') {
          validations.push({[`l3.entertainment_expenditures.${i}.family_member_name.required`]: 'required'});
        }
      }

      if (!expenditure.description || typeof expenditure.description !== 'string' || expenditure.description.trim() === '') {
        validations.push({[`l3.entertainment_expenditures.${i}.description.required`]: 'required'})
      }

    })
  }
  if (!validBoolean(l3.has_itemized_expenditures)) {
    validations.push({'l3.lobbying_expenses.has_itemized_expenditures.incomplete': 'incomplete'})
  }
  if (l3.itemized_expenditures && l3.itemized_expenditures.length > 0) {
    l3.itemized_expenditures.forEach((expenditure, i) => {

      if (!expenditure.amount  || parseInt(expenditure.amount) <= 0) {
        validations.push({[`l3.itemized_expenditures.${i}.amount.invalid`]: 'invalid'})
      }

      if (!expenditure.date) {
        validations.push({[`l3.itemized_expenditures.${i}.date.required`]: 'required'})
      }

      else if (!validDate(expenditure.date, l3.period_start, l3.period_end)) {
         validations.push({[`l3.itemized_expenditures.${i}.date.invalid`]: `invalid`})
       }

      if (!expenditure.name || typeof expenditure.name !== 'string' || expenditure.name.trim() === '') {
        validations.push({[`l3.itemized_expenditures.${i}.name.required`]: 'required'})
      }

      if (!expenditure.relationship || typeof expenditure.relationship !== 'string' || expenditure.relationship.trim() === '') {
        validations.push({[`l3.itemized_expenditures.${i}.relationship.required`]: 'required'})
      }

      if (['State Officials family member', 'Legislators family member', 'State Employees family member'].includes(expenditure.relationship)) {
        if (!expenditure.family_member_name || typeof expenditure.family_member_name !== 'string' || expenditure.family_member_name.trim() === '') {
          validations.push({[`l3.itemized_expenditures.${i}.family_member_name.required`]: 'required'});
        }
      }

      if (!expenditure.description || typeof expenditure.description !== 'string' || expenditure.description.trim() === '') {
        validations.push({[`l3.itemized_expenditures.${i}.description.required`]: 'required'})
      }
    })
  }

  if (l3.has_pac_contributions === null) {
    validations.push({'l3.election_expenses.has_pac_contributions.incomplete': 'incomplete'})
  }
  if (l3.pac_contributions && l3.pac_contributions.length > 0) {
    l3.pac_contributions.forEach((expenditure, index) => {
      if (!expenditure.pac_name) {
        validations.push({[`l3.pac_contributions.${index}.pac_name.invalid`]: 'invalid'})
      }
    })
  }

  if (l3.has_candidate_independent_expenditures === null) {
    validations.push({'l3.election_expenses.has_candidate_independent_expenditures.incomplete': 'incomplete'})
  }

  if (l3.candidate_independent_expenditures && l3.candidate_independent_expenditures.length > 0) {
    l3.candidate_independent_expenditures.forEach((expenditure, index) => {

      if (!expenditure.date) {
        validations.push({[`l3.candidate_independent_expenditures.${index}.date.required`]: 'required'})
      }
      else if (!validDate(expenditure.date, l3.period_start, l3.period_end)) {
             validations.push({[`l3.candidate_independent_expenditures.${index}.date.invalid`]: `invalid`})
      }
      if (!expenditure.name || typeof expenditure.name !== 'string' || expenditure.name.trim() === '') {
        validations.push({[`l3.candidate_independent_expenditures.${index}.name.required`]: 'required'})
      }
      if (!expenditure.amount || parseInt(expenditure.amount) <= 0) {
        validations.push({[`l3.candidate_independent_expenditures.${index}.amount.invalid`]: 'invalid'})
      }
      if (!expenditure.candidacy_id ) {
        validations.push({[`l3.candidate_independent_expenditures.${index}.candidacy_id.required`]: 'required'})
      }
      if (!expenditure.stance || typeof expenditure.stance !== 'string' || expenditure.stance.trim() === '')  {
        validations.push({[`l3.candidate_independent_expenditures.${index}.stance.required`]: 'required'})
      }
      if (!expenditure.description || typeof expenditure.description  !== 'string' || expenditure.description.trim() === '')  {
        validations.push({[`l3.candidate_independent_expenditures.${index}.description.required`]: 'required'})
      }
    })
  }

  if (!validBoolean(l3.has_other_expenditures)) {
    validations.push({'l3.lobbying_expenses.has_other_expenditures.incomplete': 'incomplete'})
  }
  if (l3.other_expenditures && l3.other_expenditures.length > 0) {
    l3.other_expenditures.forEach((expenditure, i) => {

      if (!expenditure.amount  || parseInt(expenditure.amount) <= 0) {
        validations.push({[`l3.other_expenditures.${i}.amount.invalid`]: 'invalid'})
      }

      if (!expenditure.date) {
        validations.push({[`l3.other_expenditures.${i}.date.required`]: 'required'})
      }

      else if (!validDate(expenditure.date, l3.period_start, l3.period_end)) {
             validations.push({[`l3.other_expenditures.${i}.date.invalid`]: `invalid`})
      }

      if (!expenditure.recipient || typeof expenditure.recipient !== 'string' || expenditure.recipient.trim() === '') {
        validations.push({[`l3.other_expenditures.${i}.recipient.required`]: 'required'})
      }

      if (!expenditure.description || typeof expenditure.description !== 'string' || expenditure.description.trim() === '') {
        validations.push({[`l3.other_expenditures.${i}.description.required`]: 'required'})
      }

    })
  }
  if (l3.payments_for_registered_lobbyists !== 0 && ! l3.payments_for_registered_lobbyists) {
    validations.push({'l3.lobbying_expenses.payments_for_registered_lobbyists.incomplete': 'incomplete'})
  }
  if (l3.payments_for_registered_lobbyists < 0) {
    validations.push({'l3.lobbying_expenses.payments_for_registered_lobbyists.invalid': 'invalid'})
  }
  if (l3.witnesses_retained_for_lobbying_services !== 0 && ! l3.witnesses_retained_for_lobbying_services) {
    validations.push({'l3.lobbying_expenses.witnesses_retained_for_lobbying_services.incomplete': 'incomplete'})
  }
  if (l3.witnesses_retained_for_lobbying_services < 0) {
    validations.push({'l3.lobbying_expenses.witnesses_retained_for_lobbying_services.invalid': 'invalid'})
  }
  if (l3.costs_for_promotional_materials !== 0 && ! l3.costs_for_promotional_materials) {
    validations.push({'l3.lobbying_expenses.costs_for_promotional_materials.incomplete': 'incomplete'})
  }
  if (l3.costs_for_promotional_materials < 0) {
    validations.push({'l3.lobbying_expenses.costs_for_promotional_materials.invalid': 'invalid'})
  }
  if (l3.grassroots_expenses_for_lobbying_communications !== 0 && ! l3.grassroots_expenses_for_lobbying_communications) {
    validations.push({'l3.lobbying_expenses.grassroots_expenses_for_lobbying_communications.incomplete': 'incomplete'})
  }
  if (l3.grassroots_expenses_for_lobbying_communications < 0) {
    validations.push({'l3.lobbying_expenses.grassroots_expenses_for_lobbying_communications.invalid': 'invalid'})
  }

  if (l3.lobbyist && l3.lobbyist.length > 0) {
    l3.lobbyist.forEach((lobbyist, i) => {
      if ((lobbyist.compensation !== 0 && !lobbyist.compensation)  || lobbyist.compensation < 0) {
        validations.push({[`l3.lobbyist.${i}.compensation.invalid`]: 'invalid'})
      }
      if ((lobbyist.expenses !== 0 && !lobbyist.expenses)|| lobbyist.expenses < 0) {
        validations.push({[`l3.lobbyist.${i}.expenses.invalid`]: 'invalid'})
      }
    })
  }

  if (!validBoolean(l3.has_employment_compensation)) {
    validations.push({'l3.other_compensation.has_employment_compensation.incomplete': 'incomplete'})
  }
  if (l3.employment_compensation && l3.employment_compensation.length > 0) {
    l3.employment_compensation.forEach((compensation, i) => {
      if (!compensation.amount || typeof compensation.amount !== 'string' || compensation.amount.trim() === '') {
        validations.push({[`l3.employment_compensation.${i}.amount.required`]: 'required'})
      }

      if (!compensation.name || typeof compensation.name !== 'string' || compensation.name.trim() === '') {
        validations.push({[`l3.employment_compensation.${i}.name.required`]: 'required'})
      }

      if (!compensation.description || typeof compensation.description !== 'string' || compensation.description.trim() === '') {
        validations.push({[`l3.employment_compensation.${i}.description.required`]: 'required'})
      }

      if (!compensation.relationship || typeof compensation.relationship !== 'string' || compensation.relationship.trim() === '') {
        validations.push({[`l3.employment_compensation.${i}.relationship.required`]: 'required'})
      }

      if (['State Officials family member', 'Legislators family member', 'State Employees family member'].includes(compensation.relationship)) {
        if (!compensation.family_member_name || typeof compensation.family_member_name !== 'string' || compensation.family_member_name.trim() === '') {
          validations.push({[`l3.employment_compensation.${i}.family_member_name.required`]: 'required'});
        }
      }
    })
  }

  if (!validBoolean(l3.has_professional_services_compensation)) {
    validations.push({'l3.other_compensation.has_professional_services_compensation.incomplete': 'incomplete'})
  }
  if (l3.professional_services_compensation && l3.professional_services_compensation.length > 0) {
    l3.professional_services_compensation.forEach((compensation, i) => {
      if (!compensation.amount || typeof compensation.amount !== 'string' || compensation.amount.trim() === '') {
        validations.push({[`l3.professional_services_compensation.${i}.amount.required`]: 'required'})
      }

      if (!compensation.firm_name || typeof compensation.firm_name !== 'string' || compensation.firm_name.trim() === '') {
        validations.push({[`l3.professional_services_compensation.${i}.firm_name.required`]: 'required'})
      }

      if (!compensation.description || typeof compensation.description !== 'string' || compensation.description.trim() === '') {
        validations.push({[`l3.professional_services_compensation.${i}.description.required`]: 'required'})
      }

      if (!compensation.person_name || typeof compensation.person_name !== 'string' || compensation.person_name.trim() === '') {
        validations.push({[`l3.professional_services_compensation.${i}.person_name.required`]: 'required'})
      }
    })
  }

  return validations
}

function validateElectionExpenses(l3) {
  const validations = []
  if (l3.has_itemized_contributions === null) {
    validations.push({'l3.election_expenses.has_itemized_contributions.incomplete': 'incomplete'})
  }

  if (l3.itemized_contributions && l3.itemized_contributions.length > 0) {
      l3.itemized_contributions.forEach((contribution, index) => {

        if (!contribution.amount  || parseInt(contribution.amount) <= 0) {
          validations.push({[`l3.itemized_contributions.${index}.amount.invalid`]: 'invalid'})
        }

        if (!contribution.date) {
          validations.push({[`l3.itemized_contributions.${index}.date.required`]: 'required'})
        }

        else if (!validDate(contribution.date, l3.period_start, l3.period_end)) {
           validations.push({[`l3.itemized_contributions.${index}.date.invalid`]: `invalid`})
        }

        if (!contribution.recipient || typeof contribution.recipient !== 'string' || contribution.recipient.trim() === '') {
          validations.push({[`l3.itemized_contributions.${index}.recipient.required`]: 'required'})
        }
      })
  }

  if (l3.has_in_kind_contributions === null) {
    validations.push({'l3.election_expenses.has_in_kind_contributions.incomplete': 'incomplete'})
  }

  if (l3.in_kind_contributions && l3.in_kind_contributions.length > 0) {
    l3.in_kind_contributions.forEach((contribution, i) => {

      if (!contribution.amount  || parseInt(contribution.amount) <= 0) {
        validations.push({[`l3.in_kind_contributions.${i}.amount.invalid`]: 'invalid'})
      }

      if (!contribution.date) {
        validations.push({[`l3.in_kind_contributions.${i}.date.required`]: 'required'})
      }

      else if (!validDate(contribution.date, l3.period_start, l3.period_end)) {
        validations.push({[`l3.in_kind_contributions.${i}.date.invalid`]: `invalid`})
      }

      if (!contribution.recipient || typeof contribution.recipient !== 'string' || contribution.recipient.trim() === '') {
        validations.push({[`l3.in_kind_contributions.${i}.recipient.required`]: 'required'})
      }

      if (!contribution.description || typeof contribution.description  !== 'string' || contribution.description.trim() === '')  {
        validations.push({[`l3.in_kind_contributions.${i}.description.required`]: 'required'})
      }
    })
  }

  if (l3.has_ballot_proposition_expenditures === null) {
    validations.push({'l3.election_expenses.has_ballot_proposition_expenditures.incomplete': 'incomplete'})
  }
  if (l3.has_ballot_proposition_expenditures && l3.ballot_proposition_expenditures.length > 0) {
    l3.ballot_proposition_expenditures.forEach((expenditure, index) => {
      if (!expenditure.amount  || parseInt(expenditure.amount) <= 0) {
        validations.push({[`l3.ballot_proposition_expenditures.${index}.amount.invalid`]: 'invalid'})
      }
      if (!expenditure.date) {
        validations.push({[`l3.ballot_proposition_expenditures.${index}.date.required`]: 'required'})
      }
      else if (!validDate(expenditure.date, l3.period_start, l3.period_end)) {
            validations.push({[`l3.ballot_proposition_expenditures.${index}.date.invalid`]: `invalid`})
      }
      if (!expenditure.stance) {
        validations.push({[`l3.ballot_proposition_expenditures.${index}.stance.required`]: 'required'})
      }
      if (!expenditure.description || typeof expenditure.description  !== 'string' || expenditure.description.trim() === '')  {
        validations.push({[`l3.ballot_proposition_expenditures.${index}.description.required`]: 'required'})
      }
      if (!expenditure.description || typeof expenditure.ballot_number  !== 'string' || expenditure.ballot_number.trim() === '')  {
        validations.push({[`l3.ballot_proposition_expenditures.${index}.description.required`]: 'required'})
      }
    })
  }
  if (l3.non_itemized_political_contribution_amount !== 0 && ! l3.non_itemized_political_contribution_amount) {
    validations.push({'l3.election_expenses.non_itemized_political_contribution_amount.incomplete': 'incomplete'})
  }
  if (l3.non_itemized_political_contribution_amount < 0) {
    validations.push({'l3.election_expenses.non_itemized_political_contribution_amount.invalid': 'invalid'})
  }
  return validations
}
export default function validateL3Draft(l3) {
  return [
    ...validateLobbyingExpenses(l3),
    ...validateElectionExpenses(l3)
  ]
}
