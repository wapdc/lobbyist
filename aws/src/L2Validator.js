import Moment from "moment"

/**
 * Date validation function based loosely on logic contained in PDatePicker global component.
 * @param date
 * @param period_start
 * @param period_end
 * @return {boolean|string}
 */
function validDate(date, period_start, period_end) {
  if (!date) {
    return false
  }
  period_start = new Moment(period_start, 'YYYY-MM-DD')
  period_end = new Moment(period_end, 'YYYY-MM-DD')
  date = Moment(date, 'YYYY-MM-DD')
  if (!date.isValid() || !period_start.isValid() || !period_end.isValid()) {
    return false
  }
  return (date >= period_start && date <= period_end)
}

function validSource(sources, source) {
  return sources.some(s => s.id === source)
}

export default function validateL2Draft(l2){
  const validation_errors = []

  function addError(errorCode, message = 'invalid') {
    const error = {[errorCode] : message};
    validation_errors.push(error)
  }

  if(l2.contributions_in_kind && l2.contributions_in_kind.length > 0){
    l2.has_contributions_in_kind = true
    for (let i=0; i<l2.contributions_in_kind.length; i++){
      if (!l2.contributions_in_kind[i].amount || !parseInt(l2.contributions_in_kind[i].amount) || parseInt(l2.contributions_in_kind[i].amount) <= 0) {
        addError(`l2.contributions_in_kind.${i}.amount.invalid`, 'invalid')
      }
      if (
        (!l2.contributions_in_kind[i].committee_id || !parseInt(l2.contributions_in_kind[i].committee_id) || parseInt(l2.contributions_in_kind[i].committee_id) <= 0)
        && (!l2.contributions_in_kind[i].source_name && !l2.contributions_in_kind[i].source && !validSource(l2.sources, l2.contributions_in_kind[i].source) || !parseInt(l2.contributions_in_kind[i].source) ||  parseInt(l2.contributions_in_kind[i].source) < -2 || parseInt(l2.contributions_in_kind[i].source) === 0)
        && !l2.contributions_in_kind[i].source_name) {
        addError(`l2.contributions_in_kind.${i}.source.invalid`, 'invalid')
      }
      if (!l2.contributions_in_kind[i].recipient || typeof l2.contributions_in_kind[i].recipient !== 'string' || l2.contributions_in_kind[i].recipient.trim() === '') {
        addError(`l2.contributions_in_kind.${i}.recipient.required`, 'in-kind contribution recipient required');
      }
      if (!l2.contributions_in_kind[i].date || typeof l2.contributions_in_kind[i].date !== 'string' || l2.contributions_in_kind[i].date.trim() === '') {
        addError(`l2.contributions_in_kind_itemized.${i}.date.required`, 'required');
      }
      if (!validDate(l2.contributions_in_kind[i].date, l2.period_start, l2.period_end)) {
        addError(`l2.contributions_in_kind_itemized.${i}.occasion_date.invalid`, 'invalid')
      }
    }
  }
  else {
    if (l2.has_contributions_in_kind !== false) {
      addError('l2.contributions_in_kind.incomplete', 'incomplete')
    }
  }

  if(l2.personal_expenses && l2.personal_expenses.length > 0){
    l2.has_personal_expenses = true
    for (let i=0; i<l2.personal_expenses.length; i++){
      if (!l2.personal_expenses[i].source || !parseInt(l2.personal_expenses[i].source) || !validSource(l2.sources, l2.personal_expenses[i].source)) {
        addError(`l2.personal_expenses.${i}.source.invalid`, 'invalid')
      }
      if (!l2.personal_expenses[i].amount || !parseFloat(l2.personal_expenses[i].amount) || parseFloat(l2.personal_expenses[i].amount) <= 0) {
        addError(`l2.personal_expenses.${i}.amount.invalid`, 'invalid')
      }
    }
  }
  else {
    if (l2.has_personal_expenses !== false) {
      addError('l2.personal_expenses.incomplete', 'incomplete')
    }
  }

  if(l2.entertainment && l2.entertainment.length > 0){
    l2.has_entertainment = true
    for (let i=0; i<l2.entertainment.length; i++){
      if (!l2.entertainment[i].source || !parseInt(l2.entertainment[i].source) || !validSource(l2.sources, l2.entertainment[i].source)) {
        addError(`l2.entertainment_itemized.${i}.source.invalid`, 'invalid')
      }
      if (!l2.entertainment[i].cost || !parseFloat(l2.entertainment[i].cost) || parseFloat(l2.entertainment[i].cost) <= 0) {
        addError(`l2.entertainment_itemized.${i}.cost.invalid`, 'invalid')
      }
      if (!l2.entertainment[i].occasion_date || typeof l2.entertainment[i].occasion_date !== 'string' || l2.entertainment[i].occasion_date.trim() === '') {
        addError(`l2.entertainment_itemized.${i}.occasion_date.required`, 'required');
      }
      if (!validDate(l2.entertainment[i].occasion_date, l2.period_start, l2.period_end)) {
        addError(`l2.entertainment_itemized.${i}.occasion_date.invalid`, 'invalid')
      }
      if (!l2.entertainment[i].type_of_occasion || typeof l2.entertainment[i].type_of_occasion !== 'string' || l2.entertainment[i].type_of_occasion.trim() === '') {
        addError(`l2.entertainment_itemized.${i}.type_of_occasion.required`, 'required');
      }
      if (!l2.entertainment[i].occasion_description || typeof l2.entertainment[i].occasion_description !== 'string' || l2.entertainment[i].occasion_description.trim() === '') {
        addError(`l2.entertainment_itemized.${i}.occasion_description.required`, 'required');
      }
      if (!l2.entertainment[i].participant_listing || typeof l2.entertainment[i].participant_listing !== 'string' || l2.entertainment[i].participant_listing.trim() === '') {
        addError(`l2.entertainment_itemized.${i}.participant_listing.required`, 'required');
      }
      if (!l2.entertainment[i].place || typeof l2.entertainment[i].place !== 'string' || l2.entertainment[i].place.trim() === '') {
        addError(`l2.entertainment_itemized.${i}.place.required`, 'required');
      }
    }
  }
  else {
    if (l2.has_entertainment !== false) {
      addError('l2.entertainment_all.incomplete', 'incomplete')
    }
  }

  if(l2.compensation && l2.compensation.length > 0) {
    l2.has_compensation = true
    for (let i=0; i<l2.compensation.length; i++){
      if (!l2.compensation[i].source || !parseInt(l2.compensation[i].source) || !validSource(l2.sources, l2.compensation[i].source)) {
        addError( `l2.compensation.${i}.source.invalid`, 'invalid')
      }
      if (!l2.compensation[i].amount || !parseFloat(l2.compensation[i].amount) || parseFloat(l2.compensation[i].amount) <= 0) {
        addError(`l2.compensation.${i}.amount.invalid`, 'invalid amount')
      }
    }
  }
  else {
    if (l2.has_compensation !== false) {
      addError('l2.compensation.incomplete', 'incomplete')
    }
  }

  if (l2.subcontractors && l2.subcontractors.length > 0) {
    if(l2.sub_lobbyist_payments && l2.sub_lobbyist_payments.length > 0){
      l2.has_sub_lobbyist_payments = true
      for (let i=0; i<l2.sub_lobbyist_payments.length; i++){
        if (!l2.sub_lobbyist_payments[i].amount || !parseFloat(l2.sub_lobbyist_payments[i].amount) || parseFloat(l2.sub_lobbyist_payments[i].amount) <= 0) {
          addError(`l2.sub_lobbyist_payments.${i}.amount.invalid`, 'invalid')
        }
      }
    }
    else {
      if (l2.has_sub_lobbyist_payments !== false) {
        addError('l2.sub_lobbyist_payments.incomplete')
      }
    }
  }

  if(l2.contributions_small && l2.contributions_small.length > 0){
    l2.has_contributions_small = true
    for (let i=0; i<l2.contributions_small.length; i++){
      if (!l2.contributions_small[i].amount || !parseFloat(l2.contributions_small[i].amount) || parseFloat(l2.contributions_small[i].amount) <= 0) {
        addError(`l2.contributions_small.${i}.amount.invalid`, 'invalid')
      }
      if (!l2.contributions_small[i].source || !parseInt(l2.contributions_small[i].source) || parseInt(l2.contributions_small[i].source) <= 0 || !validSource(l2.sources, l2.contributions_small[i].source)) {
        addError(`l2.contributions_small.${i}.source.invalid`, 'invalid')
      }
    }
  }
  else {
    if (l2.has_contributions_small !== false) {
      addError('l2.contributions_all.required')
    }
  }

  if(l2.contributions && l2.contributions.length > 0){
    l2.has_contributions = true
    for (let i=0; i<l2.contributions.length; i++){
      if (!l2.contributions[i].amount || !parseFloat(l2.contributions[i].amount) || parseFloat(l2.contributions[i].amount) <= 0) {
        addError(`l2.contributions_monetary.${i}.amount.invalid`, 'invalid')
      }
      if (!l2.contributions[i].date || typeof l2.contributions[i].date !== 'string' || l2.contributions[i].date.trim() === '') {
        addError(`l2.contributions_monetary.${i}.date.required`, 'required');
      }
      if (!validDate(l2.contributions[i].date, l2.period_start, l2.period_end)) {
        addError(`l2.contributions_monetary.${i}.date.invalid`, 'invalid')
      }
      if (!l2.contributions[i].recipient || typeof l2.contributions[i].recipient !== 'string' || l2.contributions[i].recipient.trim() === '') {
        addError(`l2.contributions_monetary.${i}.recipient.required`, 'required');
      }
      if (
             (!l2.contributions[i].committee_id || !parseInt(l2.contributions[i].committee_id) || parseInt(l2.contributions[i].committee_id) <= 0)
          && (!l2.contributions[i].source_name && (!l2.contributions[i].source || !parseInt(l2.contributions[i].source ) || parseInt(l2.contributions[i].source) < -2 || parseInt(l2.contributions[i].source) === 0 || !validSource(l2.sources, l2.contributions[i].source)))
          && (!l2.contributions[i].source_name))
      {
        addError(`l2.contributions_monetary.${0}.source.invalid`, 'invalid');
      }
    }
  }
  else {
    if (l2.has_contributions !== false) {
      addError('l2.contributions_all.required')
    }
  }

  if(l2.independent_expenditures_candidate && l2.independent_expenditures_candidate.length > 0){
    l2.has_independent_expenditures_candidate = true
    for (let i = 0; i < l2.independent_expenditures_candidate.length; i++){
      if (!l2.independent_expenditures_candidate[i].source || !parseInt(l2.independent_expenditures_candidate[i].source) || parseInt(l2.independent_expenditures_candidate[i].source) <= 0 || !validSource(l2.sources, l2.independent_expenditures_candidate[i].source)) {
        addError(`l2.independent_expenditures_candidate.${i}.source.invalid`, 'invalid')
      }
      if (!l2.independent_expenditures_candidate[i].recipient || typeof l2.independent_expenditures_candidate[i].recipient !== 'string') {
        addError(`l2.independent_expenditures_candidate.${i}.recipient.required`, 'required')
      }
      if (!l2.independent_expenditures_candidate[i].date || typeof l2.independent_expenditures_candidate[i].date !== 'string' || l2.independent_expenditures_candidate[i].date.trim() === '') {
        addError(`l2.independent_expenditures_candidate.${i}.date.required`, 'required')
      }
      if (!validDate(l2.independent_expenditures_candidate[i].date, l2.period_start, l2.period_end)) {
        addError(`l2.independent_expenditures_candidate.${i}.date.invalid`, 'invalid')
      }
      if (!l2.independent_expenditures_candidate[i].description || typeof l2.independent_expenditures_candidate[i].description !== 'string') {
        addError(`l2.independent_expenditures_candidate.${i}.description.required`, 'required')
      }
      if (!l2.independent_expenditures_candidate[i].amount || !parseFloat(l2.independent_expenditures_candidate[i].amount) || parseFloat(l2.independent_expenditures_candidate[i].amount) <= 0) {
        addError(`l2.independent_expenditures_candidate.${i}.amount.invalid`, 'invalid')
      }
      if (!l2.independent_expenditures_candidate[i].stance || typeof l2.independent_expenditures_candidate[i].stance !== 'string' || l2.independent_expenditures_candidate[i].stance.trim() === '' || (l2.independent_expenditures_candidate[i].stance !== 'For' && l2.independent_expenditures_candidate[i].stance !== 'Against')) {
        addError(`l2.independent_expenditures_candidate.${i}.stance.required`, 'required')
      }
      if (typeof l2.independent_expenditures_candidate[i].candidate_id !== 'number' || l2.independent_expenditures_candidate[i].candidate_id < 1) {
        addError(`l2.independent_expenditures_candidate.${i}.candidate_id.required`, 'required')
      }
      if (!l2.independent_expenditures_candidate[i].candidate_name || typeof l2.independent_expenditures_candidate[i].candidate_name !== 'string' || l2.independent_expenditures_candidate[i].candidate_name.trim() === '') {
        addError(`l2.independent_expenditures_candidate.${i}.candidate_name.required`, 'required')
      }
    }
  }
  else {
    if (l2.has_independent_expenditures_candidate !== false) {
      addError('l2.contributions_all.required')
    }
  }

  if(l2.independent_expenditures_ballot && l2.independent_expenditures_ballot.length > 0){
    l2.has_independent_expenditures_ballot = true
    for (let i = 0; i < l2.independent_expenditures_ballot.length; i++){
      if (!l2.independent_expenditures_ballot[i].source || !parseInt(l2.independent_expenditures_ballot[i].source) || parseInt(l2.independent_expenditures_ballot[i].source) <= 0 || !validSource(l2.sources, l2.independent_expenditures_ballot[i].source)) {
        addError(`l2.independent_expenditures_ballot.${i}.source.invalid`, 'invalid')
      }
      if (!l2.independent_expenditures_ballot[i].recipient || typeof l2.independent_expenditures_ballot[i].recipient !== 'string') {
        addError(`l2.independent_expenditures_ballot.${i}.recipient.required`, 'required')
      }
      if (!l2.independent_expenditures_ballot[i].date || typeof l2.independent_expenditures_ballot[i].date !== 'string' || l2.independent_expenditures_ballot[i].date.trim() === '') {
        addError(`l2.independent_expenditures_ballot.${i}.date.required`, 'required')
      }
      if (!validDate(l2.independent_expenditures_ballot[i].date, l2.period_start, l2.period_end)) {
        addError(`l2.independent_expenditures_ballot.${i}.date.invalid`, 'invalid')
      }
      if (!l2.independent_expenditures_ballot[i].description || typeof l2.independent_expenditures_ballot[i].description !== 'string') {
        addError(`l2.independent_expenditures_ballot.${i}.description.required`, 'required')
      }
      if (!l2.independent_expenditures_ballot[i].amount || !parseFloat(l2.independent_expenditures_ballot[i].amount) || parseFloat(l2.independent_expenditures_ballot[i].amount) <= 0) {
        addError(`l2.independent_expenditures_ballot.${i}.amount.invalid`, 'invalid')
      }
      if (!l2.independent_expenditures_ballot[i].stance || typeof l2.independent_expenditures_ballot[i].stance !== 'string' || l2.independent_expenditures_ballot[i].stance.trim() === '' || (l2.independent_expenditures_ballot[i].stance !== 'For' && l2.independent_expenditures_ballot[i].stance !== 'Against')) {
        addError(`l2.independent_expenditures_ballot.${i}.stance.required`, 'required')
      }
      if (!l2.independent_expenditures_ballot[i].ballot_number || typeof l2.independent_expenditures_ballot[i].ballot_number !== 'string') {
        addError(`l2.independent_expenditures_ballot.${i}.ballot_number.required`, 'required')
      }
    }
  }
  else {
    if (l2.has_independent_expenditures_ballot !== false) {
      addError('l2.contributions_all.required')
    }
  }

  if(l2.contributing_pacs && l2.contributing_pacs.length > 0){
    l2.has_contributing_pacs = true
    for (let i=0; i<l2.contributing_pacs.length; i++){
      if (!l2.contributing_pacs[i].source || !parseInt(l2.contributing_pacs[i].source) || parseInt(l2.contributing_pacs[i].source) <= 0 || !validSource(l2.sources, l2.contributing_pacs[i].source)) {
        addError(`l2.contributing_pacs.${i}.source.invalid`, 'invalid')
      }
      if (!l2.contributing_pacs[i].pac_name || typeof l2.contributing_pacs[i].pac_name !== 'string') {
        addError(`l2.contributing_pacs.${i}.pac_name.required`, 'required');
      }
    }
  }
  else {
    if (l2.has_contributing_pacs !== false) {
      addError('l2.contributions_all.required')
    }
  }

  if(l2.lobbying && l2.lobbying.length > 0 && l2.lobbying.length > 0){
    l2.has_lobbying = true
    for (let i=0; i<l2.lobbying.length; i++){
      if (!l2.lobbying[i].source || !parseInt(l2.lobbying[i].source) || parseInt(l2.lobbying[i].source) <= 0 || !validSource(l2.sources, l2.lobbying[i].source)) {
        addError(`l2.lobbying.${i}.source.invalid`, 'invalid')
      }
      function validTag(v) {
        if (typeof v === 'string') {
          return v.trim() !== ''
        }
        if (Array.isArray(v)) {
          return v.length > 0
        }
        return false;
      }
      if (!validTag(l2.lobbying[i].subject)) {
        addError(`l2.lobbying.${i}.subject.required`, 'required');
      }

      if (!validTag(l2.lobbying[i].issue) && !validTag(l2.lobbying[i].bill_number) && !validTag(l2.lobbying[i].issue_or_bill_number)) {
        addError(`l2.lobbying.${i}.issue_or_bill_number.required`, 'required');
      }
      if (!validTag(l2.lobbying[i].who_lobbied)) {
        addError(`l2.lobbying.${i}.who_lobbied.required`, 'required');
      }
    }
  }
  else {
    if (l2.has_lobbying !== false) {
      addError('l2.lobbying.incomplete')
    }
  }


  if (l2.has_any_indirect !== true && l2.has_any_indirect !== false) {
    addError('l2.expenses_indirect.incomplete')
  }

  if (l2.has_any_indirect) {
    if (l2.needL6 !== true && l2.needL6 !== false) {
      addError('l2.expenses_indirect.incomplete')
    }

    if(l2.expenses_indirect && l2.expenses_indirect.length > 0){
      for (let i=0; i<l2.expenses_indirect.length; i++){
        if (!l2.expenses_indirect[i].name_of_campaign || typeof l2.expenses_indirect[i].name_of_campaign !== 'string' ) {
          const errorCode = `l2.expenses_indirect.${i}.name_of_campaign.required`;
          validation_errors.push({[errorCode]: 'required'});
        }
        if (!l2.expenses_indirect[i].source || !parseInt(l2.expenses_indirect[i].source) || parseInt(l2.expenses_indirect[i].source) <= 0 || !validSource(l2.sources, l2.expenses_indirect[i].source)) {
          const errorCode = `l2.expenses_indirect.${i}.source.invalid`
          const error = {[errorCode] : 'invalid'};
          validation_errors.push(error)
        }
        if (!l2.expenses_indirect[i].amount || !parseFloat(l2.expenses_indirect[i].amount) || parseFloat(l2.expenses_indirect[i].amount) <= 0) {
          const errorCode = `l2.expenses_indirect.${i}.amount.invalid`
          const error = {[errorCode] : 'invalid'};
          validation_errors.push(error)
        }
        if (!l2.expenses_indirect[i].type || typeof l2.expenses_indirect[i].type !== 'string' ) {
          const errorCode = `l2.expenses_indirect.${i}.type.required`;
          validation_errors.push({[errorCode]: 'required'});
        }
        if (!l2.expenses_indirect[i].recipient || typeof l2.expenses_indirect[i].recipient !== 'string' || l2.expenses_indirect[i].recipient.trim() === '') {
          const errorCode = `l2.expenses_indirect.${i}.recipient.required`;
          validation_errors.push({[errorCode]: 'required'});
        }
        if (!l2.expenses_indirect[i].address || typeof l2.expenses_indirect[i].address !== 'string' || l2.expenses_indirect[i].address.trim() === '') {
          const errorCode = `l2.expenses_indirect.${i}.address.required`;
          validation_errors.push({[errorCode]: 'required'});
        }
        if (!l2.expenses_indirect[i].city || typeof l2.expenses_indirect[i].city !== 'string' || l2.expenses_indirect[i].city.trim() === '') {
          const errorCode = `l2.expenses_indirect.${i}.city.required`;
          validation_errors.push({[errorCode]: 'required'});
        }
        if (!l2.expenses_indirect[i].state || typeof l2.expenses_indirect[i].state !== 'string' || l2.expenses_indirect[i].state.trim() === '' || l2.expenses_indirect[i].state.trim().length !== 2) {
          const errorCode = `l2.expenses_indirect.${i}.state.required`;
          validation_errors.push({[errorCode]: 'required'});
        }
        if (!l2.expenses_indirect[i].postcode || typeof l2.expenses_indirect[i].postcode !== 'string' || l2.expenses_indirect[i].postcode.trim() === '') {
          const errorCode = `l2.expenses_indirect.${i}.postcode.required`;
          validation_errors.push({[errorCode]: 'required'});
        }
        if (!l2.expenses_indirect[i].description || typeof l2.expenses_indirect[i].description !== 'string' || l2.expenses_indirect[i].description.trim() === '') {
          const errorCode = `l2.expenses_indirect.${i}.description.required`;
          validation_errors.push({[errorCode]: 'required'});
        }

      }
    }
    else {
      if (l2.has_expenses_indirect !== false) {
        addError('l2.expenses_indirect.incomplete')
      }
    }
  }

  if(l2.printing && l2.printing.length > 0){
    l2.has_printing = true
    for (let i=0; i<l2.printing.length; i++){
      if (!l2.printing[i].source || !parseInt(l2.printing[i].source) || parseInt(l2.printing[i].source) <= 0 || !validSource(l2.sources, l2.printing[i].source)) {
        addError(`l2.printing.${i}.source.invalid`, 'invalid')
      }
      if (!l2.printing[i].amount || !parseFloat(l2.printing[i].amount) || parseFloat(l2.printing[i].amount) <= 0) {
        addError(`l2.printing.${i}.amount.invalid`, 'invalid')
      }
      if (!l2.printing[i].description || typeof l2.printing[i].description !== 'string' || l2.printing[i].description.trim() === '') {
        addError(`l2.printing.${i}.description.required`, 'required');
      }
    }
  }
  else {
    if (l2.has_printing!== false) {
      addError('l2.direct.printing.incomplete')
    }
  }

  if(l2.public_relations && l2.public_relations.length > 0){
    l2.has_public_relations = true
    for (let i=0; i<l2.public_relations.length; i++){
      if (!l2.public_relations[i].source || !parseInt(l2.public_relations[i].source) || parseInt(l2.public_relations[i].source) <= 0 || !validSource(l2.sources, l2.public_relations[i].source)) {
        addError(`l2.public_relations.${i}.source.invalid`, 'invalid')
      }
      if (!l2.public_relations[i].amount || !parseFloat(l2.public_relations[i].amount) || parseFloat(l2.public_relations[i].amount) <= 0) {
        addError(`l2.public_relations.${i}.amount.invalid`, 'invalid')
      }
      if (!l2.public_relations[i].vendor || typeof l2.public_relations[i].vendor !== 'string' || l2.public_relations[i].vendor.trim() === '') {
        addError(`l2.public_relations.${i}.vendor.required`, 'required');
      }
      if (!l2.public_relations[i].description || typeof l2.public_relations[i].description !== 'string' || l2.public_relations[i].description.trim() === '') {
        addError(`l2.public_relations.${i}.description.required`, 'required');
      }
      if (!l2.public_relations[i].date || typeof l2.public_relations[i].date !== 'string' || l2.public_relations[i].date.trim() === '') {
        addError(`l2.public_relations.${i}.date.required`, 'required');
      }
      if (!validDate(l2.public_relations[i].date, l2.period_start, l2.period_end)) {
        addError(`l2.public_relations.${i}.date.invalid`, 'invalid')
      }
    }
  }
  else {
    if (l2.has_public_relations!== false) {
      addError('l2.direct.public_relations.incomplete')
    }
  }

  if(l2.consulting && l2.consulting.length > 0){
    l2.has_consulting = true
    for (let i=0; i<l2.consulting.length; i++){
      if (!l2.consulting[i].source || !parseInt(l2.consulting[i].source) || parseInt(l2.consulting[i].source) <= 0 || !validSource(l2.sources, l2.consulting[i].source)) {
        addError(`l2.consulting.${i}.source.invalid`, 'invalid')
      }
      if (!l2.consulting[i].amount || !parseFloat(l2.consulting[i].amount) || parseFloat(l2.consulting[i].amount) <= 0) {
        addError(`l2.consulting.${i}.amount.invalid`, 'invalid')
      }
      if (!l2.consulting[i].name || typeof l2.consulting[i].name !== 'string' || l2.consulting[i].name.trim() === '') {
        addError(`l2.consulting.${i}.name.required`, 'required')
      }
      if (!l2.consulting[i].address || typeof l2.consulting[i].address !== 'string' || l2.consulting[i].address.trim() === '') {
        addError(`l2.consulting.${i}.address.required`, 'required')
      }
      if (!l2.consulting[i].city || typeof l2.consulting[i].city !== 'string' || l2.consulting[i].city.trim() === '') {
        addError(`l2.consulting.${i}.city.required`, 'required')
      }
      if (!l2.consulting[i].state || typeof l2.consulting[i].state !== 'string' || l2.consulting[i].state.trim() === '' || l2.consulting[i].state.trim().length !== 2) {
        addError(`l2.consulting.${i}.state.required`, 'required and must be a 2-character string')
      }
      if (!l2.consulting[i].postcode || typeof l2.consulting[i].postcode !== 'string' || l2.consulting[i].postcode.trim() === '') {
        addError(`l2.consulting.${i}.postcode.required`, 'required')
      }
      if (!l2.consulting[i].date || typeof l2.consulting[i].date !== 'string' || l2.consulting[i].date.trim() === '') {
        addError(`l2.consulting.${i}.date.required`, 'required')
      }
      if (!validDate(l2.consulting[i].date, l2.period_start, l2.period_end)) {
        addError(`l2.consulting.${i}.date.invalid`, 'invalid')
      }
    }
  }
  else {
    if (l2.has_consulting !== false) {
      addError('l2.direct.consulting.incomplete')
    }
  }

  if(l2.expenses_other && l2.expenses_other.length > 0){
    l2.has_expenses_other = true
    for (let i=0; i<l2.expenses_other.length; i++){
      if (!l2.expenses_other[i].source || !parseInt(l2.expenses_other[i].source) || parseInt(l2.expenses_other[i].source) <= 0 || !validSource(l2.sources, l2.expenses_other[i].source)) {
        addError(`l2.expenses_other.${i}.source.invalid`, 'invalid')
      }
      if (!l2.expenses_other[i].amount || !parseFloat(l2.expenses_other[i].amount) || parseFloat(l2.expenses_other[i].amount) <= 0) {
        addError(`l2.expenses_other.${i}.amount.invalid`, 'invalid')
      }
      if (!l2.expenses_other[i].vendor || typeof l2.expenses_other[i].vendor !== 'string' || l2.expenses_other[i].vendor.trim() === '') {
        addError(`l2.expenses_other.${i}.vendor.required`, 'required');
      }
      if (!l2.expenses_other[i].description || typeof l2.expenses_other[i].description !== 'string' || l2.expenses_other[i].description.trim() === '') {
        addError(`l2.expenses_other.${i}.description.required`, 'required');
      }
      if (!l2.expenses_other[i].date || typeof l2.expenses_other[i].date !== 'string' || l2.expenses_other[i].date.trim() === '') {
        addError(`l2.expenses_other.${i}.date.required`, 'required');
      }
      if (!validDate(l2.expenses_other[i].date, l2.period_start, l2.period_end)) {
        addError(`l2.expenses_other.${i}.date.invalid`, 'invalid')
      }
    }
  }
  else {
    if (l2.has_expenses_other!== false) {
      addError('l2.direct.expenses_other.incomplete')
    }
  }

  if (l2.entertainment_small && l2.entertainment_small.length > 0) {
    l2.has_entertainment_small = true
    for (let i=0; i<l2.entertainment_small.length; i++) {
      if (!l2.entertainment_small[i].source || !parseInt(l2.entertainment_small[i].source) || parseInt(l2.entertainment_small[i].source) <= 0 || !validSource(l2.sources, l2.entertainment_small[i].source)) {
        addError(`l2.entertainment_small.${i}.source.invalid`, 'invalid')
      }
      if (!l2.entertainment_small[i].amount || !parseFloat(l2.entertainment_small[i].amount) || parseFloat(l2.entertainment_small[i].amount) <= 0) {
        addError(`l2.entertainment_small.${i}.amount.invalid`, 'invalid')
      }
    }
  }
  else if (l2.has_entertainment_small !== false) {
    addError('l2.entertainment_all.incomplete')
  }

  return validation_errors;
}
