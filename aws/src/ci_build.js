import {runRevScripts} from '@wapdc/common/revision-manager'
import path from 'path'
import * as url from 'url'
import * as db from '@wapdc/common/wapdc-db'
import {createDemoData} from "./DemoData.js";
import util from "util";
import childProcess from "child_process";

const exec = util.promisify(childProcess.exec)

const __dirname = url.fileURLToPath(new URL('.', import.meta.url))
const scriptDirectory = path.join(__dirname, '..', '..', 'revisions')
console.info(`scriptDirectory: ${scriptDirectory}`)
const appName = 'lobbyist'

async function runScript(file) {
  try {
    const res = await exec(`sh ${file}`, {
      cwd: ".."
    })
    console.log(res.stdout)
    return true
  } catch (error) {
    throw new Error(`Error in runScript: ${error.message}`)
  }
}

await db.connect()
await db.beginTransaction()
await runRevScripts(scriptDirectory, appName)
console.info('Revision scripts ran successfully')
const stage = await db.getStage()
if (stage !== 'live') {
  await createDemoData()
  const counts = await db.fetch(`select count(*) as profiles from lobbyist_firm_profile`)
  if (parseInt(counts.profiles) === 0) {
    console.info('Populating profiles')
    await runScript('ci/populate_profiles.sh')
  }
  else {
    console.info('Skipping profiles')
  }
}
console.info('Demo data created successfully')
await db.commit()
await db.close()
