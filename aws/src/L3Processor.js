import * as rds from '@wapdc/common/wapdc-db'
import {legacyProcessor} from "./LegacyProcessor.js";
import {createDraft, deleteReportDraft, getReportDraft, saveDraft} from "@wapdc/common/drafts";
import {clientProcessor} from "./ClientProcessor.js";
import validateL3Draft from './L3Validator.js'
import {readFile} from "fs/promises";

export const emptyL3 = {
  report_type: "L3",
  period_start: null,
  period_end: null,
  lobbyist: [],
  entertainment_expenditures: [],
  itemized_expenditures: [],
  other_expenditures: [],
  has_entertainment_expenditures: null,
  has_itemized_expenditures: null,
  has_other_expenditures: null,
  payments_for_registered_lobbyists: null,
  witnesses_retained_for_lobbying_services: null,
  costs_for_promotional_materials: null,
  grassroots_expenses_for_lobbying_communications: null,
  non_itemized_political_contribution_amount: null,
  has_itemized_contributions: null,
  itemized_contributions: [],
  has_pac_contributions: null,
  pac_contributions: [],
  has_candidate_independent_expenditures: null,
  candidate_independent_expenditures: [],
  has_ballot_proposition_expenditures: null,
  ballot_proposition_expenditures: [],
  has_employment_compensation: null,
  employment_compensation: [],
  has_professional_services_compensation: null,
  professional_services_compensation: [],
  has_in_kind_contributions: null,
  in_kind_contributions: [],
  certification: {
    signature: null,
    text: null,
    name: null,
    user_name: null
  }
}
class L3Processor {

    async getL3(client_id, submission_id) {
        let l3 = await rds.fetch(`
            select s.*,
                   l3.*,
                   dd.draft_id,
                   (select json_agg(json_build_object('tid', a.tid, 'name', a.name)) from accesshub_taxonomy_term_data a) as taxonomy,
                   (select lobbyists::jsonb || contracts::jsonb
                    from (select json_agg(json_build_object('id', lc.lobbyist_contract_id, 'name', l.name)) as contracts,
                                 json_agg(json_build_object('id', lc.lobbyist_firm_id, 'name', l.name)) as lobbyists
                          from lobbyist_contract lc
                                   left join lobbyist_firm l on lc.lobbyist_firm_id = l.lobbyist_firm_id
                          where lc.lobbyist_client_id = $1) c) as contract_lookup
            from l3_submission s
                     left join l3 on l3.report_id = s.report_id
                     left join private.draft_document dd on dd.target_id = $1 and dd.report_type = 'L3' and
                                                            dd.report_key = cast(l3.report_year as text)
            where s.submission_id = $2
        `, [client_id, submission_id]);
        if (l3 && l3[0]) {
            l3 = l3[0];
        }
        l3.user_data = legacyProcessor.preProcessLegacyRecord(l3.user_data);
        return l3;
    }

    /**
     * Get the history of L3 reports for a client/employer.
     * @param client_id
     * @param report_id
     * @returns {Promise<*|null>}
     */
    async getL3History(client_id, report_id) {
        return rds.query(`select l3s.* from l3_submission l3s
                          join l3 l on l.report_id = l3s.report_id
                          where l.report_id=$2
                            and l.lobbyist_client_id = $1
                          order by l3s.submitted_at desc;`, [client_id, report_id])
    }

    async loadCurrentMetadata(clientId, reportYear, l3Report){
      const client = await clientProcessor.getClient(clientId)

      return l3Report.client = {
          lobbyist_client_id: client.lobbyist_client_id,
          name: client.name,
          address: {
              street: client.address_1,
              city: client.city,
              state: client.state,
              postcode: client.postcode,
              country: client.country
          },
          phone: client.phone,
          email: client.email
      }
    }
  mergeLobbyistDataWithL2Summary(l2Summary, lobbyistData) {
    l2Summary.forEach((l2) => {
      let matchedLobbyist = lobbyistData.find(lobbyist => lobbyist.lobbyist_contract_id === l2.fer_nid)
      if (!matchedLobbyist) {
          lobbyistData.push(
            {
              lobbyist_contract_id: l2.fer_nid,
              lobbyist_title: l2.lobbyist_title,
              contractor_title: l2.contractor_title,
              compensation_l2: l2.compensation,
              expenses_l2: l2.expenses,
            }
          )
      } else {
        matchedLobbyist.compensation_l2 = l2.compensation
        matchedLobbyist.expenses_l2 = l2.expenses
      }
    })
  }

  async readDataFile(fileName) {
    const json= await readFile(`src/tests/data/sync/${fileName}`, 'ascii');
    return JSON.parse(json);
  }

    /**
     * Creates a new draft report for a specific client/employer and year.
     * @param clientId
     * @param reportYear
     * @return {Promise<number>}
     */
    async makeDraft(clientId, reportYear) {
      if (!reportYear) {
        throw new Error('Year is required')
      }

      const l3Report = structuredClone(emptyL3)
      await this.loadCurrentMetadata(clientId, reportYear, l3Report)

      return await createDraft({
        target_type: 'lobbyist_client',
        target_id: clientId,
        user_data: l3Report,
        report_type: 'L3',
        report_key: reportYear
      })
    }

    async getDraft(clientId, reportYear) {
        let res = {}
        res.draft = await getReportDraft('lobbyist_client', clientId, 'L3', reportYear)
        if (res.draft) {
            res.l2_summary = await this.getL2Totals(reportYear, clientId)
            res.draft.user_data = Object.assign(structuredClone(emptyL3), res.draft.user_data)
            res.draft.user_data.period_start = reportYear+'-01-01'
            res.draft.user_data.period_end = reportYear+'-12-31'
            this.mergeLobbyistDataWithL2Summary(res.l2_summary, res.draft.user_data.lobbyist)
            res.validations = validateL3Draft(res.draft.user_data)
            res.committees = await this.getCommittees(reportYear)
            res.statewide_candidacies = await this.getStatewideCandidacies(reportYear)
            res.candidates = await this.getCandidates(reportYear)
            await this.loadCurrentMetadata(clientId, reportYear, res.draft.user_data)
        }
        else {
            res.validations = []
        }

        return res
    }
  async getCommittees(year) {
    return rds.query(
      `select committee_id, name
               from committee 
                 where ((continuing and cast($1 as int) between start_year and coalesce(end_year, cast($1 as int)))
                   or (election_code >= cast($1 as text)))
                 and committee_type = 'CO'
                 and filer_id is not null
               order by name`, [year]
    )
  }

  async getStatewideCandidacies(year) {
    return rds.query(
        `select c.candidacy_id,
                concat(coalesce(c.ballot_name, e.name),
                       CASE
                           WHEN c.exit_reason is null then ''
                           else ' (' || exit_reason || ')' end
                ) as name
         from candidacy c
                  join jurisdiction j on c.jurisdiction_id = j.jurisdiction_id
                  join entity e on e.entity_id = c.person_id
         where c.election_code >= cast($1 as text)
           and c.filer_id is not null
           and j.category in (1,4)
         order by name`, [year]
    )
  }

  async getCandidates(year) {
    return rds.query(
      `select ca.candidacy_id,
                  case
                      when ca.ballot_name <> e.name
                          then concat(e.name, ' (', ca.ballot_name, ')')
                      else e.name
                      end || ' - '
                      || offtitle || CASE WHEN j.jurisdiction_id<> 1000 THEN ' ' || j.name else '' end 
                      || ' ' || substr(ca.election_code,1,4) 
                      || CASE WHEN exit_reason is null then ''
                            when exit_reason='' then ''
                            else ' (' || exit_reason || ')' end 
                      as name 
               from entity e join candidacy ca on ca.person_id = e.entity_id
                 join jurisdiction j on ca.jurisdiction_id = j.jurisdiction_id
                 join foffice o on ca.office_code= o.offcode
                 where election_code >= cast($1 as text)
                   and (j.jurisdiction_id=1000 or j.category=4)
               order by name`, [year]
    )
  }

    async saveL3Draft(clientId, reportYear, saveCount, l3) {
        let saveResult = {'save_count': 0, 'validations': []};
        saveResult.validations = validateL3Draft(l3)
        saveResult.save_count = await saveDraft({
            target_type: 'lobbyist_client',
            target_id: clientId,
            report_type: 'L3',
            report_key: reportYear,
            user_data: l3,
            save_count: saveCount
        });
        return saveResult;
    }

  async deleteL3Draft(client_id, year) {
    return deleteReportDraft('lobbyist_client',client_id, 'L3', year)
  }

  async saveReport(clientId, reportYear , userData, version = '2.0') {

    const l3 = await rds.fetch(`insert into l3 (lobbyist_client_id, report_year)VALUES($1, $2) returning report_id
     `, [ clientId, reportYear])

    const report_id = l3.report_id

    const l3Submission = await rds.fetch(`insert into l3_submission (report_id, version, user_data)VALUES($1, $2 , $3) returning submission_id`,
      [report_id, version, userData])

    const submission_id = l3Submission.submission_id

    await rds.fetch(`update l3 set current_submission_id = $1 where report_id = $2`, [submission_id, report_id])
    return {  report_id , submission_id }
  }

  async submitL3Report(clientId, reportYear, userData, claims) {
    let res
    const validations = validateL3Draft(userData)
    if (validations.length > 0) {
      throw new Error('L3 report is not valid')
    }
    if(!reportYear) throw new Error('Reporting year is required')
    if(!userData) throw new Error('User data is required')
    if(!clientId) throw new Error('Client/employer id is required')

    if (!userData.certification?.signature) {
      throw new Error('Certification is missing or null');
    }

    if (userData.certification?.signature.toLowerCase().trim() !== "i certify") {
      throw new Error('Certification signature is invalid');
    }

    if (!userData.certification?.text || !userData.certification.text.trim()) {
      throw new Error('Certification text is empty or null');
    }
    if (!claims.name || !claims.name.trim()) {
      throw new Error('Name is required for certification');
    }
    if (!claims.user_name || !claims.user_name.trim()) {
      throw new Error('Username is required for certification');
    }
    userData.certification.name = claims.name
    userData.certification.user_name = claims.user_name
    // Check if there is submission data available for L3.
    const l3 = await rds.fetch(`select l3.*,l3s.version from l3 join l3_submission l3s on l3.report_id = l3s.report_id where lobbyist_client_id = $1 and report_year = $2 and l3s.superseded_id is null`, [clientId, reportYear])
    if(l3) {
      const l3Submission = await rds.fetch(`insert into l3_submission (report_id, version, user_data) values ($1, $2 , $3) returning submission_id`, [l3.report_id, '2.0', userData])
      const newSubmissionId = l3Submission.submission_id
      await rds.fetch(`update l3 set current_submission_id = $1 where report_id = $2`, [newSubmissionId, l3.report_id])
      await rds.fetch(`update l3_submission set superseded_id = $1 where submission_id = $2`, [newSubmissionId, l3.current_submission_id])
      res = { report_id : l3.report_id, submission_id : newSubmissionId }
    }
    else {
      res = await this.saveReport(clientId, reportYear, userData)
      if(res) {
        await rds.fetch(`update l3 set current_submission_id = $1 where report_id = $2`, [res.submissionId, res.reportId])
      }
    }
    await rds.execute(`select l3_save_transactions(cast($1 as int))`, [res.report_id])
    if (res) {
      await rds.fetch(`delete from private.draft_document where report_key = $1 and target_id = $2 and report_type='L3' and target_type = 'lobbyist_client'`,[reportYear, clientId])
    }
    return res;
  }

  /**
   * Fetches and returns detailed report data for a specific lobbying client and reporting year.
   *   from the database.
   *
   * @param clientId
   * @param reportYear
   * @returns {Promise<*|null>}
   */
  getReport(clientId, reportYear) {
    return  rds.fetch(`select l3.*, l3s.user_data, l3s.submitted_at, l3s.superseded_id, l3s.version
            from l3 join l3_submission l3s on l3.current_submission_id=l3s.submission_id
            where l3.lobbyist_client_id=$1 and l3.report_year=$2`,
      [clientId, reportYear])
  }

  async createAmendmentDraft(clientId, reportYear) {
    const existingAmendmentDraft = await this.getDraft(clientId, reportYear)
    if(existingAmendmentDraft?.draft) {
        return existingAmendmentDraft
    }

    const res = await this.getReport(clientId, reportYear)
    if(!res) throw new Error('No report found')

    let l3Report
    if(parseFloat(res.version) >= 2.0) {
      l3Report = res.user_data
    } else {
      l3Report = structuredClone(emptyL3)
      Object.assign(l3Report, await legacyProcessor.convertL3(res.user_data))
      const validations = validateL3Draft(l3Report)
      if (validations.length > 0) {
        l3Report.conversion_incomplete = true
      }
    }



    // If the existing report was created from an amendment, we need to clear the conversion_incomplete indicator.
    if (res.version > 2.0 && l3Report.hasOwnProperty('conversion_incomplete')) {
      delete l3Report.conversion_incomplete
    }
    l3Report.amends = res.current_submission_id
    await this.loadCurrentMetadata(clientId, reportYear, l3Report)

    // Create the draft
    return await createDraft({
      target_type: 'lobbyist_client',
      target_id: res.lobbyist_client_id,
      user_data: l3Report,
      report_type: 'L3',
      report_key: res.report_year
    });
  }

  async getL2Totals(year, clientId) {
    return await rds.query(
    `SELECT
    lc.lobbyist_contract_id as fer_nid,
      f.name as lobbyist_title,
      fc.name as contractor_title,
      COALESCE(tt.compensation,0)  + COALESCE(tt.sub_lob_comp, 0) as compensation,
      COALESCE(tt.personal_expenses, 0) as personal_expenses,
      COALESCE(tt.entertainment_expenses, 0) as entertainment_expenses, 
      COALESCE(tt.election_expsenses, 0) as election_expenses,
      COALESCE(tt.direct_lobbying_expenses, 0) as direct_lobbying_expenses,
      COALESCE(tt.expenses, 0) AS expenses,
      COALESCE(tt.indirect_lobbying_expenses, 0) as indirect_lobbying_expenses,
      COALESCE(tt.advertising_expenses, 0) as advertising_expenses,
      COALESCE(tt.compensation, 0) as lob_comp,
      COALESCE(tt.sub_lob_comp,0) AS sub_lob_comp,
      COALESCE(tt.total, 0) as total,
      lc.reports_filed
    FROM (select lc1.lobbyist_client_id, lc1.lobbyist_firm_id, lc1.lobbyist_contract_id, lc1.contractor_id, extract(year from lrp.period_start) as report_year,
                 json_agg(json_build_object('month',date_part('month', lrp.period_start),
                                            'filed', l2.report_id)) as reports_filed
    from lobbyist_contract lc1
    JOIN lobbyist_reporting_periods lrp ON lrp.lobbyist_contract_id = lc1.lobbyist_contract_id
    left join l2 on l2.lobbyist_firm_id = lrp.lobbyist_firm_id and l2.period_start = lrp.period_start
    WHERE
    lc1.lobbyist_client_id = $2
    AND extract(year from lrp.period_start) = $1
    group by lc1.lobbyist_client_id, lc1.lobbyist_firm_id, lc1.lobbyist_contract_id, lc1.contractor_id, extract(year from lrp.period_start)
  )  lc
    JOIN lobbyist_firm f on f.lobbyist_firm_id = lc.lobbyist_firm_id
    JOIN lobbyist_client c on lc.lobbyist_client_id = c.lobbyist_client_id
    LEFT JOIN lobbyist_firm fc on lc.contractor_id = fc.lobbyist_firm_id
    LEFT JOIN (
      SELECT coalesce(lco.lobbyist_contract_id, l2t.source_target_id) as source_target_id,
      extract(year from r.period_start) as reporting_year,
      sum(case when l2t.transaction_type = 'compensation' then l2t.amount end) as compensation,
      sum(case when l2t.transaction_type not in ('compensation', 'sub_lobbyist_payment') then l2t.amount end) as expenses,
      sum(case when l2t.transaction_type = 'sub_lobbyist_payment' then -1 * l2t.amount end ) as sub_lob_comp,
      sum(case when l2t.transaction_type in ('personal_expense', 'non_reimbursed_personal_expenses') then l2t.amount end ) as personal_expenses,
      sum(case when l2t.transaction_type in ('non_itemized_entertainment_expenses', 'itemized_entertainment_expense') then l2t.amount end ) as entertainment_expenses,
      sum(case when l2t.transaction_type in ('non_itemized_contributions','contributions_small_expense', 'in_kind_expense', 'itemized_contribution', 'ad_expense', 'independent_expenditures_candidate_expense', 'independent_expenditures_ballot_expense') then l2t.amount end) as election_expsenses,
      sum(case when l2t.transaction_type in ('consulting_expense', 'printing_expense', 'public_relations_expense', 'other_expense') then l2t.amount end ) as direct_lobbying_expenses,
      sum(case when l2t.transaction_type = 'advertising_expense' then l2t.amount end) as advertising_expenses, 
      sum(case when l2t.transaction_type in ('indirect_expense') then l2t.amount end) as indirect_lobbying_expenses,
      sum(case when l2t.transaction_type = 'sub_lobbyist_payment' then -1 * l2t.amount else l2t.amount end) as total
    FROM
    l2_transaction l2t
    JOIN l2 r on r.report_id = l2t.report_id
    JOIN lobbyist_contract lc3 on lc3.lobbyist_contract_id = l2t.source_target_id and l2t.source_target_type='lobbyist_contract'
    -- Payments to subcontracted lobbyists need to be attributed to the original contract between the contractor and the client
    -- in order to be able to reduce the compensation by the amount of the payment. 
    left join lobbyist_contract lco on lc3.contractor_id = lco.lobbyist_firm_id and lc3.lobbyist_client_id=lco.lobbyist_client_id
      and r.period_start between lco.period_start and lco.period_end                                          
      and lco.contractor_id is null and l2t.transaction_type='sub_lobbyist_payment' 
    WHERE lc3.lobbyist_client_id = $2 
    AND extract (year from r.period_start) = $1
    GROUP BY coalesce(lco.lobbyist_contract_id, l2t.source_target_id), extract(year from r.period_start)
  ) tt ON tt.source_target_id = lc.lobbyist_contract_id`, [year, clientId])
  }
}

const l3Processor = new L3Processor()
export { l3Processor }
