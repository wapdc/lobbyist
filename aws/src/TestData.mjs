import * as rds from "@wapdc/common/wapdc-db"

export const testLmcSubmission = JSON.parse(data).lmc

export const testEntity = {
  name: "Test entity"
};
export const testCommittee = {
  name: "Test Committee",
  committee_type: "CO",
  pac_type: "pac",
  start_year: 2020,
  registered_at: "2020-01-01"
};


export const testLobbyistFirm = {
  lobbyist_firm_id: -1,
  name: "Test Lobbyist Firm",
};

export const testLobbyistEmployer = {
  lobbyist_employer_id: -1,
  name: "Test Lobbyist Employer"
}

export async function createEntity() {
  testEntity.entity_id = (await rds.fetch("INSERT INTO entity(name, is_person) values ($1, $2) returning entity_id ",
    [testEntity.name, true])).entity_id;
  return testEntity.entity_id;
}


export async function createLobbyistFirm() {
  if (!testEntity.entity_id) {
    await createEntity();
  }
  testLobbyistFirm.entity_id = testEntity.entity_id
  testLobbyistFirm.lobbyist_firm_id = (await rds.fetch(`
    INSERT INTO lobbyist_firm(lobbyist_firm_id, entity_id, name) VALUES ($1, $2, $3) returning  lobbyist_firm_id
    `,
    [testLobbyistFirm.lobbyist_firm_id, testEntity.entity_id, testLobbyistFirm.name])).lobbyist_firm_id
  return testLobbyistFirm.lobbyist_firm_id
}

export async function createLobbyistEmployer() {
  if (!testEntity.entity_id) {
    await createEntity();
  }
  testLobbyistEmployer.entity_id = testEntity.entity_id
  testLobbyistEmployer.lobbyist_employer_id = (await rds.fetch(`
    INSERT INTO lobbyist_employer(lobbyist_employer_id, entity_id, name) VALUES ($1, $2, $3) returning lobbyist_employer_id
    `,
    [testLobbyistEmployer.lobbyist_employer_id, testEntity.entity_id, testLobbyistEmployer.name])).lobbyist_employer_id
  return testLobbyistEmployer.lobbyist_employer_id
}
