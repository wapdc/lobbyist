import firmSubmission from './demo/firm.json' assert {type: "json"}
import clientSubmission from './demo/client.json' assert {type: "json"}
import contractSubmission from './demo/contract.json' assert {type: "json"}
import subcontractSubmission from './demo/subContract.json' assert {type: "json"}
import * as rds from '@wapdc/common/wapdc-db'

const itUsers = [
  'bill.king@pdc.wa.gov',
  'david.metzler@pdc.wa.gov',
  'dillon.ching@pdc.wa.gov',
  'james.gutholm@pdc.wa.gov',
  'jim.coleman@pdc.wa.gov',
  'kenny.scott@pdc.wa.gov',
  'lindy.myers@pdc.wa.gov',
  'michael.dailey@pdc.wa.gov',
  'sravya.yandamuri@pdc.wa.gov',
]

const faStaff = [
  'ladelle.fuquay@pdc.wa.gov',
  'scott.haley@pdc.wa.gov',
  'ashley.carlson@pdc.wa.gov',
  'diana.izaguirre@pdc.wa.gov',
  'natalie.johnson@pdc.wa.gov',
  'nate.meyer@pdc.wa.gov'
]

const complianceStaff = [
  'phil.stutzman@pdc.wa.gov',
  'jennifer.hansen@pdc.wa.gov',
  'erick.agina@pdc.wa.gov',
  'tabatha.blacksmith@pdc.wa.gov',
  'alice.fiman@pdc.wa.gov',
  'tabitha.townsend@pdc.wa.gov',
  'jordan.campbell@pdc.wa.gov',
  'colin.peeples@pdc.wa.gov',
  'kim.bradford@pdc.wa.gov'
]

async function  getIds() {
  return await rds.fetch(`select nextval('lobbyist_report_seq') as nid, nextval('lobbyist_submission_seq') as vid`)
}

async function createFirm(name, users) {
  let ids
  ids = await rds.fetch(`SELECT lobbyist_firm_id as nid, current_submission_id as vid from lobbyist_firm where name = $1`, [name])
  if (!ids) {
    // Generate the ids for the submission and node
    ids = await getIds()
  }

  firmSubmission.nid = ids.nid
  firmSubmission.vid = ids.vid
  firmSubmission.title = name;

  await rds.execute(`select lobbyist_sync_accesshub($1::json)`, [JSON.stringify([firmSubmission])])
  // Grant access to the firm
  await grantAccess('lobbyist_firm', ids.nid, users)
  return ids.nid
}

/**
 * Create a lobbyist client/employer record based on submission.
 * @param name The name of the lobbyist client/employer to create
 * @param users List of pdc_user user_name's that should be granted access.
 * @return {Promise<Number>}
 */
async function createClient(name, users) {
  let ids
  ids = await rds.fetch(`SELECT lobbyist_client_id as nid, current_submission_id as vid from lobbyist_client where name = $1`, [name])
  if (!ids) {
    // Generate the ids for the submission and node
    ids = await getIds()
  }

  clientSubmission.nid = ids.nid
  clientSubmission.vid = ids.vid
  clientSubmission.title = name;

  await rds.execute(`select lobbyist_sync_accesshub($1::json)`, [JSON.stringify([clientSubmission])])
  // Grant access to the firm
  await grantAccess('lobbyist_client', ids.nid, users)

  return ids.nid
}

/**
 * Create a simple contract (no subcontractor relationship)
 * @param firm_id
 * @param client_id
 * @param name
 * @return {Promise<void>}
 */
async function createContract(firm_id, client_id, name) {
  let ids
  ids = await rds.fetch(`SELECT lobbyist_contract_id as nid, current_submission_id as vid from lobbyist_contract 
                                   where lobbyist_firm_id = $1 and lobbyist_client_id=$2
                                   and contractor_id is null`, [firm_id, client_id])
  if (!ids) {
    // Generate the ids for the submission and node
    ids = await getIds()
  }

  contractSubmission.nid = ids.nid
  contractSubmission.vid = ids.vid
  contractSubmission.title = name
  contractSubmission.field_firm_entity_reference.und[0].target_id = firm_id
  contractSubmission.field_employer_entity_reference.und[0].target_id = client_id

  await rds.execute(`select lobbyist_sync_accesshub($1::json)`, [JSON.stringify([contractSubmission])])
}

/**
 * Create a sub
 * @param firm_id ID of firm supplying the services
 * @param client_id ID of the client/employer receiving the benefit
 * @param contractor_id ID of the firm through which the subcontractor is working.
 * @param name Name to be placed on the contract.
 * @return {Promise<void>}
 */
async function createSubContract(firm_id, client_id, contractor_id, name) {
  let ids
  ids = await rds.fetch(`SELECT lobbyist_contract_id as nid, current_submission_id as vid from lobbyist_contract 
                                   where lobbyist_firm_id = $1 and lobbyist_client_id=$2
                                   and contractor_id = $3`, [firm_id, client_id, contractor_id])
  if (!ids) {
    // Generate the ids for the submission and node
    ids = await getIds()
  }

  subcontractSubmission.nid = ids.nid
  subcontractSubmission.vid = ids.vid
  subcontractSubmission.title = name
  subcontractSubmission.field_firm_entity_reference.und[0].target_id = firm_id
  subcontractSubmission.field_employer_entity_reference.und[0].target_id = client_id
  subcontractSubmission.field_firm_that_is_employing_you.und[0].target_id = contractor_id

  await rds.execute(`select lobbyist_sync_accesshub($1::json)`, [JSON.stringify([subcontractSubmission])])
}

async function grantAccess(type, id,  users) {
  for (let user of users) {

    await rds.execute(`INSERT INTO pdc_user_authorization(realm, uid, target_type, target_id, allow_modify, allow_submit) 
    VALUES ('apollo', COALESCE((select max(uid) from pdc_user where realm='apollo' and user_name=$1), '2'), $2, $3, true, true) on conflict do nothing`,
      [user, type, id])
  }
}

export async function createDemoData() {
  // Use the sequences to determine what committees
  const firmId = await createFirm('Music Lobby', itUsers)
  const subFirmId = await createFirm('ACME lobbyists', itUsers)
  const clientId = await createClient('Rocking Music Studio', itUsers)
  await createContract(firmId, clientId, 'Rocking Music Studio')
  await createSubContract(subFirmId, clientId, firmId, 'ACME Lobbyists (Music Lobby)')

  const csFirm = await createFirm('Selfless Help Lobbying', faStaff)
  const csClient = await createClient('Right U R', faStaff)
  await createContract(csFirm, csClient, 'Right U R')

  const complianceFirm = await createFirm('Hobby Lobbying', complianceStaff)
  const complianceClient = await createClient('Shining Light', complianceStaff)
  await createContract(complianceFirm, complianceClient, 'Shining Light')

}

