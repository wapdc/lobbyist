import * as rds from "@wapdc/common/wapdc-db"
import {sendMessage} from "@wapdc/common/mailer";

class PublicAgencyProcessor {
  /**
   * Gets entire list of public agencies
   * */
  async getAgencies(){
    return rds.query(
        'select * from lobbyist_public_agency a order by a.name'
    )
  }

  /**
   * Calls a postgres function that inserts a record into the lobbyist_public_agency table for new agencies
   * as well as a filer_request_log entry denoting a change to the lobbyist_public_agency and the staff member who
   * verified the change. Then writing the record to the pdc_user_authorization table giving the filer access before
   * deleting the filer_request record from the table. The postgres function returns any emails that may be on file for
   * other authorized users on the agency allowing confirmation by the agency that someone else was authorized.
   *
   * This function is utilized by customer service during the public agency verification process by PDC staff.
   *
   * $param uid
   * $param realm
   * $param verification_payload
   */
  async verifyAgencyAccess(uid, realm, verification_payload) {
    //give access
   const access_users = await rds.fetch(`select (lobbyist_public_agency_access_verification($1, $2, cast($3 as integer), cast($4 as integer), $5)) as emails`, [uid, realm, verification_payload.filer_request_id, verification_payload.lobbyist_id, verification_payload.payload])

    if(access_users.emails) {
      await sendMessage(access_users.emails, 'd-9cb0dc11bd9b42abb80bfa4d0e58889b', {agency: verification_payload.payload.name, user: verification_payload.email} )
    }

    if(verification_payload.email){
      await sendMessage(verification_payload.email, 'd-7da84081108649809a8de7ba3389c31f', {agency: verification_payload.payload.name} )
    }
  }
}

const publicAgencyProcessor = new PublicAgencyProcessor();
export {publicAgencyProcessor}