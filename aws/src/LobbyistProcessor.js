import * as rds from "@wapdc/common/wapdc-db"

class LobbyistProcessor{

  async getAccessRequests() {
    return rds.query(
        'select * from filer_request a order by a.submitted_at'
    )
  }

  async getAccessRequest(request_id) {
    return rds.fetch(
        `select a.*, puc.target as email from filer_request a left join pdc_user_contact puc on a.realm = puc.realm and a.uid = puc.uid and puc.protocol = 'email' where a.id = $1`, [request_id]
    )
  }

  /**
   * Queries filer_request table against users uid and realm to validate access
   * **/
  async hasAccessToRequest(claims, request_id){
    return await rds.fetch(`select id from filer_request where id = $1 and uid = $2 and realm = $3`,
        [request_id, claims.uid, claims.realm])
  }


  /**
   * Calls a postgres function that inserts a record into the filer_request table for entities that
   * wants to be created but require verification from admin users.
   *
   * @param uid
   * @param realm
   * @param target_id (optional) - Unique id of an existing entity
   * @param target_type - The application name e.g. "lobbyist_public_agency"
   * @param payload - Free form json that can be used as per the application
   */
  async submitFilerRequest(uid, realm, target_id = null, target_type, payload) {
    await rds.query("select lobbyist_submit_filer_request($1, $2, cast($3 as integer), $4, $5)", [uid, realm, target_id, target_type, payload])
  }

  async deleteFilerRequest(request_id){
    await rds.execute("delete from filer_request fr where fr.id = cast($1 as integer)",[request_id]);
  }

  async getUserFirmsAndClients(uid, realm) {
    const firms = await rds.query(`
        select f.*,rptcount.report_due_count
        from lobbyist_firm f
                 join pdc_user_authorization ua on ua.target_id = f.lobbyist_firm_id and ua.target_type = 'lobbyist_firm'
                 left join (
            select
                lr.lobbyist_firm_id , count(case when l2.report_id is null then 1 end) as report_due_count
            from (
                     select
                         lc.lobbyist_firm_id,
                         lrp.period_start,
                         lrp.period_end
                     from lobbyist_reporting_periods lrp
                              join lobbyist_contract lc on lc.lobbyist_contract_id = lrp.lobbyist_contract_id
                     where lrp.period_end <= now() and lrp.period_start >= '2021-01-01'
                     group by lc.lobbyist_firm_id, lrp.period_start, lrp.period_end
                 ) lr
                     join reporting_period rp on rp.report_type = 'L2' and rp.start_date = lr.period_start and rp.end_date = lr.period_end
                     left join l2 on l2.lobbyist_firm_id = lr.lobbyist_firm_id and l2.period_start = lr.period_start
            group by lr.lobbyist_firm_id
        ) rptcount on rptcount.lobbyist_firm_id = f.lobbyist_firm_id
        WHERE ua.uid = $1 and ua.realm = $2 and ua.allow_modify = true
    `, [uid, realm])
    const clients = await rds.query(`
        select e.*, rptcount.report_due_count
        from lobbyist_client e
                 join pdc_user_authorization ua on ua.target_id = e.lobbyist_client_id and ua.target_type ='lobbyist_client' 
                 left join (select lr.lobbyist_client_id,count(case when l3.report_id is null then 1 end) as report_due_count
                            from (select lc.lobbyist_client_id,extract(year from lrp.period_start) as period_start_year
                                  from lobbyist_reporting_periods lrp
                                           join lobbyist_contract lc
                                                on lc.lobbyist_contract_id = lrp.lobbyist_contract_id
                                  where extract(year from lrp.period_start) <= extract(year from now()) - 1
                                    and extract(year from lrp.period_start) >= '2021'
                                  group by lc.lobbyist_client_id, period_start_year) lr
                                     join reporting_period rp
                                          on rp.report_type = 'L3' and
                                             extract(year from rp.start_date) = lr.period_start_year
                                     left join l3 on l3.lobbyist_client_id = lr.lobbyist_client_id and
                                                     l3.report_year = lr.period_start_year
                            group by lr.lobbyist_client_id) rptcount
                           on rptcount.lobbyist_client_id = e.lobbyist_client_id
        WHERE ua.uid = $1 AND realm = $2
    `, [uid, realm])
    return {firms, clients};
  }
  async getAuthorizedUsers(id, target_type) {
    return await rds.query(`
        SELECT pua.uid, pua.realm,puc.target as email, pua.granted_date FROM pdc_user_authorization pua
           LEFT JOIN pdc_user_contact puc on puc.realm = pua.realm and puc.uid = pua.uid
        WHERE target_id = $1 AND target_type = $2 AND puc.protocol = 'email'
          AND (pua.expiration_date > now() OR pua.expiration_date IS NULL)
  `, [id, target_type]);
  }

  async revokeUserAuthorization(uid, realm, target_type, target_id) {
      await rds.execute(`
        UPDATE pdc_user_authorization pua
        SET expiration_date = now()
        WHERE pua.uid = $1 AND pua.realm = $2 AND pua.target_type = $3 AND pua.target_id = $4
        `, [uid, realm, target_type, target_id])
  }

  async updateFilerRequest(request_id, user_name, snooze_days) {
    await rds.execute(`
      update filer_request
      set agent_user_name = $1, action_date = $2
      where id = $3`,
        [user_name, snooze_days, request_id])
  }

  async hasDraftAccessToTarget(claims, target_type, target_id, require_write=false) {
    return (await this.hasAccessToTarget(claims, target_type, target_id, require_write))
      || (claims.admin && (await this.hasAccessToTarget({uid: 'PDC', realm: "PDC"}, target_type, target_id, require_write)))
  }

  async hasAccessToTarget(claims, target_type, target_id, require_modify = false) {
    const { uid, realm } = claims
    const access = await rds.fetch(
      `
          select allow_modify, allow_submit
          from pdc_user_authorization
          where uid = $1 and realm = $2 and target_type = $3 and target_id = $4
          and (expiration_date > now() OR expiration_date IS NULL)
        `,
      [uid, realm, target_type, target_id]
    )
    return access && (! require_modify || access.allow_modify)
  }

 async getReportsByQuarter(){
   return rds.query(`select 'L2' as report_type,year,quarter,count(1) as number_filed
                from (select
                         l2s.report_id,
                         l2s.submitted_at,
                         row_number() over (partition by l2s.report_id order by l2s.submitted_at) as row_num,
                         extract(year from l2s.submitted_at) as year,
                         'Q' || ceil(extract(month from l2s.submitted_at) / 3) as quarter
                     from l2 join l2_submission l2s on l2.report_id = l2s.report_id
                    ) v where v.row_num = 1
                group by year, quarter
                union
                select 'L3' as report_type,year,quarter,count(1) as number_filed
                from (select
                         l3s.report_id,
                         l3s.submitted_at,
                         row_number() over (partition by l3s.report_id order by l3s.submitted_at) as row_num,
                         extract(year from l3s.submitted_at) as year,
                         'Q' || ceil(extract(month from l3s.submitted_at) / 3) as quarter
                     from l3 join l3_submission l3s on l3.report_id = l3s.report_id
                    ) v where v.row_num = 1
                group by year, quarter
                union
                select 'Lobbyist Contract' as report_type,year,quarter,count(1) as number_filed
                from (select lcs.lobbyist_contract_id,
                               lcs.submitted_at,
                               row_number() over (partition by lcs.lobbyist_contract_id order by lcs.submitted_at) as row_num,
                               extract(year from lcs.submitted_at) as year,
                               'Q' || ceil(extract(month from lcs.submitted_at) / 3) as quarter
                        from lobbyist_contract lc join lobbyist_contract_submission lcs on lc.lobbyist_contract_id = lcs.lobbyist_contract_id
                     ) v where v.row_num = 1
                group by year, quarter
                order by year, quarter`);
  }
}

const lobbyistProcessor = new LobbyistProcessor()
export {lobbyistProcessor}