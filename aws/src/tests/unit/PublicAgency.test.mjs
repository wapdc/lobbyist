import {describe, expect, jest, test} from "@jest/globals"
//Array of other users authorized on the agency to be emailed
const expected_user_emails = {
    emails: ['michael@testpdc.com', 'james@testpdc.com']
}
jest.unstable_mockModule('@wapdc/common/mailer', () => ({
    sendMessage: jest.fn()
}))
jest.unstable_mockModule('@wapdc/common/wapdc-db', () => ({
    fetch :  jest.fn().mockResolvedValue(expected_user_emails)
}))
const mailer = await import('@wapdc/common/mailer')
const rds = await import('@wapdc/common/wapdc-db')

const {publicAgencyProcessor} = await import('../../PublicAgencyProcessor.js')

describe('PublicAgency', () => {

    test('Send email to user signifying successful verification', async () => {

        //The uid and realm of the admin user
        const user = {
            uid: 'test',
            realm: 'test'
        }

        const verification_payload = {
            agency_id: null,
            filer_request_id: -1,
            email: 'test@test.com',
            payload: {
                name: 'Sample Agency',
                address: '123 4th st',
                city: 'olympia',
                state: 'WA',
                zip: '98513',
                county: 'thurston',
                contact: 'tin',
                phone: '1231231234',
                email: 'test@test.com',
                head: 'peter'
            }
        }



        await publicAgencyProcessor.verifyAgencyAccess(user.uid, user.realm, verification_payload)

        expect(rds.fetch).toHaveBeenCalled()

        //Expectation that the mailer is called with the user being granted access
        expect(mailer.sendMessage).toHaveBeenCalledWith(verification_payload.email, 'd-7da84081108649809a8de7ba3389c31f', {agency: verification_payload.payload.name})

        //Expectation that the mailer is called with the other users on file passed back from the other users authorized on agency
        expect(mailer.sendMessage).toHaveBeenCalledWith(expected_user_emails.emails, 'd-9cb0dc11bd9b42abb80bfa4d0e58889b', {agency: verification_payload.payload.name, user: verification_payload.email})
    })
})