import * as rds from "@wapdc/common/wapdc-db"
import env from 'dotenv'
import { jest, beforeEach, afterEach, expect, test, describe } from '@jest/globals'
import { readFile } from 'fs/promises'
import { l7Processor } from '../../L7Processor.js'

env.config()
jest.setTimeout(70000)

beforeAll(() => {
  return rds.connect()
})

afterAll(() => {
  return rds.close()
})

beforeEach(async () => {
  await rds.beginTransaction()
})

afterEach(async () => {
  await rds.rollback()
})

describe('Test L7', () => {
  test('Test L7 validation', async () => {
    const fixed_l7_report = await readFile(`src/tests/data/l7/test-l7.json`, 'ascii')
    let l7_validations = await readFile(`src/tests/data/l7/test-l7-validations.json`, 'ascii')
    l7_validations = JSON.parse(l7_validations)

    for (const expected_validation of l7_validations) {
      let l7_report = JSON.parse(fixed_l7_report)
      eval(expected_validation.modification)
      const resulting_validation = await l7Processor.validateL7(l7_report)
      expect(Object.keys(resulting_validation[0])[0]).toBe(expected_validation.error)
    }
  })
  test('Test L7 submission', async () => {
    let l7_report = await readFile(`src/tests/data/l7/test-l7.json`, 'ascii')
    l7_report = JSON.parse(l7_report)

    const test_client = {
      name: 'Robert Paulson',
      user_name: 'robert@paulson.com',
      address_1: '420 Paper St',
      city: 'Wilmington',
      state: 'DE',
      postcode: '19886',
      country: 'US',
      phone: '123-456-7890'
    }

    await rds.query(
      `
        insert into lobbyist_client(lobbyist_client_id, name, email, address_1, city, state, postcode, country, phone)
        values ($1, $2, $3, $4, $5, $6, $7, $8, $9)
        returning lobbyist_client_id
      `,
      [
        -1,
        test_client.name,
        test_client.user_name,
        test_client.address_1,
        test_client.city,
        test_client.state,
        test_client.postcode,
        test_client.country,
        test_client.phone
      ]
    )

    await rds.query(
      `
        insert into pdc_user(uid, realm, user_name)
        values ($1, $2, $3)
      `,
      [
        '1234',
        'test',
        'robert@paulson.com'
      ]
    )

    const target_type = 'lobbyist_client'
    const target_id = -1
    const uid = 1234
    const realm = 'test'

    await l7Processor.submitL7(l7_report, target_type, target_id, uid, realm)

    const saved_l7 = await rds.fetch(`select * from l7 where target_id = $1`, [target_id])
    const saved_l7_submission = await rds.fetch(`select * from l7_submission where report_id = $1`, [saved_l7.report_id])

    expect(saved_l7).toBeTruthy()
    expect(saved_l7).toBeDefined()
    expect(saved_l7.report_id).toBeTruthy()
    expect(saved_l7.report_id).toBeDefined()
    expect(saved_l7.current_submission_id).toBe(saved_l7_submission.submission_id)
    expect(saved_l7.target_id).toBe(target_id)
    expect(saved_l7.target_type).toBe(target_type)

    expect(saved_l7_submission).toBeTruthy()
    expect(saved_l7_submission).toBeDefined()
    expect(saved_l7_submission.submission_id).toBeTruthy()
    expect(saved_l7_submission.submission_id).toBeDefined()
    expect(saved_l7_submission.superseded_id).toBeNull()
    expect(saved_l7_submission.report_id).toBe(saved_l7.report_id)
    expect(saved_l7_submission.version).toBe(l7_report.version)
    expect(saved_l7_submission.submitted_at).toBeTruthy()
    expect(saved_l7_submission.submitted_at).toBeDefined()
    expect(saved_l7_submission.user_data).toBeTruthy()
    expect(saved_l7_submission.user_data).toBeDefined()

    expect(saved_l7_submission.user_data.report_type).toBe(l7_report.report_type)
    expect(saved_l7_submission.user_data.version).toBe(l7_report.version)
    expect(saved_l7_submission.user_data.lobbyist.name).toBe(l7_report.lobbyist.name)
    expect(saved_l7_submission.user_data.lobbyist.target_type).toBe(l7_report.lobbyist.target_type)
    expect(saved_l7_submission.user_data.lobbyist.target_id).toBe(l7_report.lobbyist.target_id)
    expect(saved_l7_submission.user_data.lobbyist.address).toBe(l7_report.lobbyist.address)
    expect(saved_l7_submission.user_data.lobbyist.city).toBe(l7_report.lobbyist.city)
    expect(saved_l7_submission.user_data.lobbyist.state).toBe(l7_report.lobbyist.state)
    expect(saved_l7_submission.user_data.lobbyist.postcode).toBe(l7_report.lobbyist.postcode)
    expect(saved_l7_submission.user_data.employee_name).toBe(l7_report.employee_name)
    expect(saved_l7_submission.user_data.lobbyist_employment.compensation).toBe(l7_report.lobbyist_employment.compensation)
    expect(saved_l7_submission.user_data.lobbyist_employment.description).toBe(l7_report.lobbyist_employment.description)
    expect(saved_l7_submission.user_data.public_employment.description).toBe(l7_report.public_employment.description)
    expect(saved_l7_submission.user_data.certification.name).toBe(l7_report.certification.name)
    expect(saved_l7_submission.user_data.certification.statement).toBe(l7_report.certification.statement)
    expect(saved_l7_submission.user_data.certification.text).toBe(l7_report.certification.text)
    expect(saved_l7_submission.user_data.certification.user_name).toBe(test_client.user_name)
  })
  test('Test L7 amendment', async () => {
    let l7_report = await readFile(`src/tests/data/l7/test-l7.json`, 'ascii')
    l7_report = JSON.parse(l7_report)

    const test_client = {
      name: 'Robert Paulson',
      user_name: 'robert@paulson.com',
      address_1: '420 Paper St',
      city: 'Wilmington',
      state: 'DE',
      postcode: '19886',
      country: 'US',
      phone: '123-456-7890'
    }

    await rds.query(
        `
        insert into lobbyist_client(lobbyist_client_id, name, email, address_1, city, state, postcode, country, phone)
        values ($1, $2, $3, $4, $5, $6, $7, $8, $9)
        returning lobbyist_client_id
      `,
        [
          -1,
          test_client.name,
          test_client.user_name,
          test_client.address_1,
          test_client.city,
          test_client.state,
          test_client.postcode,
          test_client.country,
          test_client.phone
        ]
    )

    await rds.query(
        `
        insert into pdc_user(uid, realm, user_name)
        values ($1, $2, $3)
      `,
        [
          '1234',
          'test',
          'robert@paulson.com'
        ]
    )

    const target_type = 'lobbyist_client'
    const target_id = -1
    const uid = 1234
    const realm = 'test'

    // create an original report to amend
    const report_id = await l7Processor.submitL7(l7_report, target_type, target_id, uid, realm)

    const original_report = await rds.fetch("select * from l7 where report_id = $1",[report_id])

    // create new l7 amendment
    await l7Processor.submitL7(l7_report, target_type, target_id, uid, realm, report_id)
    const modified_report = await rds.fetch("select * from l7 where report_id = $1", [report_id]);

    expect(modified_report.current_submission_id).not.toBe(original_report.current_submission_id)

    // test l7_amendment.superseded id against the original submission_id
    const modified_original_submission = await rds.fetch(`select * from l7_submission where submission_id = $1`, [original_report.current_submission_id])

    expect(modified_original_submission.superseded_id).toBe(modified_report.current_submission_id)
  })
})