import * as rds from "@wapdc/common/wapdc-db"
import env from 'dotenv'
import {jest, beforeAll, afterAll, expect, test, describe} from '@jest/globals'
import {l2Processor} from '../../L2Processor.js'
import * as registrations from '../data/l1/registrations.js'
import * as l2Reports from '../data/l2/L2Reports.js'
import * as legacyReports from '../data/l2/legacyL2Report.js'
import {readFile} from "fs/promises";
import validateL2Draft from '../../L2Validator.js'
import {createTestData} from "../data/TestData.js";

env.config()
jest.setTimeout(70000)

beforeAll(async() => {
    await rds.connect()
    await rds.beginTransaction()

})
afterAll(async () => {
    await rds.rollback()
    await rds.close()
})

describe('Can create draft',  () => {
    // Create a firm and set its id.

    const firm = registrations.firm


    // Async calls must be in a before all because async describe blocks are not supported.
    beforeAll(async () => {
        await createTestData()

        // Isert a legacy L2 report and submission into the l2 and l2_submission tables
        l2Processor.saveReport(firm.lobbyist_firm_id, '2024-01-01', legacyReports.fullL2, '1.0')

        // Insert a new l2 report and submission into the L2 and l2_submission tables.
        l2Processor.saveReport(firm.lobbyist_firm_id, '2024-03-01', l2Reports.fullL2)
    })


    test('Create a draft without copying data', async () => {

        // Call Create draft with the firmID and no copy data.
        const draft_id = await l2Processor.makeDraft(firm.lobbyist_firm_id, '2024-02-01')

        // Assert that the draft was created
        const draft_l2 = await rds.fetch(`SELECT *
                                          FROM private.draft_document
                                          WHERE draft_id = $1`, [draft_id])
        expect(draft_l2).toBeDefined()

        // Verify the fields important fields in the draft table (report_type, target_type, report_key)
        expect(draft_l2.draft_id).toBeTruthy()
        expect(draft_l2.target_id).toBe(firm.lobbyist_firm_id)
        expect(draft_l2.target_type).toBe('lobbyist_firm')
        expect(draft_l2.user_data).toBeTruthy()
        expect(draft_l2.user_data).toBeDefined()

        // Verify that the firm data is in the user_data of the object
        expect(draft_l2.user_data.firm.address.street).toBe(firm.address.street)
        expect(draft_l2.user_data.firm.address.city).toBe(firm.address.city)
        expect(draft_l2.user_data.firm.address.state).toBe(firm.address.state)
        expect(draft_l2.user_data.firm.address.postcode).toBe(firm.address.postcode)

        const draftExpected = structuredClone(l2Reports.emptyL2);

        draftExpected.firm = draft_l2.user_data.firm;

        // Verify that the expected properties on a draft are set. (e.g. compensation, has_compensation)
        expect(draft_l2.user_data).toEqual(draftExpected)

    })

    const copyDraftScenarios = [
        [{personal_expenses: false, compensation: false, sub_lobbyist_payments: false, contributing_pacs: false}],
        [{personal_expenses: true, compensation: false, sub_lobbyist_payments: false, contributing_pacs: false}],
        [{personal_expenses: false, compensation: true, sub_lobbyist_payments: false, contributing_pacs: false}],
        [{personal_expenses: false, compensation: false, sub_lobbyist_payments: true, contributing_pacs: false}],
        [{personal_expenses: false, compensation: false, sub_lobbyist_payments: false, contributing_pacs: true}]
    ]

    test.each(copyDraftScenarios)('Create draft copying data from prior L2', async (copyOptions) => {
        // Make sure we have no drafts left over from prior tests
        await rds.execute(`delete
                           from private.draft_document
                           where target_type = 'lobbyist_firm'
                             and target_id = 1`)

        // Call createDraft, specifying the period_start to get data from, and which data to copy.
        const draft_id = await l2Processor.makeDraft(
          firm.lobbyist_firm_id,
          '2024-04-01',
          '2024-03-01',
          copyOptions
        )

        // Assert that the draft was created
        const draft_l2 = await rds.fetch(`SELECT *
                                          FROM private.draft_document
                                          WHERE draft_id = $1`, [draft_id])


        const expectedDraft = structuredClone(l2Reports.emptyL2)
        expectedDraft.firm = draft_l2.user_data.firm;
        for (const [property, value] of Object.entries(copyOptions)) {
            if (value) {
                expectedDraft[property] = l2Reports.fullL2[property]
                expectedDraft['has_' + property] = l2Reports.fullL2['has_' + property]
            }
        }

        expect(draft_l2.user_data).toEqual(expectedDraft)

    })

    test.each(copyDraftScenarios)('Create draft copying data from legacy L2', async (copyOptions) => {
        // Make sure we have no drafts left over from prior tests
        await rds.execute(`delete
                           from private.draft_document
                           where target_type = 'lobbyist_firm'
                             and target_id = 1`)

        // Call createDraft, specifying the period_start to get data from, and which data to copy.
        const draft_id = await l2Processor.makeDraft(
          firm.lobbyist_firm_id,
          '2024-04-01',
          '2024-01-01',
          copyOptions
        )

        // Assert that the draft was created
        const draft_l2 = await rds.fetch(`SELECT *
                                          FROM private.draft_document
                                          WHERE draft_id = $1`, [draft_id])

        const expectedDraft = structuredClone(l2Reports.emptyL2)
        expectedDraft.firm = draft_l2.user_data.firm;
        for (const [property, value] of Object.entries(copyOptions)) {
            if (value) {
                expectedDraft[property] = l2Reports.processedLegacyL2[property]
                expectedDraft['has_' + property] = l2Reports.processedLegacyL2['has_' + property]
            }
        }

        expect(draft_l2.user_data).toEqual(expectedDraft)
    })

    test('Submit L2 Report', async () => {
        const user_data = l2Reports.fullL2;
        const user_info = {
            name: 'test',
            user_name:'username'
        }
        // Call the actual submitL2 function
        const { report_id, submission_id } = await l2Processor.submitL2Report(firm.lobbyist_firm_id, '2024-02-01', user_data, user_info);

        // Fetch and verify l2_submission data
        const l2submission = await rds.fetch(`SELECT * FROM l2_submission WHERE submission_id = $1`, [submission_id]);
        expect(l2submission).toBeDefined();
        expect(l2submission.report_id).toBe(report_id)
        expect(l2submission.version).toBe("2.0");
        expect(new Date().getTime()).toBeGreaterThan(new Date(l2submission.submitted_at).getTime());
        expect(l2submission.user_data).toBeTruthy()
        expect(l2submission.user_data).toBeDefined()
        expect(l2submission.user_data).toEqual(user_data);
        expect(l2submission.user_data.certification.name).toEqual(user_info.name);
        expect(l2submission.user_data.certification.user_name).toEqual(user_info.user_name);

        // Fetch and verify l2 data
        const l2 = await rds.fetch(`SELECT * FROM l2 WHERE report_id = $1`, [report_id]);
        expect(l2).toBeDefined();
        expect(l2.report_id).toBeDefined()
        expect(l2.lobbyist_firm_id).toBe(firm.lobbyist_firm_id);
        expect(l2.current_submission_id).toBe(submission_id);
        expect(l2.drupal_nid).toBeNull();

        // get the transactions from the database
        const transactions = await rds.query(`SELECT t.*, 
           e.address, e.city, e.state, e.postcode, e.expense_type, e.is_ad, e.description, e.vendor, e.date, 
           et.occasion_type, et.participants, et.place,
           c.recipient_id, c.recipient_target,
           ie.stance, ie.candidacy_id, ie.ballot_number
        FROM l2_transaction t
           left join l2_expense e on e.l2_transaction_id=t.l2_transaction_id
           left join l2_entertainment et on et.l2_transaction_id=t.l2_transaction_id
           left join l2_contribution c on c.l2_transaction_id=t.l2_transaction_id
           left join l2_independent_expenditure ie on ie.l2_transaction_id=t.l2_transaction_id
         WHERE report_id = $1 order by l2_transaction_id`, [l2.report_id]);

        // Make sure we found the compensation trasaction
        const compensation = transactions.find((t) => t.transaction_type === 'compensation')
        const expectedCompensation = l2Reports.fullL2.compensation[0]
        expect(compensation).toBeDefined()
        expect(parseFloat(compensation.amount)).toBe(expectedCompensation.amount)
        expect(compensation.source_target_id).toBe(expectedCompensation.source)
        expect(compensation.source_target_type).toBe('lobbyist_contract')
        expect(compensation.source_name).toBe('Source 2')

        // Make sure sublobbyist payments are recorded properly
        {
            const expectedExpense = l2Reports.fullL2.sub_lobbyist_payments[0]
            const expense = transactions.find((t) => t.transaction_type === 'sub_lobbyist_payment')
            expect(expense).toBeDefined()
            expect(parseFloat(expense.amount))
            expect(expense.source_target_id).toBe(expectedExpense.source)
            expect(expense.source_target_type).toBe('lobbyist_contract')
            expect(expense.source_name).toBe('Test Client/Employer (Test Subcontracted Firm)')
        }

        // Make sure the personal expenses are recorded correctly
        {
            const expectedExpense = l2Reports.fullL2.personal_expenses[0]
            const expense = (transactions.filter((t) => t.transaction_type === 'personal_expense'))[0]
            expect(expense).toBeDefined()
            expect(parseFloat(expense.amount))
            expect(expense.source_target_id).toBe(expectedExpense.source)
            expect(expense.source_target_type).toBe('lobbyist_contract')
            expect(expense.source_name).toBe('Source 2')
        }
        {
            const expectedExpense = l2Reports.fullL2.personal_expenses[1]
            const expense = (transactions.filter((t) => t.transaction_type === 'personal_expense'))[1]
            expect(expense).toBeDefined()
            expect(parseFloat(expense.amount))
            expect(expense.source_target_id).toBe(expectedExpense.source)
            expect(expense.source_target_type).toBe('lobbyist_firm')
            expect(expense.source_name).toBe('Source 1')
        }

        // Entertainment expenses non-itemized
        {
            const expectedExpense = l2Reports.fullL2.entertainment_small[0]
            const expense = (transactions.find((t) => t.transaction_type === 'non_itemized_entertainment_expenses'))
            expect(expense).toBeDefined()
            expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
            expect(expense.source_target_type).toBe('lobbyist_contract')
            expect(expense.source_target_id).toBe(expectedExpense.source)
            expect(expense.source_name).toBe("Source 2")
        }

        // Entertainment expenses itemized
        {
            const expectedExpense = l2Reports.fullL2.entertainment[0]
            const expense = (transactions.find((t) => t.transaction_type === 'itemized_entertainment_expense'))
            expect(expense).toBeDefined()
            expect(expense.source_target_type).toBe('lobbyist_contract')
            expect(expense.source_target_id).toBe(expectedExpense.source)
            expect(expense.source_name).toBe("Source 2")
            expect(parseFloat(expense.amount)).toBe(expectedExpense.cost)
            expect(expense.date).toBe(expectedExpense.occasion_date)
            expect(expense.description).toBe(expectedExpense.occasion_description)
            expect(expense.place).toBe(expectedExpense.place)
            expect(expense.occasion_type).toBe(expectedExpense.type_of_occasion)
            expect(expense.participants).toBe(expectedExpense.participant_listing)
        }

        // Indirect/grassroots lobbying expenses
        {
            const expectedExpense = l2Reports.fullL2.expenses_indirect[0]
            const expense = (transactions.find((t) => t.transaction_type === 'indirect_expense'))
            expect(expense).toBeDefined()
            expect(expense.source_target_type).toBe('lobbyist_contract')
            expect(expense.source_target_id).toBe(expectedExpense.source)
            expect(expense.source_name).toBe("Source 2")
            expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
            expect(expense.description).toBe(expectedExpense.description)
            expect(expense.address).toBe(expectedExpense.address)
            expect(expense.city).toBe(expectedExpense.city)
            expect(expense.state).toBe(expectedExpense.state)
            expect(expense.postcode).toBe(expectedExpense.postcode)
            expect(expense.expense_type).toBe(expectedExpense.type)
            expect(expense.vendor).toBe(expectedExpense.recipient)
        }

      // Direct Consulting and Expert witness details
      {
        const expectedExpense = l2Reports.fullL2.consulting[0]
        const expense = (transactions.find((t) => t.transaction_type === 'consulting_expense'))
        expect(expense).toBeDefined()
        expect(expense.source_target_type).toBe('lobbyist_contract')
        expect(expense.source_target_id).toBe(expectedExpense.source)
        expect(expense.source_name).toBe("Source 2")
        expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
        expect(expense.address).toBe(expectedExpense.address)
        expect(expense.city).toBe(expectedExpense.city)
        expect(expense.state).toBe(expectedExpense.state)
        expect(expense.postcode).toBe(expectedExpense.postcode)
        expect(expense.date).toBe(expectedExpense.date)
        expect(expense.vendor).toBe(expectedExpense.name)
      }

      //  Direct printing and literature details
      {
        const expectedExpense = l2Reports.fullL2.printing[0]
        const expense = (transactions.find((t) => t.transaction_type === 'printing_expense'))
        expect(expense).toBeDefined()
        expect(expense.source_target_type).toBe('lobbyist_firm')
        expect(expense.source_target_id).toBe(expectedExpense.source)
        expect(expense.source_name).toBe("Source 1")
        expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
        expect(expense.description).toBe(expectedExpense.description)
      }

      // Direct Public Relations and polling details
      {
        const expectedExpense = l2Reports.fullL2.public_relations[0]
        const expense = (transactions.find((t) => t.transaction_type === 'public_relations_expense'))
        expect(expense).toBeDefined()
        expect(expense.source_target_type).toBe('lobbyist_firm')
        expect(expense.source_target_id).toBe(expectedExpense.source)
        expect(expense.source_name).toBe("Source 1")
        expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
        expect(expense.description).toBe(expectedExpense.description)
        expect(expense.date).toBe(expectedExpense.date)
        expect(expense.vendor).toBe(expectedExpense.vendor)
      }

      // Direct Other Expenses details
      {
        const expectedExpense = l2Reports.fullL2.expenses_other[0]
        const expense = (transactions.find((t) => t.transaction_type === 'other_expense'))
        expect(expense).toBeDefined()
        expect(expense.source_target_type).toBe('lobbyist_contract')
        expect(expense.source_target_id).toBe(expectedExpense.source)
        expect(expense.source_name).toBe("Source 2")
        expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
        expect(expense.description).toBe(expectedExpense.description)
        expect(expense.vendor).toBe(expectedExpense.vendor)
        expect(expense.date).toBe(expectedExpense.date)
      }

        // election expense monetary contribution
      {
        const expectedExpense = l2Reports.fullL2.contributions[0]
        const expense = (transactions.find((t) => t.transaction_type === 'itemized_contribution'))
        expect(expense).toBeDefined()
        expect(expense.source_target_type).toBe('lobbyist_contract')
        expect(expense.source_target_id).toBe(expectedExpense.source)
        expect(expense.source_name).toBe("Source 2")
        expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
        expect(expense.vendor).toBe(expectedExpense.recipient)
        expect(expense.date).toBe(expectedExpense.date)
        expect(expense.expense_type).toBe('monetary')
        expect(expense.recipient_target).toBe(expectedExpense.recipient_type)
        expect(expense.recipient_id).toBe(expectedExpense.recipient_id)
      }

      // election expense in kind contribution
      {
        const expectedExpense = l2Reports.fullL2.contributions_in_kind[0]
        const expense = (transactions.find((t) => t.transaction_type === 'in_kind_expense'))
        expect(expense).toBeDefined()
        expect(expense.source_target_type).toBe('lobbyist_contract')
        expect(expense.source_target_id).toBe(expectedExpense.source)
        expect(expense.source_name).toBe("Source 2")
        expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
        expect(expense.description).toBe(expectedExpense.description)
        expect(expense.vendor).toBe(expectedExpense.vendor)
        expect(expense.date).toBe(expectedExpense.date)
        expect(expense.recipient_target).toBe(expectedExpense.recipient_type)
        expect(expense.recipient_id).toBe(expectedExpense.recipient_id)
        expect(expense.is_ad).toBe(expectedExpense.is_advertisment)
      }

      // election expense small contribution
      {
        const expectedExpense = l2Reports.fullL2.contributions_small[0]
        const expense = (transactions.find((t) => t.transaction_type === 'contributions_small_expense'))
        expect(expense).toBeDefined()
        expect(expense.source_target_type).toBe('lobbyist_contract')
        expect(expense.source_target_id).toBe(expectedExpense.source)
        expect(expense.source_name).toBe("Source 2")
        expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
      }

      // election independent expenditure - candidate
      {
        const expected_expense = l2Reports.fullL2.independent_expenditures_candidate[0]
        const expense = (transactions.find((t) => t.transaction_type === 'independent_expenditures_candidate_expense'))
        expect(expense).toBeDefined()
        expect(expense.source_target_type).toBe('lobbyist_contract')
        expect(expense.source_target_id).toBe(expected_expense.source)
        expect(expense.source_name).toBe("Source 2")
        expect(parseFloat(expense.amount)).toBe(expected_expense.amount)
        expect(expense.description).toBe(expected_expense.description)
        expect(expense.recipient).toBe(expected_expense.vendor)
        expect(expense.date).toBe(expected_expense.date)
        expect(expense.stance).toBe(expected_expense.stance)
        expect(expense.candidacy_id).toBe(expected_expense.candidate_id)
      }

      // election independent expenditure - ballot
      {
        const expected_expense = l2Reports.fullL2.independent_expenditures_ballot[0]
        const expense = (transactions.find((t) => t.transaction_type === 'independent_expenditures_ballot_expense'))
        expect(expense).toBeDefined()
        expect(expense.source_target_type).toBe('lobbyist_contract')
        expect(expense.source_target_id).toBe(expected_expense.source)
        expect(expense.source_name).toBe("Source 2")
        expect(parseFloat(expense.amount)).toBe(expected_expense.amount)
        expect(expense.description).toBe(expected_expense.description)
        expect(expense.recipient).toBe(expected_expense.vendor)
        expect(expense.date).toBe(expected_expense.date)
        expect(expense.stance).toBe(expected_expense.stance)
        expect(expense.ballot_number).toBe(expected_expense.ballot_number)
      }

        // check draft got deleted
        const draft = await rds.fetch(`select * from private.draft_document where target_id = $1 and report_key = $2 and report_type='L2' and target_type = 'lobbyist_firm'`,[firm.lobbyist_firm_id, '2024-02-01']);
        expect(draft).toBeNull();
    });
    test('Create draft from existing L2 report', async () => {
        const l2 = await rds.fetch(`SELECT * FROM l2 WHERE lobbyist_firm_id = $1 and period_start = $2`, [firm.lobbyist_firm_id, '2024-03-01']);
        const draftExpected = await rds.fetch('select * from l2_submission where submission_id = $1', [l2.current_submission_id])

        const draftId = await l2Processor.createAmendmentDraft(firm.lobbyist_firm_id,l2.period_start)
        const l2draft = await rds.fetch(`select * from private.draft_document where draft_id = $1`, [draftId])
        draftExpected.user_data.firm = l2draft.user_data.firm
        draftExpected.user_data.sources = l2draft.user_data.sources;
        draftExpected.user_data.amends = draftExpected.submission_id

        expect(l2draft).toBeDefined()
        expect(l2draft.draft_id).toBeTruthy()
        expect(l2draft.target_id).toBe(firm.lobbyist_firm_id)
        expect(l2draft.target_type).toBe('lobbyist_firm')
        expect(l2draft.user_data).toBeTruthy()
        expect(l2draft.user_data).toBeDefined()
        // verify that the newly created draft's user data has the data from the old report.
        expect(l2draft.user_data).toEqual(draftExpected.user_data)
    });
    test('test validate l2 draft', async () => {
        const fixed_l2_report = await readFile(`src/tests/data/l2/test-l2.json`, 'ascii')
        let l2_validations = await readFile(`src/tests/data/l2/test-l2-validations.json`, 'ascii')
        l2_validations = JSON.parse(l2_validations)
        for (const expected_validation of l2_validations) {
            let l2 = JSON.parse(fixed_l2_report)
            eval(expected_validation.modification)
            const resulting_validation = validateL2Draft(l2)
            const foundError = resulting_validation.find(e => e.hasOwnProperty(expected_validation.error))
            expect(foundError[expected_validation.error]).toBeDefined()
        }
    });

})
