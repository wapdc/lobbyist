import env from "dotenv";
import {test, expect, afterAll, beforeAll, describe, jest} from "@jest/globals";
import * as rds from "@wapdc/common/wapdc-db";
import {createTestData} from "../data/TestData.js";
import { l1Processor, emptyL1 } from "../../L1Processor.js";
import {client, firm, subContractedFirm, agents} from "../data/l1/registrations.js";
import validateL1Draft from "../../L1Validator.js";
import * as l1Reports from "../data/l1/L1Reports.js";
import { validations } from '../data/l1/L1TestValidator.js'

env.config()
jest.setTimeout(70000)

beforeAll(async() => {
  await rds.connect()
  await rds.beginTransaction()
})

afterAll(async () => {
  await rds.rollback()
  await rds.close()
})

describe('L1 Lobbyist Contract reports',  () => {
  let draft_id
  beforeAll(async () => {
    await createTestData();
  })

  test('Create a employer draft without copying data', async () => {

    draft_id = await l1Processor.makeDraft(firm.lobbyist_firm_id, client.lobbyist_client_id, '2025-01-01', '2026-12-31', null)

    const draft_l1 = await rds.fetch(`select * from private.draft_document where draft_id=$1`, [draft_id])

    expect(draft_l1).toBeDefined()
    expect(draft_l1.draft_id).toBeTruthy()
    expect(draft_l1.target_id).toBe(firm.lobbyist_firm_id)
    expect(draft_l1.target_type).toBe('lobbyist_firm')
    expect(draft_l1.report_key).toBe(client.lobbyist_client_id.toString() + ':2025-01-01:2026-12-31')
    expect(draft_l1.period_start).toBe('2025-01-01')
    expect(draft_l1.period_end).toBe('2026-12-31')
    expect(draft_l1.user_data).toBeTruthy()
    expect(draft_l1.user_data).toBeDefined()

    // Check values
    const l1 = draft_l1.user_data
    expect(l1.period_start).toBe('2025-01-01')
    expect(l1.period_end).toBe('2026-12-31')
    expect(l1.firm).toBeDefined()
    expect(l1.firm.address.street).toBe(firm.address.street)
    expect(l1.firm.address.city).toBe(firm.address.city)
    expect(l1.firm.address.state).toBe(firm.address.state)
    expect(l1.firm.address.postcode).toBe(firm.address.postcode)

    expect(l1.client).toBeDefined()
    expect(l1.client.address.street).toBe(client.address.street)
    expect(l1.client.address.city).toBe(client.address.city)
    expect(l1.client.address.state).toBe(client.address.state)
    expect(l1.client.address.postcode).toBe(client.address.postcode)

    const draftExpected = structuredClone(emptyL1);

    draftExpected.firm = l1.firm;
    draftExpected.client = l1.client;
    draftExpected.agents = l1.agents;
    draftExpected.period_start = '2025-01-01'
    draftExpected.period_end = '2026-12-31'

    // Verify that the expected properties on a draft are set. (e.g. compensation)
    expect(l1).toEqual(draftExpected)
  })

  test('Create a subcontracting draft without copying data', async () => {

    const draft_id = await l1Processor.makeDraft(subContractedFirm.lobbyist_firm_id, client.lobbyist_client_id, '2025-01-01', '2026-12-31', firm.lobbyist_firm_id)

    const draft_l1 = await rds.fetch(`select * from private.draft_document where draft_id=$1`, [draft_id])
    expect(draft_l1).toBeDefined()
    expect(draft_l1.draft_id).toBeTruthy()
    expect(draft_l1.target_id).toBe(subContractedFirm.lobbyist_firm_id)
    expect(draft_l1.target_type).toBe('lobbyist_firm')
    expect(draft_l1.report_key).toBe(subContractedFirm.lobbyist_firm_id + '-' + client.lobbyist_client_id + '-' + firm.lobbyist_firm_id.toString() + ':2025-01-01:2026-12-31')
    expect(draft_l1.period_start).toBe('2025-01-01')
    expect(draft_l1.period_end).toBe('2026-12-31')
    expect(draft_l1.user_data).toBeTruthy()
    expect(draft_l1.user_data).toBeDefined()

    // Check values
    const l1 = draft_l1.user_data
    expect(l1.period_start).toBe('2025-01-01')
    expect(l1.period_end).toBe('2026-12-31')
    expect(l1.firm).toBeDefined()
    expect(l1.firm.address.street).toBe(subContractedFirm.address.street)
    expect(l1.firm.address.city).toBe(subContractedFirm.address.city)
    expect(l1.firm.address.state).toBe(subContractedFirm.address.state)
    expect(l1.firm.address.postcode).toBe(subContractedFirm.address.postcode)

    expect(l1.client).toBeDefined()
    expect(l1.client.address.street).toBe(client.address.street)
    expect(l1.client.address.city).toBe(client.address.city)
    expect(l1.client.address.state).toBe(client.address.state)
    expect(l1.client.address.postcode).toBe(client.address.postcode)

    expect(l1.contractor).toBeDefined()
    expect(l1.contractor.address.street).toBe(firm.address.street)
    expect(l1.contractor.address.city).toBe(firm.address.city)
    expect(l1.contractor.address.state).toBe(firm.address.state)
    expect(l1.contractor.address.postcode).toBe(firm.address.postcode)


    const draftExpected = structuredClone(emptyL1);

    draftExpected.firm = l1.firm;
    draftExpected.client = l1.client;
    draftExpected.contractor = l1.contractor;
    draftExpected.period_start = '2025-01-01'
    draftExpected.period_end = '2026-12-31'

    // Verify that the expected properties on a draft are set. (e.g. compensation)
    expect(l1).toEqual(draftExpected)
  })


  test('Do not allow overlapping drafts', async() => {
    let error= false
    try {
      await l1Processor.makeDraft(firm.lobbyist_firm_id, client.lobbyist_client_id, '2025-01-01', '2028-12-31',null)
      await l1Processor.makeDraft(subContractedFirm.lobbyist_firm_id, client.lobbyist_client_id, '2025-01-01', '2028-12-31',firm.lobbyist_firm_id)
     }
    catch(e) {
      if (e.message.includes('draft already exists')) {
        error=true
      }
    }
    expect(error).toBe(true)
  })

  test('test validate l1 draft', async () => {
    for (const validation of validations) {
      let l1 = structuredClone(l1Reports.fullL1)
      validation.modification(l1)
      //check the negative case for having an agent with a bad certification date
      agents[0].training_certified = '2022-01-01'
      const resulting_validations = validateL1Draft(l1, agents)
      expect(resulting_validations.length).toBe(2)
      expect(Object.keys(resulting_validations[0])[0]).toEqual(validation.error)
    }
  })

  test('Can submit a draft', async() => {
    const savedDraft = await l1Processor.getFirmL1Draft(firm.lobbyist_firm_id, draft_id)
    await l1Processor.saveL1Draft(firm.lobbyist_firm_id, draft_id, savedDraft.draft.save_count , l1Reports.fullL1)
    const certification = {
      signature: 'I Certify',
      text: 'I certify that stuff is true',
    }
    const claims = {
      name: 'Test User',
      user_name: 'test@noreply.com'
    }
    const expectedCertification = {
      signature: certification.signature,
      text: certification.text,
      name: claims.name,
      user_name: claims.user_name
    }

    await l1Processor.firmCertify(firm.lobbyist_firm_id, draft_id, certification, claims )
    const draft = await rds.fetch(`SELECT * FROM private.draft_document where draft_id = $1`, [draft_id])
    expect(draft.approval_target_type).toBe('lobbyist_client')
    expect(parseInt(draft.approval_target_id)).toBe(client.lobbyist_client_id)
    expect(draft.user_data.certification).toEqual(expectedCertification)
  })

  test('Editing contract removes certification draft', async () => {
    const savedDraft = await l1Processor.getClientL1Draft(client.lobbyist_client_id, draft_id)
    await l1Processor.saveClientL1Draft(client.lobbyist_client_id, draft_id, savedDraft.draft.save_count, savedDraft.draft.user_data)
    const draft = await rds.fetch(`SELECT * FROM private.draft_document where draft_id = $1`, [draft_id])
    expect(draft.user_data.certification?.signature).toBeFalsy()
    expect(draft.user_data.certification?.signature).toBeFalsy()
  })

  test('Client certify but firm not certified', async () => {
    const certification = {
      signature: 'I Certify',
      text: 'I certify that stuff is true',
    }
    const claims = {
      name: 'Test User',
      user_name: 'test@noreply.com'
    }
    const result = await l1Processor.clientCertify(client.lobbyist_client_id, draft_id, certification, claims, false, 'A message')
    expect(result.lobbyist_contract_id).toBeFalsy()
    expect(result.submission_id).toBeFalsy()
    const savedDraft = await rds.fetch(`Select * from private.draft_document where draft_id = $1`, [draft_id])
    expect(savedDraft.user_data.revision_message.message).toBe('A message')
  })

  test('Can approve submitted draft', async () =>{
    const approval = {
      signature: 'I Certify',
      text: 'I certify that stuff is true',
    }
    const claims = {
      name: 'Test User',
      user_name: 'test@noreply.com'
    }
    const draft = await rds.fetch(`SELECT * FROM private.draft_document where draft_id = $1`, [draft_id])

    const {lobbyist_contract_id, submission_id} = await l1Processor.firmCertify(firm.lobbyist_firm_id, draft_id, approval, claims)

    const expectedApproval = {
      signature: approval.signature,
      text: approval.text,
      name: claims.name,
      user_name: claims.user_name
    }
    const l1 = await rds.fetch(`SELECT * FROM lobbyist_contract lc where lobbyist_contract_id = $1 `, [lobbyist_contract_id])
    const l1s = await rds.fetch(`SELECT * FROM lobbyist_contract_submission where submission_id = $1`, [submission_id])

    // Checked saved report
    expect(l1.lobbyist_client_id).toBe(client.lobbyist_client_id)
    expect(l1.lobbyist_firm_id).toBe(firm.lobbyist_firm_id)
    expect(l1.contractor_id).toBeNull()
    expect(l1.current_submission_id).toBe(submission_id)
    expect(l1.drupal_nid).toBeNull()
    expect(l1.period_start).toBe('2025-01-01')
    expect(l1.period_end).toBe('2026-12-31')

    // CHeck saved submission
    expect(l1s.lobbyist_contract_id).toBe(lobbyist_contract_id)
    expect(l1s.superseded_id).toBeNull()
    expect(l1s.version).toBe('2.0')
    expect(l1s.submitted_at).toBeTruthy()
    const expectedL1 = structuredClone(draft.user_data)
    delete expectedL1.revision_message

    expectedL1.certification = expectedApproval
    expect(l1s.user_data).toEqual(expectedL1)


    // Get all the reporting periods for the contract so we can make sure they exists.
    const rps = await rds.query(`select * from lobbyist_reporting_periods where lobbyist_contract_id = $1 order by period_start`, [lobbyist_contract_id])
    expect(rps).toBeDefined()
    expect(rps.length).toBe(expectedL1.period_year_months.length)
    for (let i=0; i<rps.length; i++) {
      expect(rps[i].period_start).toBe(expectedL1.period_year_months[i].year_month + '-01')
      expect(rps[i].period_end).toContain(expectedL1.period_year_months[i].year_month)
      expect(rps[i].lobbyist_client_id).toBe(client.lobbyist_client_id)
      expect(rps[i].lobbyist_firm_id).toBe(firm.lobbyist_firm_id)
      expect(rps[i].exempt).toBe(expectedL1.period_year_months[i].exempt)
    }

    // Verify that the draft was deleted.
    const remainingDraft  = await rds.fetch(`select * from private.draft_document where draft_id=$1`, [draft_id])
    expect(remainingDraft).toBeNull()

  })



  test('Create draft amendment', async () => {
    const l1 = await rds.fetch(`SELECT * FROM lobbyist_contract WHERE lobbyist_firm_id = $1 and period_end = $2`, [firm.lobbyist_firm_id, '2024-12-31']);
    const draftExpected = await rds.fetch('select * from lobbyist_contract_submission where submission_id = $1', [l1.current_submission_id])

    const draftId = await l1Processor.createAmendmentDraft(firm.lobbyist_firm_id, l1.current_submission_id)
    const l1draft = await rds.fetch(`select * from private.draft_document where draft_id = $1`, [draftId])
    draftExpected.user_data.firm = l1draft.user_data.firm
    draftExpected.user_data.amends = draftExpected.submission_id

    expect(l1draft).toBeDefined()
    expect(l1draft.draft_id).toBeTruthy()
    expect(l1draft.target_id).toBe(firm.lobbyist_firm_id)
    expect(l1draft.target_type).toBe('lobbyist_firm')
    expect(l1draft.user_data).toBeTruthy()
    expect(l1draft.user_data).toBeDefined()
    // verify that the newly created draft's user data has the data from the old report.
    expect(l1draft.user_data).toEqual(draftExpected.user_data)
  });

  test('Create draft amendment from legacy report', async () => {
    const l1 = await rds.fetch(`SELECT * FROM lobbyist_contract WHERE lobbyist_firm_id = $1 and period_end = $2`, [firm.lobbyist_firm_id, '2022-12-31']);
    const draftId = await l1Processor.createAmendmentDraft(firm.lobbyist_firm_id, l1.current_submission_id)
    const l1draft = await rds.fetch(`select * from private.draft_document where draft_id = $1`, [draftId])

    expect(l1draft).toBeDefined()
    expect(l1draft.draft_id).toBeTruthy()
    expect(l1draft.target_id).toBe(firm.lobbyist_firm_id)
    expect(l1draft.target_type).toBe('lobbyist_firm')
    expect(l1draft.user_data).toBeTruthy()
    expect(l1draft.user_data).toBeDefined()
    // verify that the newly created draft's user data has the data from the old report.
    expect(l1draft.user_data).toEqual(l1Reports.processedLegacyL1)
  });
})

