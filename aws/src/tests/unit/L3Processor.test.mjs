import env from "dotenv";
import {afterAll, beforeAll, describe, expect, jest, test} from "@jest/globals";
import * as rds from "@wapdc/common/wapdc-db";
import * as registrations from '../data/l1/registrations.js'
import {createTestData} from "../data/TestData.js";
import {l3Processor} from "../../L3Processor.js";
import * as l3Reports from "../data/l3/L3Reports.js";
import { validations } from '../data/l3/L3TestValidator.js'
import validateL3Draft from '../../L3Validator.js'
import {readFile} from "fs/promises";
import {l2Processor} from "../../L2Processor.js";

env.config()
jest.setTimeout(70000)

beforeAll(async() => {
  await rds.connect()
  await rds.beginTransaction()


})
afterAll(async () => {
  await rds.rollback()
  await rds.close()
})

describe('Can create draft', () => {
  const client = registrations.client

  beforeAll(async () => {
    await createTestData()
    const json  = await readFile(`src/tests/data/sync/l3.json`, 'ascii')
    let legacy_l3_report = JSON.parse(json)
    await l3Processor.saveReport(2, 2024, legacy_l3_report, '2.0')
  })

  test('Create an L3 draft', async() => {
    const copyOptions = {
      has_ballot_proposition_expenditures: true, has_candidate_independent_expenditures: true, has_pac_contributions: true
    }
    const draftId = await l3Processor.makeDraft(client.lobbyist_client_id, 2024);

    const draftL3 = await rds.fetch(`SELECT * FROM private.draft_document where draft_id = $1`, [draftId])

    const expectedDraft = structuredClone(l3Reports.emptyL3)
    expectedDraft.firm = draftL3.user_data.firm;
    for (const [property, value] of Object.entries(copyOptions)) {
      if (value) {
        expectedDraft[property] = l3Reports.fullL3[property]
        expectedDraft['has_' + property] = l3Reports.fullL3['has_' + property]
      }
    }
    expect(draftL3).toBeDefined()

    // Verify that the import fields in the draft table are there.
    expect(draftL3.report_type).toBe('L3')
    expect(draftL3.target_id).toBe(client.lobbyist_client_id)
    expect(draftL3.target_type).toBe('lobbyist_client')
    expect(draftL3.user_data).toBeDefined()

    // Verify data on the firm
    const l3 = draftL3.user_data
    expect(l3.client.address.street).toBe(client.address.street)
    expect(l3.client.address.city).toBe(client.address.city)
    expect(l3.client.address.state).toBe(client.address.state)
    expect(l3.client.address.postcode).toBe(client.address.postcode)

    const draftExpected = structuredClone(l3Reports.emptyL3);
    draftExpected.client = l3.client
    draftExpected.l2_summary = l3.l2_summary
    expect(draftL3.user_data).toEqual(draftExpected)
  })

  test('Submit L3 Report', async () => {
    const user_data = l3Reports.fullL3;
    const user_info = {
      name: 'test',
      user_name:'username'
    }

    // Call the actual submitL3 function
    const {  report_id,  submission_id } = await l3Processor.submitL3Report(client.lobbyist_client_id, 2024, user_data, user_info);

    // Fetch and verify l3_submission data
    const l3Submission = await rds.fetch(`SELECT * FROM l3_submission WHERE submission_id = $1`, [submission_id]);
    expect(l3Submission).toBeDefined();
    expect(l3Submission.report_id).toBe(report_id)
    expect(l3Submission.version).toBe("2.0");
    expect(new Date().getTime()).toBeGreaterThan(new Date(l3Submission.submitted_at).getTime());
    expect(l3Submission.user_data).toBeTruthy()
    expect(l3Submission.user_data).toBeDefined()
    expect(l3Submission.user_data).toEqual(user_data);
    expect(l3Submission.user_data.certification.name).toEqual(user_info.name);
    expect(l3Submission.user_data.certification.user_name).toEqual(user_info.user_name);

    // Fetch and verify l3 data
    const l3 = await rds.fetch(`SELECT * FROM l3 WHERE report_id = $1`, [report_id]);
    expect(l3).toBeDefined();
    expect(l3.report_id).toBeDefined()
    expect(l3.lobbyist_client_id).toBe(client.lobbyist_client_id);
    expect(l3.report_year).toBe(2024)
    expect(l3.current_submission_id).toBe(submission_id);
    expect(l3.drupal_nid).toBeNull();

    // get the transactions from the database
    const transactions = await rds.query(`SELECT t.*,
                                            ie.stance, ie.candidacy_id as ie_candidacy_id, ie.ballot_number,
                                            c.committee_id, c.candidacy_id as c_candidacy_id,
                                            l.lobbyist_contract_id
                                          FROM l3_transaction t
                                            left join l3_independent_expenditure ie on ie.l3_transaction_id = t.l3_transaction_id
                                            left join l3_contribution c on c.l3_transaction_id = t.l3_transaction_id
                                            left join l3_lobbyist l on l.l3_transaction_id = t.l3_transaction_id
                                          WHERE report_id = $1 order by l3_transaction_id`, [l3.report_id]);

    // get the compensations from the database
    const compensations = await rds.query(`SELECT *
                                          FROM l3_compensation
                                          WHERE report_id = $1`, [l3.report_id]);

    function checkTotal(property) {
      const expense = transactions.find((t) => t.transaction_type === property)
      const expectedExpense = l3Reports.fullL3[property]

      expect(expense).toBeDefined()
      expect(parseFloat(expense.amount)).toBe(expectedExpense)
      expect(expense.date).toBeNull()
      expect(expense.description).toBeNull()
    }
    // Make sure we found the payment to registered lobbyist transaction
    checkTotal('payments_for_registered_lobbyists')
    checkTotal('witnesses_retained_for_lobbying_services')
    checkTotal('costs_for_promotional_materials')
    checkTotal('grassroots_expenses_for_lobbying_communications')
    checkTotal('non_itemized_political_contribution_amount')

    // Make sure we find the itemized entertainment expenditures
    {
      const expectedExpense = l3Reports.fullL3.entertainment_expenditures[0]
      const expense = transactions.find(t => t.transaction_type === 'entertainment_expense')
      let expectedRelationship = null
      expect(expense).toBeDefined()
      expect(expense.name).toBe(expectedExpense.family_member_name)
      expect(expense.official_name).toBe(expectedExpense.name)
      if (expense.is_family === true && expense.official_type === 'State Official') {
        expectedRelationship = 'State Officials family member'
      }
      expect(expectedRelationship).toBe(expectedExpense.relationship)
      expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
      expect(expense.date).toBe(expectedExpense.date)
      expect(expense.description).toBe(expectedExpense.description)
    }

    // Make sure we find the itemized expenditures
    {
      const expectedExpense = l3Reports.fullL3.itemized_expenditures[0]
      const expense = transactions.find(t => t.transaction_type === 'itemized_expense')
      let expectedItemizedRelationship = null
      expect(expense).toBeDefined()
      expect(expense.name).toBe(expectedExpense.family_member_name)
      expect(expense.official_name).toBe(expectedExpense.name)
      if (expense.is_family === true && expense.official_type === 'State Official') {
        expectedItemizedRelationship = 'State Officials family member'
      }
      expect(expectedItemizedRelationship).toBe(expectedExpense.relationship)
      expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
      expect(expense.date).toBe(expectedExpense.date)
      expect(expense.description).toBe(expectedExpense.description)
    }

    // Make sure we find the other expenditures
    {
      const expectedExpense = l3Reports.fullL3.other_expenditures[0]
      const expense = transactions.find(t => t.transaction_type === 'other_expense')
      expect(expense).toBeDefined()
      expect(expense.name).toBe(expectedExpense.recipient)
      expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
      expect(expense.date).toBe(expectedExpense.date)
      expect(expense.description).toBe(expectedExpense.description)
    }

    // Make sure we find the itemized contributions
    {
      const expectedExpense = l3Reports.fullL3.itemized_contributions[0]
      const expense = transactions.find(t => t.transaction_type === 'itemized_contribution')
      expect(expense).toBeDefined()
      expect(expense.date).toBe(expectedExpense.date)
      expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
      expect(expense.c_candidacy_id).toBe(expectedExpense.recipient_id)
    }

    // IE's for candidates
    {
      const expectedExpense = l3Reports.fullL3.candidate_independent_expenditures[0]
      const expense = (transactions.find((t) => t.transaction_type === 'ie_candidate'))
      expect(expense).toBeDefined()
      expect(expense.ie_candidacy_id).toBe(expectedExpense.candidacy_id)
      expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
      expect(expense.date).toBe(expectedExpense.date)
      expect(expense.description).toBe(expectedExpense.description)
      expect(expense.name).toBe(expectedExpense.name)
      expect(expense.stance).toBe(expectedExpense.stance)
    }

    // IE's for ballot proposition
    {
      const expectedExpense = l3Reports.fullL3.ballot_proposition_expenditures[0]
      const expense = (transactions.find((t) => t.transaction_type === 'ie_ballot_proposition'))
      expect(expense).toBeDefined()
      expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
      expect(expense.date).toBe(expectedExpense.date)
      expect(expense.description).toBe(expectedExpense.description)
      expect(expense.ballot_number).toBe(expectedExpense.ballot_number)
      expect(expense.stance).toBe(expectedExpense.stance)
    }

    // Make sure we find the in kind contributions
    {
      const expectedExpense = l3Reports.fullL3.in_kind_contributions[0]
      const expense = transactions.find(t => t.transaction_type === 'in_kind_contribution')
      expect(expense).toBeDefined()
      expect(expense.date).toBe(expectedExpense.date)
      expect(expense.description).toBe(expectedExpense.description)
      expect(parseFloat(expense.amount)).toBe(expectedExpense.amount)
      expect(expense.c_candidacy_id).toBe(expectedExpense.recipient_id)
    }

    // Lobbyist corrected compensation totals
    {
      const expected_transaction = l3Reports.fullL3.lobbyist[0]
      const transaction = transactions.find(t => t.transaction_type === 'lobbyist_compensation_total' && t.lobbyist_contract_id === 10)
      expect(transaction).toBeDefined()
      expect(parseFloat(transaction.amount)).toBe(expected_transaction.compensation)
      expect(transaction.lobbyist_title).toBe(expected_transaction.name)
      expect(transaction.lobbyist_contract_id).toBe(expected_transaction.lobbyist_contract_id)
    }

    // Lobbyist corrected expense totals
    {
      const expected_transaction = l3Reports.fullL3.lobbyist[0]
      const transaction = transactions.find(t => t.transaction_type === 'lobbyist_expense_total' && t.lobbyist_contract_id === 10)
      expect(transaction).toBeDefined()
      expect(parseFloat(transaction.amount)).toBe(expected_transaction.expenses)
      expect(transaction.lobbyist_title).toBe(expected_transaction.name)
      expect(transaction.lobbyist_contract_id).toBe(expected_transaction.lobbyist_contract_id)
    }

    // Lobbyist reported compensation totals
    {
      const expected_transaction = l3Reports.fullL3.lobbyist[0]
      const transaction = transactions.find(t => t.transaction_type === 'lobbyist_reported_compensation_total' && t.lobbyist_contract_id === 10)
      expect(transaction).toBeDefined()
      expect(parseFloat(transaction.amount)).toBe(expected_transaction.compensation_l2)
      expect(transaction.lobbyist_title).toBe(expected_transaction.name)
      expect(transaction.lobbyist_contract_id).toBe(expected_transaction.lobbyist_contract_id)
    }

    // Lobbyist reported expense totals
    {
      const expected_transaction = l3Reports.fullL3.lobbyist[0]
      const transaction = transactions.find(t => t.transaction_type === 'lobbyist_reported_expense_total' && t.lobbyist_contract_id === 10)
      expect(transaction).toBeDefined()
      expect(parseFloat(transaction.amount)).toBe(expected_transaction.expenses_l2)
      expect(transaction.lobbyist_title).toBe(expected_transaction.name)
      expect(transaction.lobbyist_contract_id).toBe(expected_transaction.lobbyist_contract_id)
    }

    // Employment compensation
    {
      const expected_compensation = l3Reports.fullL3.employment_compensation[0]
      const compensation = (compensations.find((t) =>  t.official_type === 'State Official'))
      let expectedRelationship = null
      expect(compensation).toBeDefined()
      expect(compensation.person_name).toBe(expected_compensation.family_member_name)
      expect(compensation.official_name).toBe(expected_compensation.name)
      expect(compensation.compensation).toBe(expected_compensation.amount)
      expect(compensation.description).toBe(expected_compensation.description)
     if (compensation.is_family === true && compensation.official_type === 'State Official') {
        expectedRelationship = 'State Officials family member'
      }
      expect(expectedRelationship).toBe(expected_compensation.relationship)
    }

    // Professional services compensation
    {
      const expected_compensation = l3Reports.fullL3.professional_services_compensation[0]
      const compensation = (compensations.find((t) =>  t.official_type === null))
      expect(compensation).toBeDefined()
      expect(compensation.person_name).toBe(expected_compensation.person_name)
      expect(compensation.org_name).toBe(expected_compensation.firm_name)
      expect(compensation.compensation).toBe(expected_compensation.amount)
      expect(compensation.description).toBe(expected_compensation.description)
    }

    //check draft got deleted
    const draft = await rds.fetch(`select * from private.draft_document where target_id = $1 and report_key = $2 and report_type='L3' and target_type = 'lobbyist_client'`,[client.lobbyist_client_id, '2024']);
    expect(draft).toBeNull();
  });


  test('Create draft copying data from legacy L3', async () => {
    const client = registrations.client

    const json  = await readFile(`src/tests/data/l3/LegacyL3Report.json`, 'ascii')
    let legacy_l3_report = JSON.parse(json)
    const  {  report_id , submission_id } = await l3Processor.saveReport(2, 2023, legacy_l3_report, '1.0')


    const amendment_draft_id = await l3Processor.createAmendmentDraft(2, 2023)

    const expected_json  = await readFile(`src/tests/data/l3/ExpectedConvertedL3Report.json`, 'ascii')
    let expected_l3 = JSON.parse(expected_json)
    // Assert that the draft was created
    const draft_l3 = await rds.fetch(`SELECT *
                                          FROM private.draft_document
                                          WHERE draft_id = $1`, [amendment_draft_id])
    expected_l3.amends = submission_id
    expected_l3.client = draft_l3.user_data.client

    expect(draft_l3.user_data).toEqual(expected_l3)

  })



  test('Create draft from existing L3 report', async () => {
    const l3 = await rds.fetch(`SELECT * FROM l3 WHERE lobbyist_client_id = $1 and report_year = $2`, [client.lobbyist_client_id, 2024]);
    const draftExpected = await rds.fetch('select * from l3_submission where submission_id = $1', [l3.current_submission_id])
    const draftId = await l3Processor.createAmendmentDraft(client.lobbyist_client_id, 2024)
    const l3draft = await rds.fetch(`select * from private.draft_document where draft_id = $1`, [draftId])
    draftExpected.user_data.client = l3draft.user_data.client
    draftExpected.user_data.amends = draftExpected.submission_id
    draftExpected.user_data.l2_summary = l3draft.user_data.l2_summary

    expect(l3draft).toBeDefined()
    expect(l3draft.draft_id).toBeTruthy()
    expect(l3draft.target_id).toBe(client.lobbyist_client_id)
    expect(l3draft.target_type).toBe('lobbyist_client')
    expect(l3draft.user_data).toBeTruthy()
    expect(l3draft.user_data).toBeDefined()
    // verify that the newly created draft's user data has the data from the old report.
    expect(l3draft.user_data).toEqual(draftExpected.user_data)
  });

  test('test validate l3 draft', async () => {
    for (const validation of validations) {
      let l3 = structuredClone(l3Reports.fullL3)
      validation.modification(l3)
      const resulting_validations = validateL3Draft(l3)
      expect(resulting_validations.length).toBe(1)
      expect(Object.keys(resulting_validations[0])[0]).toEqual(validation.error)
    }
  })

})
