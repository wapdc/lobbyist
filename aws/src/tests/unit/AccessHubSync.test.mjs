import env from 'dotenv';
import {jest, expect, test, describe} from '@jest/globals';
import {readFile} from "fs/promises";
import * as rds from "@wapdc/common/wapdc-db"

env.config();
jest.setTimeout(70000);


// NOTE: Unlike traditional tests we are not rolling back transactions
// between tests. This is because we don't want to have to insert rows into the database
// but would rather use the synced transactions built from the test.
beforeAll(async () => {
  await rds.connect();
  await rds.beginTransaction();
});

afterAll(async () => {
  await rds.rollback();
  await rds.close();
});

async function readDataFile(fileName) {
  const json= await readFile(`src/tests/data/sync/${fileName}`, 'ascii');
  return JSON.parse(json);
}

async function syncFile(fileName) {
  const node = await readDataFile(fileName);
  await rds.query("select lobbyist_sync_accesshub($1)", [JSON.stringify([node])]);
  return node;
}

describe('Can sync all node types', ()  => {

  test('Firm',async () => {
    const node = await syncFile('firm.json');

    const firm = await rds.fetch("SELECT * FROM lobbyist_firm WHERE lobbyist_firm_id = $1", [node.nid])
    expect(firm.name).toBe(node.title)
    expect(firm.entity_id).toBeTruthy()
    const entity  = await rds.fetch("SELECT * from entity where entity_id=$1", [firm.entity_id]);
    expect(entity.name).toBe(node.title);
    expect(entity.email).toBe(node.field_email.und[0].email);

    expect(firm.drupal_nid).toBe(parseInt(node.nid))
    expect(firm.email).toBe(node.field_email.und[0].email)

    expect(firm.current_submission_id).toBe(parseInt(node.vid));

    // Contact fields
    const address = node.field_address.und[0];
    expect(firm.address_1).toBe(address.thoroughfare)
    expect(firm.address_2).toBe(address.premise)
    expect(entity.address).toBe(address.thoroughfare + ' ' + address.premise)
    expect(firm.city).toBe(address.locality)
    expect(entity.city).toBe(address.locality)
    expect(firm.state).toBe(address.administrative_area)
    expect(entity.state).toBe(address.administrative_area)
    expect(firm.postcode).toBe(address.postal_code)
    expect(entity.postcode).toBe(address.postal_code)
    expect(firm.country).toBe(address.country)
    expect(firm.phone).toBe(node.field_firm_phone_number.und[0].value)
    expect(entity.phone).toBe(node.field_firm_phone_number.und[0].value)
    expect(firm.phone_alternate).toBe(node.field_mobile_number.und[0].value);
    expect(firm.temp_phone).toBe(node.field_temporary_thurston_phone_.und[0].value);
    const tempAddress = node.field_temporary_thurston_addr.und[0];
    expect(firm.temp_address_1).toBe(tempAddress.thoroughfare);
    expect(firm.temp_address_2).toBe(tempAddress.premise);
    expect(firm.temp_city).toBe(tempAddress.locality);
    expect(firm.temp_state).toBe(tempAddress.administrative_area);
    expect(firm.temp_postcode).toBe(tempAddress.postal_code);
    expect(firm.temp_country).toBe(tempAddress.country);
    const agents = await rds.query(
      "SELECT a.* FROM lobbyist_agent a WHERE lobbyist_firm_id = $1", [firm.lobbyist_firm_id]);
    expect(agents.length).toBe(node.field_lob_firm_agents.und.length)
    node.field_lob_firm_agents.und.map(agent => {
      const nameField = agent.field_firm_agent_name.und[0];
      const searchName = (`${nameField.title} ${nameField.given} ${nameField.middle} ${nameField.family} ${nameField.generational}`).trim()
      const savedAgent = agents.find(a => a.name === searchName)
      expect(savedAgent).toBeTruthy()
      expect(savedAgent.starting).toBeTruthy()
      expect(savedAgent.ending).toBeNull()
      expect(savedAgent.year_first_employed).toBe(parseInt(agent.field_year_first_employed.und[0].value.slice(0,4)))
      expect(savedAgent.bio).toBe(agent.field_agent_bio.und[0].value)
      expect(savedAgent.agent_picture).toBe(agent.field_agent_picture.und[0].uri)
      expect(savedAgent.picture_updated).toBeTruthy()
      expect(savedAgent.training_certified).toBe(agent.field_lob_cert_date.und[0].value.slice(0,10))
    })
  })

  test('Client', async () => {
    const node = await syncFile('employer.json');
    
    // Retrieve saved employer
    const employer= await rds.fetch("select * from lobbyist_employer where lobbyist_employer_id = $1", [node.nid])
    expect(employer.name).toBe(node.title)
    expect(employer.entity_id).toBeTruthy()
    const entity  = await rds.fetch("SELECT * from entity where entity_id=$1", [employer.entity_id]);
    expect(entity.name).toBe(node.title);
    expect(entity.email).toBe(node.field_email.und[0].email);
    const address = node.field_address.und[0];
    expect(employer.address_1).toBe(address.thoroughfare)
    expect(employer.address_2).toBe(address.premise)
    expect(entity.address).toBe(address.thoroughfare + ' ' + address.premise)
    expect(employer.city).toBe(address.locality)
    expect(entity.city).toBe(address.locality)
    expect(employer.state).toBe(address.administrative_area)
    expect(entity.state).toBe(address.administrative_area)
    expect(employer.postcode).toBe(address.postal_code)
    expect(entity.postcode).toBe(address.postal_code)
    expect(employer.country).toBe(address.country)
    expect(employer.phone).toBe(node.field_phone.und[0].value)
    expect(entity.phone).toBe(node.field_phone.und[0].value)
    expect(employer.description).toBe(node.field_employer_s_occupation_busi.und[0].value)
  })

  test('Client contract', async () => {
    const node= await syncFile('employment-contract.json');
    const contract = await rds.fetch("SELECT * from lobbyist_contract where lobbyist_contract_id=$1", [node.nid])
    expect(contract).toBeTruthy()
    expect(contract.drupal_nid).toBe(parseInt(node.nid))
    expect(contract.lobbyist_contract_id).toBe(parseInt(node.nid))
    expect(contract.lobbyist_firm_id).toBe(parseInt(node.field_firm_entity_reference.und[0].target_id))
    expect(contract.lobbyist_client_id).toBe(parseInt(node.field_employer_entity_reference.und[0].target_id))
    expect(contract.period_start).toBe('2021-01-01')
    expect(contract.period_end).toBe('2024-12-31')

    const periods = await rds.query(`SELECT * from lobbyist_reporting_periods p where lobbyist_contract_id=$1 order by p.period_start`, [node.nid] )
    expect(periods.length).toBe(node.field_select_your_filing_periods.und.length)
    for (let i=0;  i<periods.length; i++) {
      const period = periods[i];
      const expectedPeriod = node.field_select_your_filing_periods.und[i];
      expect(period.period_start.slice(0,10)).toBe(expectedPeriod.value.slice(0,10));
      expect(period.lobbyist_firm_id).toBe(contract.lobbyist_firm_id)
      expect(period.lobbyist_client_id).toBe(contract.lobbyist_client_id)
      if (period.period_start.slice(0,10) === '2024-08-01') {
        expect(period.exempt).toBe(true)
      }
      else {
        expect(period.exempt).toBe(false)
      }
    }
  })

  test('L2', async() => {
    const node = await syncFile('l2.json');
    const l2 = await rds.fetch(`SELECT * FROM l2 WHERE report_id = $1`, [node.nid])
    expect(l2).toBeTruthy();
    expect(l2.report_id).toBe(parseInt(node.nid))
    expect(l2.lobbyist_firm_id).toBe(parseInt(node.og_group_ref.und[0].target_id))
    expect(l2.current_submission_id).toBe(parseInt(node.vid))
    expect(l2.drupal_nid).toBe(parseInt(node.nid))
    expect(l2.period_start).toBe(node.field_filing_period_date.und[0].value.slice(0,10))
    const end_date = new Date(l2.period_start);
    end_date.setFullYear(end_date.getFullYear(), end_date.getMonth()+1, 0);
    expect(new Date(l2.period_start)).toEqual(end_date)

    //Check that the appropriate compensation transactions were created
    const l2CompensationTransactions = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'compensation' order by l2_transaction_id`, [node.nid])
    expect(l2CompensationTransactions).toBeTruthy()
    expect(l2CompensationTransactions.length).toBe(2)
    expect(l2CompensationTransactions[0].amount).toBe('111.11')
    expect(l2CompensationTransactions[1].amount).toBe('222.22')
    expect(l2CompensationTransactions[1].source_target_type).toBe('lobbyist_contract')

    //Check that the appropriate sub_lobbyist_payment transactions were created
    const l2SubLobbyistPayments = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'sub_lobbyist_payment' order by l2_transaction_id`, [node.nid])
    expect(l2SubLobbyistPayments).toBeTruthy()
    expect(l2SubLobbyistPayments.length).toBe(2)
    expect(l2SubLobbyistPayments[0].amount).toBe('3456.11')
    expect(l2SubLobbyistPayments[1].amount).toBe('8765.22')
    expect(l2SubLobbyistPayments[1].source_target_type).toBe('lobbyist_contract')

    //Check that the appropriate personal_expense transactions were created
    const l2PersonalExpenses = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'personal_expense' order by l2_transaction_id`, [node.nid])
    expect(l2PersonalExpenses).toBeTruthy()
    expect(l2PersonalExpenses.length).toBe(3)
    expect(l2PersonalExpenses[0].amount).toBe('200.00')
    expect(l2PersonalExpenses[0].source_target_type).toBe('lobbyist_contract')
    expect(l2PersonalExpenses[1].amount).toBe('300.99')
    expect(l2PersonalExpenses[1].source_target_type).toBe('lobbyist_contract')
    expect(l2PersonalExpenses[2].amount).toBe('777.66')
    expect(l2PersonalExpenses[2].source_target_type).toBe('lobbyist_firm')

    //Check that any non_reimbursed_personal_expenses
    const l2NonReimbursedPersonalExpenses = await rds.fetch( `SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'non_reimbursed_personal_expenses' order by l2_transaction_id`, [node.nid])
    expect(l2NonReimbursedPersonalExpenses).toBeTruthy()
    expect(l2NonReimbursedPersonalExpenses.amount).toBe('1000.00')
    expect(l2NonReimbursedPersonalExpenses.transaction_type).toBe('non_reimbursed_personal_expenses')
    expect(l2NonReimbursedPersonalExpenses.source_target_type).toBe('lobbyist_firm')

    //Check that any advertising_expense
    const l2AdvertisingExpenses = await rds.fetch( `SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'advertising_expense' order by l2_transaction_id`, [node.nid])
    expect(l2AdvertisingExpenses).toBeTruthy()
    expect(l2AdvertisingExpenses.amount).toBe('1000.00')
    expect(l2AdvertisingExpenses.transaction_type).toBe('advertising_expense')
    expect(l2AdvertisingExpenses.source_target_type).toBe('lobbyist_contract')

    //Check that the appropriate non itemized entertainment transactions were created
    const l2NonItemizedEntertainmentTransactions = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'non_itemized_entertainment_expenses' order by l2_transaction_id`, [node.nid])
    expect(l2NonItemizedEntertainmentTransactions).toBeTruthy()
    expect(l2NonItemizedEntertainmentTransactions.length).toBe(2)
    expect(l2NonItemizedEntertainmentTransactions[0].amount).toBe('111.11')
    expect(l2NonItemizedEntertainmentTransactions[1].amount).toBe('222.22')
    expect(l2NonItemizedEntertainmentTransactions[0].source_target_type).toBe('lobbyist_contract')
    expect(l2NonItemizedEntertainmentTransactions[1].source_target_type).toBe('lobbyist_firm')

    // Check that the appropriate itemized entertainment transactions were created in l2_transaction table
    const l2ItemizedEntertainmentTransactions = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'itemized_entertainment_expense' order by l2_transaction_id`, [node.nid])
    expect(l2ItemizedEntertainmentTransactions).toBeTruthy()
    expect(l2ItemizedEntertainmentTransactions.length).toBe(1)
    expect(l2ItemizedEntertainmentTransactions[0].amount).toBe('111.11')
    expect(l2ItemizedEntertainmentTransactions[0].source_target_type).toBe('lobbyist_contract')

   // Check that the appropriate itemized entertainment transactions were created in l2_entertainment table
    const l2EntertainmentTransactions = await rds.query(`SELECT * FROM l2_entertainment WHERE l2_transaction_id = $1`, [l2ItemizedEntertainmentTransactions[0].l2_transaction_id])
    expect(l2EntertainmentTransactions).toBeTruthy();
    expect(l2EntertainmentTransactions.length).toBe(1);
    expect(l2EntertainmentTransactions[0].occasion_type).toBe('Entertainment')
    expect(l2EntertainmentTransactions[0].place).toBe('Some place');
    expect(l2EntertainmentTransactions[0].participants).toBe('participants by name and cost per person, or you can attach a file below and type the name of the file here.');

    // Check that the appropriate itemized entertainment transactions were created in l2_expense table
    const l2ExpenseTransactions = await rds.query(`SELECT * FROM l2_expense WHERE l2_transaction_id = $1`, [l2ItemizedEntertainmentTransactions[0].l2_transaction_id])
    expect(l2ExpenseTransactions).toBeTruthy();
    expect(l2ExpenseTransactions.length).toBe(1);
    expect(l2ExpenseTransactions[0].date).toBe('2023-09-13')
    expect(l2ExpenseTransactions[0].description).toBe('Entertainment expense description');

    // Check that the appropriate non_itemized contribution by employer transactions were created in l2_transaction table
    const l2NonItemizedContributionTransactions = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'non_itemized_contributions' order by l2_transaction_id`, [node.nid])
    expect(l2NonItemizedContributionTransactions).toBeTruthy()
    expect(l2NonItemizedContributionTransactions[0].source_target_type).toBe('lobbyist_firm')
    expect(l2NonItemizedContributionTransactions[0].amount).toBe('1600.00')
    expect(l2NonItemizedContributionTransactions[1].source_target_type).toBe('lobbyist_contract')
    expect(l2NonItemizedContributionTransactions[1].amount).toBe('1700.00')

    // Check that there no expense records created for non_itemized contributions
    const l2NonItemizedContributionExpenses = await rds.query(`SELECT * FROM l2_expense where l2_transaction_id in ($1, $2) order by l2_transaction_id`, [l2NonItemizedContributionTransactions[0].l2_transaction_id, l2NonItemizedContributionTransactions[1].l2_transaction_id])
    expect(l2NonItemizedContributionExpenses.length).toBe(0)

    // Check that the appropriate itemized contribution transactions were created in l2_transaction table
    const l2ItemizedContributionTransactions = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'itemized_contribution' order by l2_transaction_id`, [node.nid])
    expect(l2ItemizedContributionTransactions).toBeTruthy()
    expect(l2ItemizedContributionTransactions[0].source_target_type).toBe('lobbyist_firm')
    expect(l2ItemizedContributionTransactions[0].amount).toBe('111.11')
    expect(l2ItemizedContributionTransactions[0].source_name).toBe('The Music Man')
    expect(l2ItemizedContributionTransactions[1].source_target_type).toBe('lobbyist_contract')
    expect(l2ItemizedContributionTransactions[1].amount).toBe('222.22')
    expect(l2ItemizedContributionTransactions[1].source_name).toBe('The Music Man')

    // Check that the appropriate itemized contribution transactions were created in l2_expense table
    const l2ItemizedContributionExpenses = await rds.query(`SELECT * FROM l2_expense where l2_transaction_id in ($1, $2) order by l2_transaction_id`, [l2ItemizedContributionTransactions[0].l2_transaction_id, l2ItemizedContributionTransactions[1].l2_transaction_id])
    expect(l2ItemizedContributionExpenses.length).toBe(2)
    expect(l2ItemizedContributionExpenses[0].vendor).toBe('Dave')
    expect(l2ItemizedContributionExpenses[0].date).toBe('2023-09-12')
    expect(l2ItemizedContributionExpenses[1].vendor).toBe('Not Dave')
    expect(l2ItemizedContributionExpenses[1].date).toBe('2023-09-12')

    //check that the itemized contributions by employer's pac transactions were created
    const l2ItemizedContributionByPACTransactions = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 
                               and transaction_type = 'itemized_contribution' and source_target_type = 'legacy_committee' order by l2_transaction_id`, [node.nid])
    expect(l2ItemizedContributionByPACTransactions).toBeTruthy()
    expect(l2ItemizedContributionByPACTransactions[0].amount).toBe('111.11')
    expect(l2ItemizedContributionByPACTransactions[0].source_name).toBe('Some Pac Name')
    expect(l2ItemizedContributionByPACTransactions[1].amount).toBe('321.11')
    expect(l2ItemizedContributionByPACTransactions[1].source_name).toBe('The Druids of Paranor')

    // Check that the appropriate itemized contribution transactions were created in l2_expense table
    const l2ItemizedContributionByPACExpenses = await rds.query(`SELECT * FROM l2_expense where l2_transaction_id in ($1, $2) order by l2_transaction_id`, [l2ItemizedContributionByPACTransactions[0].l2_transaction_id, l2ItemizedContributionByPACTransactions[1].l2_transaction_id])
    expect(l2ItemizedContributionByPACExpenses.length).toBe(2)
    expect(l2ItemizedContributionByPACExpenses[0].vendor).toBe('Some pac')
    expect(l2ItemizedContributionByPACExpenses[0].date).toBe('2023-09-11')
    expect(l2ItemizedContributionByPACExpenses[1].vendor).toBe('Walker Boh')
    expect(l2ItemizedContributionByPACExpenses[1].date).toBe('2023-09-12')

    //check that the other itemized contributions transactions were created
    const l2OtherItemizedContributionTransactions = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 
                               and transaction_type = 'itemized_contribution' and source_target_type = 'other' order by l2_transaction_id`, [node.nid])
    expect(l2OtherItemizedContributionTransactions).toBeTruthy()
    expect(l2OtherItemizedContributionTransactions[0].amount).toBe('111.11')
    expect(l2OtherItemizedContributionTransactions[0].source_name).toBe('Some name of a recipient')

    // Check that the appropriate itemized contribution transactions were created in l2_expense table
    const l2OtherItemizedContributionExpenses = await rds.query(`SELECT * FROM l2_expense where l2_transaction_id in ($1) order by l2_transaction_id`, [l2OtherItemizedContributionTransactions[0].l2_transaction_id])
    expect(l2OtherItemizedContributionExpenses.length).toBe(1)
    expect(l2OtherItemizedContributionExpenses[0].vendor).toBe('Other recipient')
    expect(l2OtherItemizedContributionExpenses[0].date).toBe('2023-09-11')

    //check that the other non_itemized contributions transaction was created
    const l2OtherContributionsTransaction = await rds.fetch(`Select * from l2_transaction where report_id = $1
                                                                                             and transaction_type = 'non_itemized_contributions' and source_target_type = 'other' order by l2_transaction_id`, [node.nid])
    expect(l2OtherContributionsTransaction).toBeTruthy()
    expect(l2OtherContributionsTransaction.amount).toBe("5000.00")

    // Check that the appropriate other expenses transactions were created in l2_transaction table
    const l2_OtherExpensesTransactions = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'other_expense' order by l2_transaction_id`, [node.nid])
    expect(l2_OtherExpensesTransactions).toBeTruthy()
    expect(l2_OtherExpensesTransactions.length).toBe(1)
    expect(l2_OtherExpensesTransactions[0].amount).toBe('111.11')
    expect(l2_OtherExpensesTransactions[0].source_target_type).toBe('lobbyist_contract')

    // Check that the appropriate other expenses transactions were created in l2_expense table
    const l2_OtherExpenses = await rds.query(`SELECT * FROM l2_expense WHERE l2_transaction_id = $1 order by l2_transaction_id`, [l2_OtherExpensesTransactions[0].l2_transaction_id])
    expect(l2_OtherExpenses).toBeTruthy()
    expect(l2_OtherExpenses.length).toBe(1);
    expect(l2_OtherExpenses[0].date).toBe('2023-09-11')
    expect(l2_OtherExpenses[0].address).toBe('711 Capital Blvd Suite 201')
    expect(l2_OtherExpenses[0].city).toBe('Olympia')
    expect(l2_OtherExpenses[0].state).toBe('WA')
    expect(l2_OtherExpenses[0].postcode).toBe('98501')
    expect(l2_OtherExpenses[0].vendor).toBe('Other recipient name')

    // Check that the appropriate advertising expenses Political ads, public, relations, polling transactions were created in l2_transaction table
    const l2PoliticalAdsTransactions = await rds.query(`SELECT * FROM l2_transaction WHERE report_id = $1 and transaction_type = 'ad_expense' order by l2_transaction_id`, [node.nid])
    expect(l2PoliticalAdsTransactions).toBeTruthy()
    expect(l2PoliticalAdsTransactions.length).toBe(1)
    expect(l2PoliticalAdsTransactions[0].amount).toBe('111.11')
    expect(l2_OtherExpensesTransactions[0].source_target_type).toBe('lobbyist_contract')

    // Check that the appropriate advertising expenses Political ads, public, relations, polling transactions were created in l2_expense table
    const l2PoliticalAdsExpenses = await rds.query(`SELECT * FROM l2_expense WHERE l2_transaction_id = $1 order by l2_transaction_id`, [l2PoliticalAdsTransactions[0].l2_transaction_id])
    expect(l2PoliticalAdsExpenses).toBeTruthy()
    expect(l2PoliticalAdsExpenses.length).toBe(1);
    expect(l2PoliticalAdsExpenses[0].date).toBe('2023-09-11')
    expect(l2PoliticalAdsExpenses[0].vendor).toBe('political ad vendor')
    expect(l2PoliticalAdsExpenses[0].description).toBe('Political ad description')
  })

  test('L3', async() => {
    const node = await syncFile('l3.json')
    const l3 = await rds.fetch(`SELECT * FROM l3 WHERE report_id=$1`, [node.nid])
    expect(l3).toBeTruthy()
    expect(l3.report_id).toBe(parseInt(node.nid))
    expect(l3.lobbyist_client_id).toBe(parseInt(node.og_group_ref.und[0].target_id))
    expect(l3.current_submission_id).toBe(parseInt(node.vid))
    expect(l3.drupal_nid).toBe(parseInt(node.nid))
    expect(l3.report_year).toBe(parseInt(node.field_filing_period_year.und[0].value))

    // get the transactions from the database
    const transactions = await rds.query(`SELECT t.*
                                          FROM l3_transaction t
                                          WHERE report_id = $1 order by l3_transaction_id`, [l3.report_id]);

    function checkNotListedTotal(property, legacyProperty) {
      const expense = transactions.find((t) => t.transaction_type === property)
      const expectedExpense = node.field_expend_not_listed.und[0][legacyProperty].und[0].value

      expect(expense).toBeDefined()
      expect(parseFloat(expense.amount)).toBe(parseFloat(expectedExpense))
      expect(expense.date).toBeNull()
      expect(expense.description).toBeNull()
    }
    // Make sure we found the payment to registered lobbyist transaction
    checkNotListedTotal('payments_for_registered_lobbyists', 'field_vendor')
    checkNotListedTotal('witnesses_retained_for_lobbying_services', 'field_expert_retainer')
    checkNotListedTotal('costs_for_promotional_materials', 'field_inform_material')
    checkNotListedTotal('grassroots_expenses_for_lobbying_communications', 'field_lobbying_comm')


    // Check that the appropriate Ie Candidate transactions were created in l3_transaction table
    const l3IeCandidateTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 and transaction_type = 'ie_candidate' order by l3_transaction_id`, [node.nid])
    expect(l3IeCandidateTransactions).toBeTruthy()
    expect(l3IeCandidateTransactions.length).toBe(1)
    expect(l3IeCandidateTransactions[0].amount).toBe('200.00')
    expect(l3IeCandidateTransactions[0].date).toBe('2022-10-04')
    expect(l3IeCandidateTransactions[0].description).toBe('test')
    // Check that the appropriate Ie Candidate transactions were created in l3_independent_expenditure table
    const l3IeCandidate = await rds.query(`SELECT * FROM l3_independent_expenditure WHERE l3_transaction_id = $1 order by l3_transaction_id`, [l3IeCandidateTransactions[0].l3_transaction_id])
    expect(l3IeCandidate).toBeTruthy()
    expect(l3IeCandidate.length).toBe(1);
    expect(l3IeCandidate[0].stance).toBe('Against')

    // Check that the appropriate Ie ballot proposition transactions were created in l3_transaction table
    const l3IeBallotPropositionTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 and transaction_type = 'ie_ballot_proposition' order by l3_transaction_id`, [node.nid])
    expect(l3IeBallotPropositionTransactions).toBeTruthy()
    expect(l3IeBallotPropositionTransactions.length).toBe(1)
    expect(l3IeBallotPropositionTransactions[0].amount).toBe('100.00')
    expect(l3IeBallotPropositionTransactions[0].date).toBe('2023-10-01')
    expect(l3IeBallotPropositionTransactions[0].description).toBe('test')
    // Check that the appropriate Ie BallotProposition transactions were created in l3_independent_expenditure table
    const l3IeBallotProposition = await rds.query(`SELECT * FROM l3_independent_expenditure WHERE l3_transaction_id = $1 order by l3_transaction_id`, [l3IeBallotPropositionTransactions[0].l3_transaction_id])
    expect(l3IeBallotProposition).toBeTruthy()
    expect(l3IeBallotProposition.length).toBe(1);
    expect(l3IeBallotProposition[0].stance).toBe('Against')
    expect(l3IeBallotProposition[0].ballot_number).toBe('test B- 123')

    // Check that the appropriate Employment compensation transactions were created in l3_compensation table
    const l3EmpCompTransactions = await rds.query(`SELECT * FROM l3_compensation WHERE report_id = $1 and org_name is null order by l3_compensation_id`, [node.nid])
    expect(l3EmpCompTransactions).toBeTruthy()
    expect(l3EmpCompTransactions.length).toBe(1)
    expect(l3EmpCompTransactions[0].is_family).toBe(true)
    expect(l3EmpCompTransactions[0].official_type).toBe('State Official')
    expect(l3EmpCompTransactions[0].person_name).toBe('Dave')
    expect(l3EmpCompTransactions[0].description).toBe('set')
    expect(l3EmpCompTransactions[0].compensation).toBe('1 to 4,499')

    // Check that the appropriate Professional services transactions were created in l3_compensation table
    const l3ProfSvcsTransactions = await rds.query(`SELECT * FROM l3_compensation WHERE report_id = $1 and org_name is not null order by l3_compensation_id`, [node.nid])
    expect(l3ProfSvcsTransactions).toBeTruthy()
    expect(l3ProfSvcsTransactions.length).toBe(1)
    expect(l3ProfSvcsTransactions[0].org_name).toBe('Some firm')
    expect(l3ProfSvcsTransactions[0].person_name).toBe('some dudette')
    expect(l3ProfSvcsTransactions[0].description).toBe('Test')
    expect(l3ProfSvcsTransactions[0].compensation).toBe('4,500 to 23,999')

    // Check that the appropriate Entertainment expenses transactions were created in l3_transaction table
    const l3EntertainmentExpenseTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 and transaction_type = 'entertainment_expense' order by l3_transaction_id`, [node.nid])
    expect(l3EntertainmentExpenseTransactions).toBeTruthy()
    expect(l3EntertainmentExpenseTransactions.length).toBe(1)
    expect(l3EntertainmentExpenseTransactions[0].name).toBe('Test')
    expect(l3EntertainmentExpenseTransactions[0].is_family).toBe(true)
    expect(l3EntertainmentExpenseTransactions[0].official_type).toBe('State Official')
    expect(l3EntertainmentExpenseTransactions[0].amount).toBe('20.00')
    expect(l3EntertainmentExpenseTransactions[0].date).toBe('2022-10-04')
    expect(l3EntertainmentExpenseTransactions[0].description).toBe('test stuff')

    // Check that the appropriate itemized expenses transactions were created in l3_transaction table
    const l3ItemizedExpenseTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 and transaction_type = 'itemized_expense' order by l3_transaction_id`, [node.nid])
    expect(l3ItemizedExpenseTransactions).toBeTruthy()
    expect(l3ItemizedExpenseTransactions.length).toBe(1)
    expect(l3ItemizedExpenseTransactions[0].name).toBe('Some dude')
    expect(l3ItemizedExpenseTransactions[0].amount).toBe('50.00')
    expect(l3ItemizedExpenseTransactions[0].description).toBe('just because')
    expect(l3ItemizedExpenseTransactions[0].date).toBe('2022-10-01')

    // Check that the appropriate other expenses transactions were created in l3_transaction table
     const l3OtherExpenseTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 and transaction_type = 'other_expense' order by l3_transaction_id`, [node.nid])
     expect(l3OtherExpenseTransactions).toBeTruthy()
     expect(l3OtherExpenseTransactions.length).toBe(1)
     expect(l3OtherExpenseTransactions[0].name).toBe('test')
     expect(l3OtherExpenseTransactions[0].amount).toBe('25000.00')
     expect(l3OtherExpenseTransactions[0].description).toBe('some description')
     expect(l3OtherExpenseTransactions[0].date).toBe('2022-01-01')

    // Check that the appropriate Political contributions transactions were created in l3_transaction  table
    const l3PoliticalContributionTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 and transaction_type = 'itemized_contribution' order by l3_transaction_id`, [node.nid])
    expect(l3PoliticalContributionTransactions).toBeTruthy()
    expect(l3PoliticalContributionTransactions.length).toBe(1)
    expect(l3PoliticalContributionTransactions[0].amount).toBe('100.00')
    expect(l3PoliticalContributionTransactions[0].date).toBe('2022-10-15')
    expect(l3PoliticalContributionTransactions[0].name).toBe('Dave the dave')

    const l3NonItemizedPoliticalContributionTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 and transaction_type = 'non_itemized_political_contribution_amount' order by l3_transaction_id`, [node.nid])
    expect(l3NonItemizedPoliticalContributionTransactions).toBeTruthy()
    expect(l3NonItemizedPoliticalContributionTransactions.length).toBe(1)
    expect(l3NonItemizedPoliticalContributionTransactions[0].amount).toBe('20.00')

    // Check that the appropriate Lobbyist corrected Compensation Total transactions were created in the l3_transaction table
    const l3LobbyistCompensationTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 AND transaction_type = 'lobbyist_compensation_total' order by l3_transaction_id`, [node.nid])
    expect(l3LobbyistCompensationTransactions).toBeTruthy()
    expect(l3LobbyistCompensationTransactions.length).toBe(1)
    expect(l3LobbyistCompensationTransactions[0].amount).toBe('3000.00')
    // Verify corresponding entry's in the l3_lobbyist table
    const l3LobbyistCompensationEntries = await rds.query(`SELECT * FROM l3_lobbyist WHERE l3_transaction_id = $1`, [l3LobbyistCompensationTransactions[0].l3_transaction_id])
    expect(l3LobbyistCompensationEntries).toBeTruthy()
    expect(l3LobbyistCompensationEntries.length).toBe(1)
    expect(l3LobbyistCompensationEntries[0].lobbyist_contract_id).toBe(101788)

    // Check that the appropriate Lobbyist corrected Expense Total transactions were created in the l3_transaction table
    const l3LobbyistExpenseTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 AND transaction_type = 'lobbyist_expense_total' order by l3_transaction_id`, [node.nid])
    expect(l3LobbyistExpenseTransactions).toBeTruthy()
    expect(l3LobbyistExpenseTransactions.length).toBe(1)
    expect(l3LobbyistExpenseTransactions[0].amount).toBe('20.00')
   // Verify corresponding entry's in the l3_lobbyist table
    const l3LobbyistExpenseEntries = await rds.query(`SELECT * FROM l3_lobbyist WHERE l3_transaction_id = $1`, [l3LobbyistExpenseTransactions[0].l3_transaction_id])
    expect(l3LobbyistExpenseEntries).toBeTruthy()
    expect(l3LobbyistExpenseEntries.length).toBe(1)
    expect(l3LobbyistExpenseEntries[0].lobbyist_contract_id).toBe(101788)

    // Check that the appropriate Lobbyist reported Compensation Total transactions were created in the l3_transaction table
    const l3LobbyistReportedCompensationTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 AND transaction_type = 'lobbyist_reported_compensation_total' order by l3_transaction_id`, [node.nid])
    expect(l3LobbyistReportedCompensationTransactions).toBeTruthy()
    expect(l3LobbyistReportedCompensationTransactions.length).toBe(1)
    expect(l3LobbyistReportedCompensationTransactions[0].amount).toBe('13191.38')
    expect(l3LobbyistReportedCompensationTransactions[0].name).toBe('ADVOCATES INC*')

    // Check that the appropriate Lobbyist reported Expense Total transactions were created in the l3_transaction table
    const l3LobbyistReportedExpenseTransactions = await rds.query(`SELECT * FROM l3_transaction WHERE report_id = $1 AND transaction_type = 'lobbyist_reported_expense_total' order by l3_transaction_id`, [node.nid])
    expect(l3LobbyistReportedExpenseTransactions).toBeTruthy()
    expect(l3LobbyistReportedExpenseTransactions.length).toBe(1)
    expect(l3LobbyistReportedExpenseTransactions[0].amount).toBe('0.00')
    expect(l3LobbyistReportedExpenseTransactions[0].name).toBe('ADVOCATES INC*')
  })
});