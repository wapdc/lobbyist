export const legacyL1Report = {
    title: 'Rocking Music Studio',
    log: '',
    status: '1',
    comment: '1',
    promote: '0',
    sticky: '0',
    nid: 1003012,
    type: 'firm_emp_relationship',
    language: 'und',
    created: '1720458362',
    changed: '1726092404',
    tnid: '0',
    translate: '0',
    uuid: 'd355a389-8011-40ce-949d-12443af48072',
    revision_timestamp: '1726092404',
    revision_uid: '6893',
    amount_of_reimbursement: [],
    are_you_reimbursed_for_lob: '0',
    authorization_date: '2024-09-11 15:06:44',
    certification_date: [],
    compensation_label: {
        safe_value: '<p>What is your pay (compensation) for lobbying?</p>\n'
    },
    description_of_employment: [{tid: '21'}],
    emp_expense_pay_chkbox: 'no',
    emp_explain_pay: [],
    employer_area_of_interests: [{tid: '26'}, {tid: '23'}],
    employer_autorization: '1',
    employer_entity_reference: 1003011,
    employer_or_subcontracting: 'employer',
    firm_entity_reference: 1003009,
    firm_i_certify: '1',
    firm_that_is_employing_you: [],
    have_reimbursement_agreeme: '0',
    lob_compensation_amount: [{value: '2000.00'}],
    lob_compensation_period: [{value: 'month'}],
    lobbying_length: 'permanent',
    lobbying_length_explain: [],
    lobbyist_compen_other: [],
    per_period: [],
    select_your_filing_periods: [
        {
            value: '2021-01-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-02-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-03-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-04-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-05-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-06-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-07-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-08-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-09-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-10-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-11-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2021-12-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-01-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-02-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-03-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-04-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-05-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-06-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-07-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-08-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-09-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-10-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-11-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2022-12-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-01-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-02-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-03-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-04-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-06-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-07-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-08-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-09-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-10-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-11-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2023-12-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-01-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-02-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-03-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-04-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-05-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-06-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-07-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-08-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-09-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-10-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-11-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        },
        {
            value: '2024-12-01 00:00:00',
            timezone: 'America/Los_Angeles',
            timezone_db: 'America/Los_Angeles',
            date_type: 'datetime'
        }
    ],
    og_group_ref: '118969',
    legacy_l1: '0',
    legacy_l1_text: "This report was filed in the PDC's legacy filing system.\r\n" +
        `An electronic image is available on the <a href='https://www.pdc.wa.gov/browse/more-ways-to-follow-the-money/advanced-search/view-reports?category=Advanced%20Search' target='_blank'>PDC website</a> by searching for form type "L1".`,
    paper_l1: [],
    authorization_text: {
        safe_value: '<p>By typing &quot;I CERTIFY&quot; in the box below, I hereby certify the employment authority to lobby described in this registration statement</p>\n'
    },
    certification_text: {
        safe_value: '<p>By typing &quot;I CERTIFY&quot; in the box below, I hereby certify that the above is a true, complete and correct statement.</p>\n'
    },
    exemption_periods: [],
    cid: '0',
    last_comment_timestamp: '1720458362',
    last_comment_name: null,
    last_comment_uid: '6893',
    comment_count: '0',
    name: 'metzler.dl@gmail.com',
    picture: '0',
    data: 'a:1:{s:17:"mimemail_textonly";i:0;}',
    employer: 'Rocking Music Studio',
    firm: 'Music Lobby'
}
