export const firm = {
  lobbyist_firm_id: 1,
  name: "Test Firm",
  address:  {
    street: "12345 Some Street",
    city: "olympia",
    state: "WA",
    postcode: "98501"
  },
  phone: "3605551212",
  email: "firm@noreply.com"
}

export const agents = [
  {
    lobbyist_firm_id: 1,
    name: "Special Agent Quigley",
    starting: '2019-01-18',
    year_first_employed: 2019,
    training_certified: "2024-10-10",
    bio: 'I believe that I am indeed the most special agent.'
  },
  {
    lobbyist_firm_id: 1,
    name: "Special Agent Lux",
    starting: '2012-01-18',
    year_first_employed: 2012,
    ending: '2024-10-10',
    training_certified: "2024-10-10",
    bio: 'I believe that I am indeed the most special agent, as that Quigley is wrong.'
  },
  {
    lobbyist_firm_id: 1,
    name: "Special Agent Gadget",
    starting: '2025-01-18',
    year_first_employed: 2019,
    training_certified: "2024-10-10",
    bio: 'I believe that I am indeed the most special(est) agent.'
  }
]

export const client= {
  lobbyist_client_id: 2,
  name: "Test Client/Employer",
  address:  {
    street: "12345 Some Street",
    city: "olympia",
    state: "WA",
    country: "US",
    postcode: "98501"
  },
  phone: "3605551212",
  email: "firm@noreply.com"
}

export const subContractedFirm =  {
  lobbyist_firm_id: 3,
  name: "Test Subcontracted Firm",
  address:  {
    street: "12345 Some other Street",
    city: "olympia",
    state: "WA",
    postcode: "98501"
  },
  phone: "3605551212",
  email: "firm@noreply.com"
}

export const simpleContract = {
  lobbyist_contract_id: 10,
  firm: structuredClone(firm),
  client: structuredClone(client),
  current_submission_id: 99,
  version: 2.0,
  period_start: '2023-01-01',
  period_end: '2024-12-31',
  periods: [
    '2024-01-01',
    '2024-02-01',
    '2024-03-01',
    '2024-04-01',
    '2024-05-01'
  ]
}

export const simpleContractSubmission = {
  lobbyist_contract_id: 10,
  submission_id: 99,
  version: '2.0',
  user_data: {
    firm: structuredClone(firm),
    client: structuredClone(client),
    period_start: '2023-01-01',
    period_end: '2024-12-31',
    periods: [
      '2024-01-01',
      '2024-02-01',
      '2024-03-01',
      '2024-04-01',
      '2024-05-01'
    ]
  }
}

export const subcontractedContract = {
  lobbyist_contract_id: 11,
    firm: structuredClone(subContractedFirm),
    client: structuredClone(client),
    contract: structuredClone(firm),
    version: 2.0,
    period_start: '2023-01-01',
    period_end: '2024-12-31',
    periods: [
    '2024-01-01',
    '2024-02-01',
    '2024-03-01',
    '2024-04-01',
    '2024-05-01'
  ]
}

export const legacyContract = {
  lobbyist_contract_id: 12,
  firm: structuredClone(firm),
  client: structuredClone(client),
  current_submission_id: 999,
  version: 1.0,
  period_start: '2019-01-01',
  period_end: '2022-12-31',
  periods: [
    '2019-01-01',
    '2019-02-01',
    '2019-03-01',
    '2019-04-01',
    '2019-05-01',
    '2019-06-01',
    '2019-07-01',
    '2019-08-01',
    '2019-09-01',
    '2019-10-01',
    '2019-11-01',
    '2019-12-01',
    '2020-01-01',
    '2020-02-01',
    '2020-03-01',
    '2020-04-01',
    '2020-05-01',
    '2020-06-01',
    '2020-07-01',
    '2020-08-01',
    '2020-09-01',
    '2020-10-01',
    '2020-11-01',
    '2020-12-01',
    '2021-01-01',
    '2021-02-01',
    '2021-03-01',
    '2021-04-01',
    '2021-05-01',
    '2021-06-01',
    '2021-07-01',
    '2021-08-01',
    '2021-09-01',
    '2021-10-01',
    '2021-11-01',
    '2021-12-01',
    '2022-01-01',
    '2022-02-01',
    '2022-03-01',
    '2022-04-01',
    '2022-05-01',
    '2022-06-01',
    '2022-07-01',
    '2022-08-01',
    '2022-09-01',
    '2022-10-01',
    '2022-11-01',
    '2022-12-01'
  ]
}
export const legacyContractSubmission = {
  lobbyist_contract_id: 12,
  submission_id: 999,
  version: 1.0,
  user_data: {
    vid: 2003477,
    uid: null,
    title: 'Rocking Music Studio',
    log: '',
    status: '1',
    comment: '1',
    promote: '0',
    sticky: '0',
    nid: 1003012,
    type: 'firm_emp_relationship',
    language: 'und',
    created: '1720458362',
    changed: '1726092404',
    tnid: '0',
    translate: '0',
    uuid: 'd355a389-8011-40ce-949d-12443af48072',
    revision_timestamp: '1726092404',
    revision_uid: '6893',
    amount_of_reimbursement: [],
    are_you_reimbursed_for_lob: '0',
    authorization_date: '2024-09-11 15:06:44',
    certification_date: [],
    compensation_label: {
      safe_value: '<p>What is your pay (compensation) for lobbying?</p>\n'
    },
    description_of_employment: [{tid: '21'}],
    emp_expense_pay_chkbox: 'no',
    emp_explain_pay: [],
    employer_area_of_interests: [{tid: '26'}, {tid: '23'}],
    employer_autorization: '1',
    employer_entity_reference: 2,
    employer_or_subcontracting: 'employer',
    firm_entity_reference: 1,
    firm_i_certify: '1',
    firm_that_is_employing_you: [],
    have_reimbursement_agreeme: '0',
    lob_compensation_amount: [{value: '2000.00'}],
    lob_compensation_period: [{value: 'month'}],
    lobbying_length: 'permanent',
    lobbying_length_explain: [],
    lobbyist_compen_other: [],
    per_period: [],
    select_your_filing_periods: [
      {
        value: '2019-01-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-02-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-03-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-04-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-05-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-06-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-07-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-08-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-09-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-10-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-11-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2019-12-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-01-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-02-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-03-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-04-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-05-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-06-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-07-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-08-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-09-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-10-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-11-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2020-12-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-01-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-02-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-03-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-04-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-05-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-06-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-07-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-08-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-09-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-10-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-11-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2021-12-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-01-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-02-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-03-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-04-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-05-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-06-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-07-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-08-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-09-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-10-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-11-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      },
      {
        value: '2022-12-01 00:00:00',
        timezone: 'America/Los_Angeles',
        timezone_db: 'America/Los_Angeles',
        date_type: 'datetime'
      }
    ],
    og_group_ref: '118969',
    legacy_l1: '0',
    legacy_l1_text: "This report was filed in the PDC's legacy filing system.\r\n" +
        `An electronic image is available on the <a href='https://www.pdc.wa.gov/browse/more-ways-to-follow-the-money/advanced-search/view-reports?category=Advanced%20Search' target='_blank'>PDC website</a> by searching for form type "L1".`,
    paper_l1: [],
    authorization_text: {
      safe_value: '<p>By typing &quot;I CERTIFY&quot; in the box below, I hereby certify the employment authority to lobby described in this registration statement</p>\n'
    },
    certification_text: {
      safe_value: '<p>By typing &quot;I CERTIFY&quot; in the box below, I hereby certify that the above is a true, complete and correct statement.</p>\n'
    },
    exemption_periods: [],
    cid: '0',
    last_comment_timestamp: '1720458362',
    last_comment_name: null,
    last_comment_uid: '6893',
    comment_count: '0',
    name: 'submitter@meow.com',
    picture: '0',
    data: 'a:1:{s:17:"mimemail_textonly";i:0;}',
    employer: 'Test Client/Employer',
    firm: 'Test Firm',
    contractor: null
  }
}
