export const validations = [
  {
    modification: (l1) => {
      l1.period_start = '2027-01-01'
      l1.period_year_months = []
    },
    error: 'l1.period_start',
  },
  // compensation
  {
    modification: (l1) => l1.compensation.amount = null,
    error: 'l1.compensation.amount.invalid',
  },
  {
    modification: (l1) => l1.compensation.amount = -1,
    error: 'l1.compensation.amount.invalid',
  },
  {
    modification: (l1) => l1.compensation.period = null,
    error: 'l1.compensation.period.required',
  },
  {
    modification: (l1) => l1.compensation.period = ' ',
    error: 'l1.compensation.period.required',
  },
  {
    modification: (l1) => {
      l1.compensation.other = null
      l1.compensation.period = 'Other'
    },
    error: 'l1.compensation.other.required',
  },
  {
    modification: (l1) => {
      l1.compensation.other = ' '
      l1.compensation.period = 'Other'
    },
    error: 'l1.compensation.other.required',
  },
  {
    modification: (l1) => l1.compensation.description = null,
    error: 'l1.compensation.description.required',
  },
  {
    modification: (l1) => l1.compensation.description = [],
    error: 'l1.compensation.description.required',
  },
  // areas of interest
  {
    modification: (l1) => l1.areas_of_interest = [],
    error: 'l1.areas_of_interest',
  },
  // reporting periods
  {
    modification: (l1) => l1.period_year_months = null,
    error: 'l1.period_year_months.required',
  },
  // exemptions
  {
    modification: (l1) => l1.period_year_months = [{ year_month: '2025-05', exempt: null }],
    error: 'l1.period_year_months.invalid',
  },

  //expenses and reimbursement
  {
    modification: (l1) => l1.expenses.has_agreement = null,
    error: 'l1.expenses.has_agreement.incomplete',
  },
  {
    modification: (l1) => l1.expenses.incidentals = null,
    error: 'l1.expenses.incidentals.incomplete',
  },
  {
    modification: (l1) => l1.expenses.pay_directly = null,
    error: 'l1.expenses.pay_directly.incomplete',
  },
  {
    modification: (l1) => {
      l1.expenses.has_agreement = true
      l1.expenses.amount = null
    },
    error: 'l1.expenses.amount.invalid',
  },
  {
    modification: (l1) => {
      l1.expenses.has_agreement = true
      l1.expenses.amount = -1
    },
    error: 'l1.expenses.amount.invalid',
  },
  {
    modification: (l1) => {
      l1.expenses.has_agreement = true
      l1.expenses.period = null
    },
    error: 'l1.expenses.period.required',
  },
  {
    modification: (l1) => {
      l1.expenses.has_agreement = true
      l1.expenses.period = ' '
    },
    error: 'l1.expenses.period.required',
  },

  {
    modification: (l1) => {
      l1.expenses.pay_directly = true
      l1.expenses.explanation = []
    },
    error: 'l1.expenses.explanation.required',
  }
]
