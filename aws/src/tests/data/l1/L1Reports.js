export const fullL1 = {
    report_type: "L1",
    period_start: "2025-01-01",
    period_end: "2026-12-31",
    firm: {
        lobbyist_firm_id: 1,
        name: "Test Firm",
        address: "1234 Some Street",
        city: "Olympia",
        state: "WA",
        postcode: "98501",
        phone: "360-555-1212",
        email: "noreply@example.com"
    },
    client: {
        lobbyist_client_id: 2,
        name: "Test Client",
        address: "1234 Some Other Street",
        city: "Olympia",
        state: "WA",
        postcode: "98506",
        phone: "360-555-1111",
        email: "donotreply@example.com"
    },
    expenses: {
        has_agreement: true,
        amount: 23.01,
        period: "Per month",
        incidentals: false,
        pay_directly: true,
        explanation: 'I am under duress and am coding this against my will. Send help!'
    },
    compensation: {
        amount: 134,
        period: 'Other',
        other: 'Some other period that is not month, hourly or year for which I would explain but I would rather not',
        description: [
            'Unsalaried officer or member of group',
            'Full time employee',
            'Lobbying is only part of other duties',
            'Part time or temporary employee',
            'Sole duty is lobbying',
            'Contractor or similar agreement'
        ]
    },
    areas_of_interest: [
        'Agriculture'
    ],
    period_year_months: [
        {
            year_month: '2025-01',
            exempt: false,
        },
        {
            year_month: '2025-05',
            exempt: false,
        },
        {
            year_month: '2026-02',
            exempt: false,
        },
        {
            year_month: '2026-04',
            exempt: false,
        },
    ],
     certification: {
        signature: "I Certify",
        text: "I certify that this is test data and doesn't really mean anything.",
        name: "Test User",
        user_name: "test@user.com"
    },
    approval: {},
}

export const processedLegacyL1 = {
    "period_start": "2019-01-01",
        "period_end": "2022-12-31",
        "firm": {
        "lobbyist_firm_id": 1,
            "name": "Test Firm",
            "address": {
            "street": "12345 Some Street",
                "city": "olympia",
                "state": "WA",
                "postcode": "98501",
                "country": null
        },
        "phone": null,
            "email": "firm@noreply.com"
    },
    "client": {
        "lobbyist_client_id": 2,
            "name": "Test Client/Employer",
            "address": {
            "street": "12345 Some Street",
                "city": "olympia",
                "state": "WA",
                "postcode": "98501",
                "country": "US"
        },
        "phone": "3605551212",
            "email": "firm@noreply.com"
    },
    "contractor": {},
    "certification": {},
    "approval": {},
    "compensation": {
        "amount": 2000,
            "period": "Per Month",
            "other": null,
            "description": [
            "Full time employee"
        ]
    },
    "expenses": {
        "has_agreement": false,
            "amount": null,
            "period": null,
            "incidentals": false,
            "pay_directly": false,
            "explanation": []
    },
    "areas_of_interest": [
        "Education",
        "Other"
    ],
        "period_year_months": [
        {
            "year_month": "2019-01",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-02",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-03",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-04",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-05",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-06",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-07",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-08",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-09",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-10",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-11",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2019-12",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-01",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-02",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-03",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-04",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-05",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-06",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-07",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-08",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-09",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-10",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-11",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2020-12",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-01",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-02",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-03",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-04",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-05",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-06",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-07",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-08",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-09",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-10",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-11",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2021-12",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-01",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-02",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-03",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-04",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-05",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-06",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-07",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-08",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-09",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-10",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-11",
            "exempt": false,
            "filed": false
        },
        {
            "year_month": "2022-12",
            "exempt": false,
            "filed": false
        }
    ],
        "revision_message": {},
    "amends": 999
}
