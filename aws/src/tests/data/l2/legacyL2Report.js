export const fullL2 = {
  "advertising": [
    {
      "amount": "200.00",
      "employer": "10"
    }
  ],
  "business_phone": "3604120868",
  "compensation": [
    {
      "amount": "1000.00",
      "employer": "10"
    }
  ],
  "contributions": [
    {
      "amount": "500.00",
      "created_at": {
        "value": "2023-12-13 00:00:00",
        "timezone": "America/Los_Angeles",
        "timezone_db": "America/Los_Angeles",
        "date_type": "datetime"
      },
      "employer": "10",
      "person_s_entertained": "Dave"
    }
  ],
  "filing_period_date": {
    "value": "2023-12-01 00:00:00",
    "timezone": "America/Los_Angeles",
    "timezone_db": "America/Los_Angeles",
    "date_type": "datetime"
  },
  "non_reimbursed_amount_": "5000.00",
  "other": [
    {
      "amount": "200.00",
      "created_at": {
        "value": "2023-12-20 00:00:00",
        "timezone": "America/Los_Angeles",
        "timezone_db": "America/Los_Angeles",
        "date_type": "datetime"
      },
      "person_s_entertained": "Some Reciepient",
      "recipient_address": {
        "country": "US",
        "administrative_area": "WA",
        "sub_administrative_area": "",
        "locality": "Olympia",
        "dependent_locality": "",
        "postal_code": "98501",
        "thoroughfare": "711 Capitol Blvd",
        "premise": "",
        "sub_premise": "",
        "organisation_name": "",
        "name_line": "",
        "first_name": "",
        "last_name": "",
        "data": null
      },
      "employer": "10"
    }
  ],
  "percent_legislature": "80",
  "percent_state": "20",
  "personal_expenses": [
    {
      "amount": "1000.00",
      "employer": "10"
    },
    {
      "amount": "1000.00",
      "employer": "1"
    }
  ],
  "political_ads": [
    {
      "amount": "300.00",
      "created_at": {
        "value": "2023-12-19 00:00:00",
        "timezone": "America/Los_Angeles",
        "timezone_db": "America/Los_Angeles",
        "date_type": "datetime"
      },
      "person_s_entertained": "Some vendor",
      "short_description": "Favorite songs poll",
      "employer": "10"
    }
  ],
  "og_group_ref": "1",
  "address": {
    "country": "US",
    "administrative_area": "WA",
    "sub_administrative_area": null,
    "locality": "OLYMPIA",
    "dependent_locality": "",
    "postal_code": "98513",
    "thoroughfare": "9612 65TH LN SE",
    "premise": "",
    "sub_premise": null,
    "organisation_name": null,
    "name_line": null,
    "first_name": null,
    "last_name": null,
    "data": null
  },
  "employer_transmit": [
    {
      "amount": "100.00",
      "employer": "115091"
    }
  ],
  "employers_pac": [
    {
      "pac_name": "A manual pac",
      "employer": "10"
    }
  ],
  "entertainment": [
    {
      "cost": "100.00",
      "occasion_date": {
        "value": "2023-12-31 00:00:00",
        "timezone": "America/Los_Angeles",
        "timezone_db": "America/Los_Angeles",
        "date_type": "datetime"
      },
      "occasion_description": "Open mic",
      "place": "Buzz ",
      "type_of_occasion": "2",
      "employer": "10",
      "participant_listing": "Dave\r\nNatalie"
    }
  ],
  "firm_name": "David Metzler",
  "advertising_to_report": "1",
  "compensation_to_report": "1",
  "contributions_to_report": [
    {
      "value": "1"
    }
  ],
  "entertainment_to_report": "1",
  "lobbying_to_report": "1",
  "other_to_report": "1",
  "personal_ex_to_report": "1",
  "uploaded_files": [],
  "aggregate_total_of_all_non": "300.00",
  "cont_emp_pac": [
    {
      "id": "519306",
      "amount": "500.00",
      "employer": "10",
      "occasion_date": {
        "value": "2023-12-13 00:00:00",
        "timezone": "America/Los_Angeles",
        "timezone_db": "America/Los_Angeles",
        "date_type": "datetime"
      },
      "pac_name": "The committee of music",
      "person_s_entertained": "Some Committee"
    }
  ],
  "cont_no_pac": [
    {
      "amount": "500.00",
      "name_of_contributor": "David Metzler",
      "occasion_date": {
        "value": "2023-12-13 00:00:00",
        "timezone": "America/Los_Angeles",
        "timezone_db": "America/Los_Angeles",
        "date_type": "datetime"
      },
      "person_s_entertained": "01/01/23"
    }
  ],
  "list_of_recipients_by_name": "Bob\r\njill \r\nfred",
  "non_item_ent": [
    {
      "id": "519302",
      "amount": "200.00",
      "employer": "115091"
    }
  ],
  "sub_mat": [
    {
      "id": "519310",
      "employer": "115091",
      "mat_of_prop": {
        "tid": "41"
      },
      "issue_or_bill_number": "12345",
      "legislative_agency": "Copyright committee"
    }
  ],
  "any_political_ads": "1",
  "any_non_itemized_ent": "1",
  "any_other_pac_cont": "1",
  "do_you_have_any_non_itemiz": "1",
  "do_you_have_any_other_cont": "1",
  "pac_con_to_report": "1",
  "sub_con_to_report": "0",
  "sub_lobbyist_comp": [
    {
      "amount": "1000.00",
      "sub_lobbyist": "11"
    }
  ],
}