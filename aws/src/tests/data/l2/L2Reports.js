/**
 * This file is used to house sample L2 data for use in tests. It exports a variety of scenarios
 */


export const fullL2 = {
  "report_type": "L2",
  "firm": {
    "name": "Test Firm",
    "address": "1234 Some Street",
    "city": "Olympia",
    "state": "WA",
    "postcode": "98501",
    "phone": "360-555-1212",
    "email": "noreply@example.com"
  },
  "sources": [
    {"id": 1, "name": "Source 1"},
    {"id": 10, "name": "Source 2"},
  ],
  "subcontractors": [{
    "lobbyist_contract_id": 11,
    "name": "Test Client/Employer (Test Subcontracted Firm)"
  }],
  "has_personal_expenses": true,
  "personal_expenses": [
    {
      "amount": 1000.00,
      "source": 10
    },
    {
      "amount": 1000.00,
      "source": 1
    }
  ],
  "has_compensation": true,
  "compensation": [
    {
      "amount": 1000,
      "source": 10
    }
  ],
  "has_sub_lobbyist_payments": true,
  "sub_lobbyist_payments": [
    {
      "amount": 1000,
      "source": 11
    }
  ],
  "has_contributions": true,
  "contributions": [
    {
      "amount": 500,
      "date": "2024-04-15",
      "source" : 10,
      "recipient_id": 7332,
      "recipient": "Dave",
      "committee_id": null,
      "recipient_type": "candidacy",
      "name": null,
    }
  ],
  "has_contributions_in_kind": true,
  "contributions_in_kind": [
    {
      "recipient_type": "candidacy",
      "recipient_id": 1,
      "recipient": "Some committee",
      "source": 10,
      "is_advertisment": true,
      "date": "2024-04-25",
      "name": null,
      "committee_id": null,
      "description": "Some description",
      "vendor": "Acme Inc.",
      "amount": 100.00
    },
  ],
  "has_contributions_small": true,
  "contributions_small": [
    {
      "amount": 100.00,
      "source" : 10
    }
  ],
  "has_contributing_pacs": true,
  "contributing_pacs": [
    {
      "pac_name": "Some Other pac",
      "source" : 10,
      "committee_id": 12345
    },
    {
      "pac_name": "A manual pac",
      "source" : 10,
      "committee_id": null
    }
  ],
  "has_independent_expenditures_candidate": true,
  "independent_expenditures_candidate": [
    {
      "source": 10,
      "date": "2024-04-08",
      "recipient": "Stevothy for Governor",
      "description": "This really describes the ad real well",
      "amount": 856.25,
      "candidate_name": "Billy bob joe",
      "candidate_id": 5,
      "stance": "For"
    }
  ],
  "has_independent_expenditures_ballot": true,
  "independent_expenditures_ballot": [
    {
      "source": 10,
      "date": "2024-04-08",
      "recipient": "Hankothy for Governor",
      "description": "This really describes the ad real well",
      "amount": 432.23,
      "ballot_number": "4",
      "stance": "Against"
    }
  ],
  "has_entertainment": true,
  "entertainment": [
    {
      "cost": 100.00,
      "occasion_date": "2024-04-30",
      "occasion_description": "Open mic",
      "place": "Buzz ",
      "type_of_occasion": "Reception",
      "source" : 10,
      "participant_listing": "Dave\r\nNatalie"
    }
  ],
  "has_entertainment_small": true,
  "entertainment_small": [
    {
      "amount": 200,
      "source" : 10
    }
  ],
  "has_printing": true,
  "printing" : [
    {
      "source": 1,
      "description": "some description",
      "amount": 34
    }
  ],
  "has_consulting": true,
  "consulting" : [
    {
      "source": 10,
      "name": "some name",
      "date": "2024-04-01",
      "amount": 500,
      "address": "1234 some street",
      "city": "Olympia",
      "state": "WA",
      "postcode": "98007"
    }
  ],
  "has_expenses_other": true,
  "expenses_other": [
    {
      "source": 10,
      "vendor": "some vendor",
      "date": "2024-04-30",
      "description": "test",
      "amount": 100
    }
  ],
  "has_lobbying": true,
  "lobbying": [
    {
      "source" : 10,
      "subject": ["Education"],
      "bill_number": ["HB-12345"],
      "issue": ["Music rights"],
      "who_lobbied": ["Copyright committee"]
    }
  ],
  "has_any_indirect": true,
  "needL6": true,
  "has_expenses_indirect": true,
  "expenses_indirect": [
    {
      "name_of_campaign": "Bill King campaign",
      "source": 10,
      "recipient": "some vendor",
      "address": "1234 some street",
      "city": "olympia",
      "state": "WA",
      "postcode": "98501",
      "type": "Advertising",
      "description": "description text here",
      "amount": 34
    }
  ],
  "has_public_relations": true,
  "public_relations": [
    {
      "source": 1,
      "vendor": "some vendor",
      "date": "2024-04-30",
      "description": "test",
      "amount": 100
    }
  ],
  "certification": {
    signature: "I Certify",
    text: "I certify that this is test data and doesn't really mean anything.",
    name: "Test User",
    user_name: "test@user.com"
  },
  "period_start": "2024-04-01",
  "period_end": "2024-04-30"
}

export const processedLegacyL2 = {
  "report_type": "L2",
  "firm": {
    "name": "Test Firm",
    "address": "1234 Some Street",
    "city": "Olympia",
    "state": "WA",
    "postcode": "98501",
    "phone": "360-555-1212",
    "email": "noreply@example.com"
  },
  "sources": [
    {"id": 1, "name": "Source 1"},
    {"id": 10, "name": "Source 2"},
  ],
  "subcontractors": [{
    "lobbyist_contract_id": 11,
    "name": "Test Client/Employer (Test Subcontracted Firm)"
  }],
  "has_personal_expenses": true,
  "personal_expenses": [
    {
      "amount": 1000.00,
      "source": 10
    },
    {
      "amount": 1000.00,
      "source": 1
    }
  ],
  "has_compensation": true,
  "compensation": [
    {
      "amount": 1000,
      "source": 10
    }
  ],
  "has_sub_lobbyist_payments": true,
  "sub_lobbyist_payments": [
    {
      "amount": 1000,
      "source": 11
    }
  ],
  "has_contributions": true,
  "contributions": [
    {
      "amount": 500,
      "date": "2024-04-15",
      "source" : 10,
      "recipient_id": 7332,
      "recipient": "Dave",
      "committee_id": null,
      "recipient_type": "candidacy",
      "name": null,
    }
  ],
  "has_contributions_in_kind": true,
  "contributions_in_kind": [
    {
      "recipient_type": "candidacy",
      "recipient_id": 1,
      "recipient": "Some committee",
      "source": 10,
      "is_advertisment": true,
      "date": "2024-04-25",
      "name": null,
      "committee_id": null,
      "description": "Some description",
      "vendor": "Acme Inc.",
      "amount": 100.00
    },
  ],
  "has_contributions_small": true,
  "contributions_small": [
    {
      "amount": 100.00,
      "source" : 10
    }
  ],
  "has_contributing_pacs": true,
  "contributing_pacs": [
    {
      "pac_name": "A manual pac",
      "source" : 10,
    }
  ],
  "has_independent_expenditures_candidate": true,
  "independent_expenditures_candidate": [
    {
      "source": 10,
      "date": "2024-04-08",
      "recipient": "Stevothy for Governor",
      "description": "This really describes the ad real well",
      "amount": 856.25,
      "candidate_name": "Billy bob joe",
      "candidate_id": 5,
      "stance": "For"
    }
  ],
  "has_independent_expenditures_ballot": true,
  "independent_expenditures_ballot": [
    {
      "source": 10,
      "date": "2024-04-08",
      "recipient": "Hankothy for Governor",
      "description": "This really describes the ad real well",
      "amount": 432.23,
      "ballot_number": "4",
      "stance": "Against"
    }
  ],
  "has_entertainment": true,
  "entertainment": [
    {
      "cost": 100.00,
      "occasion_date": "2024-04-30",
      "occasion_description": "Open mic",
      "place": "Buzz ",
      "type_of_occasion": "Reception",
      "source" : 10,
      "participant_listing": "Dave\r\nNatalie"
    }
  ],
  "has_entertainment_small": true,
  "entertainment_small": [
    {
      "amount": 200,
      "source" : 10
    }
  ],
  "has_printing": true,
  "printing" : [
    {
      "source": 1,
      "description": "some description",
      "amount": 34
    }
  ],
  "has_consulting": true,
  "consulting" : [
    {
      "source": 10,
      "name": "some name",
      "date": "2024-04-01",
      "amount": 500,
      "address": "1234 some street",
      "city": "Olympia",
      "state": "WA",
      "postcode": "98007"
    }
  ],
  "has_expenses_other": true,
  "expenses_other": [
    {
      "source": 10,
      "vendor": "some vendor",
      "date": "2024-04-30",
      "description": "test",
      "amount": 100
    }
  ],
  "has_lobbying": true,
  "lobbying": [
    {
      "source" : 10,
      "subject": ["Education"],
      "bill_number": ["HB-12345"],
      "issue": ["Music rights"],
      "legislative_agency": ["Copyright committee"]
    }
  ],
  "has_any_indirect": true,
  "needL6": true,
  "has_expenses_indirect": true,
  "expenses_indirect": [
    {
      "name_of_campaign": "Bill King campaign",
      "source": 10,
      "recipient": "some vendor",
      "address": "1234 some street",
      "city": "olympia",
      "state": "WA",
      "postcode": "98501",
      "type": "Advertising",
      "description": "description text here",
      "amount": 34
    }
  ],
  "has_public_relations": true,
  "public_relations": [
    {
      "source": 1,
      "vendor": "some vendor",
      "date": "2024-04-30",
      "description": "test",
      "amount": 100
    }
  ],
  "certification": {
    signature: "I Certify",
    text: "I certify that this is test data and doesn't really mean anything.",
    name: "Test User",
    user_name: "test@user.com"
  },
  "period_start": "2024-04-01",
  "period_end": "2024-04-30"
}

export const emptyL2 = {
  "report_type": "L2",
  "firm": {
    "name": "Test Firm",
    "address": "1234 Some Street",
    "city": "Olympia",
    "state": "WA",
    "postcode": "98501",
    "phone": "360-555-1212",
    "email": "noreply@example.com"
  },
  "sources": [
    {"id": 1, "name": "Test Firm"},
    {"id": 10, "name": "Test Client/Employer"},
  ],
  "subcontractors": [{
    "lobbyist_contract_id": 11,
    "name": "Test Client/Employer (Test Subcontracted Firm)"
  }],
  "has_compensation": null,
  "compensation": [],
  "has_sub_lobbyist_payments": null,
  "sub_lobbyist_payments": [],
  "has_personal_expenses": null,
  "personal_expenses": [],
  "has_contributions_small": null,
  "contributions_small": [],
  "has_contributions": null,
  "contributions": [],
  "has_contributions_in_kind": null,
  "contributions_in_kind": [],
  "has_independent_expenditures_candidate": null,
  "independent_expenditures_candidate": [],
  "has_independent_expenditures_ballot": null,
  "independent_expenditures_ballot": [],
  "has_contributing_pacs": null,
  "contributing_pacs": [],
  "has_entertainment": null,
  "entertainment": [],
  "has_entertainment_small": null,
  "entertainment_small": [],
  "has_printing": null,
  "printing": [],
  "has_public_relations": null,
  "public_relations": [],
  "has_consulting": null,
  "consulting": [],
  "has_expenses_other": null,
  "expenses_other": [],
  "has_lobbying": null,
  "lobbying": [],
  "has_any_indirect": null,
  "needL6": null,
  "has_expenses_indirect": null,
  "expenses_indirect": [],
  "certification": {
    signature: null,
    text: null,
    name: null,
    user_name: null
  },
  "period_start": null,
  "period_end": null
}
