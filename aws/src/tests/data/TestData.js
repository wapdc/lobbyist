import * as rds from "@wapdc/common/wapdc-db";
import {subcontractedContract, subContractedFirm, legacyContract, legacyContractSubmission} from "./l1/registrations.js";
import * as registrations from "../data/l1/registrations.js";

export async function createTestData() {
  const firm = registrations.firm
  const client = registrations.client
  const contract = registrations.simpleContract
  const contractSubmission = registrations.simpleContractSubmission
  const subcontract = registrations.subcontractedContract
  const subcontractedFirm = registrations.subContractedFirm
  const agents = registrations.agents


  await rds.execute(`insert into lobbyist_firm(lobbyist_firm_id, name, email, address_1, city, state, postcode)
                           VALUES ($1, $2, $3, $4, $5, $6, $7)`,
    [firm.lobbyist_firm_id, firm.name, firm.email, firm.address.street, firm.address.city, firm.address.state, firm.address.postcode])

  //create the agents for the firm
  for (const a of agents) {
    await rds.execute(`insert into lobbyist_agent(lobbyist_firm_id, name, starting, year_first_employed, bio, training_certified) 
                             VALUES($1, $2, $3, $4, $5, $6)`,
        [a.lobbyist_firm_id, a.name, a.starting, a.year_first_employed, a.bio, a.training_certified])
  }

  // create the client
  await rds.execute(`insert into lobbyist_client(lobbyist_client_id, name, email, address_1, city, state,
                                                       postcode, phone, country)
                           VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`,
    [client.lobbyist_client_id, client.name, client.email, client.address.street, client.address.city, client.address.state, client.address.postcode, client.phone, client.address.country])

  // Create the subcontracted firm
  await rds.execute(`insert into lobbyist_firm(lobbyist_firm_id, name, email, address_1, city, state, postcode)
                           VALUES ($1, $2, $3, $4, $5, $6, $7)`,
    [subcontractedFirm.lobbyist_firm_id, subcontractedFirm.name, subcontractedFirm.email, subcontractedFirm.address.street, subContractedFirm.address.city, subcontractedFirm.address.state, subcontractedFirm.address.postcode])

    // Add the contract
  await rds.execute(`INSERT INTO lobbyist_contract (lobbyist_contract_id, lobbyist_firm_id, lobbyist_client_id, period_start, period_end, current_submission_id)
                           VALUES ($1, $2, $3, $4, $5, $6)
                           RETURNING lobbyist_contract_id`,
    [contract.lobbyist_contract_id, firm.lobbyist_firm_id, client.lobbyist_client_id, contract.period_start, contract.period_end, contract.current_submission_id])

    // Add the contract_submission
    await rds.execute(`INSERT INTO lobbyist_contract_submission (submission_id, lobbyist_contract_id, version, user_data)
                       VALUES ($1, $2, $3, $4)
                       RETURNING submission_id`,
        [contractSubmission.submission_id, contractSubmission.lobbyist_contract_id, contractSubmission.version, contractSubmission.user_data])

    // Add the reporting periods
  for (let period_start of contract.periods) {
    await rds.execute(`insert into lobbyist_reporting_periods(lobbyist_client_id, lobbyist_firm_id,
                                                                                   period_start, period_end,
                                                                                   lobbyist_contract_id)
                               values ($1, $2, $3, cast($3 as date) + INTERVAL '1 MONTH' - interval '1 day', $4)`,
      [client.lobbyist_client_id, firm.lobbyist_firm_id, period_start, contract.lobbyist_contract_id])
  }

  // Create the subcontract
  await rds.execute(`INSERT INTO lobbyist_contract (lobbyist_contract_id, lobbyist_firm_id, lobbyist_client_id, contractor_id, period_start, period_end)
                           VALUES ($1, $2, $3, $4, $5, $6)
                           RETURNING lobbyist_contract_id`,
    [subcontractedContract.lobbyist_contract_id, subcontractedFirm.lobbyist_firm_id, client.lobbyist_client_id, firm.lobbyist_firm_id, contract.period_start, contract.period_end])

  for (let period_start of contract.periods) {
    await rds.execute(`insert into wapdc.public.lobbyist_reporting_periods(lobbyist_client_id, lobbyist_firm_id,
                                                                                   period_start, period_end,
                                                                                   lobbyist_contract_id)
                               values ($1, $2, $3, cast($3 as date) + INTERVAL '1 MONTH' - interval '1 day', $4)`,
      [client.lobbyist_client_id, subContractedFirm.lobbyist_firm_id, period_start, subcontract.lobbyist_contract_id])
  }

    // Add the legacy contract
    await rds.execute(`INSERT INTO lobbyist_contract (lobbyist_contract_id, lobbyist_firm_id, lobbyist_client_id, period_start, period_end, current_submission_id)
                           VALUES ($1, $2, $3, $4, $5, $6)
                           RETURNING lobbyist_contract_id`,
        [legacyContract.lobbyist_contract_id, firm.lobbyist_firm_id, client.lobbyist_client_id, legacyContract.period_start, legacyContract.period_end, legacyContract.current_submission_id])

    // Add the legacy contract_submission
    await rds.execute(`INSERT INTO lobbyist_contract_submission (submission_id, lobbyist_contract_id, version, user_data)
                       VALUES ($1, $2, $3, $4)
                       RETURNING submission_id`,
        [legacyContractSubmission.submission_id, legacyContractSubmission.lobbyist_contract_id, legacyContractSubmission.version, legacyContractSubmission.user_data])

    // Add the reporting periods
    for (let period_start of legacyContract.periods) {
        await rds.execute(`insert into lobbyist_reporting_periods(lobbyist_client_id, lobbyist_firm_id,
                                                                                   period_start, period_end,
                                                                                   lobbyist_contract_id)
                               values ($1, $2, $3, cast($3 as date) + INTERVAL '1 MONTH' - interval '1 day', $4)`,
            [client.lobbyist_client_id, firm.lobbyist_firm_id, period_start, legacyContract.lobbyist_contract_id])
    }

}