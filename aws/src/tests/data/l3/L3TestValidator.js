export const validations = [
  {
    modification: (l3) => l3.has_entertainment_expenditures = null,
    error: 'l3.lobbying_expenses.has_entertainment_expenditures.incomplete',
  },
  {
    modification: (l3) => l3.has_itemized_expenditures = null,
    error: 'l3.lobbying_expenses.has_itemized_expenditures.incomplete',
  },
  {
    modification: (l3) => l3.itemized_expenditures[0].amount = null,
    error: 'l3.itemized_expenditures.0.amount.invalid',
  },
  {
    modification: (l3) => l3.itemized_expenditures[0].date = null,
    error: 'l3.itemized_expenditures.0.date.required',
  },
  {
    modification: (l3) => l3.itemized_expenditures[0].date = '2023-09-03',
    error: 'l3.itemized_expenditures.0.date.invalid',
  },
  {
    modification: (l3) => l3.itemized_expenditures[0].name = null,
    error: 'l3.itemized_expenditures.0.name.required',
  },

  {
    modification: (l3) => l3.itemized_expenditures[0].relationship = null,
    error: 'l3.itemized_expenditures.0.relationship.required',
  },

  {
    modification: (l3) => l3.itemized_expenditures[0].family_member_name = null,
    error: 'l3.itemized_expenditures.0.family_member_name.required',
  },

  {
    modification: (l3) => l3.itemized_expenditures[0].description = null,
    error: 'l3.itemized_expenditures.0.description.required',
  },
  {
    modification: (l3) => l3.has_other_expenditures = null,
    error: 'l3.lobbying_expenses.has_other_expenditures.incomplete',
  },
  {
    modification: (l3) => l3.payments_for_registered_lobbyists = null,
    error: 'l3.lobbying_expenses.payments_for_registered_lobbyists.incomplete',
  },
  {
    modification: (l3) => l3.payments_for_registered_lobbyists = -500,
    error: 'l3.lobbying_expenses.payments_for_registered_lobbyists.invalid',
  },
  {
    modification: (l3) => l3.witnesses_retained_for_lobbying_services = null,
    error: 'l3.lobbying_expenses.witnesses_retained_for_lobbying_services.incomplete',
  },
  {
    modification: (l3) => l3.witnesses_retained_for_lobbying_services = -700.00,
    error: 'l3.lobbying_expenses.witnesses_retained_for_lobbying_services.invalid',
  },
  {
    modification: (l3) => l3.costs_for_promotional_materials = null,
    error: 'l3.lobbying_expenses.costs_for_promotional_materials.incomplete',
  },
  {
    modification: (l3) => l3.costs_for_promotional_materials = -200,
    error: 'l3.lobbying_expenses.costs_for_promotional_materials.invalid',
  },
  {
    modification: (l3) => l3.grassroots_expenses_for_lobbying_communications = null,
    error: 'l3.lobbying_expenses.grassroots_expenses_for_lobbying_communications.incomplete',
  },
  {
    modification: (l3) => l3.grassroots_expenses_for_lobbying_communications = -1,
    error: 'l3.lobbying_expenses.grassroots_expenses_for_lobbying_communications.invalid',
  },
  {
    modification: (l3) => l3.has_itemized_contributions = null,
    error: 'l3.election_expenses.has_itemized_contributions.incomplete',
  },

  {
    modification: (l3) => l3.has_pac_contributions = null,
    error: 'l3.election_expenses.has_pac_contributions.incomplete',
  },

  {
    modification: (l3) => l3.has_candidate_independent_expenditures = null,
    error: 'l3.election_expenses.has_candidate_independent_expenditures.incomplete',
  },

  {
    modification: (l3) => l3.has_ballot_proposition_expenditures = null,
    error: 'l3.election_expenses.has_ballot_proposition_expenditures.incomplete',
  },
  {
    modification: (l3) => l3.non_itemized_political_contribution_amount = null,
    error: 'l3.election_expenses.non_itemized_political_contribution_amount.incomplete',
  },
  {
    modification: (l3) => l3.non_itemized_political_contribution_amount = -500,
    error: 'l3.election_expenses.non_itemized_political_contribution_amount.invalid',
  },

  {
    modification: (l3) => l3.itemized_contributions[0].amount = -100,
    error: 'l3.itemized_contributions.0.amount.invalid',
  },

  {
    modification: (l3) => l3.itemized_contributions[0].recipient = null,
    error: 'l3.itemized_contributions.0.recipient.required',
  },

  {
    modification: (l3) => l3.itemized_contributions[0].date = null,
    error: 'l3.itemized_contributions.0.date.required',
  },

  {
    modification: (l3) => l3.itemized_contributions[0].date = "2023-09-12",
    error: 'l3.itemized_contributions.0.date.invalid',
  },

  {
    modification: (l3) => l3.other_expenditures[0].amount = -100,
    error: 'l3.other_expenditures.0.amount.invalid',
  },

  {
    modification: (l3) => l3.other_expenditures[0].recipient = null,
    error: 'l3.other_expenditures.0.recipient.required',
  },

  {
    modification: (l3) => l3.other_expenditures[0].date = null,
    error: 'l3.other_expenditures.0.date.required',
  },

  {
    modification: (l3) => l3.other_expenditures[0].date = "2023-03-14",
    error: 'l3.other_expenditures.0.date.invalid',
  },

  {
    modification: (l3) => l3.other_expenditures[0].description = null,
    error: 'l3.other_expenditures.0.description.required',
  },

  {
    modification: (l3) => l3.pac_contributions[0].pac_name = null,
    error: 'l3.pac_contributions.0.pac_name.invalid',
  },

  {
    modification: (l3) => l3.entertainment_expenditures[0].amount = -100,
    error: 'l3.entertainment_expenditures.0.amount.invalid',
  },

  {
    modification: (l3) => l3.entertainment_expenditures[0].name = null,
    error: 'l3.entertainment_expenditures.0.name.required',
  },

  {
    modification: (l3) => l3.entertainment_expenditures[0].relationship = null,
    error: 'l3.entertainment_expenditures.0.relationship.required',
  },

  {
    modification: (l3) => l3.entertainment_expenditures[0].family_member_name = null,
    error: 'l3.entertainment_expenditures.0.family_member_name.required',
  },

  {
    modification: (l3) => l3.entertainment_expenditures[0].date = null,
    error: 'l3.entertainment_expenditures.0.date.required',
  },

  {
    modification: (l3) => l3.entertainment_expenditures[0].date = "2025-09-03",
    error: 'l3.entertainment_expenditures.0.date.invalid',
  },

  {
    modification: (l3) => l3.entertainment_expenditures[0].description = null,
    error: 'l3.entertainment_expenditures.0.description.required',
  },


  {
    modification: (l3) => l3.candidate_independent_expenditures[0].amount = -100,
    error: 'l3.candidate_independent_expenditures.0.amount.invalid',
  },

  {
    modification: (l3) => l3.candidate_independent_expenditures[0].name = null,
    error: 'l3.candidate_independent_expenditures.0.name.required',
  },

  {
    modification: (l3) => l3.candidate_independent_expenditures[0].date = null,
    error: 'l3.candidate_independent_expenditures.0.date.required',
  },
  {
    modification: (l3) => l3.candidate_independent_expenditures[0].date = "2022-09-09",
    error: 'l3.candidate_independent_expenditures.0.date.invalid',
  },
  {
    modification: (l3) => l3.candidate_independent_expenditures[0].description = null,
    error: 'l3.candidate_independent_expenditures.0.description.required',
  },
  {
    modification: (l3) => l3.candidate_independent_expenditures[0].stance = null,
    error: 'l3.candidate_independent_expenditures.0.stance.required',
  },
  {
    modification: (l3) => l3.lobbyist[0].compensation = -100,
    error: 'l3.lobbyist.0.compensation.invalid',
  },
  {
    modification: (l3) => l3.lobbyist[0].expenses = -100,
    error: 'l3.lobbyist.0.expenses.invalid',
  },
  {
    modification: (l3) => l3.lobbyist[0].compensation = null,
    error: 'l3.lobbyist.0.compensation.invalid',
  },
  {
    modification: (l3) => l3.lobbyist[0].expenses = null,
    error: 'l3.lobbyist.0.expenses.invalid',
  },

  {
    modification: (l3) => l3.employment_compensation[0].amount = null,
    error: 'l3.employment_compensation.0.amount.required',
  },

  {
    modification: (l3) => l3.employment_compensation[0].name = null,
    error: 'l3.employment_compensation.0.name.required',
  },

  {
    modification: (l3) => l3.employment_compensation[0].relationship = null,
    error: 'l3.employment_compensation.0.relationship.required',
  },

  {
    modification: (l3) => l3.employment_compensation[0].family_member_name = null,
    error: 'l3.employment_compensation.0.family_member_name.required',
  },

  {
    modification: (l3) => l3.employment_compensation[0].description = null,
    error: 'l3.employment_compensation.0.description.required',
  },

  {
    modification: (l3) => l3.professional_services_compensation[0].amount = null,
    error: 'l3.professional_services_compensation.0.amount.required',
  },

  {
    modification: (l3) => l3.professional_services_compensation[0].firm_name = null,
    error: 'l3.professional_services_compensation.0.firm_name.required',
  },

  {
    modification: (l3) => l3.professional_services_compensation[0].person_name = null,
    error: 'l3.professional_services_compensation.0.person_name.required',
  },

  {
    modification: (l3) => l3.professional_services_compensation[0].description = null,
    error: 'l3.professional_services_compensation.0.description.required',
  },

  {
    modification: (l3) => l3.has_in_kind_contributions = null,
    error: 'l3.election_expenses.has_in_kind_contributions.incomplete',
  },

  {
    modification: (l3) => l3.in_kind_contributions[0].amount = -100,
    error: 'l3.in_kind_contributions.0.amount.invalid',
  },

  {
    modification: (l3) => l3.in_kind_contributions[0].recipient = null,
    error: 'l3.in_kind_contributions.0.recipient.required',
  },

  {
    modification: (l3) => l3.in_kind_contributions[0].date = null,
    error: 'l3.in_kind_contributions.0.date.required',
  },

  {
    modification: (l3) => l3.in_kind_contributions[0].date = "2023-09-12",
    error: 'l3.in_kind_contributions.0.date.invalid',
  },

  {
    modification: (l3) => l3.in_kind_contributions[0].description = null,
    error: 'l3.in_kind_contributions.0.description.required',
  },

]
