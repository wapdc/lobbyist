export const fullL3 = {
    report_type: "L3",
    period_start: "2024-01-01",
    period_end: "2024-12-31",
    lobbyist: [
        {
            "lobbyist_contract_id": 10,
            "lobbyist_title": "Music Lobby",
            "contractor_title": null,
            "compensation_l2": 20,
            "expenses_l2": 20,
            "compensation": 100,
            "expenses": 0
        },
        {
            "lobbyist_contract_id": 12,
            "lobbyist_title": "ACME lobbyists",
            "contractor_title": "Music Lobby",
            "compensation_l2": 0,
            "expenses_l2": 0,
            "compensation": 5,
            "expenses": 3
        }
    ],
    itemized_expenditures: [
        {
            "date": "2024-08-04",
            "name": "John",
            "relationship": "State Officials family member",
            "family_member_name": "Joe",
            "amount": 199.99,
            "description": "to do better"
        }
    ],
    entertainment_expenditures: [
        {
            "name": "test",
            "relationship": "State Officials family member",
            "family_member_name": "Joe",
            "amount": 34.00,
            "date": "2024-06-03",
            "description": "some description"
        }
    ],
    other_expenditures: [
        {
            "date": "2024-08-04",
            "amount": 200,
            "recipient": "Test MAYOR CITY OF OLYMPIA 2024",
            "description": "some purpose"
        }
    ],
    has_entertainment_expenditures: true,
    has_itemized_expenditures: true,
    has_other_expenditures: true,
    payments_for_registered_lobbyists: 1111.00,
    witnesses_retained_for_lobbying_services: 2222.00,
    costs_for_promotional_materials: 3333.00,
    grassroots_expenses_for_lobbying_communications: 4444.00,
    non_itemized_political_contribution_amount: 2.00,
    has_itemized_contributions: true,
    itemized_contributions: [
        {
            "date": "2024-07-08",
            "amount": 200,
            "recipient_type": "candidacy",
            "recipient_id": -2,
            "recipient": "MAYOR CITY OF OLYMPIA 2024"
        }
    ],
    has_in_kind_contributions: true,
    in_kind_contributions: [
        {
            "date": "2024-07-08",
            "amount": 200,
            "description": "some description",
            "recipient_type": "candidacy",
            "recipient_id": -2,
            "recipient": "MAYOR CITY OF OLYMPIA 2024"
        }
    ],
    has_pac_contributions: true,
    pac_contributions: [
        {
            "pac_name": 'Freedom for Shannara and the Four Lands',
            "committee_id": null
        },
        {
            "pac_name": "Herman and the Hermits",
            "committee_id": 12345
        }
    ],
    has_candidate_independent_expenditures: true,
    candidate_independent_expenditures: [
        {
            "id": 1,
            "name": "Bob for Bob",
            "amount": 23.23,
            "candidacy_id": -1,
            "date": "2024-06-28",
            "stance": "for",
            "description": "Media buy to promote things no one cares about."
        }
    ],
    has_ballot_proposition_expenditures: true,
    ballot_proposition_expenditures: [
        {
            "amount": 23.23,
            "ballot_number": "1234",
            "date": "2024-06-28",
            "stance": "Against",
            "description": "Media buy to promote things no one cares about."
        }
    ],
    has_employment_compensation: true,
    employment_compensation: [
        {
            "name": "John",
            "relationship": "State Officials family member",
            "family_member_name": "Joe",
            "description": "employment compensation",
            "amount": "$500,000 to $749,999"
        },
    ],
    has_professional_services_compensation: true,
    professional_services_compensation: [
        {
            "firm_name": "Joe",
            "person_name": "John",
            "amount": "$500,000 to $749,999",
            "description": "professional services compensation"
        }
    ],
    certification: {
        signature: "I Certify",
        text: "I certify that this is test data and doesn't really mean anything.",
        name: "Test User",
        user_name: "test@user.com"
    }
}

export const emptyL3 = {
    report_type: "L3",
    period_start: null,
    period_end: null,
    lobbyist: [],
    entertainment_expenditures: [],
    itemized_expenditures: [],
    other_expenditures: [],
    has_entertainment_expenditures: null,
    has_itemized_expenditures: null,
    has_other_expenditures: null,
    payments_for_registered_lobbyists: null,
    witnesses_retained_for_lobbying_services: null,
    costs_for_promotional_materials: null,
    grassroots_expenses_for_lobbying_communications: null,
    non_itemized_political_contribution_amount: null,
    has_itemized_contributions: null,
    itemized_contributions: [],
    has_pac_contributions: null,
    pac_contributions: [],
    has_candidate_independent_expenditures: null,
    candidate_independent_expenditures: [],
    has_ballot_proposition_expenditures: null,
    ballot_proposition_expenditures: [],
    has_employment_compensation: null,
    employment_compensation: [],
    has_professional_services_compensation: null,
    professional_services_compensation: [],
    has_in_kind_contributions: null,
    in_kind_contributions: [],
    certification: {
        signature: null,
        text: null,
        name: null,
        user_name: null
    }
}
