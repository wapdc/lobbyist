import {createDraft, deleteDraft, saveDraft} from "@wapdc/common/drafts";
import {firmProcessor} from "./FirmProcessor.js";
import * as rds from "@wapdc/common/wapdc-db";
import {fetch} from "@wapdc/common/wapdc-db";
import {legacyProcessor} from "./LegacyProcessor.js";
import {clientProcessor} from "./ClientProcessor.js";
import validateL1Draft from "./L1Validator.js";

export const emptyL1 = {
  period_start: null,
  period_end: null,
  firm: {},
  client: {},
  contractor: {},
  certification: {},
  approval: {},
  compensation: {
    amount: null,
    period: null,
    other: null,
    description: []
  },
  expenses: {
    has_agreement: null,
    amount: null,
    period: null,
    incidentals: null,
    pay_directly: null,
    explanation: null
  },
  areas_of_interest: [],
  period_year_months: [],
  revision_message: {}
}

class L1Processor {

  async loadCurrentMetadata(firmId, clientId, l1Report, contractorId) {
    const firm = await firmProcessor.getFirm(firmId)
    if (!firm) {
      throw new Error('Firm not found')
    }
    l1Report.firm = {
      lobbyist_firm_id: firm.lobbyist_firm_id,
      name: firm.name,
      address: {
        street: firm.address_1,
        city: firm.city,
        state: firm.state,
        postcode: firm.postcode,
        country: firm.country
      },
      phone: firm.phone,
      email: firm.email
    }

    const client = await clientProcessor.getClient(clientId)
    if (!client) {
      throw new Error('Client not found')
    }
    l1Report.client = {
      lobbyist_client_id: client.lobbyist_client_id,
      name: client.name,
      address: {
        street: client.address_1,
        city: client.city,
        state: client.state,
        postcode: client.postcode,
        country: client.country
      },
      phone: client.phone,
      email: client.email
    }
    if (contractorId != null) {
      const contractor = await firmProcessor.getFirm(contractorId)
      if (!contractor) {
        throw new Error('contract not found')
      }
      l1Report.contractor = {
        contractor_id: contractorId,
        name: contractor.name,
        address: {
          street: contractor.address_1,
          city: contractor.city,
          state: contractor.state,
          postcode: contractor.postcode,
          country: contractor.country
        },
        phone: contractor.phone,
        email: contractor.email
      }
    }
    await this.getFiledReportingPeriods(firmId, clientId, contractorId, l1Report)
  }

  async getFiledReportingPeriods(firmId, clientId, contractorId, l1Report) {
    const disabledReportingPeriods = await rds.fetch(`
                select jsonb_agg(distinct to_char(l2.period_start, 'yyyy-mm')) as year_months
                from l2
                         left join l2_transaction t on l2.report_id = t.report_id
                         left join l2_lobbying_activity a on l2.report_id = a.report_id
                         join lobbyist_contract lc on (lc.lobbyist_contract_id = t.source_target_id and t.source_target_type = 'lobbyist_contract')
                    or (lc.lobbyist_contract_id = a.source_target_id and a.source_target_type = 'lobbyist_contract')
            where (lc.lobbyist_client_id = $1 or lc.contractor_id = $2) and lc.lobbyist_firm_id = $3`,
        [clientId, contractorId, firmId])

    l1Report.period_year_months?.forEach(ym => {
      ym.filed = !!disabledReportingPeriods?.year_months?.includes(ym.year_month);
    })
  }

  /**
   * @function makeDraft
   * @return {Promise<Object>}
   * @description This function creates an empty draft L1 contract.
   */
  async makeDraft(firm_id, client_id, period_start, period_end, contractor_id, current_submission_id) {
    if (!period_start || !period_end) {
      throw new Error ('Missing coverage period')
    }
    if (!period_start.match(/^\d{4}-\d{2}-\d{2}/)) {
      throw new Error ('Invalid start date')
    }
    if (!period_end.match(/^\d{4}-\d{2}-\d{2}/)) {
      throw new Error ('Invalid end date')
    }
    if (!period_start.endsWith('-01-01')) {
      throw new Error('Start date must be the first day of a year')
    }
    if (!period_end.endsWith('-12-31')) {
      throw new Error('End date must be the last day of a year')
    }
    const contractorIdOrClientId = contractor_id ? firm_id + '-' + client_id + '-' + contractor_id : client_id
    // Determine if draft overlaps existing one
    const existingDrafts = await rds.query(`
    SELECT dd.draft_id, dd.report_key, dd.period_start, dd.period_end
    FROM private.draft_document dd
    WHERE dd.target_type = 'lobbyist_firm'
    AND dd.target_id = $1
    AND dd.report_type = 'L1'
    and (dd.report_key like $2 || ':%')
    and (
        ($3 between dd.period_start and dd.period_end)
         or ($4 between dd.period_start and dd.period_end)
         or ($3 >= dd.period_start and $4 <= dd.period_end)
        )
`, [firm_id, contractorIdOrClientId, period_start, period_end ]);
    if (existingDrafts.length > 0) {
      throw new Error('A draft already exists that overlaps with this draft')
    }
    let user_data = structuredClone(emptyL1)

    // carry forward
    if (current_submission_id) {
      const { version, user_data: current_user_data } = await rds.fetch(`select version, user_data from lobbyist_contract_submission where submission_id = $1`, [current_submission_id])

      // period_year_months are not carried forward
      current_user_data.period_year_months = []
      if (version === '2.0') {
        user_data = current_user_data
      }
      else {
        user_data = await legacyProcessor.convertL1(current_user_data, false)
      }
    }

    user_data.period_start = period_start
    user_data.period_end = period_end
    await this.loadCurrentMetadata(firm_id, client_id, user_data, contractor_id ? contractor_id : null)
    const report_key = contractor_id ? firm_id + '-' + client_id + '-' + contractor_id + ':' + period_start + ':' +  period_end : client_id + ':' + period_start + ':' +  period_end

    const draft_id = await createDraft({
      target_type: 'lobbyist_firm',
      target_id: firm_id,
      report_type: 'L1',
      period_start,
      period_end,
      user_data,
      report_key,
    })
    // This can be removed when createDraft supports period start and period end.
    await rds.execute('update private.draft_document set period_start=$1, period_end=$2 where draft_id=$3', [period_start, period_end, draft_id])
    return draft_id
  }

  /**
   * Retrieves the draft L1 for a particular firmId/draftId pair.
   * @param firmId
   * @param draftId
   * @return {Promise<*|null>}
   */
  async getFirmL1Draft(firmId, draftId) {
    const res = {}
    res.draft =  await fetch(`
    SELECT dd.*, lf.name, lf.lobbyist_firm_id, fp.profile_id as firm_profile_id, fp.current_submission_id as firm_profile_submission_id FROM private.draft_document dd
      join lobbyist_firm lf on lf.lobbyist_firm_id = dd.target_id and dd.target_type='lobbyist_firm'
      left join lobbyist_firm_profile fp on fp.lobbyist_firm_id = lf.lobbyist_firm_id and dd.period_end between fp.period_start and fp.period_end     
    WHERE draft_id = $1
      and lf.lobbyist_firm_id = $2
      and dd.report_type = 'L1'
    `,
      [parseInt(draftId, 10), parseInt(firmId)]);

    const clientId = res.draft.user_data.client.lobbyist_client_id
    const contractorId = res.draft.user_data.contractor?.contractor_id || null
    await this.loadCurrentMetadata(firmId, clientId , res.draft.user_data, contractorId)
    res.agents = await firmProcessor.getAgents(firmId)
    res.validations = validateL1Draft(res.draft.user_data, res.agents)
    return res
  }

 async getContractorL1Draft(contractorId, draftId) {
   const res = {}
   res.draft =  await fetch(`
    SELECT * FROM private.draft_document dd
      join lobbyist_firm lf on lf.lobbyist_firm_id = dd.approval_target_id and dd.target_type='lobbyist_firm'               
    WHERE draft_id = $1
      and dd.approval_target_id = $2
      and dd.report_type = 'L1'
    `,
     [parseInt(draftId, 10), parseInt(contractorId, 10)]);
   const clientId = res.draft.user_data.client.lobbyist_client_id
   const firmId = res.draft.user_data.firm.lobbyist_firm_id || null
   await this.loadCurrentMetadata(firmId, clientId , res.draft.user_data, contractorId)
   res.agents = await firmProcessor.getAgents(firmId)
   res.validations = validateL1Draft(res.draft.user_data, res.agents)
   return res
 }

  async getClientL1Draft(clientId, draftId) {
    const res = {}
    res.draft =  await fetch(`
    SELECT * FROM private.draft_document dd
      join lobbyist_client lc on lc.lobbyist_client_id = dd.approval_target_id and dd.approval_target_type='lobbyist_client'               
    WHERE draft_id = $1
      and lc.lobbyist_client_id = $2
      and dd.report_type = 'L1'
    `,
      [parseInt(draftId, 10), parseInt(clientId)]);

    const firmId = res.draft.user_data.firm.lobbyist_firm_id

    //Grab the firm's agents for the employer side of the draft so that validation works properly
    res.agents = await firmProcessor.getAgents(firmId)
    await this.loadCurrentMetadata(firmId, clientId , res.draft.user_data, null)
    return res
  }


  async saveContractorL1Draft(contractorId, draftId, saveCount, l1) {
    // Validate input parameters
    if (!contractorId || !draftId || !l1) {
      throw new Error('Missing required parameters');
    }

    // Fetch the existing draft
    const existingDraft = await this.getContractorL1Draft(contractorId, draftId);

    if (!existingDraft || !existingDraft.draft) {
      throw new Error('Draft not found');
    }

    const saveResult = {save_count: 0, validations: []}
    saveResult.validations = validateL1Draft(l1, existingDraft.agents)

    // Clear out certification and approval
    l1.certification = structuredClone(emptyL1.certification)
    l1.approval = structuredClone(emptyL1.approval)

    // Save the updated draft
    saveResult.save_count = await saveDraft({
      save_count: saveCount,
      draft_id: draftId,
      user_data: l1
    });

    return saveResult;
  }


  async saveClientL1Draft(clientId, draftId, saveCount, l1) {
    // Validate input parameters
    if (!clientId || !draftId || !l1) {
      throw new Error('Missing required parameters');
    }

    // Fetch the existing draft
    const existingDraft = await this.getClientL1Draft(clientId, draftId);

    if (!existingDraft || !existingDraft.draft) {
      throw new Error('Draft not found');
    }

    const saveResult = {save_count: 0, validations: []}
    saveResult.validations = validateL1Draft(l1, existingDraft.agents)

    // Clear out certification and approval
    l1.certification = structuredClone(emptyL1.certification)
    l1.approval = structuredClone(emptyL1.approval)

    // Save the updated draft
    saveResult.save_count = await saveDraft({
      save_count: saveCount,
      draft_id: draftId,
      user_data: l1
    });

    return saveResult;
  }


  async saveL1Draft(firmId, draftId, saveCount, l1) {
    // Validate input parameters
    if (!firmId || !draftId || !l1) {
      throw new Error('Missing required parameters');
    }

    // Fetch the existing draft
    const existingDraft = await this.getFirmL1Draft(firmId, draftId);

    if (!existingDraft || !existingDraft.draft) {
      throw new Error('Draft not found');
    }

    const saveResult = {save_count: 0, validations: []}
    saveResult.validations = validateL1Draft(l1, existingDraft.agents)

    // Clear out certification and approval
    l1.certification = structuredClone(emptyL1.certification)
    l1.approval = structuredClone(emptyL1.approval)

    // Save the updated draft
    saveResult.save_count = await saveDraft({
      save_count: saveCount,
      draft_id: draftId,
      user_data: l1
    });

    return saveResult;
  }

  async deleteL1Draft(firm_id, draft_id) {
    const draft = await rds.fetch(`select * from private.draft_document 
         where draft_id = $1 and target_type='lobbyist_firm' and target_id = $2`, [draft_id, firm_id])
    if (!draft) {
      throw new Error('Draft not found')
    }
    await deleteDraft(draft_id)
  }

  async getContractSubmissionByClient(client_id, submission_id){
    const contract =  await rds.fetch(`
      select lcs.*, lc.lobbyist_firm_id, lc.period_start, lc.period_end from lobbyist_contract_submission lcs 
        join lobbyist_contract lc on lc.lobbyist_contract_id = lcs.lobbyist_contract_id
        join lobbyist_client c on lc.lobbyist_client_id = c.lobbyist_client_id  
      where c.lobbyist_client_id = $1 and lcs.submission_id = $2 and lc.contractor_id is null
    `,
      [client_id, submission_id])
    if (contract.version === '1.0') {
      contract.user_data = legacyProcessor.preProcessLegacyRecord(contract.user_data)
      const l1 = contract.user_data;
      const employer = await rds.fetch(`SELECT name from lobbyist_client where lobbyist_client_id = $1::integer`, [l1.employer_entity_reference])
      const firm = await rds.fetch(`SELECT name from lobbyist_firm WHERE lobbyist_firm_id = $1::integer`, [l1.firm_entity_reference])
      contract.user_data = {
        ...l1,
        employer: employer.name,
        firm: firm.name,
        contractor: contract.contractor
      }
    }
    return contract;
  }

  async getContractSubmissionByFirm(firm_id, submission_id){
    const contract =  await rds.fetch(`
      select lcs.*, cf.name as contractor, lc.lobbyist_firm_id, lc.period_end, lc.period_start, lc.lobbyist_client_id, lc.contractor_id,
         fp.profile_id as firm_profile_id, fp.current_submission_id as firm_profile_submission_id    
      from lobbyist_contract_submission lcs 
        join lobbyist_contract lc on lc.lobbyist_contract_id = lcs.lobbyist_contract_id
        join lobbyist_firm f on lc.lobbyist_firm_id = f.lobbyist_firm_id
        left join lobbyist_firm_profile fp on fp.lobbyist_firm_id = f.lobbyist_firm_id
          and lc.period_end between fp.period_start and fp.period_end  
        left join lobbyist_firm cf on lc.contractor_id = cf.lobbyist_firm_id  
      where (f.lobbyist_firm_id = $1 or lc.contractor_id=$1) and lcs.submission_id=$2
    `,
      [firm_id, submission_id])
    if (contract.version === '1.0') {
      contract.user_data = legacyProcessor.preProcessLegacyRecord(contract.user_data)
      const l1 = contract.user_data;
      const employer = await rds.fetch(`SELECT name from lobbyist_client where lobbyist_client_id = $1::integer`, [l1.employer_entity_reference])
      const firm = await rds.fetch(`SELECT name from lobbyist_firm WHERE lobbyist_firm_id = $1::integer`, [l1.firm_entity_reference])
      contract.user_data = {
        ...l1,
        employer: employer.name,
        firm: firm.name,
        contractor: contract.contractor
      }
    }
    return contract;
  }

  getContractsListForEmployer(firm_id, client_id) {
    return rds.query(`
        SELECT
            lf.name::text as lobbyist_firm_name,
            lf.lobbyist_firm_id::int as lobbyist_firm_id,
            lcl.lobbyist_client_id::int as lobbyist_client_id,
            coalesce(jlc.period_start::text,(draft_data ->> 'period_start')::text) as period_start,
            coalesce(jlc.period_end::text,(draft_data ->> 'period_end')::text) as period_end,
            jlc.current_submission_id,
            jlc.submitted_at,
            jlc.draft_id as draft_id,
            jlc.updated_at as updated_at, 
            case when jlc.draft_id is null then 'Submitted'
                 when jlc.draft_data->'certification'->>'signature' is null and
                      jlc.draft_data->'approval'->>'signature' is null
                     and jlc.approval_target_id is not null
                     then 'Awaiting both lobbyist and client/employer certification'
                 when jlc.draft_data->'approval'->>'signature' is null and jlc.draft_data->'contractor'->>'contractor_id' is not null
                     then 'Awaiting contractor certification'
                 when jlc.draft_data->'approval'->>'signature' is null and jlc.approval_target_id is not null
                     then 'Awaiting client/employer certification'
                 when jlc.draft_data->'certification'->>'signature' is null and jlc.approval_target_id is not null
                     then 'Awaiting lobbyist certification'
                  else 'Draft'
                end as status
        FROM

            (select lc.lobbyist_contract_id, lc.current_submission_id, lc.period_end, lc.period_start,
                    lcs.submission_id, lcs.submitted_at, lcs.user_data as submission_data, 
                    coalesce(lc.lobbyist_client_id, (dd.user_data -> 'client' ->> 'lobbyist_client_id')::int) as lobbyist_client_id,
                    coalesce(lc.lobbyist_firm_id, dd.target_id) as lobbyist_firm_id,
                    dd.approval_target_id,
                    dd.draft_id, dd.updated_at, dd.target_id as draft_firm_id, dd.user_data as draft_data
             from lobbyist_contract as lc
                      JOIN
                  lobbyist_contract_submission lcs ON lc.current_submission_id = lcs.submission_id
                      FULL OUTER JOIN
                  private.draft_document as dd ON dd.target_id::int = lc.lobbyist_firm_id AND (dd.user_data -> 'client' ->> 'lobbyist_client_id')::int = lobbyist_client_id
                      and  dd.target_type = 'lobbyist_firm' and 
                      lc.period_start::date = (lcs.user_data ->> 'period_start')::date and lc.period_end::date = (dd.user_data ->> 'period_end')::date
            ) as jlc
                JOIN
            lobbyist_firm lf ON jlc.lobbyist_firm_id = lf.lobbyist_firm_id
                JOIN
            lobbyist_client lcl ON jlc.lobbyist_client_id = lcl.lobbyist_client_id
        WHERE
            lf.lobbyist_firm_id = $1 and jlc.lobbyist_client_id = $2
        ORDER BY
            jlc.submitted_at DESC
`, [firm_id, client_id]);
  }

  async getContractSubmissionHistoryByFirm(firm_id, submission_id)  {
    return rds.query(`
      select * from lobbyist_contract_submission s 
               where s.lobbyist_contract_id in 
                     (select lc.lobbyist_contract_id from lobbyist_contract_submission lcs
                      join lobbyist_contract lc on lc.lobbyist_contract_id = lcs.lobbyist_contract_id
                      where lcs.submission_id = $2 and lc.lobbyist_firm_id = $1)
      order by s.submitted_at
    `, [firm_id, submission_id])
  }

  async getContractSubmissionHistoryByClient(client_id, submission_id)  {
    return rds.query(`
      select * from lobbyist_contract_submission s 
               where s.lobbyist_contract_id in
                     (select lc.lobbyist_contract_id from lobbyist_contract_submission lcs
                      join lobbyist_contract lc on lc.lobbyist_contract_id = lcs.lobbyist_contract_id
                      where lcs.submission_id = $2 and lc.lobbyist_client_id = $1)
      order by s.submitted_at
    `, [client_id, submission_id])
  }

  async getClientListByContractor(firm_id){
    return rds.query(`select lc.lobbyist_firm_id,
                             lc.lobbyist_contract_id as lobbyist_contract_id,
                             lc.contractor_id,
                             lc.current_submission_id,
                             lc.period_start,
                             lc.period_end,
                             c.name,
                             c.lobbyist_client_id,
                             lf.name                 as subcontractor_name
                      from lobbyist_contract lc
                               join lobbyist_client c on lc.lobbyist_client_id = c.lobbyist_client_id
                              join lobbyist_firm lf on lc.lobbyist_firm_id = lf.lobbyist_firm_id
                      where lc.lobbyist_firm_id = $1 and lc.contractor_id is null
    `, [firm_id])
  }

  async lobbyistActivityLog(lobbyist_firm_id, lobbyist_client_id, lobbyist_contractor_id, action, message) {
    if (!lobbyist_firm_id || !lobbyist_client_id || !action || !message) {
      throw new Error('Missing required fields for logging action')
    }
    await rds.fetch(`INSERT INTO lobbyist_activity_log (lobbyist_firm_id, lobbyist_client_id, lobbyist_contractor_id,action, message)
                     VALUES ($1, $2, $3, $4, $5)`,
      [lobbyist_firm_id, lobbyist_client_id, lobbyist_contractor_id || null, action, message]
    )
  }

  async submit(draftId, l1, isSubcontractor = false) {

    const period_start = l1.period_start
    const period_end = l1.period_end
    const client = l1.client
    const firm = l1.firm
    let lobbyist_contract_id,existingL1

    // clear out the revision message from the draft before submission
    if (l1.revision_message)
      delete l1.revision_message

    if(isSubcontractor) {
      existingL1 = await rds.fetch(`select * from lobbyist_contract where
                                        lobbyist_firm_id = $1 and lobbyist_client_id=$2 and contractor_id = $3
                                    and $4 between period_start and period_end`, [firm.lobbyist_firm_id, client.lobbyist_client_id,l1.contractor.contractor_id, period_start])
      if (!existingL1) {
        const result = await rds.fetch(`INSERT INTO lobbyist_contract(lobbyist_firm_id, lobbyist_client_id,contractor_id, period_start, period_end) 
                VALUES($1, $2, $3, $4, $5) returning lobbyist_contract_id`, [firm.lobbyist_firm_id, client.lobbyist_client_id,l1.contractor.contractor_id, period_start, period_end])
        lobbyist_contract_id = result.lobbyist_contract_id
      } else {
        lobbyist_contract_id = existingL1.lobbyist_contract_id
      }
    } else {
      existingL1 = await rds.fetch(`select * from lobbyist_contract where 
                                     lobbyist_firm_id = $1 and lobbyist_client_id=$2  and contractor_id is null 
                                    and $3 between period_start and period_end`, [firm.lobbyist_firm_id, client.lobbyist_client_id, period_start])
      if (!existingL1) {
        const result = await rds.fetch(`INSERT INTO lobbyist_contract(lobbyist_firm_id, lobbyist_client_id, period_start, period_end) 
                VALUES($1, $2, $3, $4) returning lobbyist_contract_id`, [firm.lobbyist_firm_id, client.lobbyist_client_id, period_start, period_end])
        lobbyist_contract_id = result.lobbyist_contract_id
      } else {
        lobbyist_contract_id = existingL1.lobbyist_contract_id
      }
    }
    const {submission_id}  = await rds.fetch(`INSERT INTO lobbyist_contract_submission (lobbyist_contract_id, version, user_data)
               VALUES ($1, $2, $3) RETURNING submission_id`, [lobbyist_contract_id, '2.0', l1])

    await rds.execute(`delete from lobbyist_reporting_periods where lobbyist_contract_id = $1`, [lobbyist_contract_id])
    for (let i = 0; i < l1.period_year_months.length; i++) {
      const period = l1.period_year_months[i]
      const start = period.year_month + '-01'

      await rds.execute(`INSERT INTO lobbyist_reporting_periods(lobbyist_client_id, lobbyist_firm_id, period_start, period_end, lobbyist_contract_id, exempt)
                         VALUES($1, $2, cast($3 as date), cast($3 as date) + interval '1 month' - interval '1 day', $4, cast($5 as bool))`, [client.lobbyist_client_id, firm.lobbyist_firm_id, start, lobbyist_contract_id, period.exempt])
    }

    // Update current submission and amendment chain.
    if (existingL1) {
      await rds.execute(`UPDATE lobbyist_contract_submission set superseded_id=$2 where submission_id=$1`, [existingL1.current_submission_id, submission_id])
    }
    await rds.execute(`UPDATE lobbyist_contract set current_submission_id = $2 where lobbyist_contract_id = $1`, [lobbyist_contract_id, submission_id])
    await rds.execute(`DELETE FROM private.draft_document WHERE draft_id = $1`, [draftId])
    // Log the action
    await this.lobbyistActivityLog(firm.lobbyist_firm_id,client.lobbyist_client_id,l1.contractor?.contractor_id,'approve','L1 contract approved successfully')
    return {
      lobbyist_contract_id: lobbyist_contract_id,
        submission_id: submission_id,
      message: 'L1 contract approved successfully',
      submitted: true,
      success: true
    }
  }

  async firmCertify(firmId, draftId, certification, claims, message = null) {
    // Validate input parameters
    if (!firmId || !draftId || !certification) {
      throw new Error('Missing required parameters');
    }

    // Fetch the existing draft
    const draft = await this.getFirmL1Draft(firmId, draftId);

    if (!draft || !draft.draft) {
      throw new Error('Draft not found');
    }

    // Validate the draft
    const validations = validateL1Draft(draft.draft.user_data, draft.agents);
    if (validations.length > 0) {
      throw new Error('Draft validation failed');
    }

    // append user data.
    certification.name = claims.name
    certification.user_name = claims.user_name

    const l1 = draft.draft.user_data;

    // Add certification to the draft
    l1.certification = certification;

    if (message) {
      l1.revision_message = {
        message: message,
        name: claims?.name || null,
        user_name: claims?.user_name || null,
      }
    }

    const {approval_target_id, approval_target_type} = l1.contractor?.contractor_id ? {
        approval_target_id: l1.contractor.contractor_id,
        approval_target_type: 'lobbyist_firm'
      } : { approval_target_id: l1.client.lobbyist_client_id, approval_target_type: 'lobbyist_client'}

    let result
    if (l1.certification?.signature && l1.approval?.signature) {
      result = await this.submit(draftId,l1)
    }
    else {
      // Save the updated draft
      await rds.execute(`UPDATE private.draft_document SET user_data = $2,
                                    approval_target_id = $3, 
                                    approval_target_type = $4,
                                    updated_at = now()
                                WHERE draft_id = $1`, [
                                  draftId,
                                  draft.draft.user_data,
                                  approval_target_id,
                                  approval_target_type])
      // Log the action
      await this.lobbyistActivityLog(firmId,draft.draft.user_data.client.lobbyist_client_id,draft.draft.user_data.contractor?.contractor_id,'submit','L1 contract submitted for approval.')
      result = {
        success: true,
        message: 'L1 contract certified by lobbyist',
        submitted: false
      }
    }
    return result
  }


  async clientCertify(clientId, draftId, approval, claims, isSubcontractor, message) {
    if (!clientId || !draftId || !approval || !claims) {
      throw new Error('Missing required parameters');
    }
    let draft
    // Fetch the existing draft
    if(isSubcontractor)
      draft = await this.getContractorL1Draft(clientId, draftId)
    else
      draft = await this.getClientL1Draft(clientId, draftId)

    if (!draft || !draft.draft) {
      throw new Error('Draft not found');
    }

    // Validate the draft
    const validations = validateL1Draft(draft.draft.user_data, draft.agents);
    if (validations.length > 0) {
      throw new Error('Draft validation failed');
    }
    const l1 = draft.draft.user_data

    // append user data.
    approval.name = claims.name
    approval.user_name = claims.user_name

    // Add certification to the draft
    l1.approval = approval;

    // Add message if provided
    if (message) {
      l1.revision_message = {
        message: message,
        name: claims?.name || null,
        user_name: claims?.user_name || null,
      }
    }


    let result
    if (l1.approval?.signature && l1.certification?.signature) {
      result = await this.submit(draftId, l1, isSubcontractor)
    }
    else {
      // Save the updated draft
      await rds.execute(`UPDATE private.draft_document SET user_data = $2,
                                  updated_at = now()
                              WHERE draft_id = $1`, [
        draftId,
        draft.draft.user_data
      ])
      result = {
        draft_id: draftId,
        submitted: false,
        success: true,
      }
    }

    return result
  }

  async returnL1(id, draftId, message,not_working_with_lobbyist, claims, is_sub_contractor) {
    if (!id || !draftId || !message || !claims) {
      throw new Error('Missing required parameters')
    }
    let draft,removeFromList
    // Fetch the existing draft
    if(is_sub_contractor)
       draft = await this.getContractorL1Draft(id, draftId)
    else
       draft = await this.getClientL1Draft(id, draftId)

    if (!draft || !draft.draft) {
      throw new Error('Draft not found')
    }

    const userData = draft.draft.user_data || {}

    //if they are not working with the lobbyist remove them from the list
    const approvalTargetId = not_working_with_lobbyist ? null : id
    if(is_sub_contractor) {
      removeFromList = not_working_with_lobbyist ? null : 'lobbyist_firm'
    }
    else {
       removeFromList = not_working_with_lobbyist ? null : 'lobbyist_client'
    }


    userData.revision_message = {
      message: message,
      is_not_my_contract: not_working_with_lobbyist,
      name: claims?.name || null,
      user_name: claims?.user_name || null,
    }
    // clear out certification
    if (userData.certification)
    userData.certification = {}

    // Update the draft
    await rds.execute(`update private.draft_document
                       set user_data  = $2,
                           updated_at = now(),
                           approval_target_type = $3,
                           approval_target_id = $4
                       where draft_id = $1`, [draftId, userData, removeFromList, approvalTargetId]
    )
    // Log the action
    await this.lobbyistActivityLog(draft.draft.user_data.firm.lobbyist_firm_id,draft.draft.user_data.client.lobbyist_client_id,draft.draft.user_data.contractor?.contractor_id,'return',message)
    return {
      draftId: draftId,
      message: `Draft ${draftId} rejected/returned successfully`
    }
  }

  async getL1ClientActivityLog(clientId) {
    return rds.query(`select la.lobbyist_firm_id,
                                   la.lobbyist_client_id,
                                   la.action,
                                   la.message,
                                   la.updated_at,
                                   c.name client_name,
                                   lf.name firm_name       
                      from lobbyist_activity_log la
                               join lobbyist_client c on la.lobbyist_client_id = c.lobbyist_client_id
                              join lobbyist_firm lf on la.lobbyist_firm_id = lf.lobbyist_firm_id
                      where la.lobbyist_client_id = $1 and la.lobbyist_contractor_id is null
                      order by la.updated_at desc
    `, [clientId])
  }

  async getL1FirmActivityLog(firmId){
    return rds.query(`select la.lobbyist_firm_id,
                             la.lobbyist_client_id,
                             la.lobbyist_contractor_id,
                             la.action,
                             la.message,
                             la.updated_at,
                             c.name client_name,
                             lf.name firm_name,
                             lf2.name contractor_name
                      from lobbyist_activity_log la
                               join lobbyist_client c on la.lobbyist_client_id = c.lobbyist_client_id
                               join lobbyist_firm lf on la.lobbyist_firm_id = lf.lobbyist_firm_id
                               left join lobbyist_firm lf2 on la.lobbyist_contractor_id = lf2.lobbyist_firm_id
                      where la.lobbyist_firm_id = $1 or la.lobbyist_contractor_id = $1
                      order by la.updated_at desc
    `, [firmId])

  }
  async createAmendmentDraft(firmId, submissionId) {
    const submission = await this.getContractSubmissionByFirm(firmId, submissionId)
    if(!submission) throw new Error('No report found')

    let l1
    if(parseFloat(submission.version) >= 2.0) {
      l1 = submission.user_data
    } else {
      //just empty at the moment for 1.0
      l1 = structuredClone(emptyL1)

        Object.assign(l1, await legacyProcessor.convertL1(submission.user_data))
        l1.period_start = submission.period_start
        l1.period_end = submission.period_end
    }

    await this.loadCurrentMetadata(firmId, submission.lobbyist_client_id, l1, submission.contractor_id)
    const contractorIdOrClientId = submission.contractor_id
        ? firmId + '-' + submission.lobbyist_client_id + '-' + submission.contractor_id
        : submission.lobbyist_client_id

    l1.amends = submissionId
    const existingAmendmentDraft = await rds.fetch(`
      SELECT *
      FROM private.draft_document dd
      WHERE dd.target_type = 'lobbyist_firm'
        AND dd.target_id = $1
        AND dd.report_type = 'L1'
        and (dd.report_key like $2 || ':%')
        and (
        ($3 between dd.period_start and dd.period_end)
          or ($4 between dd.period_start and dd.period_end)
          or ($3 >= dd.period_start and $4 <= dd.period_end)
        )
    `, [firmId, contractorIdOrClientId, submission.period_start, submission.period_end ]);

    const reportKey = submission.contractor_id
        ? firmId + '-' + submission.lobbyist_client_id + '-' + submission.contractor_id + ':' + submission.period_start + ':' +  submission.period_end
        : submission.lobbyist_client_id + ':' + submission.period_start + ':' +  submission.period_end

    if(existingAmendmentDraft) {
      return existingAmendmentDraft.draft_id
    }
    // Create the draft
    const draft_id = await createDraft({
      target_type: 'lobbyist_firm',
      target_id: firmId,
      user_data: l1,
      report_type: 'L1',
      report_key: reportKey
    });

    // This can be removed when createDraft supports period start and period end.
    await rds.execute('update private.draft_document set period_start=$1, period_end=$2 where draft_id=$3', [submission.period_start, submission.period_end, draft_id])

    return draft_id;
  }
}

const l1Processor = new L1Processor()

export { l1Processor }
