import {createDraft, deleteReportDraft, getReportDraft, saveDraft} from "@wapdc/common/drafts";
import {firmProcessor} from "./FirmProcessor.js";
import * as rds from '@wapdc/common/wapdc-db'
import {legacyProcessor} from "./LegacyProcessor.js";
import validateL2Draft from './L2Validator.js'

const emptyL2 = {
    "report_type": "L2",
    "sources": [
    ],
    "subcontractors": [],
    "has_compensation": null,
    "compensation": [],
    "has_sub_lobbyist_payments": null,
    "sub_lobbyist_payments": [],
    "has_personal_expenses": null,
    "personal_expenses": [],
    "has_contributions_small": null,
    "contributions_small": [],
    "has_contributions": null,
    "contributions": [],
    "has_contributions_in_kind": null,
    "contributions_in_kind": [],
    "has_independent_expenditures_candidate": null,
    "independent_expenditures_candidate": [],
    "has_independent_expenditures_ballot": null,
    "independent_expenditures_ballot": [],
    "has_contributing_pacs": null,
    "contributing_pacs": [],
    "has_entertainment": null,
    "entertainment": [],
    "has_entertainment_small": null,
    "entertainment_small": [],
    "has_printing": null,
    "printing": [],
    "has_public_relations": null,
    "public_relations": [],
    "has_consulting": null,
    "consulting": [],
    "has_expenses_other": null,
    "expenses_other": [],
    "has_lobbying": null,
    "lobbying": [],
    "has_any_indirect": null,
    "needL6":null,
    "has_expenses_indirect": null,
    "expenses_indirect": [],
    "certification": {
        signature: null,
        text: null,
        name: null,
        user_name: null
    },
    "period_start": null,
    "period_end": null,
}

class L2Processor {

    /**
     * Helper function that calculates the end date of the period that starts on a given dateStr
     *   and assuming the period is a month long, formats the end date in a YYY-MM-DD format.
     */
    makePeriodEndDate(dateStr) {
        let date = new Date(dateStr);
        date.setMonth(date.getMonth() + 1);
        date.setDate(0);
        return date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-'
            + date.getDate().toString().padStart(2, '0')
    }

    /**
     * Fetches and returns detailed report data for a specific lobbying firm and reporting period.
     *   from the database, ensuring data integrity and security through the use of parameterized
     *   queries and inner joins.
     *
     * @param firmId
     * @param periodStart
     * @returns {Promise<*|null>}
     */
    getReport(firmId, periodStart) {
        return  rds.fetch(`select l2.*, l2s.user_data, l2s.submitted_at, l2s.superseded_id, l2s.version
            from l2 join l2_submission l2s on l2.current_submission_id=l2s.submission_id
            where l2.lobbyist_firm_id=$1 and l2.period_start=$2`,
          [firmId, periodStart])
    }

    /**
     * This process loads current metadata into an existing draft report. It retrieves
     *   the firm information, sets the reporting period, populates the sources array with the firms
     *   clients and subcontractors that have active contracts during the specific period.
     * @param firmId
     * @param periodStart
     * @param l2Report
     * @returns {Promise<void>}
     */
    async loadCurrentMetadata(firmId, periodStart, l2Report) {
        const firm = await firmProcessor.getFirm(firmId)

        // Load in firm information
        l2Report.firm = {
            lobbyist_firm_id: firm.lobbyist_firm_id,
            name: firm.name,
            address: {
                street: firm.address_1,
                city: firm.city,
                state: firm.state,
                postcode: firm.postcode,
                country: firm.country
            },
            phone: firm.phone,
            email: firm.email
        };

        l2Report.sources = [
            {id: firm.lobbyist_firm_id, name: firm.name}
        ]

        // Find clients that have contracts during the reporting period.
        const clients = await rds.query(`SELECT 
                distinct lc.lobbyist_contract_id, 
                         case when lc.contractor_id is not null then c.name || '(' || lsc.name || ')'
                         else c.name end as name  
                from lobbyist_client c join lobbyist_reporting_periods rp on rp.lobbyist_client_id=c.lobbyist_client_id 
                   join lobbyist_contract lc on lc.lobbyist_contract_id=rp.lobbyist_contract_id   
                left join lobbyist_firm lsc on lsc.lobbyist_firm_id=lc.contractor_id
                WHERE rp.lobbyist_firm_id=$1 and rp.period_start=cast($2 as date)`, [firmId, periodStart])

        for (const client of clients) {
            l2Report.sources.push({id: client.lobbyist_contract_id, name: client.name})
        }
        l2Report.subcontractors = await rds.query(`SELECT 
                distinct lc.lobbyist_contract_id, 
                         case when lc.contractor_id is not null then c.name || ' (' || lsc.name || ')'
                         else c.name end as name  
                from lobbyist_client c join lobbyist_reporting_periods rp on rp.lobbyist_client_id=c.lobbyist_client_id 
                   join lobbyist_contract lc on lc.lobbyist_contract_id=rp.lobbyist_contract_id   
                join lobbyist_firm lsc on lsc.lobbyist_firm_id=lc.lobbyist_firm_id
                WHERE lc.contractor_id=$1 and rp.period_start=cast($2 as date)`, [firmId, periodStart] )

    }

    /**
     * Creates a draft L2 report for a lobbyist firm, optionally copying data from a previous report.
     *
     * @async
     * @function makeDraft
     * @param {string} firmId - The ID of the lobbyist firm.
     * @param {string} periodStart - The start date of the reporting period.
     * @param {string|null} [copyFrom=null] - The reporting period for the previous report to copy data from (optional).
     * @param {Object|null} [copyOptions=null] - An object specifying which properties to copy (optional).
     * @throws {Error} If periodStart is not provided or if the specified report to copy from doesn't exist.
     * @returns {Promise<Object>} The created draft report.
     *
     * @description
     * This function performs the following steps:
     * 1. Creates an empty L2 report structure.
     * 2. Loads current metadata for the firm and period.
     * 3. If copyFrom is provided:
     *    - Retrieves the specified previous report.
     *    - For each property in copyOptions:
     *      - If the property exists in the source report and is selected for copying:
     *        - Filters and copies entries related to current sources and subcontractors.
     *        - Sets a flag indicating if the property has any entries.
     * 4. Creates and returns a new draft report.
     *
     * Note: This function handles both new L2 report formats (version >= 2.0) and legacy formats,
     * converting legacy formats as necessary.
     */

    async makeDraft(firmId, periodStart, copyFrom = null, copyOptions = null) {
        if(!periodStart) throw new Error('Period start is required')
        const l2Report = structuredClone(emptyL2)

        await this.loadCurrentMetadata(firmId, periodStart,  l2Report);

        if (copyFrom) {
            const pastL2 = await this.getReport(firmId, copyFrom)
            if (!pastL2) {
                throw new Error('Cannot find existing L2 to copy from')
            }

            const sourceL2 = (parseFloat(pastL2.version) >= 2.0) ? pastL2.user_data : (await legacyProcessor.convertL2(pastL2.user_data))

            for (const [property, value] of Object.entries(copyOptions)) {
                // Legacy data will not be in an array and will be skipped
                if (Array.isArray(l2Report[property]) && ! Array.isArray(sourceL2[property])) {
                    continue
                }
                if (value && sourceL2.hasOwnProperty(property)) {
                    /**
                     * filters out entries for sources that are not contained within the current filing period
                     * subcontractors are in a different array than other sources
                     */
                    if (property === 'lobbying') {
                        l2Report['lobbying'] = sourceL2['lobbying'].map(activity => ({
                            who_lobbied: activity.who_lobbied,
                            source: activity.source,
                            subject: activity.subject,
                        })).filter(x => l2Report.sources.some(y => x.source === y.id))
                    }
                    else if (property === 'sub_lobbyist_payments') {
                        l2Report['sub_lobbyist_payments'] = sourceL2['sub_lobbyist_payments']
                            .filter(x => l2Report.subcontractors.some(y => x.source === y.lobbyist_contract_id))
                    }
                    else {
                        l2Report[property] = sourceL2[property].filter(x => l2Report.sources.some(y => x.source === y.id))
                    }
                    l2Report['has_' + property] = l2Report[property] && (l2Report[property].length > 0)
                }
            }
        }

        // Create the draft
        return await createDraft({
            target_type: 'lobbyist_firm',
            target_id: firmId,
            user_data: l2Report,
            report_type: 'L2',
            report_key: periodStart
        })
    }

    async saveReport(firm_id, period_start , user_data, version = '2.0') {

        const res = await rds.fetch('select period_end from lobbyist_reporting_periods where lobbyist_firm_id = $1 and period_start = $2', [firm_id, period_start])
        const period_end = res.period_end

        const l2 = await rds.fetch(`insert into l2 (lobbyist_firm_id, period_start, period_end)VALUES($1, $2, $3) returning report_id
     `, [ firm_id, period_start, period_end])

        const report_id = l2.report_id

        const l2Submission = await rds.fetch(`insert into l2_submission (report_id, version, user_data)VALUES($1, $2 , $3) returning submission_id`,
          [report_id, version, user_data])

        const submission_id = l2Submission.submission_id

        await rds.fetch(`update l2 set current_submission_id = $1 where report_id = $2`, [submission_id, report_id])
        return { report_id , submission_id }
    }

    /**
     * Get a draft for a reporting period.
     * @param firmId
     * @param periodStart
     * @returns {Promise<*|null>}
     */
    async getDraft(firmId, periodStart) {
        let res = {};
        res.draft = await getReportDraft('lobbyist_firm', firmId, 'L2', periodStart);

        if(!res.draft) {
            return null
        }
        res.draft.user_data.period_start = periodStart;
        res.draft.user_data.period_end = this.makePeriodEndDate(periodStart);

        await this.loadCurrentMetadata(firmId, periodStart, res.draft.user_data)
        res.validations = validateL2Draft(res.draft.user_data)
        res.subjectList = (await this.getSubjects()).map(source => source.name)
        res.committees = await this.getCommittees(periodStart)
        res.candidates = await this.getCandidates(periodStart)
        return res;
    }

    async submitL2Report(firm_id, period_start, user_data, claims) {
        let res
        const validations = validateL2Draft(user_data);
        if (validations.length > 0) {
            throw new Error('L2 report is not valid')
        }
        if(!period_start) throw new Error('Period start is required')
        if(!user_data) throw new Error('User data is required')
        if(!firm_id) throw new Error('Firm id is required')

        if (!user_data.certification?.signature) {
            throw new Error('Certification is missing or null');
        }

        if (user_data.certification?.signature.toLowerCase().trim() !== "i certify") {
            throw new Error('Certification signature is invalid');
        }

        if (!user_data.certification?.text || !user_data.certification.text.trim()) {
            throw new Error('Certification text is empty or null');
        }
        if (!claims.name || !claims.name.trim()) {
            throw new Error('Name is required for certification');
        }
        if (!claims.user_name || !claims.user_name.trim()) {
            throw new Error('Username is required for certification');
        }
        user_data.certification.name = claims.name
        user_data.certification.user_name = claims.user_name
        // Check if there is submission data available for L2.
        const l2 = await rds.fetch(`select l2.*,l2s.version from l2 join l2_submission l2s on l2.report_id = l2s.report_id where lobbyist_firm_id = $1 and period_start = $2 and l2s.superseded_id is null`, [firm_id, period_start])
        if(l2) {
            const l2Submission = await rds.fetch(`insert into l2_submission (report_id, version, user_data) values ($1, $2 , $3) returning submission_id`, [l2.report_id, '2.0', user_data])
            const newSubmissionId = l2Submission.submission_id
            await rds.fetch(`update l2 set current_submission_id = $1 where report_id = $2`, [newSubmissionId, l2.report_id])
            await rds.fetch(`update l2_submission set superseded_id = $1 where submission_id = $2`, [newSubmissionId, l2.current_submission_id])
            await rds.fetch(`delete from private.draft_document where report_key = $1 and target_id = $2 and report_type='L2' and target_type = 'lobbyist_firm'`,[period_start, firm_id])
            res = { report_id : l2.report_id, submission_id : newSubmissionId }
        }
        else {
            res = await this.saveReport(firm_id, period_start, user_data)
            if(res) {
               await rds.fetch(`update l2 set current_submission_id = $1 where report_id = $2`, [res.submissionId, res.reportId])
               await rds.fetch(`delete from private.draft_document where report_key = $1 and target_id = $2 and report_type='L2' and target_type = 'lobbyist_firm'`,[period_start, firm_id])
           }
        }
        await rds.execute(`select l2_save_transactions(cast($1 as int))`, [res.report_id])
        return res;
    }

    /**
     * Get the history of L2 reports for a firm.
     * @param firmId
     * @param report_id
     * @returns {Promise<*|null>}
     */
    async getL2History(firmId, report_id) {
        return rds.query(`select l2s.* from l2_submission l2s
                          join public.l2 l on l.report_id = l2s.report_id
                          where l.report_id=$1
                            and l.lobbyist_firm_id = $2
                          order by l2s.submitted_at desc;`, [report_id, firmId])
    }


    async saveL2Draft(firmId, periodStart, saveCount, l2) {
        let saveResult = {'save_count': 0, 'validations': []};
        saveResult.validations = validateL2Draft(l2);
        saveResult.save_count = await saveDraft({
            target_type: 'lobbyist_firm',
            target_id: firmId,
            report_type: 'L2',
            report_key: periodStart,
            user_data: l2,
            save_count: saveCount,
        });
        return saveResult;
    }

    async createAmendmentDraft(firmId, periodStart) {
        const res = await this.getReport(firmId, periodStart)
        if(!res) throw new Error('No report found')

        let l2Report
        if(parseFloat(res.version) >= 2.0) {
            l2Report = res.user_data
        } else {
            l2Report = structuredClone(emptyL2)
            Object.assign(l2Report, await legacyProcessor.convertL2(res.user_data))
        }
        // If the existing report was created from an amendment, we need to clear the conversion_incomplete indicator.
        if (res.version > 2.0 && l2Report.hasOwnProperty('conversion_incomplete')) {
           delete l2Report.conversion_incomplete
        }
        l2Report.amends = res.current_submission_id

        await this.loadCurrentMetadata(firmId, periodStart, l2Report)

        const existingAmendmentDraft = await this.getDraft(firmId, periodStart)

        if(existingAmendmentDraft) {
            return existingAmendmentDraft
        }
        // Create the draft
        return await createDraft({
            target_type: 'lobbyist_firm',
            target_id: res.lobbyist_firm_id,
            user_data: l2Report,
            report_type: 'L2',
            report_key: res.period_start
        });
    }

    async deleteL2Draft(firm_id, period_start) {
        return deleteReportDraft('lobbyist_firm',firm_id, 'L2', period_start)
    }
    async getSubjects(){
        return rds.query(
          // excluding the subject where tid is 23 (Other)
          'select a.name from accesshub_taxonomy_term_data a where a.vid = 3 and a.tid!=23 order by a.name'
        )
    }

    async getCommittees(periodStart) {
        const year = periodStart.split('-')[0]
        return rds.query(
          `select committee_id, name 
               from committee 
                 where ((continuing and cast($1 as int) between start_year and coalesce(end_year, cast($1 as int)))
                   or (election_code >= cast($1 as text)))
                 and committee_type = 'CO'
                 and filer_id is not null
               order by name`, [year]
        )
    }

    async getCandidates(periodStart) {
        const year = periodStart.split('-')[0]
        return rds.query(
          `select ca.candidacy_id,
                  case
                      when ca.ballot_name <> e.name
                          then concat(e.name, ' (', ca.ballot_name, ')')
                      else e.name
                      end || ' - '
                      || offtitle || CASE WHEN j.jurisdiction_id<> 1000 THEN ' ' || j.name else '' end 
                      || ' ' || substr(ca.election_code,1,4) 
                      || CASE WHEN exit_reason is null then ''
                            when exit_reason='' then ''
                            else ' (' || exit_reason || ')' end 
                      as name 
               from entity e join candidacy ca on ca.person_id = e.entity_id
                 join jurisdiction j on ca.jurisdiction_id = j.jurisdiction_id
                 join foffice o on ca.office_code= o.offcode
                 where election_code >= cast($1 as text)
               order by name`, [year]
        )
    }
}

const l2Processor = new L2Processor()
export { l2Processor }
