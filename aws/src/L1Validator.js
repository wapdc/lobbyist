import Moment from 'moment'
/**
 * Date validation function based loosely on logic contained in PDatePicker global component.
 * @param date
 * @param period_start
 * @param period_end
 * @return {boolean|string}
 */
function validDate(date, period_start, period_end) {
  if (!date) {
    return false
  }
  period_start = new Moment(period_start, 'YYYY-MM-DD')
  period_end = new Moment(period_end, 'YYYY-MM-DD')
  date = Moment(date, 'YYYY-MM-DD')
  if (!date.isValid() || !period_start.isValid() || !period_end.isValid()) {
    return false
  }
  return (date >= period_start && date <= period_end)
}

function validBoolean(value) {
  return value !== undefined && value !== null && typeof value === 'boolean'
}

export default function validateL1Draft(l1, agents) {
  const validation_errors = []

  function addError(errorCode, message = 'invalid') {
    const error = {[errorCode]: message};
    validation_errors.push(error)
  }

  // Check to see if period start and end dates are at least internally consistent.
  // This should error if they are backwards.
  if (!validDate(l1.period_start, l1.period_start, l1.period_end) || !validDate(l1.period_end, l1.period_start, l1.period_end)) {
    addError('l1.period_start', 'Period dates invalid')
  }

  // compensation
  if (!l1.compensation.amount || !parseFloat(l1.compensation.amount) || parseFloat(l1.compensation.amount) <= 0) {
    addError(`l1.compensation.amount.invalid`, 'Invalid amount')
  }
  if (!l1.compensation.period || typeof l1.compensation.period !== 'string' || l1.compensation.period.trim() === '') {
    addError(`l1.compensation.period.required`, 'Compensation period required');
  }
  if (!l1.compensation.description || l1.compensation.description.length === 0) {
    addError(`l1.compensation.description.required`, 'Description of employment required');
  }
  if ((!l1.compensation.other || l1.compensation.other.trim() === '') && l1.compensation.period === 'Other') {
    addError(`l1.compensation.other.required`, 'Explain the complexities of your contract period!!');
  }

  // areas of interest
  if (!l1.areas_of_interest || l1.areas_of_interest.length === 0) {
    addError('l1.areas_of_interest', 'Employer areas of interest required')
  }

  // reporting periods
  if (! l1.period_year_months) {
    addError('l1.period_year_months.required', 'Reporting period year/months required');
  }
  else {
    for (const period_year_month of l1.period_year_months) {
      if (! validDate(period_year_month.year_month + '-01', l1.period_start, l1.period_end)) {
        addError('l1.period_year_months.invalid', 'Period year/month invalid')
        break
      }
      if (period_year_month.exempt !== true && period_year_month.exempt !== false) {
        addError('l1.period_year_months.invalid', 'Period year/month invalid')
        break
      }
    }
  }
  // expenses and reimbursement
  if (!validBoolean(l1.expenses.has_agreement)) {
    addError(`l1.expenses.has_agreement.incomplete`, 'incomplete')
  }
  if (!validBoolean(l1.expenses.incidentals)) {
    addError(`l1.expenses.incidentals.incomplete`, 'incomplete')
  }
  if (!validBoolean(l1.expenses.pay_directly)) {
    addError(`l1.expenses.pay_directly.incomplete`, 'incomplete')
  }
  if (l1.expenses.has_agreement) {
    if ((!l1.expenses.amount || !parseFloat(l1.expenses.amount) || parseFloat(l1.expenses.amount) <= 0)) {
      addError(`l1.expenses.amount.invalid`, 'Invalid amount')
    } else if ((!l1.expenses.period || typeof l1.expenses.period !== 'string' || l1.expenses.period.trim() === '')) {
      addError(`l1.expenses.period.required`, 'Invalid required')
    }
  }
  if ((l1.expenses.pay_directly === true)  && (!l1.expenses.explanation || typeof l1.expenses.explanation !== 'string' || l1.expenses.explanation.trim() === '')) {
    addError(`l1.expenses.explanation.required`, 'Explain any direct payments')
  }

  /*
    Check valid certifications starting in October of year prior to biennium starting. Due to issues with amending
    legacy contracts we are checking 27 months prior to the end date to make sure contracts are checked when amending into
    current expectations.
  */
  const agentCertThresholdDate = (new Moment(l1.period_end, 'YYYY-MM-DD')).subtract(27, 'M').add(1, 'day').format('YYYY-MM-DD')

  if(agents.length === 0) {
    addError('l1.agents.required', 'Agents required')
  }
  for(const agent of agents) {
    //There are some nulls in the training_certified field; maybe that was fine in the past, but we should enforce valid dates in the future
    if(agent.training_certified) {
      if(agent.training_certified < agentCertThresholdDate) {
        addError(`l1.agent.training_certified.invalid`, `Training certification for ${agent.name} is not valid for this reporting period.`)
      }
    } else {
      addError(`l1.agent.training_certified.invalid`, `Training certification for ${agent.name} is non-existent.`)
    }
  }
  return validation_errors
}
