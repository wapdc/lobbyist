import * as rds from "@wapdc/common/wapdc-db"

class L7Processor {
  async validateL7(l7_report) {
    const validation_errors = []

    const validations = [
      {
        condition: '! l7_report',
        code: 'l7_report.incomplete',
        message: 'L7 required'
      },
      {
        condition: '! l7_report.report_type',
        code: 'l7_report.report_type.incomplete',
        message: 'Report type required'
      },
      {
        condition: '! l7_report.version',
        code: 'l7_report.version.incomplete',
        message: 'Version required'
      },
      {
        condition: 'l7_report.version !== "1.0"',
        code: 'l7_report.version.invalid',
        message: 'Version invalid'
      },
      {
        condition: '! l7_report.lobbyist',
        code: 'l7_report.lobbyist.incomplete',
        message: 'Lobbyist required'
      },
      {
        condition: '! l7_report.lobbyist?.name',
        code: 'l7_report.lobbyist.name.incomplete',
        message: 'Lobbyist name required'
      },
      {
        condition: '! l7_report.lobbyist?.address',
        code: 'l7_report.lobbyist.address.incomplete',
        message: 'Lobbyist address required'
      },
      {
        condition: '! l7_report.lobbyist?.city',
        code: 'l7_report.lobbyist.city.incomplete',
        message: 'Lobbyist city required'
      },
      {
        condition: '! l7_report.lobbyist?.state',
        code: 'l7_report.lobbyist.state.incomplete',
        message: 'Lobbyist state required'
      },
      {
        condition: '! l7_report.lobbyist?.postcode',
        code: 'l7_report.lobbyist.postcode.incomplete',
        message: 'Lobbyist postcode required'
      },
      {
        condition: '! l7_report.employee_name',
        code: 'l7_report.employee_name.incomplete',
        message: 'Empoyee name required'
      },
      {
        condition: '! l7_report.lobbyist_employment',
        code: 'l7_report.lobbyist_employment.incomplete',
        message: 'Lobbyist employment required'
      },
      {
        condition: '! l7_report.lobbyist_employment?.description',
        code: 'l7_report.lobbyist_employment.description.incomplete',
        message: 'Lobbyist employment description required'
      },
      {
        condition: '! l7_report.lobbyist_employment?.description',
        code: 'l7_report.lobbyist_employment.description.incomplete',
        message: 'Lobbyist employment description required'
      },
      {
        condition: '! l7_report.public_employment',
        code: 'l7_report.public_employment.incomplete',
        message: 'Public employment required'
      },
      {
        condition: '! l7_report.public_employment?.description',
        code: 'l7_report.public_employment.description.incomplete',
        message: 'Public employment description required'
      },
      {
        condition: '! l7_report.certification',
        code: 'l7_report.certification.incomplete',
        message: 'Certification required'
      },
      {
        condition: '! l7_report.certification?.name',
        code: 'l7_report.certification.name.incomplete',
        message: 'Certification name required'
      },
      {
        condition: '! l7_report.certification?.statement',
        code: 'l7_report.certification.statement.incomplete',
        message: 'Certification statement required'
      },
      {
        condition: 'l7_report.certification?.statement.toLowerCase().trim() !== "i certify"',
        code: 'l7_report.certification.statement.invalid',
        message: 'Certification statement invalid'
      },
      {
        condition: '! l7_report.certification?.text',
        code: 'l7_report.certification.text.incomplete',
        message: 'Certification text required'
      },
    ]

    for(const validation of validations) {
      if (eval(validation.condition)) {
        const error = { [validation.code]: validation.message }
        validation_errors.push(error)
        return validation_errors
      }
    }

    return []
  }

  async getL7Reports(target_id, target_type) {
    return rds.query(`select submission_id
                                   , superseded_id
                                   , version
                                   , submitted_at
                                   , user_data
                                   , L7.report_id as report_id
                                   , current_submission_id
                                   , target_id
                                   , target_type
                              from l7_submission L7s
                                       join l7 on l7.current_submission_id = l7s.submission_id
                                  and l7.report_id = l7s.report_id
                              where target_id = $1
                                and target_type = $2
                              order by submitted_at desc;`, [target_id, target_type])
  }

  async getL7History(report_id) {
    return rds.query(
      `
        select submission_id, superseded_id, submitted_at, user_data
        from l7_submission
        where report_id = $1
        order by submitted_at desc;
      `, [report_id]
    )
  }

  async getL7Submission(submission_id) {
    return rds.fetch(`select superseded_id
                               , report_id
                               , version
                               , submitted_at
                               , user_data
                          from wapdc.public.l7_submission L7s
                          where submission_id = $1;`, [submission_id])
  }

  async submitL7(l7_report, target_type, target_id, uid, realm, old_report_id = null) {
    const errors = await this.validateL7(l7_report)

    if (errors.length > 0) {
      const error_message = errors.reduce((acc, error, index) => {
        return acc + Object.values(error)[0] + ((index !== errors.length - 1) ? ', ' : '')
      }, '')

      throw new Error(error_message)
    }

    // should we have user_name in claims? if so replace this
    let user_name = await rds.fetch('select user_name from public.pdc_user where uid = $1 and realm = $2', [uid, realm])
    l7_report.certification.user_name = user_name['user_name']

    const { report_id } = await rds.fetch('select l7_submit($1, $2, $3, $4) as report_id', [l7_report, target_type, target_id, old_report_id])
    return report_id
  }
}

const l7Processor = new L7Processor()
export {l7Processor}
