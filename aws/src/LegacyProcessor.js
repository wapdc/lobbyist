import * as rds from '@wapdc/common/wapdc-db'
import { emptyL1 } from './L1Processor.js'

class LegacyProcessor {
  arrayFields = [
    'field_compensation',
    'field_personal_expenses',
    'field_sub_lobbyist_comp',
    'field_entertainment',
    'field_non_item_ent',
    'field_contributions',
    'field_cont_emp_pac',
    'field_cont_no_pac',
    'field_employer_transmit',
    'field_employers_pac',
    'field_advertising',
    'field_political_ads',
    'field_other',
    'field_sub_mat',
    'field_select_your_filing_periods',
    'field_lob_firm_agents',
    'field_contributions_to_report',
    'field_political_contributions',
    'field_other_l3_expenditures',
    'field_expend_not_listed',
    'field_ie_support',
    'field_itemized_l3_expenditures',
    'field_l3_emp_comp',
    'field_l3_entertainment',
    'field_l3_pac',
    'field_l3_prof_svcs',
    'field_lobbyist',
    'field_totals_correction',
    'field_description_of_employment',
    'field_employer_area_of_interests',
    'field_ballot_proposition',
    'field_l3_emp_comp',
    'field_exemption_periods',
    'field_funders',
    'field_address',
    'field_name_registered_lob',
    'field_contact_name',
    'field_lob_compensation_amount',
    'field_lob_compensation_period',
    'field_firm_entity_reference',
    'field_employer_entity_reference',
  ];

  preProcessLegacyRecord(originalRecord){
    let formattedRecord = {};
    let oRecord = JSON.parse(JSON.stringify(originalRecord));
    if (typeof oRecord === 'object') {
      for (const [key, value] of Object.entries(oRecord)) {
        let readableKey = this.getReadableKey(key);
        formattedRecord[readableKey] = this.getValue(key, value);
        if (Array.isArray(formattedRecord[readableKey])) {
          for (let i = 0; i < formattedRecord[readableKey].length; i++) {
            formattedRecord[readableKey][i] = this.preProcessLegacyRecord(formattedRecord[readableKey][i]);
          }
        } else if ( formattedRecord[readableKey] && typeof (formattedRecord[readableKey]) === 'object') {
          if (formattedRecord[readableKey].hasOwnProperty('value')) {
            formattedRecord[readableKey] = formattedRecord[readableKey].value
          }
          else {
            formattedRecord[readableKey] = this.preProcessLegacyRecord(formattedRecord[readableKey]);
          }
        }
      }
    }
    return formattedRecord;
  }

  getReadableKey(key){
    let readableKey = JSON.parse(JSON.stringify(key)).replace("__", "_");
    readableKey = readableKey.replace("field_", "");
    readableKey.replace("select_", "");
    readableKey.replace("_chbx", "");
    return readableKey;
  }

  getValue(current_field, current_value) {
    // helper function
    function getInnerValue(current_value, is_array_field) {
      const inner_value = current_value['und'][0]
      // If it's an object with a single key "value", return the value directly
      if (inner_value && typeof inner_value === 'object' && Object.keys(inner_value).length === 1 && current_value['und'].length === 1) {
        if (inner_value['value']) {
          return inner_value['value']
        }
        if (inner_value['safe_value']) {
          return inner_value['safe_value']
        }
        if (inner_value['target_id']) {
          return parseInt(inner_value['target_id'])
        }
      }
      // default case
      return Boolean(is_array_field) ? current_value['und'] : inner_value
    }

    // Extract the value directly if it's a single object inside an array
    if (current_value && current_value['und'] && Array.isArray(current_value['und']) && current_value['und'].length > 0) {
      if (this.arrayFields.includes(current_field)) {
        return getInnerValue(current_value, true)
      }
      // if not an array field but has a value
      if (current_value['und'][0]) {
        return getInnerValue(current_value, false)
      }
      return null
    }
    return current_value
  }

  getLegacyPeriod(value) {
    switch (value) {
      case 'month':
        return 'Per Month'
      case 'day':
        return 'Per Day'
      case 'hour':
        return 'Per Hour'
      case 'hourly':
        return 'Per Hour'
      case 'year':
        return 'Per Year'
      case 'other':
        return 'Other'
      default:
        return null
    }
  }

  async convertL1(legacy_record, with_period_year_months=true) {
    legacy_record = this.preProcessLegacyRecord(legacy_record)
    const user_data = structuredClone(emptyL1)

    const taxonomy = await rds.query("select * from accesshub_taxonomy_term_data")
    function convertTid(tid) {
      return (taxonomy.find(r => parseInt(r.tid) === parseInt(tid))?.name) ?? null
    }

    function convertAmount(amount) {
      return amount === null ? null : parseFloat(amount?.toString())
    }

    function getValue(current_value) {
      if (Array.isArray(current_value) && current_value.length === 1) {
        return current_value[0].value
      }
      return null
    }

    // compensation
    user_data.compensation.period = this.getLegacyPeriod(getValue(legacy_record.lob_compensation_period))
    user_data.compensation.amount = convertAmount(getValue(legacy_record.lob_compensation_amount))
    user_data.compensation.description = legacy_record.description_of_employment.map(description => convertTid(description.tid))
    user_data.compensation.other = null

    // expenses
    user_data.expenses.has_agreement = Boolean(legacy_record.have_reimbursement_agreeme === '1')
    user_data.expenses.amount = convertAmount(legacy_record.amount_of_reimbursement)
    user_data.expenses.period = this.getLegacyPeriod(legacy_record.per_period)
    user_data.expenses.incidentals = Boolean(legacy_record.are_you_reimbursed_for_lob === '1')
    user_data.expenses.pay_directly = Boolean(legacy_record.emp_expense_pay_chkbox === 'yes')
    user_data.expenses.explanation = legacy_record.emp_explain_pay

    // areas_of_interest
    user_data.areas_of_interest = legacy_record.employer_area_of_interests.map(area => convertTid(area.tid))

    if (! with_period_year_months) {
      return user_data
    }

    // period_year_months
    const period_year_months = legacy_record.select_your_filing_periods.map(period => ({year_month: period.value.slice(0, 7)}))
    for (const exemption_period of legacy_record.exemption_periods) {
      for (const period_year_month of period_year_months) {
        if (period_year_month.year_month === exemption_period.value.slice(0, 7)) {
          period_year_month.exempt = true
        }
      }
    }
    for (const period_year_month of period_year_months) {
      if (period_year_month.exempt === undefined) {
        period_year_month.exempt = false
      }
    }
    user_data.period_year_months = period_year_months

    return user_data
  }

  async convertL2(legacyRecord) {
    // First fix the payload
    const l2 = this.preProcessLegacyRecord(legacyRecord)
    const newL2 = {}

    function convertOccassionType(value) {
      const types = [
        { value: '1', label: 'Entertainment' },
        { value: '2', label: 'Reception'},
        { value: '3', label: 'Travel lodging and subsistence'},
        { value: '4', label: 'Enrollment and course fees'},
      ]
      return (types.find(r => r.value === value)?.label)?? null
    }

    // Helper function to format data.
    function convertDate(date) {
      if (date) {
        return date.substring(0,10)
      }
      return null
    }

    // Helper function to massage certain field types.
    function fixEntries(entries, map = {}) {
      return entries.map(entry => {
        const newEntry = {}
        for (const [property, value] of Object.entries(entry) ) {
          const newProperty = map[property]
          if (newProperty) {
            switch (newProperty) {
              case "subject":
                newEntry[newProperty] = [value.name]
                break;
              case "type_of_occasion":
                newEntry[newProperty] = convertOccassionType(value)
                break;
              case "occasion_date":
                newEntry[newProperty] = convertDate(value)
                break;
              case "cost":
              case "amount":
                newEntry[newProperty] = value === null ? null :  parseFloat(value?.toString())
                break;
              case "source":
                newEntry[newProperty] = parseInt(value.toString());
                break;
              default:
                newEntry[newProperty] = value ? value : null;
            }
          }
        }
        return newEntry
      })
    }

    // Convert amount
    function convertAmount(amount) {
      return amount === null ? null :  parseFloat(amount?.toString())
    }

    const l2Map = {
      personal_expenses: {
        field: 'personal_expenses',
        map: {amount: 'amount', employer: 'source'},
        convert: fixEntries
      },
      compensation: {
        field: 'compensation',
        map: {amount: 'amount', employer:'source'},
        convert: fixEntries
      },
      sub_lobbyist_payments: {
        field: 'sub_lobbyist_comp',
        map: {amount: 'amount', sub_lobbyist:'source'},
        convert: fixEntries
      },
      non_reimbursed_amount_: {
        field: 'non_reimbursed_amount',
        convert: convertAmount
      },
      entertainment: {
        field: 'entertainment',
        map: {cost: 'cost', occasion_date: 'occasion_date',
          occasion_description: 'occasion_description',
          place: 'place', employer: 'source',
          type_of_occasion: 'type_of_occasion',
          participant_listing: 'participant_listing'},
        convert: fixEntries,
      },
      entertainment_small: {
        field: 'non_item_ent',
        map: {amount: 'amount', employer : 'source'},
        convert: fixEntries
      },
      lobbying: {
        field: 'sub_mat',
        map: {mat_of_prop: 'subject', employer: 'source', issue_or_bill_number: 'issue_or_bill_number', legislative_agency: 'who_lobbied'},
        convert: fixEntries
      },
      contributing_pacs: {
        field: 'employers_pac',
        map: {pac_name: 'pac_name', employer: 'source'},
        convert: fixEntries
      },
    }

    // Determine if there are non-transferable properties with data on the report.
    // If so mark the conversion as incomplete so that we can warn the user.
    const nonTransferable = ['advertising', 'contributions', 'other', 'political_ads', 'employer_transmit', 'cont_emp_pac', 'cont_no_pac']
    const foundNonTransferable = nonTransferable.filter(f => l2[f].length > 0)
    newL2.conversion_incomplete = foundNonTransferable.length > 0
    // IF we had none of the non-converted data assume these categories are also empty in the converted report.
    if (!newL2.conversion_incomplete) {
      newL2.has_political_ads = false;
      newL2.has_contributions = false;
      newL2.has_contributions_small = false;
      newL2.has_contributions_in_kind = false;
      newL2.has_contributing_pacs = false;
      newL2.has_printing = false;
      newL2.has_public_relations = false;
      newL2.has_consulting = false;
      newL2.has_expenses_other = false;
      newL2.has_expenses_indirect = false;
      newL2.has_independent_expenditures_candidate = false;
      newL2.has_independent_expenditures_ballot = false;
      newL2.has_any_indirect = false;
    }

    for (const [property, config] of Object.entries(l2Map)) {
      if (l2[config.field]) {
        // Array type means we have to set has_ property
        if (config.convert === fixEntries) {
            newL2[`has_${property}`] = (l2[config.field].length > 0);
        }
        newL2[property] = config.convert(l2[config.field], config.map)
      }
    }
    return newL2
  }

  async convertL3(legacyRecord) {
    // First fix the payload
    const l3 = this.preProcessLegacyRecord(legacyRecord)

    const newL3 = {}
    const taxonomy = await rds.query("select * from accesshub_taxonomy_term_data")

    // Helper function to format dates
    function convertDate(date) {
      if (date) {
        return date.substring(0,10)
      }
      return null
    }

    function convertTid(tid) {
      return (taxonomy.find(r => parseInt(r.tid) === parseInt(tid))?.name) ?? null
    }

    // Helper function to massage certain field types.
    function fixL3Entries(entries, map = {}) {

      // Grab contract for lobbyist in the convert L3 process
      async function getContract(contract_id) {
        const { name } =  await rds.fetch('SELECT f.name from lobbyist_contract lc JOIN lobbyist_firm f on lc.lobbyist_firm_id = f.lobbyist_firm_id WHERE lc.lobbyist_contract_id = $1', [contract_id])
        return name
      }

      function convertCompensationRanges(range) {
        let converted_tid = convertTid(range.tid)
          // switch case to find substring amount using old ranges: 4,499; 23,999; 47,999; 119,999; 120,000; Return null if can't be compared accurately to current ranges.
          switch (true) {
            case converted_tid.includes("4,499"):
            case converted_tid.includes("23,999"):
              return "less than $30,000"
            default:
              return null
          }
      }

      return entries.map(entry => {
        const newEntry = {}
        for (const [property, value] of Object.entries(entry) ) {
          const newProperty = map[property]
          if (newProperty) {
            switch (newProperty) {
              case "stance":
                newEntry[newProperty] = value === "1" ? "For" : "Against"
                break;
              case "title":
              case "relationship":
              case "code":
                newEntry[newProperty] = convertTid(value.tid)
                break;
              case "cost":
              case "expenses_l2":
              case "compensation_l2":
              case "amount":
                if ((typeof value) === 'string'){
                  newEntry[newProperty] = value === null ? null : parseFloat(value?.toString())
                } else {
                  // else amount is an old legacy value
                  convertCompensationRanges(value)
                  newEntry[newProperty] = value === null ? null : convertCompensationRanges(value)
                }
                break;
              case "recipient_type":
                newEntry[newProperty] = 'candidacy'
                break;
              case "date":
                newEntry[newProperty] = convertDate(value)
                break;
              case "source":
                newEntry[newProperty] = parseInt(value.toString())
                break;
              case "lobbyist_contract_id":
                newEntry[newProperty] = value;
                getContract(value).then(v => {
                  newEntry['lobbyist_title'] = v
                })
                break;
              default:
                newEntry[newProperty] = value ? value : null;
            }
          }
        }
        return newEntry
      })
    }

    const l3Map = {
      has_entertainment_expenditures: {
        field: 'do_you_have_entertainment_'
      },
      has_pac_contributions: {
        field: 'do_you_have_a_pac_to_list_'
      },
      payments_for_registered_lobbyists: {
        field: 'expend_not_listed',
        subfield: 'vendor'
      },
      witnesses_retained_for_lobbying_services: {
        field: 'expend_not_listed',
        subfield: 'expert_retainer'
      },
      costs_for_promotional_materials: {
        field: 'expend_not_listed',
        subfield: 'inform_material'
      },
      grassroots_expenses_for_lobbying_communications: {
        field: 'expend_not_listed',
        subfield: 'lobbying_comm'
      },
      itemized_expenditures: {
        field: 'itemized_l3_expenditures',
        map: {date: 'date', amount: 'amount', field_name: 'recipient', short_description: 'description'},
        convert: fixL3Entries
      },
      entertainment_expenditures: {
        field: 'l3_entertainment',
        map: {entertain_name: 'name', relationship: 'title', amount: 'amount', date: 'date', description: 'description'},
        convert: fixL3Entries
      },
      other_expenditures: {
        field: 'other_l3_expenditures',
        map: {date: 'date', amount: 'amount', field_name: 'recipient', description: 'description'},
        convert: fixL3Entries
      },
      non_itemized_political_contribution_amount: {
        field: 'aggregate_contrib'
      },
      itemized_contributions: {
        field: 'political_contributions',
        map: {date: 'date', amount: 'amount', field_name: 'import_recipient', id: 'recipient_type'},
        convert: fixL3Entries
      },
      pac_contributions: {
        field: 'l3_pac',
        map: {pac_name: 'pac_name'},
        convert: fixL3Entries
      },
      candidate_independent_expenditures: {
        field: 'ie_support',
        map: {candidate_s_name: 'name',  date: 'date', amount: 'amount', supported_or_opposed: 'stance', description: 'description', candidacy_id: null},
        convert: fixL3Entries
      },
      ballot_proposition_expenditures: {
        field: 'ballot_proposition',
        map: {date: 'date', amount: 'amount', supported_or_opposed: 'stance', ie_description: 'description', description: 'ballot_number'},
        convert: fixL3Entries
      },
      employment_compensation: {
        field: 'l3_emp_comp',
        map: {code: 'amount', name: 'name', description: 'description', relationship: 'relationship'},
        convert: fixL3Entries
      },
      professional_services_compensation: {
        field: 'l3_prof_svcs',
        map: {amount: 'code' , firm_name: 'firm_name',field_name: 'person_name', description: 'description'},
        convert: fixL3Entries
      },
      lobbyist: {
        field: 'totals_correction',
        map: { comp: 'compensation_l2', exp: 'expenses_l2', lobbyist: 'lobbyist_contract_id', lobbyist_title: 'lobbyist_title'},
        convert: fixL3Entries
      }
    }

    let d = new Date(l3.filing_period_year)
    newL3.period_start =  d.getFullYear() + '-01-01'
    newL3.period_end =  d.getFullYear() + '-12-31'
    newL3.has_in_kind_contributions = null;
    newL3.has_itemized_contributions = false;
    newL3.has_itemized_expenditures = false;
    newL3.has_other_expenditures = false;
    newL3.has_ballot_proposition_expenditures = false;
    newL3.has_candidate_independent_expenditures = false;
    newL3.has_employment_compensation = false;
    newL3.has_entertainment_expenditures = false;
    newL3.has_pac_contributions = false;
    newL3.has_professional_services_compensation = false;

    for (const [property, config] of Object.entries(l3Map)) {
      if (l3[config.field]) {
        // Array type means we have to set has_ property
        if (config.convert === fixL3Entries && property != 'lobbyist') {
          newL3[`has_${property}`] = (l3[config.field].length > 0);
        }
        if (config.convert) {
          newL3[property] = config.convert(l3[config.field], config.map)
        } else {
          let field_value = l3[config.field]
          if (config.subfield) {
            if (field_value[0]) {
              //string is passed from Drupal but integer expected
              newL3[property] = parseFloat(field_value[0][config.subfield])
            } else {
              newL3[property] = 0
            }
          } else {
            newL3[property] = parseFloat(l3[config.field])
          }
        }
      }
    }
    return newL3
  }
}

const legacyProcessor = new LegacyProcessor()
export {legacyProcessor}
