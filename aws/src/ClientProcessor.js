import * as rds from "@wapdc/common/wapdc-db"
import {legacyProcessor} from "./LegacyProcessor.js";

class ClientProcessor {
    async getClients() {
        return await rds.query(`SELECT *
                                FROM lobbyist_client
                                ORDER BY name`);
    }

    async getClient(id) {
        return rds.fetch(`
            select *
            from lobbyist_client c
            where c.lobbyist_client_id = $1
        `, [id])
    }

    async getClientUsers() {
        return await rds.query(`
            SELECT c.lobbyist_client_id AS "client_id",
                   c.name               AS "client_name",
                   c.email              AS "client_email",
                   c.phone              AS "client_phone",
                   u.user_name          AS "user_name"
            FROM pdc_user u
                     JOIN pdc_user_authorization ua
                          ON u.uid = ua.uid
                              AND u.realm = ua.realm
                     JOIN lobbyist_client c
                          ON ua.target_id = c.lobbyist_client_id
                              AND ua.target_type = 'lobbyist_client';
        `);
    }

    async getL3Submissions(id, hasDraftAccess) {
        return rds.query(`
            select rp.report_year,
                   l3.report_id,
                   s.submission_id,
                   s.submitted_at,
                   rpv.due_date,
                   dd.draft_id, 
                   case
                       when l3.report_id is not null and dd.draft_id is not null and $2::bool then 'Amending'
                       when l3.report_id is not null then 'Filed'
                       when coalesce(dd.draft_id, ld.draft_id) is not null and $2::bool then 'Started'
                       when now()  > rpv.due_date + interval '1 day' then 'Overdue'
                       end                                         as status,
                   case when ld.draft_id is not null then true end as legacy_draft,
                   ld.user_data ->> 'nid'                          as drupal_nid
            from (select lrp.lobbyist_client_id, extract(year from lrp.period_start) as report_year
                  from lobbyist_reporting_periods lrp
                  where lrp.period_start <= now()
                  group by lrp.lobbyist_client_id, extract(year from lrp.period_start)) rp
                     left join reporting_period rpv on rpv.start_date = cast(rp.report_year || '-01-01' as date)
                and rpv.report_type = 'L3'
                     left join L3 on l3.report_year = rp.report_year and rp.lobbyist_client_id = l3.lobbyist_client_id
                     left join l3_submission s on l3.current_submission_id = s.submission_id
                     left join private.draft_document dd on dd.target_type='lobbyist_client'
                        and dd.target_id = rp.lobbyist_client_id
                        and dd.report_type = 'L3'  
                        and dd.report_key=cast(rp.report_year as text)
                        and $2::bool
                     left join private.draft_document ld on ld.report_type = 'L3-Legacy'
                        and ld.target_id = rp.lobbyist_client_id
                        and ld.target_type = 'lobbyist_client'
                        and ld.report_key = rp.report_year::text
                        and $2::bool
            where rp.lobbyist_client_id = $1
            order by rp.report_year desc
        `, [id, hasDraftAccess])
    }

    async getClientFirms(id) {
        return rds.query(`
            select distinct on (lrp.year, lc.lobbyist_firm_id)
                f.name, lc.lobbyist_contract_id, lrp.year as contract_year, lrp.months, lc.current_submission_id
            from (select
                      lrp.lobbyist_contract_id,
                      date_part('year', lrp.period_start) as year,
                      json_agg(json_build_object('month', date_part('month', lrp.period_start), 'filed', l2.report_id, 'exempt', lrp.exempt)) as months
                  from lobbyist_reporting_periods lrp
                           left join l2 on lrp.lobbyist_firm_id = l2.lobbyist_firm_id
                      and lrp.period_start = l2.period_start
                  group by date_part('year', lrp.period_start), lrp.lobbyist_contract_id
                 ) lrp join lobbyist_contract lc on lrp.lobbyist_contract_id = lc.lobbyist_contract_id
                       join lobbyist_firm f on f.lobbyist_firm_id = lc.lobbyist_firm_id
            where lobbyist_client_id = $1 and lc.contractor_id is null
        `, [id])
    }

  getLegacyContractsToApprove(client_id) {
    return rds.query(`
      SELECT dd.report_key as drupal_nid,
             COALESCE(lf.name, user_data->>'title') as title,
             dd.updated_at
        from private.draft_document dd
        left join lobbyist_firm lf on lf.lobbyist_firm_id=cast(user_data->'field_firm_entity_reference'->'und'->0->>'target_id' as int)
        WHERE dd.report_type = 'L-Contract-Legacy' and dd.target_type='lobbyist_client'
          and dd.target_id=$1
      UNION
      select sub.user_data->>'nid' as drupal_nid,
             COALESCE(lf.name, user_data->>'title') as title,
             to_timestamp(CAST(sub.user_data->>'revision_timestamp' as numeric)) as updated_at
      from lobbyist_contract lc
               join lobbyist_contract_submission sub on sub.submission_id = lc.current_submission_id
               join lobbyist_firm lf on lf.lobbyist_firm_id = lc.lobbyist_firm_id
      where sub.user_data->>'field_authorization_date' = '[]' and lc.lobbyist_client_id = $1
      and lc.contractor_id is null
    `, [client_id])
  }

  getContractsToApprove(client_id) {
      return rds.query(`
        SELECT lf.name as firm, dd.draft_id, dd.updated_at,
               case when dd.user_data->>'revision_message' is not null and dd.approval_target_id is not null and (dd.user_data->>'certification' is null or dd.user_data->>'certification' = '{}' )
                        then 'Returned to lobbyist for approval'
                    when dd.user_data->>'certification' is null and dd.approval_target_id is null
                        then 'Awaiting for both lobbyist and client/employer approval'
                    when dd.user_data->>'certification' is not null and dd.approval_target_id is not null
                        then 'Awaiting for client/employer approval'
                    else 'Draft'
                   end as status
        FROM private.draft_document dd
             left join lobbyist_firm lf on lf.lobbyist_firm_id = dd.target_id and dd.target_type='lobbyist_firm'
             WHERE dd.approval_target_type='lobbyist_client'
               and dd.approval_target_id = $1
               AND dd.report_type='L1'
      `, [client_id])
  }

  async getLobbyistClientRegistration(client_id, submission_id) {
    let result = await rds.fetch(`
        select s.*
        from lobbyist_client_submission s
        where s.submission_id = $1 and s.lobbyist_client_id=$2
    `, [submission_id, client_id]);
    result.user_data = legacyProcessor.preProcessLegacyRecord(result.user_data);
    return result;
  }

  async getClientSubmissionHistory(client_id)  {
    return rds.query(`
      SELECT s.* FROM lobbyist_client_submission s
      WHERE s.lobbyist_client_id = $1
    `, [client_id])
  }
}

const clientProcessor = new ClientProcessor()
export { clientProcessor }