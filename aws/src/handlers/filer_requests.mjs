import {claims, getRestApi, waitForCompletion} from "@wapdc/common/rest-api";
import { lobbyistProcessor } from "../LobbyistProcessor.js"
import { l7Processor } from "../L7Processor.js"
import { clientProcessor } from "../ClientProcessor.js";
import { firmProcessor } from "../FirmProcessor.js"
import { publicAgencyProcessor } from '../PublicAgencyProcessor.js'
import { setTestEmailAddress } from "@wapdc/common/mailer";
import {l1Processor} from "../L1Processor.js";
import {l2Processor} from "../L2Processor.js";
import {l3Processor} from "../L3Processor.js";

const api = getRestApi({compressed: true});


/**
 * GET /user endpoint. Returns the claims attached to the request.
 *
 * @param {object} req - The HTTP request object.
 * @param {object} res - The HTTP response object.
 */
api.get('/filer/user', async (req, res) => {
  res.json({ claims, success: true });
});

api.use(['/firm/:firm_id', '/firm/:firm_id/*'], async(req, res, next) => {
  const { firm_id } = req.params
  const require_modify = req.method !== 'GET'
  const has_permission =
    (!require_modify && claims.admin)
    || (firm_id && (await lobbyistProcessor.hasAccessToTarget(claims, 'lobbyist_firm', firm_id, require_modify)))
  if (! has_permission) {
    console.error("Access denied to lobbyist firm", claims)
    return res.error({ type: "Access denied", message: "Access denied (lobbyist firm)", statusCode: 403 })
  }
  setTestEmailAddress(claims.email ?? null)
  next()
})

api.use(['/client/:client_id', '/client/:client_id/*'], async(req, res, next) => {
  const { client_id } = req.params
  const require_modify = req.method !== 'GET'
  const has_permission =
    (!require_modify && claims.admin)
    || (client_id && (await lobbyistProcessor.hasAccessToTarget(claims, 'lobbyist_client', client_id, require_modify)))
  if (! has_permission) {
    console.error("Access denied to lobbyist client", claims)
    return res.error({ type: "Access denied", message: "Access denied (lobbyist client)", statusCode: 403 })
  }
  next()
})

api.use(['/access-request/:request_id'], async(req, res, next) => {
  const {request_id} = req.params
  const has_permission = request_id && await lobbyistProcessor.hasAccessToRequest(claims, request_id)
  if(! has_permission){
    console.error("Access denied to public agency request", claims)
    return res.error({type: "Access denied", message: "Access denied (lobbyist access request)", statusCode: 403})
  }
  next()
})

api.use(['/public-agency/:agency_id', '/public-agency/:agency_id/*'], async (req, res, next) => {
  const { agency_id } = req.params
  const require_modify = req.method !== 'GET'
  const has_permission = agency_id && await lobbyistProcessor.hasAccessToTarget(claims, 'lobbyist_public_agency', agency_id, require_modify)
  if (! has_permission) {
    console.error("Access denied to public agency", claims)
    return res.error({ type: "Access denied", message: "Access denied (lobbyist_public_agency)", statusCode: 403 })
  }
  next()
})

api.get('/client/:client_id', async (req, res) => {
  res.json(await clientProcessor.getClient(parseInt(req.params.client_id)));
})

api.get('/client/:client_id/client-registration/submission-history', async (req, res) => {
  res.json(await clientProcessor.getClientSubmissionHistory(req.params.client_id));
})

api.get('/firm/:firm_id', async (req, res) => {
  res.json({firm: await firmProcessor.getFirm(req.params.firm_id)});
})

api.get('/firm/:firm_id/firm-registration/submission-history', async (req, res) => {
  res.json(await firmProcessor.getFirmSubmissionHistory(req.params.firm_id));
})

api.get('/client/:client_id/l7-reports', async (req, res) => {
  res.json(await l7Processor.getL7Reports(parseInt(req.params.client_id), 'lobbyist_client'))
})

api.get('/client/:client_id/reports', async (req, res) => {
  const hasDraftAccess = await lobbyistProcessor.hasDraftAccessToTarget(claims, 'lobbyist_client', req.params.client_id, false)
  const l7Reports = await l7Processor.getL7Reports(parseInt(req.params.client_id), 'lobbyist_client');
  const l3Reports = await clientProcessor.getL3Submissions(req.params.client_id, hasDraftAccess);
  const firms = await clientProcessor.getClientFirms(req.params.client_id);
  const legacyContractsToApprove = await clientProcessor.getLegacyContractsToApprove(req.params.client_id);
  const contractsToApprove = await clientProcessor.getContractsToApprove(req.params.client_id)
  res.json({'l7Reports': l7Reports, 'l3_reports': l3Reports, firms, legacyContractsToApprove, contractsToApprove});
})

api.get('/firm/:firm_id/l7-reports', async (req, res) => {
  res.json(await l7Processor.getL7Reports(parseInt(req.params.firm_id), 'lobbyist_firm'))
})

api.get('/firm/:firm_id/reports', async (req, res) => {
  const hasDraftAccess = await lobbyistProcessor.hasDraftAccessToTarget(claims, 'lobbyist_firm', req.params.firm_id, false)
  const l7Reports = await l7Processor.getL7Reports(parseInt(req.params.firm_id), 'lobbyist_firm');
  const l2Periods = await firmProcessor.getL2Periods(req.params.firm_id, hasDraftAccess);
  const legacySubContractsToApprove = await firmProcessor.getLegacySubContractsToApprove(req.params.firm_id);
  const subContractsToApprove = await firmProcessor.getSubContractsToApprove(req.params.firm_id);
  const clients = await firmProcessor.getFirmClients(req.params.firm_id);
  const subcontracted_lobbyists = await firmProcessor.getSubcontractedLobbyists(req.params.firm_id);
  const employment_contracts = await firmProcessor.getEmploymentContracts(req.params.firm_id);
  const agents = await firmProcessor.getFirmAgents(req.params.firm_id);
  const pendingLegacyContracts = await firmProcessor.getPendingLegacyContracts(req.params.firm_id);
  const pendingL1Contracts = await firmProcessor.getPendingL1Contracts(req.params.firm_id);
  const profiles = await firmProcessor.getProfiles(req.params.firm_id);
  res.json({profiles,  l7Reports, l2Periods, clients, agents,
    subcontracted_lobbyists, employment_contracts,
    legacySubContractsToApprove, subContractsToApprove, pendingLegacyContracts,
    pendingL1Contracts});
})

api.get('/firm/:firm_id/profile-submission/:submission_id', async(req, res) => {
  const {firm_id, submission_id}  = req.params;
  const profile = await firmProcessor.getFirmProfileSubmission(firm_id, submission_id)
  res.json(profile)
})

api.get('/firm/:firm_id/profile/:profile_id/history', async(req, res) => {
  const {firm_id, profile_id} = req.params
  res.json(await firmProcessor.getFirmProfileHistory(firm_id, profile_id))
})

api.get('/firm/:firm_id/l2/:submission_id', async (req, res) => {
  res.json(await firmProcessor.getL2(req.params.firm_id, req.params.submission_id))
})

api.get('/client/:client_id/l3/:submission_id', async (req, res) => {
  res.json(await l3Processor.getL3(req.params.client_id, req.params.submission_id))
})

api.get('/client/:client_id/l7-reports/:report_id/history', async (req, res) => {
  res.json(await l7Processor.getL7History(req.params.report_id))
})

api.get('/firm/:firm_id/l7-reports/:report_id/history', async (req, res) => {
  res.json(await l7Processor.getL7History(req.params.report_id))
})

api.get('/my-firms-and-clients', async (req, res) => {
  res.json(await lobbyistProcessor.getUserFirmsAndClients(claims.uid, claims.realm))
})

api.get('/all-clients', async (req, res) => {
  res.json(await clientProcessor.getClients())
})

api.get('/all-firms', async (req, res) => {
  res.json(await firmProcessor.getFirms())
})

api.get('/clients', async (req, res) => {
  res.json(await l1Processor.getClientListByContractor(req.query.firm_id))
})

api.get('/firm/:firm_id/firm-registration/:submission_id', async(req, res) =>{
  res.json(await firmProcessor.getLobbyistFirmRegistration(req.params.firm_id, req.params.submission_id))
});

api.get('/client/:client_id/client-registration/:submission_id', async(req, res) => {
  res.json(await clientProcessor.getLobbyistClientRegistration(req.params.client_id, req.params.submission_id))
})

api.get('/public-agency/agencies', async(req, res) =>{
  const agencies = await publicAgencyProcessor.getAgencies();
  res.json( { agencies, success: true})
});

api.get('/public-agency/:agency_id/users', async(req, res) => {
  const authorized_users = await lobbyistProcessor.getAuthorizedUsers(req.params.agency_id, 'lobbyist_public_agency');
  res.json({authorized_users})
})

api.get('/client/:client_id/submission/:submission_id', async (req, res) => {
  res.json(await l7Processor.getL7Submission(parseInt(req.params.submission_id)))
})
api.get('/firm/:firm_id/submission/:submission_id', async (req, res) => {
  res.json(await l7Processor.getL7Submission(parseInt(req.params.submission_id)))
})
/**
 * Takes a public agency filer request and submit it filer_request table for verification
 *
 * Post data should be json object containing the following two properties:
 *   payload - The content of the public agency profile to submit.
 * @param {object} req - The HTTP request object.
 * @param {object} res - The HTTP response object.
 */
api.post('public-agency/request', async (req, res) => {
  const { target_id, payload } = req.body;
  const submitRequestResponse = await lobbyistProcessor.submitFilerRequest(claims.uid, claims.realm, target_id, 'lobbyist_public_agency', payload);
  res.json({submitRequestResponse, success: true});
})


/**
* User Deletes public agency filer request
*/
api.delete('access-request/:request_id', async (req, res) => {
  res.json(await lobbyistProcessor.deleteFilerRequest(req.params.request_id))
})

api.post('public-agency/revoke', async (req, res) => {
  const {uid, realm, target_id, target_type} = req.body
  res.json(await lobbyistProcessor.revokeUserAuthorization(uid, realm, target_type, target_id))
})

// submit an l7 report
api.post('/firm/:firm_id/l7-report/submit', async(req, res) => {
  const { l7_report, target_type, target_id } = req.body
  const submission_response = await l7Processor.submitL7(l7_report, target_type, target_id, claims.uid, claims.realm, l7_report?.report_id)
  res.json({ submission_response, success: true })
})
api.post('/client/:client_id/l7-report/submit', async(req, res) => {
  const { l7_report, target_type, target_id } = req.body
  const submission_response = await l7Processor.submitL7(l7_report, target_type, target_id, claims.uid, claims.realm, l7_report?.report_id)
  res.json({ submission_response, success: true })
})

api.post('/firm/:firm_id/l1/draft',async(req, res) => {
  const { client_id, period_start, period_end, contractor_id, current_submission_id} = req.body
  const { firm_id} = req.params
  const draft_id = await l1Processor.makeDraft(firm_id, client_id, period_start, period_end, contractor_id, current_submission_id)
  res.json({ draft_id, success: true })
})

api.post('/firm/:firm_id/l1/:submission_id/amendment', async(req, res) => {
  res.json(await l1Processor.createAmendmentDraft(req.params.firm_id,req.params.submission_id));
})

api.get('/firm/:firm_id/l1/draft/:draft_id', async(req, res) => {
  const { firm_id, draft_id } = req.params
  res.json(await l1Processor.getFirmL1Draft(firm_id, draft_id))
})

api.put('/firm/:firm_id/l1/draft/:draft_id', async (req, res) => {
  const { firm_id, draft_id } = req.params;
  const {save_count, user_data} = req.body
  try {
    const saveResult = await l1Processor.saveL1Draft(firm_id, draft_id, save_count, user_data);
    res.json(saveResult)
  }
  catch (e) {
    if (e.toString().includes('Save count mismatch')) {
      res.error({type: 'Conflict', message: "Save count mismatch", statusCode: 409})
    }
    else {
      throw(e)
    }
  }
})

api.delete('/firm/:firm_id/l1/draft/:draft_id', async(req, res) => {
  const {firm_id, draft_id} = req.params
  await l1Processor.deleteL1Draft(firm_id, draft_id)
  res.json({ success: true })
})

api.post('/firm/:firm_id/l1/draft/:draft_id/certify', async(req, res) => {
  const {firm_id, draft_id} = req.params
  const {certification, message} = req.body
  await l1Processor.firmCertify(firm_id, draft_id, certification, claims, message)
  res.json({ success: true })
})

api.post('/firm/:firm_id/l1/draft/:draft_id/contractor-certify', async(req, res) => {
  const {firm_id, draft_id} = req.params
  const {approval, message} = req.body
  res.json(await l1Processor.clientCertify(firm_id, draft_id, approval, claims, true, message))
})

api.post('/firm/:firm_id/l1/draft/:draft_id/return', async(req, res) => {
  const {firm_id, draft_id} = req.params
  const { message, not_working_with_lobbyist } = req.body
  res.json(await l1Processor.returnL1(firm_id, draft_id, message, not_working_with_lobbyist, claims, true))
})

api.get('/firm/:firm_id/l1/draft/:draft_id/contractor', async(req, res) => {
  const { firm_id, draft_id } = req.params
  res.json(await l1Processor.getContractorL1Draft(firm_id, draft_id))
})

api.put('/firm/:firm_id/l1/contractor-draft/:draft_id', async (req, res) => {
  const { firm_id, draft_id } = req.params;
  const {save_count, user_data} = req.body
  try {
    const saveResult = await l1Processor.saveContractorL1Draft(firm_id, draft_id, save_count, user_data);
    res.json(saveResult)
  }
  catch (e) {
    if (e.toString().includes('Save count mismatch')) {
      res.error({type: 'Conflict', message: "Save count mismatch", statusCode: 409})
    }
    else {
      throw(e)
    }
  }
})


api.get('/client/:client_id/l1/draft/:draft_id', async(req, res) => {
  const { client_id, draft_id } = req.params
  res.json(await l1Processor.getClientL1Draft(client_id, draft_id))
})

api.put('/client/:client_id/l1/draft/:draft_id', async (req, res) => {
  const { client_id, draft_id } = req.params;
  const {save_count, user_data} = req.body
  try {
    const saveResult = await l1Processor.saveClientL1Draft(client_id, draft_id, save_count, user_data);
    res.json(saveResult)
  }
  catch (e) {
    if (e.toString().includes('Save count mismatch')) {
      res.error({type: 'Conflict', message: "Save count mismatch", statusCode: 409})
    }
    else {
      throw(e)
    }
  }
})

api.post('/client/:client_id/l1/draft/:draft_id/certify', async(req, res) => {
  const {client_id, draft_id} = req.params
  const {approval, message} = req.body
  res.json(await l1Processor.clientCertify(client_id, draft_id, approval, claims, false, message))
})

api.post('/client/:client_id/l1/draft/:draft_id/return', async(req, res) => {
  const {client_id, draft_id} = req.params
  const { message, not_working_with_lobbyist } = req.body
  res.json(await l1Processor.returnL1(client_id, draft_id, message, not_working_with_lobbyist, claims, false))
})

api.get('/client/:client_id/l1/:submission_id', async (req, res) => {
  const contract = await l1Processor.getContractSubmissionByClient(req.params.client_id, req.params.submission_id)
  res.json(contract)
})

api.get('/client/:client_id/l1/:submission_id/submission-history', async (req, res) => {
  res.json(await l1Processor.getContractSubmissionHistoryByClient(req.params.client_id, req.params.submission_id));
})

api.get('/client/:client_id/l1/activity-log', async (req, res) => {
  res.json(await l1Processor.getL1ClientActivityLog(req.params.client_id))
})

api.get('/firm/:firm_id/l1/activity-log', async (req, res) => {
  res.json(await l1Processor.getL1FirmActivityLog(req.params.firm_id))
})
/**
 * Submit an l2 draft report
 */
api.post('/firm/:firm_id/l2/:period_start/draft', async(req, res) => {
  const { firm_id, period_start } = req.params
  const { copyFrom, copyOptions } = req.body
  const draft_id = await l2Processor.makeDraft(firm_id, period_start, copyFrom, copyOptions)
  res.json({ draft_id, success: true })
})

/**
 * Get the draft for the given lobbyist firm and period start
 */
api.get('/firm/:firm_id/l2/:period_start/draft', async(req, res) => {
    const { firm_id, period_start } = req.params;
    const l2 = await l2Processor.getDraft( firm_id, period_start )
    res.json( l2 )
})

// submit L2 report
api.post('/firm/:firm_id/l2/:period_start/submit', async(req, res) => {
  const { firm_id, period_start, user_data } = req.body
  const submission_response = await l2Processor.submitL2Report(firm_id, period_start, user_data, claims)
  res.json({ submission_response, success: true })
})

api.get('/firm/:firm_id/report/:report_id/history', async(req, res) => {
    const reports = await l2Processor.getL2History(req.params.firm_id, req.params.report_id);
    res.json(reports);
})

api.get('/client/:client_id/report/:report_id/history', async(req, res) => {
  const reports = await l3Processor.getL3History(req.params.client_id, req.params.report_id);
  res.json(reports);
})

/**
 * Save the draft
 */
api.put('/firm/:firm_id/l2/:period_start/draft', async(req, res) => {
  const { firm_id, period_start } = req.params;
  const {save_count, user_data} = req.body
  try {
    const saveResult = await l2Processor.saveL2Draft(firm_id, period_start, save_count, user_data);
    res.json(saveResult)
  }
  catch (e) {
    if (e.toString().includes('Save count mismatch')) {
      res.error({type: 'Conflict', message: "Save count mismatch", statusCode: 409})
    }
    else {
      throw(e)
    }
  }
})

api.post('/firm/:firm_id/l2/:period_start/amendment', async(req, res) => {
  res.json(await l2Processor.createAmendmentDraft(req.params.firm_id,req.params.period_start));
})

api.delete('firm/:firm_id/l2/draft/:period_start', async (req, res) => {
  await l2Processor.deleteL2Draft(req.params.firm_id, req.params.period_start)
  res.json({success: true})
})

api.get('firm/:firm_id/l1/:submission_id', async (req, res) => {
  const contract = await l1Processor.getContractSubmissionByFirm(req.params.firm_id, req.params.submission_id)
  res.json(contract)
})

api.get('/firm/:firm_id/l1/:submission_id/submission-history', async (req, res) => {
  res.json(await l1Processor.getContractSubmissionHistoryByFirm(req.params.firm_id, req.params.submission_id));
})

api.get('/firm/:firm_id/contract-submission/:client_id/submission-history', async (req, res) => {
  res.json(await l1Processor.getContractsListForEmployer(req.params.firm_id, req.params.client_id, req.params.name));
})


api.get('/firm/subjects', async (req, res) => {
  res.json(await l2Processor.getSubjects());
})

/**
 * Create a new draft l3
 */
api.post('/client/:client_id/l3/:year/draft', async(req, res) => {
  const { client_id, year } = req.params;
  const draft_id = await l3Processor.makeDraft(client_id, year)
  res.json({ draft_id, success: true })
})

/**
 * Get committees
 */
api.get('client/committees/:year',async(req, res) => {
  const { year } = req.params;
  const pac_committees = await l3Processor.getCommittees( year )
  res.json( pac_committees )
})

/**
 * Get the draft for the given lobbyist client and year
 */
api.get('/client/:client_id/l3/:year/draft', async(req, res) => {
  const { client_id, year } = req.params;
  const l3 = await l3Processor.getDraft( client_id, year )
  res.json( l3 )
})

/**
 * Save the draft
 */
api.put('/client/:client_id/l3/:year/draft', async(req, res) => {
  const { client_id, year } = req.params;
  const {save_count, user_data} = req.body
  try {
    const saveResult = await l3Processor.saveL3Draft(client_id, year, save_count, user_data);
    res.json(saveResult)
  }
  catch (e) {
    if (e.toString().includes('Save count mismatch')) {
      res.error({type: 'Conflict', message: "Save count mismatch", statusCode: 409})
    }
    else {
      throw(e)
    }
  }
})

api.delete('client/:client_id/l3/draft/:year', async (req, res) => {
  await l3Processor.deleteL3Draft(req.params.client_id, req.params.year)
  res.json({success: true})
})

// submit L3 report
api.post('/client/:client_id/l3/:year/submit', async(req, res) => {
  const { client_id, year, user_data } = req.body
  const submission_response = await l3Processor.submitL3Report(client_id, year, user_data, claims)
  res.json({ submission_response, success: true })
})

api.post('/client/:client_id/l3/:year/amendment', async(req, res) => {
  res.json(await l3Processor.createAmendmentDraft(req.params.client_id,req.params.year));
})

/**
 * AWS Lambda handler. Runs the API with the provided event and context.
 *
 * @param {object} event - The AWS Lambda event object.
 * @param {object} context - The AWS Lambda context object.
 * @returns {Promise} - The promise to return a response.
 */
export const lambdaHandler = async (event, context) => {
  const result = await api.run(event, context);
  await waitForCompletion()
  return result;
};
