
import {claims, getRestApi, waitForCompletion} from "@wapdc/common/rest-api";
import * as rds from "@wapdc/common/wapdc-db"
import {lobbyistProcessor} from "../LobbyistProcessor.js";
import {publicAgencyProcessor} from "../PublicAgencyProcessor.js"
import { firmProcessor } from "../FirmProcessor.js";
import { clientProcessor } from "../ClientProcessor.js";
import { setTestEmailAddress } from "@wapdc/common/mailer";

const api = getRestApi({compressed: true})

api.use('admin/*', (req, res, next) => {
  if (!claims.admin) {
    return res.error({ type: "Access denied", message: "Access denied (admin)", statusCode: 403 })
  }
  setTestEmailAddress(claims.email ?? null);
  next()
})

api.get('admin/access-requests', async (req, res) => {
  const access_requests = await lobbyistProcessor.getAccessRequests()
  res.json( {access_requests})
})

api.get('admin/access-request/:request_id', async (req, res) => {
  const {request_id} = req.params
  const access_request = await lobbyistProcessor.getAccessRequest(request_id)
  res.json( { access_request, success: true})
})

/**
 * During the public agency verification process an agency record is updated on verification
 * or a new agency is created if a match can not be found. The following parameters are
 * passed to allow the processor and function to determine if a post or put is required.
 *
 * @param {object} req - The HTTP request object.
 * @param {object} res - The HTTP response object.
 * @param uid
 * @param realm
 * @param filer_request_id
 * @param agency_id
 * @param payload
 */
api.post('admin/verify-agency', async (req, res) => {
  await publicAgencyProcessor.verifyAgencyAccess(claims.uid, claims.realm, req.body);
  res.json({success: true})
})

api.post('admin/access-request/update', async (req, res) => {
  res.json(await lobbyistProcessor.updateFilerRequest(req.body.request_id, req.body.user_name, req.body.action_date))
})

api.delete('admin/access-request/:request_id', async (req, res) => {
  const { request_id } = req.params
  await lobbyistProcessor.deleteFilerRequest(request_id)
  res.json({success: true})
})

api.get('/admin/firms', async (req, res) => {
  res.json(await firmProcessor.getFirms());
})

api.get('/admin/firm-users', async (req, res) => {
  res.json(await firmProcessor.getFirmUsers())
})

api.get('/admin/clients', async (req, res) => {
  res.json(await clientProcessor.getClients());
})

api.get('/admin/contracts', async (req, res) => {
    res.json(await firmProcessor.getContracts());
});

api.get('/admin/client-users', async (req, res) => {
  res.json(await clientProcessor.getClientUsers());
})

api.get('/admin/new-reports-by-quarter', async (req, res) => {
  res.json(await lobbyistProcessor.getReportsByQuarter());
})

/**
 * AWS Lambda handler. Runs the API with the provided event and context.
 *
 * @param {object} event - The AWS Lambda event object.
 * @param {object} context - The AWS Lambda context object.
 * @returns {Promise} - The promise to return a response.
 */
export const lambdaHandler = async (event, context) => {
  const result =  await api.run(event, context)
  await waitForCompletion()
  return result
}
