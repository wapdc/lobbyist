create table if not exists l2_independent_expenditure
(
    l2_transaction_id int references l2_transaction(l2_transaction_id) on delete cascade,
    candidacy_id int,
    stance text,
    ballot_number text
);
