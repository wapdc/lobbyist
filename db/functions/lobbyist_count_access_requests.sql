-- DROP function lobbyist_count_access_requests()
CREATE OR REPLACE FUNCTION lobbyist_count_access_requests(context json) returns integer language sql as $$
    select count(1) from filer_request
    as result
    where (agent_user_name is null or context->'user'->>'user_name' = agent_user_name)
      and (action_date is null or action_date <= now()::date)
        and target_type ilike 'lobbyist%';
$$
