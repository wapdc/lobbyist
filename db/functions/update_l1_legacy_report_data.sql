create or replace function update_l1_legacy_report_data(p_submission_id int) returns void
    language plpgsql as
$$
DECLARE
    p_json json;
    v_json text;
    v_firm_name text;
    v_employer_name text;
    v_contractor_name text;
    n_employment_rec record;
    n_interest_rec record;
    taxonomy_value text;
    search_string text;
BEGIN

    select user_data into p_json from lobbyist_contract_submission where submission_id = p_submission_id;
    select name into v_employer_name
    from lobbyist_client where lobbyist_client_id = CAST(p_json->'field_employer_entity_reference'->'und'->0->>'target_id' as int);

    select name into v_firm_name
    from lobbyist_firm where lobbyist_firm_id = cast(p_json->'field_firm_entity_reference'->'und'->0->>'target_id' as int);

    select name into v_contractor_name
    from lobbyist_firm where lobbyist_firm_id = cast(p_json->'field_firm_that_is_employing_you'->'und'->0->>'target_id' as int);

    -- update json
    p_json := jsonb_set(p_json::jsonb, '{employer}', to_jsonb(coalesce(v_employer_name,'') ), true);
    p_json := jsonb_set(p_json::jsonb, '{firm}', to_jsonb(coalesce(v_firm_name,'')), true);
    p_json := jsonb_set(p_json::jsonb, '{contractor}', to_jsonb(coalesce(v_contractor_name,'')), true);

    v_json := p_json::text;

    -- description of employment
    if json_typeof(p_json->'field_description_of_employment') = 'object' then
        for n_employment_rec in select j as n_json from json_array_elements(p_json->'field_description_of_employment'->'und') j
            loop
                if n_employment_rec.n_json->>'tid' is not null then
                    search_string := concat('"tid": ', '"', n_employment_rec.n_json->>'tid', '"');
                    select name into taxonomy_value from accesshub_taxonomy_term_data where tid::text = n_employment_rec.n_json->>'tid';
                    v_json := replace(v_json::text, search_string, concat('"name":', '"', taxonomy_value, '"'));
                end if;
            end loop;
    end if;


    -- areas of interest
    if json_typeof(p_json->'field_employer_area_of_interests') = 'object' then
        for n_interest_rec in select j as n_json from json_array_elements(p_json->'field_employer_area_of_interests'->'und') j
            loop
                if n_interest_rec.n_json->>'tid' is not null then
                    search_string := concat('"tid": ', '"', n_interest_rec.n_json->>'tid', '"');
                    select name into taxonomy_value from accesshub_taxonomy_term_data where tid::text = n_interest_rec.n_json->>'tid';
                    v_json := replace(v_json::text, search_string, concat('"name":', '"', taxonomy_value, '"'));
                end if;
            end loop;
    end if;

    -- Update table with modified json
    update lobbyist_contract_submission
    set user_data = v_json::jsonb
    where submission_id = p_submission_id;

END
$$


