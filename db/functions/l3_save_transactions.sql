CREATE OR REPLACE FUNCTION l3_save_transactions(p_report_id INTEGER) returns void language plpgsql volatile as $$
    declare
        v_json json;
        v_client_id integer;
        v_rec record;
        v_transaction_id int;
        v_relation boolean;
        v_official_type text;

    BEGIN
      -- Delete all transactions from the existing report in case we are amending
      DELETE FROM l3_transaction WHERE report_id = p_report_id;

      -- Delete all compensations from the existing report in case we are amending
      DELETE FROM l3_compensation WHERE report_id = p_report_id;

      -- Load current records.
      select l3s.user_data, l3.lobbyist_client_id, c.name into v_json, v_client_id from l3
                                                                                        join l3_submission l3s on l3.current_submission_id = l3s.submission_id
                                                                                        join lobbyist_client c on c.lobbyist_client_id = l3.lobbyist_client_id
      where l3.report_id = p_report_id;

      -- Insert entertainment expenses
      if v_json->>'has_entertainment_expenditures' then
          INSERT INTO l3_transaction (report_id, name, official_name, is_family, official_type, amount, transaction_type, date, description)
          SELECT
              p_report_id,
              j->>'family_member_name',
              j->>'name' ,
              case when j->>'relationship' like '%family member%' then true else false end,
              case
                  when j->>'relationship' in ('State Officials family member', 'State Official') then 'State Official'
                  when j->>'relationship' in ('Legislators family member', 'Legislator') then 'Legislator'
                  when j->>'relationship' in ('State Employees family member', 'State Employee') then 'State Employee'
              end,
              cast(j->>'amount' as numeric(16,2)),
              'entertainment_expense',
              cast(j->>'date' as date),
              j->>'description'
              from json_array_elements(v_json->'entertainment_expenditures') j;
      end if;

      -- Insert itemized expenditures
      if v_json->>'has_itemized_expenditures' then
          INSERT INTO l3_transaction (report_id, name, official_name, is_family, official_type, amount, transaction_type, date, description)
          SELECT
              p_report_id,
              j->>'family_member_name',
              j->>'name' ,
              case when j->>'relationship' like '%family member%' then true else false end,
              case
                  when j->>'relationship' in ('State Officials family member', 'State Official') then 'State Official'
                  when j->>'relationship' in ('Legislators family member', 'Legislator') then 'Legislator'
                  when j->>'relationship' in ('State Employees family member', 'State Employee') then 'State Employee'
                  end,
              cast(j->>'amount' as numeric(16,2)),
              'itemized_expense',
              cast(j->>'date' as date),
              j->>'description'
          from json_array_elements(v_json->'itemized_expenditures') j;
      end if;


      -- Insert other expenses
      if v_json->>'has_other_expenditures' then
          INSERT INTO l3_transaction (report_id, name, amount, transaction_type, date, description)
          SELECT
              p_report_id,
              j->>'recipient',
              cast(j->>'amount' as numeric(16,2)),
              'other_expense',
              cast(j->>'date' as date),
              j->>'description'
          from json_array_elements(v_json->'other_expenditures') j;
      end if;

      -- Insert any payments for registered lobbyists indicated
      if v_json->>'payments_for_registered_lobbyists' is not null then
          INSERT INTO l3_transaction (report_id, amount, transaction_type)
          VALUES (p_report_id,
                 cast(v_json->>'payments_for_registered_lobbyists' as numeric(16,2)),
                 'payments_for_registered_lobbyists');
      end if;

      if v_json->>'witnesses_retained_for_lobbying_services' is not null then
          INSERT INTO l3_transaction (report_id, amount, transaction_type)
          VALUES (p_report_id,
                  cast(v_json->>'witnesses_retained_for_lobbying_services' as numeric(16,2)),
                  'witnesses_retained_for_lobbying_services');
      end if;

      if v_json->>'costs_for_promotional_materials' is not null then
          INSERT INTO l3_transaction (report_id, amount, transaction_type)
          VALUES (p_report_id,
                  cast(v_json->>'costs_for_promotional_materials' as numeric(16,2)),
                  'costs_for_promotional_materials');
      end if;

      if v_json->>'grassroots_expenses_for_lobbying_communications' is not null then
          INSERT INTO l3_transaction (report_id, amount, transaction_type)
          VALUES (p_report_id,
                  cast(v_json->>'grassroots_expenses_for_lobbying_communications' as numeric(16,2)),
                  'grassroots_expenses_for_lobbying_communications');
      end if;

      if v_json->>'non_itemized_political_contribution_amount' is not null then
          INSERT INTO l3_transaction (report_id, amount, transaction_type)
          VALUES (p_report_id,
                 cast(v_json->>'non_itemized_political_contribution_amount' as numeric(16,2)),
                 'non_itemized_political_contribution_amount');
      end if;

      if cast(v_json->>'has_candidate_independent_expenditures' as boolean) then
          FOR v_rec IN (
              select
                  cast(c->>'candidacy_id' as int)as candidacy_id,
                  cast(c->>'amount' as numeric(16,2)) as amount,
                  c->>'name' as name,
                  c->>'description' as description,
                  c->>'stance' as stance,
                  cast(c->>'date' as date) as date
              from json_array_elements(v_json->'candidate_independent_expenditures') c
          )
              LOOP
                  INSERT INTO l3_transaction (report_id,name, amount, transaction_type, date, description)
                  VALUES(p_report_id,v_rec.name, v_rec.amount, 'ie_candidate',v_rec.date, v_rec.description )
                  returning l3_transaction_id into v_transaction_id;

                  INSERT INTO l3_independent_expenditure(l3_transaction_id, stance, candidacy_id) VALUES (v_transaction_id, v_rec.stance, v_rec.candidacy_id );

              END LOOP;
      end if;

      if cast(v_json->>'has_ballot_proposition_expenditures' as boolean) then
          FOR v_rec IN (
              select
                  cast(c->>'amount' as numeric(16,2)) as amount,
                  c->>'ballot_number' as ballot_number,
                  c->>'description' as description,
                  c->>'stance' as stance,
                  cast(c->>'date' as date) as date
              from json_array_elements(v_json->'ballot_proposition_expenditures') c
          )
              LOOP
                  INSERT INTO l3_transaction (report_id, amount, transaction_type, date, description)
                  VALUES(p_report_id, v_rec.amount, 'ie_ballot_proposition',v_rec.date, v_rec.description )
                  returning l3_transaction_id into v_transaction_id;

                  INSERT INTO l3_independent_expenditure(l3_transaction_id, stance, ballot_number) VALUES (v_transaction_id, v_rec.stance, v_rec.ballot_number );

              END LOOP;
      end if;

      -- Insert itemized contributions
      if cast(v_json->>'has_itemized_contributions' as boolean) then
          for v_rec in (
              select
                  cast(c->>'date' as date) as date,
                  cast(c->>'amount' as numeric(16,2)) as amount,
                  case when c->>'recipient_type' = 'candidacy' then cast(c->>'recipient_id' as int) end candidacy_id,
                  case when c->>'recipient_type' = 'committee' then cast(c->>'recipient_id' as int) end committee_id
              from json_array_elements(v_json->'itemized_contributions') c
          )
              loop
                  insert into l3_transaction (report_id, amount, transaction_type, date)
                  values(p_report_id, v_rec.amount, 'itemized_contribution', v_rec.date)
                  returning l3_transaction_id into v_transaction_id;

                  insert into l3_contribution(l3_transaction_id, committee_id, candidacy_id) values (v_transaction_id, v_rec.committee_id, v_rec.candidacy_id);

              end loop;
      end if;

      -- Insert in-kind contributions
      if cast(v_json->>'has_in_kind_contributions' as boolean) then
          for v_rec in (
              select
                  cast(c->>'date' as date) as date,
                  cast(c->>'amount' as numeric(16,2)) as amount,
                  c->>'description' as description,
                  case when c->>'recipient_type' = 'candidacy' then cast(c->>'recipient_id' as int) end candidacy_id,
                  case when c->>'recipient_type' = 'committee' then cast(c->>'recipient_id' as int) end committee_id
              from json_array_elements(v_json->'in_kind_contributions') c
          )
              loop
                  insert into l3_transaction (report_id, amount, transaction_type, date, description)
                  values(p_report_id, v_rec.amount, 'in_kind_contribution', v_rec.date, v_rec.description)
                  returning l3_transaction_id into v_transaction_id;

                  insert into l3_contribution(l3_transaction_id, committee_id, candidacy_id) values (v_transaction_id, v_rec.committee_id, v_rec.candidacy_id);

              end loop;
      end if;

      -- Insert employment compensation
      if cast(v_json->>'has_employment_compensation' as boolean) then
          for v_rec in (
              select
                c->>'name' as official_name,
                c->>'family_member_name' as family_member_name,
                c->>'relationship' as relationship,
                c->>'amount' as compensation,
                c->>'description' as description
              from json_array_elements(v_json->'employment_compensation') c
          )
              loop
                  v_official_type = case
                                        when v_rec.relationship in ('State Officials family member', 'State Official') then 'State Official'
                                        when v_rec.relationship in ('Legislators family member', 'Legislator') then 'Legislator'
                                        when v_rec.relationship in ('State Employees family member', 'State Employee') then 'State Employee'
                                  end;
                  v_relation = case when v_rec.relationship like '%family member%' then true else false end;
                  insert into l3_compensation(report_id, person_name, official_name, is_family, official_type, compensation, description)
                  values (p_report_id, v_rec.family_member_name, v_rec.official_name, v_relation, v_official_type, v_rec.compensation, v_rec.description);
              end loop;
      end if;

      -- Insert professional services compensation
      if cast(v_json->>'has_professional_services_compensation' as boolean) then
          for v_rec in (
              select
                c->>'person_name' as person_name,
                c->>'firm_name' as org_name,
                c->>'amount' as compensation,
                c->>'description' as description
              from json_array_elements(v_json->'professional_services_compensation') c
          )
              loop
                  insert into l3_compensation(report_id, person_name, org_name, compensation, description)
                  values (p_report_id, v_rec.person_name, v_rec.org_name, v_rec.compensation, v_rec.description);
              end loop;
      end if;

      -- Insert lobbyist corrected total compensation
      for v_rec in (
          select
              cast(l->>'compensation' as numeric(16,2)) as amount,
              cast(l->>'lobbyist_contract_id' as int) lobbyist_contract_id,
              l->>'lobbyist_title' as name
          from json_array_elements(v_json->'lobbyist') l
      )
          loop
              insert into l3_transaction (report_id, amount, transaction_type, name)
              values(p_report_id, v_rec.amount, 'lobbyist_compensation_total', v_rec.name)
              returning l3_transaction_id into v_transaction_id;

              insert into l3_lobbyist(l3_transaction_id, lobbyist_contract_id) values (v_transaction_id, v_rec.lobbyist_contract_id);
          end loop;

      -- Insert lobbyist reported total compensation
      for v_rec in (
          select
              cast(l->>'compensation_l2' as numeric(16,2)) as amount,
              cast(l->>'lobbyist_contract_id' as int) lobbyist_contract_id,
              l->>'lobbyist_title' as name
          from json_array_elements(v_json->'lobbyist') l
      )
          loop
              insert into l3_transaction (report_id, amount, transaction_type, name)
              values(p_report_id, v_rec.amount, 'lobbyist_reported_compensation_total', v_rec.name)
              returning l3_transaction_id into v_transaction_id;

              insert into l3_lobbyist(l3_transaction_id, lobbyist_contract_id) values (v_transaction_id, v_rec.lobbyist_contract_id);
          end loop;
      -- Insert lobbyist corrected total expense
      for v_rec in (

          select
              cast(l->>'expenses' as numeric(16,2)) as amount,
              cast(l->>'lobbyist_contract_id' as int) lobbyist_contract_id,
              l->>'lobbyist_title' as name
          from json_array_elements(v_json->'lobbyist') l
      )
          loop
              insert into l3_transaction (report_id, amount, transaction_type, name)
              values(p_report_id, v_rec.amount, 'lobbyist_expense_total', v_rec.name)
              returning l3_transaction_id into v_transaction_id;

              insert into l3_lobbyist(l3_transaction_id, lobbyist_contract_id) values (v_transaction_id, v_rec.lobbyist_contract_id);
          end loop;

      -- Insert lobbyist reported total expense
      for v_rec in (

          select
              cast(l->>'expenses_l2' as numeric(16,2)) as amount,
              cast(l->>'lobbyist_contract_id' as int) lobbyist_contract_id,
              l->>'lobbyist_title' as name
          from json_array_elements(v_json->'lobbyist') l
      )
          loop
              insert into l3_transaction (report_id, amount, transaction_type, name)
              values(p_report_id, v_rec.amount, 'lobbyist_reported_expense_total', v_rec.name)
              returning l3_transaction_id into v_transaction_id;

              insert into l3_lobbyist(l3_transaction_id, lobbyist_contract_id) values (v_transaction_id, v_rec.lobbyist_contract_id);
          end loop;

      end
$$;
