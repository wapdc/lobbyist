create or replace function lobbyist_submit_filer_request(p_uid text, p_realm text, p_target_id integer, p_target_type text, p_payload json) returns void
    language plpgsql as
    $$
BEGIN
    insert into public.filer_request(uid, realm, possible_target_id, submitted_at, payload, target_type)
    values (p_uid, p_realm, p_target_id, current_timestamp, p_payload, p_target_type);
END;
$$