create or replace function l2_save_lobbying_activity(p_report_id INTEGER) RETURNS void language plpgsql volatile as $$

DECLARE
    v_rec jsonb;
BEGIN
    --Remove any records that may be there from a previous version
    delete from l2_lobbying_activity where report_id = p_report_id;

    for v_rec in select json_array_elements(user_data->'lobbying') l from l2_submission l2s
                             where l2s.report_id = p_report_id and l2s.superseded_id is null
        loop
            insert into l2_lobbying_activity(report_id, source_target_id, source_target_type, activity)
            values(p_report_id, cast(v_rec->>'source' as integer), 'lobbyist_contract', v_rec - 'source');
        end loop;
end;
$$;
