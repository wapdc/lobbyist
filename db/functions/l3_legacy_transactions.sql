create or replace function l3_legacy_transactions(p_json json) returns void
    language plpgsql as
$$
declare
    v_report_id int := cast(p_json->>'nid' as int);
    v_client_info record;
    j_totals JSON;
    n_ballot_proposition_rec record;
    v_l3_transaction_id int;
    n_ie_candidate_rec record;
    n_emp_comp_rec record;
    v_relationship_type text;
    v_comp_amount text;
    n_prof_svcs_rec record;
    n_entertainment_expense_rec record;
    n_itemized_expense_rec record;
    n_other_expense_rec record;
    n_political_contribution_rec record;
    n_lobbyist_totals_rec record;
    v_relation boolean;
    v_official_type text;
    n_lobbyist_reported_totals_rec record;
    v_html_data text;
    reg_match text[];
    v_name text;
    v_compensation numeric;
    v_expenses numeric;
    v_table_header_count int;
    v_regex_pattern text;

begin
    --due to the possibility of an amendment, we first delete the existing transactions
    delete from l3_transaction where report_id = v_report_id;
    delete from l3_compensation where report_id = v_report_id;

    --grab the firm info for use throughout the file
    select * into v_client_info from lobbyist_client where lobbyist_client_id
                                                           = cast(p_json->'og_group_ref'->'und'->0->>'target_id' as int);

    if p_json->'field_expend_not_listed'->'und' is not null then
      j_totals := p_json->'field_expend_not_listed'->'und'->0;

      INSERT INTO l3_transaction(report_id, amount, transaction_type)
      VALUES (v_report_id, cast(j_totals->'field_vendor'->'und'->0->>'value' as numeric(16,2)), 'payments_for_registered_lobbyists');
      INSERT INTO l3_transaction(report_id, amount, transaction_type)
      VALUES (v_report_id, cast(j_totals->'field_expert_retainer'->'und'->0->>'value' as numeric(16,2)), 'witnesses_retained_for_lobbying_services');
      INSERT INTO l3_transaction(report_id, amount, transaction_type)
      VALUES (v_report_id, cast(j_totals->'field_inform_material'->'und'->0->>'value' as numeric(16,2)), 'costs_for_promotional_materials');
      INSERT INTO l3_transaction(report_id, amount, transaction_type)
      VALUES (v_report_id, cast(j_totals->'field_lobbying_comm'->'und'->0->>'value' as numeric(16,2)), 'grassroots_expenses_for_lobbying_communications');

    end if;

    -- Aggregate total of all non-itemized political contributions
    if p_json->'field_aggregate_contrib'->'und' is not null and p_json->'field_aggregate_contrib'->'und'->0->>'value' is not null then
        INSERT INTO l3_transaction(report_id, amount, transaction_type)
        VALUES (v_report_id, cast(p_json->'field_aggregate_contrib'->'und'->0->>'value' as numeric(16,2)), 'non_itemized_political_contribution_amount');
    end if;


    if json_typeof(p_json->'field_ballot_proposition') = 'object' then
        for n_ballot_proposition_rec in select j as n_json from json_array_elements(p_json->'field_ballot_proposition'->'und') j
            loop
                insert into l3_transaction(
                    report_id,
                    amount,
                    transaction_type,
                    date,
                    description)
                values(
                          v_report_id,
                          cast(n_ballot_proposition_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'ie_ballot_proposition',
                          cast(n_ballot_proposition_rec.n_json->'field_date'->'und'->0->>'value' as date),
                          n_ballot_proposition_rec.n_json->'field_ie_description'->'und'->0->>'value'
                      )
                returning l3_transaction_id into v_l3_transaction_id;

                insert into l3_independent_expenditure(
                    l3_transaction_id,
                    stance,
                    ballot_number)
                VALUES(
                          v_l3_transaction_id,
                          case when n_ballot_proposition_rec.n_json->'field_supported_or_opposed'->'und'->0->>'value' = '0' then 'Against'
                               when n_ballot_proposition_rec.n_json->'field_supported_or_opposed'->'und'->0->>'value' = '1' then 'For'
                              end,
                          n_ballot_proposition_rec.n_json->'field_description'->'und'->0->>'value'
                      );
            end loop;
    end if;

    if json_typeof(p_json->'field_ie_support') = 'object' then
        for n_ie_candidate_rec in select j as n_json from json_array_elements(p_json->'field_ie_support'->'und') j
            loop
                insert into l3_transaction(
                    report_id,
                    amount,
                    transaction_type,
                    date,
                    description)
                values(
                          v_report_id,
                          cast(n_ie_candidate_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'ie_candidate',
                          cast(n_ie_candidate_rec.n_json->'field_date'->'und'->0->>'value' as date),
                          n_ie_candidate_rec.n_json->'field_description'->'und'->0->>'value'
                      )
                returning l3_transaction_id into v_l3_transaction_id;

                insert into l3_independent_expenditure(
                    l3_transaction_id,
                    stance)
                VALUES(
                          v_l3_transaction_id,
                          case when n_ie_candidate_rec.n_json->'field_supported_or_opposed'->'und'->0->>'value' = '0' then 'Against'
                               when n_ie_candidate_rec.n_json->'field_supported_or_opposed'->'und'->0->>'value' = '1' then 'For'
                          end
                      );
            end loop;
    end if;

    -- Employment compensation transactions
    if json_typeof(p_json->'field_l3_emp_comp') = 'object' then
        for n_emp_comp_rec in select j as n_json from json_array_elements(p_json->'field_l3_emp_comp'->'und') j
            loop
                v_relationship_type = cast(n_emp_comp_rec.n_json->'field_relationship'->'und'->0->>'tid' as text);
                v_comp_amount = cast(n_emp_comp_rec.n_json->'field_code'->'und'->0->>'tid' as text);
                v_relation = case
                                 when v_relationship_type in ('127', '128', '110','112','113') then true else false
                             end;
                v_official_type = case
                                      when v_relationship_type in ('127', '109') then 'State Official'
                                      when v_relationship_type in ('128', '108') then 'Legislator'
                                      when v_relationship_type in ('111', '110') then 'State Employee'
                                  end;
                insert into l3_compensation(
                    report_id,
                    person_name,
                    is_family,
                    official_type,
                    compensation,
                    description)
                VALUES(
                          v_report_id,
                          n_emp_comp_rec.n_json->'field_field_name'->'und'->0->>'value',
                          v_relation,
                          v_official_type,
                          case when v_comp_amount = '103' then '1 to 4,499'
                               when v_comp_amount = '104' then '4,500 to 23,999'
                               when v_comp_amount = '105' then '24,000 to 47,999'
                               when v_comp_amount = '106' then '48,000 to 119,999'
                               when v_comp_amount = '107' then '120,000 or more'
                              end,
                          n_emp_comp_rec.n_json->'field_description'->'und'->0->>'value'
                      );
            end loop;
    end if;

    -- Professional services transactions
    if json_typeof(p_json->'field_l3_prof_svcs') = 'object' then
        for n_prof_svcs_rec in select j as n_json from json_array_elements(p_json->'field_l3_prof_svcs'->'und') j
            loop
                v_comp_amount = cast(n_prof_svcs_rec.n_json->'field_code'->'und'->0->>'tid' as text);
                insert into l3_compensation(
                    report_id,
                    person_name,
                    org_name,
                    compensation,
                    description)
                VALUES(
                          v_report_id,
                          n_prof_svcs_rec.n_json->'field_field_name'->'und'->0->>'value',
                          n_prof_svcs_rec.n_json->'field_firm_name'->'und'->0->>'value',
                          case when v_comp_amount = '103' then '1 to 4,499'
                               when v_comp_amount = '104' then '4,500 to 23,999'
                               when v_comp_amount = '105' then '24,000 to 47,999'
                               when v_comp_amount = '106' then '48,000 to 119,999'
                               when v_comp_amount = '107' then '120,000 or more'
                              end,
                          n_prof_svcs_rec.n_json->'field_description'->'und'->0->>'value'
                      );
            end loop;
    end if;

    -- Entertainment expenses transactions
    if json_typeof(p_json->'field_l3_entertainment') = 'object' then
        for n_entertainment_expense_rec in select j as n_json from json_array_elements(p_json->'field_l3_entertainment'->'und') j
            loop
                v_relationship_type = cast(n_entertainment_expense_rec.n_json->'field_relationship'->'und'->0->>'tid' as text);
                v_relation = case when v_relationship_type in ('127', '128', '110','112','113') then true else false end;
                v_official_type = case
                                      when v_relationship_type in ('127', '109') then 'State Official'
                                      when v_relationship_type in ('128', '108') then 'Legislator'
                                      when v_relationship_type in ('111', '110') then 'State Employee'
                    end;
                insert into l3_transaction(
                    report_id,
                    name,
                    is_family,
                    official_type,
                    amount,
                    transaction_type,
                    date,
                    description)
                VALUES(
                          v_report_id,
                          n_entertainment_expense_rec.n_json->'field_entertain_name'->'und'->0->>'value',
                          v_relation,
                          v_official_type,
                          cast(n_entertainment_expense_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'entertainment_expense',
                          cast(n_entertainment_expense_rec.n_json->'field_date'->'und'->0->>'value' as date),
                          n_entertainment_expense_rec.n_json->'field_description'->'und'->0->>'value'
                      );
            end loop;
    end if;

    -- itemized expenses transactions
    if json_typeof(p_json->'field_itemized_l3_expenditures') = 'object' then
        for n_itemized_expense_rec in select j as n_json from json_array_elements(p_json->'field_itemized_l3_expenditures'->'und') j
            loop
                insert into l3_transaction(
                    report_id,
                    name,
                    amount,
                    transaction_type,
                    date,
                    description)
                VALUES(
                          v_report_id,
                          n_itemized_expense_rec.n_json->'field_field_name'->'und'->0->>'value',
                          cast(n_itemized_expense_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'itemized_expense',
                          cast(n_itemized_expense_rec.n_json->'field_date'->'und'->0->>'value' as date),
                          n_itemized_expense_rec.n_json->'field_short_description'->'und'->0->>'value'
                      );
            end loop;
    end if;

    -- other expenses transactions
    if json_typeof(p_json->'field_other_l3_expenditures') = 'object' then
        for n_other_expense_rec in select j as n_json from json_array_elements(p_json->'field_other_l3_expenditures'->'und') j
            loop
                insert into l3_transaction(
                    report_id,
                    name,
                    amount,
                    transaction_type,
                    date,
                    description)
                VALUES(
                          v_report_id,
                          n_other_expense_rec.n_json->'field_field_name'->'und'->0->>'value',
                          cast(n_other_expense_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'other_expense',
                          cast(n_other_expense_rec.n_json->'field_date'->'und'->0->>'value' as date),
                          n_other_expense_rec.n_json->'field_description'->'und'->0->>'value'
                      );
            end loop;
    end if;


    -- Political contributions transactions
    if json_typeof(p_json->'field_political_contributions') = 'object' then
        for n_political_contribution_rec in select j as n_json from json_array_elements(p_json->'field_political_contributions'->'und') j
            loop
                insert into l3_transaction(
                    report_id,
                    amount,
                    transaction_type,
                    date,
                    name)
                VALUES(
                          v_report_id,
                          cast(n_political_contribution_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'itemized_contribution',
                          cast(n_political_contribution_rec.n_json->'field_date'->'und'->0->>'value' as date),
                          n_political_contribution_rec.n_json->'field_field_name'->'und'->0->>'value'
                      );
            end loop;
    end if;

    -- lobbyist corrected totals
    if json_typeof(p_json->'field_totals_correction') = 'object' then
        for n_lobbyist_totals_rec in select j as n_json from json_array_elements(p_json->'field_totals_correction'->'und') j
          loop
                -- Insert lobbyist total compensation
                insert into l3_transaction (
                    report_id,
                    amount,
                    transaction_type
                )
                values (
                           v_report_id,
                           cast(n_lobbyist_totals_rec.n_json->'field_comp'->'und'->0->>'value' as numeric),
                           'lobbyist_compensation_total'
                       )
                returning l3_transaction_id into v_l3_transaction_id;

                insert into l3_lobbyist (
                    l3_transaction_id,
                    lobbyist_contract_id
                )
                values (
                           v_l3_transaction_id,
                           cast(n_lobbyist_totals_rec.n_json->'field_lobbyist'->'und'->0->>'target_id' as int)
                       );

                -- Insert lobbyist total expense
                insert into l3_transaction (
                    report_id,
                    amount,
                    transaction_type
                )
                values (
                           v_report_id,
                           cast(n_lobbyist_totals_rec.n_json->'field_exp'->'und'->0->>'value' as numeric),
                           'lobbyist_expense_total'
                       )
                returning l3_transaction_id INTO v_l3_transaction_id;

                insert into l3_lobbyist (
                    l3_transaction_id,
                    lobbyist_contract_id
                )
                values (
                           v_l3_transaction_id,
                           cast(n_lobbyist_totals_rec.n_json->'field_lobbyist'->'und'->0->>'target_id' as int)
                       );
            end loop;
    end if;

    -- lobbyist reported totals
    if json_typeof(p_json->'field_name_registered_lob') = 'object' then
        for n_lobbyist_reported_totals_rec in select j as n_json from json_array_elements(p_json->'field_name_registered_lob'->'und') j
            loop
                -- extract html data from the 'value' field
                v_html_data := n_lobbyist_reported_totals_rec.n_json->>'value';

                -- Count table headers to determine the column positions
                v_table_header_count := ( select count(*) from regexp_matches(v_html_data, '<th .*?>(.*?)</th>', 'g'));

                v_regex_pattern := case
                                       when v_table_header_count = 4 then
                                           '<td><span><a .*?>(.*?)</a></span></td>\s*<td align="right">\$?(.*?)</td>\s*<td align="right">\$?(.*?)</td>'
                                       else
                                           '<td><span><a .*?>(.*?)</a></span></td>\s*<td align="right">\$?(.*?)</td>\s*<td align="right">\$?(.*?)</td>\s*<td align="right">\$?(.*?)</td>'
                    end;

                -- Use regular expressions to extract the data
                for reg_match in select regexp_matches(v_html_data,v_regex_pattern,'g')
                    loop
                        v_name := trim(regexp_replace(trim(reg_match[1]), '\s+', ' ', 'g'));  -- convert all types of spaces into a single space
                        v_compensation := cast(nullif(reg_match[2], '') as numeric(16,2));
                        if v_table_header_count = 4 then
                            v_expenses := cast(nullif(reg_match[3], '') as numeric(16,2));
                        else
                            v_expenses := cast(nullif(reg_match[4], '') as numeric(16,2));
                        end if;
                        -- Insert lobbyist total expense
                        if v_expenses is not null then
                            insert into l3_transaction (
                                report_id,
                                amount,
                                transaction_type,
                                name
                            )
                            values (
                                       v_report_id,
                                       v_expenses,
                                       'lobbyist_reported_expense_total',
                                       v_name
                                   );
                        end if;
                    -- Insert lobbyist total compensation
                        if v_compensation is not null then
                            insert into l3_transaction (
                                report_id,
                                amount,
                                transaction_type,
                                name
                            )
                            values (
                                       v_report_id,
                                       v_compensation,
                                       'lobbyist_reported_compensation_total',
                                       v_name
                                   );
                        end if;
                    end loop;
            end loop;
    end if;

end
$$;
