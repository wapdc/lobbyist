create or replace function update_l3_legacy_report_data(p_submission_id int) returns void
    language plpgsql as
$$
    DECLARE
      p_json json;
      i json;
      lobbyists jsonb;
      lobbyist_name text;

    BEGIN

      select user_data into p_json from l3_submission where submission_id = p_submission_id;

      -- build the lobbyists object
      select jsonb_build_object() into lobbyists;

      for i in select * from json_array_elements(p_json->'field_totals_correction'->'und')
      loop

        select lfir.name
        into lobbyist_name
        from lobbyist_contract lcon join lobbyist_firm lfir on lcon.lobbyist_firm_id = lfir.lobbyist_firm_id
        where lcon.lobbyist_contract_id = cast(i->'field_lobbyist'->'und'->0->>'target_id' as integer);

        select jsonb_set(lobbyists, array[cast(i->'field_lobbyist'->'und'->0->>'target_id' as text)], to_jsonb(lobbyist_name)) into lobbyists;

      end loop;

      select jsonb_build_object('lobbyists', lobbyists) into lobbyists;

      -- update the l3 user_data to have the lobbyist object
      update l3_submission
      set user_data = p_json::jsonb || lobbyists::jsonb
      where submission_id = p_submission_id;

    END
$$
