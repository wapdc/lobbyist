create or replace function l7_submit(p_l7_report json, p_target_type text, p_target_id integer, p_original_report_id integer) returns integer
language plpgsql as
$$
  declare
    v_submission_id integer;
    v_report_id integer;
    v_lobbyist_count integer default 0;
    v_original_submission_id integer;
  begin

    -- validate the lobbyist client / firm exists
    if p_target_type = 'lobbyist_client' then
       select count(lobbyist_client_id) into v_lobbyist_count from public.lobbyist_client where p_target_id = lobbyist_client_id;
    elsif p_target_type = 'lobbyist_firm' then
       select count(lobbyist_firm_id) into v_lobbyist_count from public.lobbyist_firm where p_target_id = lobbyist_firm_id;
    end if;
    if v_lobbyist_count = 0 then raise exception 'No lobbyist found'; end if;

    -- insert into l7 table
    if p_original_report_id is null then
        insert into public.l7( target_id, target_type)
        values(p_target_id, p_target_type)
        returning report_id into v_report_id;
    else
        -- ensure the report id exists
        select report_id, current_submission_id
          into v_report_id, v_original_submission_id
        from l7
        where report_id = p_original_report_id
          and target_id = p_target_id
          and target_type = p_target_type;
        if v_report_id is null then
            raise exception 'Invalid amended report_id';
        end if;
    end if;

    -- insert into l7_submission table
    insert into public.l7_submission(superseded_id, report_id, version, submitted_at, user_data)
    values(null, v_report_id, p_l7_report->>'version', current_timestamp, p_l7_report)
    returning submission_id into v_submission_id;

    -- update the report_id in the l7_submission table
    update l7 set current_submission_id = v_submission_id where report_id = v_report_id;

    if p_original_report_id is not null then
        update l7_submission set superseded_id = v_submission_id where submission_id = v_original_submission_id;
    end if;

    return v_report_id;
  end;
$$
