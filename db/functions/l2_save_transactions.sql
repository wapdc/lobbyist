create or replace function l2_save_transactions(p_report_id INTEGER) RETURNS void language plpgsql volatile as $$

    declare
        v_json json;
        v_firm_id integer;
        v_rec RECORD;
        v_transaction_id INT;
  BEGIN
      -- Delete all transactions from existing report in case we are amending
      DELETE FROM l2_transaction WHERE report_id = p_report_id;

      select l2s.user_data, l2.lobbyist_firm_id, f.name into v_json, v_firm_id from l2
          join l2_submission l2s on l2.current_submission_id = l2s.submission_id
          join lobbyist_firm f on f.lobbyist_firm_id = l2.lobbyist_firm_id
          where l2.report_id = p_report_id;

      -- Insert any compensation entries if indicated
      if cast(v_json->>'has_compensation' as boolean) then
          INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
           select 'lobbyist_contract',
                  cast(s->>'id' as int),
                  s->>'name',
                  p_report_id,
                  cast(c->>'amount' as numeric(16,2)),
                  'compensation'
                from json_array_elements(v_json->'compensation') c
                   join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id';
      end if;

      -- Insert payments to subcontracted lobbyists
      if cast(v_json->>'has_sub_lobbyist_payments' as boolean) then
        INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
          select 'lobbyist_contract', cast(s->>'lobbyist_contract_id' as int), s->>'name', p_report_id, cast(c->>'amount' as numeric(16,2)), 'sub_lobbyist_payment'
          FROM json_array_elements(v_json->'sub_lobbyist_payments') c
                  JOIN json_array_elements(v_json->'subcontractors') s on c->>'source' = s->>'lobbyist_contract_id';
      end if;

      if cast(v_json->>'has_personal_expenses' as boolean) then
          INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
          select case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end,
                 cast(s->>'id' as int),
                 s->>'name',
                 p_report_id,
                 cast(c->>'amount' as numeric(16,2)),
                 'personal_expense'
          from json_array_elements(v_json->'personal_expenses') c
                   join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id';
      end if;

      -- Non-itemized entertainment
      if cast(v_json->>'has_entertainment_small' as boolean) then
          INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
          select case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end,
                 cast(s->>'id' as int),
                 s->>'name',
                 p_report_id,
                 cast(c->>'amount' as numeric(16,2)),
                 'non_itemized_entertainment_expenses'
          from json_array_elements(v_json->'entertainment_small') c
                   join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id';
      end if;

      if cast(v_json->>'has_entertainment' as boolean) then
          FOR v_rec IN (
              select
                  case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end source_type,
                  cast(s->>'id' as int) source_id,
                  s->>'name' as source_name,
                  cast(c->>'cost' as numeric(16,2)) as cost,
                  c->>'occasion_description' as description,
                  c->>'type_of_occasion' as occasion_type,
                  c->>'participant_listing' as participant_listing,
                  c->>'place' as place,
                  cast(c->>'occasion_date' as date) as occasion_date
                from json_array_elements(v_json->'entertainment') c
                join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id'
          )
        LOOP
          INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
          VALUES(v_rec.source_type, v_rec.source_id, v_rec.source_name, p_report_id, v_rec.cost, 'itemized_entertainment_expense')
          returning l2_transaction_id into v_transaction_id;

          INSERT INTO l2_expense(l2_transaction_id, description, date) VALUES (v_transaction_id, v_rec.description, v_rec.occasion_date );

          INSERT INTO l2_entertainment(l2_transaction_id, occasion_type, place, participants)
          VALUES (v_transaction_id, v_rec.occasion_type, v_rec.place, v_rec.participant_listing);
        END LOOP;
      end if;

    -- Indirect/grass roots lobbying
      if cast(v_json->>'has_expenses_indirect' as boolean) then
          FOR v_rec IN (
              select
                  case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end source_type,
                  cast(s->>'id' as int) source_id,
                  s->>'name' as source_name,
                  cast(c->>'amount' as numeric(16,2)) as cost,
                  c->>'type' as expense_type,
                  c->>'description' as description,
                  c->>'recipient' as recipient_name,
                  c->>'address' as address,
                  c->>'city' as city,
                  c->>'state' as state,
                  c->>'postcode' as postcode
              from json_array_elements(v_json->'expenses_indirect') c
                       join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id'
          )
              LOOP
                  INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
                  VALUES(v_rec.source_type, v_rec.source_id, v_rec.source_name, p_report_id, v_rec.cost, 'indirect_expense')
                  returning l2_transaction_id into v_transaction_id;

                  INSERT INTO l2_expense(l2_transaction_id, address, city, state, postcode, expense_type, description, vendor)
                        VALUES (v_transaction_id, v_rec.address, v_rec.city, v_rec.state, v_rec.postcode,v_rec.expense_type, v_rec.description,v_rec.recipient_name);

              END LOOP;
      end if;

      -- Direct Consulting and Expert witness details
      if cast(v_json->>'has_consulting' as boolean) then
          FOR v_rec IN (
              select
                  case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end source_type,
                  cast(s->>'id' as int) source_id,
                  s->>'name' as source_name,
                  cast(c->>'amount' as numeric(16,2)) as amount,
                  c->>'name' as vendor,
                  c->>'address' as address,
                  c->>'city' as city,
                  c->>'state' as state,
                  c->>'postcode' as postcode,
                  cast(c->>'date' as date) as date
              from json_array_elements(v_json->'consulting') c
                       join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id'
          )
              LOOP
                  INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
                  VALUES(v_rec.source_type, v_rec.source_id, v_rec.source_name, p_report_id, v_rec.amount, 'consulting_expense')
                  returning l2_transaction_id into v_transaction_id;

                  INSERT INTO l2_expense(l2_transaction_id, address, city, state, postcode, vendor, date)
                  VALUES (v_transaction_id, v_rec.address, v_rec.city, v_rec.state, v_rec.postcode, v_rec.vendor, v_rec.date);

              END LOOP;
      end if;

      -- Direct printing and literature details
      if cast(v_json->>'has_printing' as boolean) then
          FOR v_rec IN (
              select
                  case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end source_type,
                  cast(s->>'id' as int) source_id,
                  s->>'name' as source_name,
                  cast(c->>'amount' as numeric(16,2)) as amount,
                  c->>'description' as description
              from json_array_elements(v_json->'printing') c
                       join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id'
          )
              LOOP
                  INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
                  VALUES(v_rec.source_type, v_rec.source_id, v_rec.source_name, p_report_id, v_rec.amount, 'printing_expense')
                  returning l2_transaction_id into v_transaction_id;

                  INSERT INTO l2_expense(l2_transaction_id, description)
                  VALUES (v_transaction_id, v_rec.description);

              END LOOP;
      end if;

      -- Direct Public Relations and polling details
      if cast(v_json->>'has_public_relations' as boolean) then

          FOR v_rec IN (
              select
                  case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end source_type,
                  cast(s->>'id' as int) source_id,
                  s->>'name' as source_name,
                  cast(c->>'amount' as numeric(16,2)) as amount,
                  c->>'vendor' as vendor,
                  c->>'description' as description,
                  cast(c->>'date' as date) as date
              from json_array_elements(v_json->'public_relations') c
                       join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id'
          )
              LOOP
                  INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
                  VALUES(v_rec.source_type, v_rec.source_id, v_rec.source_name, p_report_id, v_rec.amount, 'public_relations_expense')
                  returning l2_transaction_id into v_transaction_id;

                  INSERT INTO l2_expense(l2_transaction_id, description, vendor, date)
                  VALUES (v_transaction_id, v_rec.description, v_rec.vendor, v_rec.date);
              END LOOP;
      end if;

      -- Direct Other Expenses details
      if cast(v_json->>'has_expenses_other' as boolean) then
          FOR v_rec IN (
              select
                  case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end source_type,
                  cast(s->>'id' as int) source_id,
                  s->>'name' as source_name,
                  cast(c->>'amount' as numeric(16,2)) as cost,
                  c->>'description' as description,
                  c->>'vendor' as vendor,
                  cast(c->>'date' as date) as date
              from json_array_elements(v_json->'expenses_other') c
                       join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id'
          )
              LOOP
                  INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
                  VALUES(v_rec.source_type, v_rec.source_id, v_rec.source_name, p_report_id, v_rec.cost, 'other_expense')
                  returning l2_transaction_id into v_transaction_id;

                  INSERT INTO l2_expense(l2_transaction_id, description, date, vendor) VALUES (v_transaction_id, v_rec.description, v_rec.date, v_rec.vendor );
              END LOOP;
      end if;

      -- Election Monetary Contribution
      if cast(v_json->>'has_contributions' as boolean) then
          FOR v_rec IN (
              select
                  case
                      when c->>'source' = '-1' then 'Other'
                      when c->>'source' = '-2' then 'PAC'
                      when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm'
                      else 'lobbyist_contract'
                      end as source_type,
                  case
                      when c->>'source' = '-1' then -1
                      when c->>'source' = '-2' then cast(c->>'committee_id' as int)
                      else cast(s->>'id' as int)
                      end as source_id,
                  case
                      when c->>'source' in ('-1', '-2') then c->>'name'
                      else s->>'name'
                      end as source_name,
                  cast(c->>'date' as date) as date,
                  cast(c->>'amount' as numeric(16,2)) as cost,
                  c->>'recipient_type' as recipient_type,
                  cast(c->>'recipient_id' as int) as recipient_id,
                  c->>'recipient' as recipient_name
              from json_array_elements(v_json->'contributions') c
                       left join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id'
          )
              LOOP
                  INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
                  VALUES(v_rec.source_type, v_rec.source_id, v_rec.source_name, p_report_id, v_rec.cost, 'itemized_contribution')
                  returning l2_transaction_id into v_transaction_id;

                  INSERT INTO l2_expense(l2_transaction_id, expense_type, vendor, date) VALUES (v_transaction_id, 'monetary', v_rec.recipient_name, v_rec.date);

                  INSERT INTO l2_contribution(l2_transaction_id, recipient_id, recipient_target) VALUES (v_transaction_id, v_rec.recipient_id ,v_rec.recipient_type);
              END LOOP;
      end if;

      -- Election In-kind Contribution
      if cast(v_json->>'has_contributions_in_kind' as boolean) then
          FOR v_rec IN (
              select
                  case
                      when c->>'source' = '-1' then 'Other'
                      when c->>'source' = '-2' then 'PAC'
                      when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm'
                      else 'lobbyist_contract'
                      end as source_type,
                  case
                      when c->>'source' = '-1' then -1
                      when c->>'source' = '-2' then cast(c->>'committee_id' as int)
                      else cast(s->>'id' as int)
                      end as source_id,
                  case
                      when c->>'source' = '-1' then c->>'name'
                      when c->>'source' = '-2' then c->>'name'
                      else s->>'name'
                      end as source_name,
                  cast(c->>'amount' as numeric(16,2)) as cost,
                  c->>'recipient_type' as recipient_type,
                  cast(c->>'recipient_id' as int) as recipient_id,
                  c->>'description' as description,
                  cast(c->>'date' as date) as date,
                  c->>'vendor' as vendor,
                  cast(c->>'is_advertisment' as boolean) as is_ad,
                  c->>'name' as name,
                  c->>'committee_id' as committee_id
              from json_array_elements(v_json->'contributions_in_kind') c
                       left join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id'
          )
              LOOP
                  INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
                  VALUES(v_rec.source_type, v_rec.source_id, v_rec.source_name, p_report_id, v_rec.cost, 'in_kind_expense')
                  returning l2_transaction_id into v_transaction_id;

                  INSERT INTO l2_expense(l2_transaction_id, description, is_ad, vendor,date ) VALUES (v_transaction_id, v_rec.description, v_rec.is_ad, v_rec.vendor, v_rec.date );

                  INSERT INTO l2_contribution(l2_transaction_id, recipient_id, recipient_target) VALUES (v_transaction_id, v_rec.recipient_id ,v_rec.recipient_type);
              END LOOP;
      end if;

      -- election small contribution
      if cast(v_json->>'has_contributions_small' as boolean) then
          INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
          select case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end,
                 cast(s->>'id' as int),
                 s->>'name',
                 p_report_id,
                 cast(c->>'amount' as numeric(16,2)),
                 'contributions_small_expense'
          from json_array_elements(v_json->'contributions_small') c
                   join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id';
      end if;

      -- independent expenditure - candidate
      if cast(v_json->>'has_independent_expenditures_candidate' as boolean) then
          FOR v_rec IN (
              select
                  case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end source_type,
                  cast(s->>'id' as int) source_id,
                  s->>'name' as source_name,
                  cast(c->>'amount' as numeric(16,2)) as cost,
                  c->>'description' as description,
                  c->>'recipient' as vendor,
                  cast(c->>'date' as date) as date,
                  c->>'stance' as stance,
                  cast(c->>'candidate_id' as int) as candidacy_id
              from json_array_elements(v_json->'independent_expenditures_candidate') c
              join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id'
          )
              LOOP
                  INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
                  VALUES(v_rec.source_type, v_rec.source_id, v_rec.source_name, p_report_id, v_rec.cost, 'independent_expenditures_candidate_expense')
                  returning l2_transaction_id into v_transaction_id;

                  INSERT INTO l2_expense(l2_transaction_id, description, date, vendor)
                  VALUES (v_transaction_id, v_rec.description, v_rec.date, v_rec.vendor);

                  INSERT INTO l2_independent_expenditure(l2_transaction_id, candidacy_id, stance)
                  VALUES (v_transaction_id, v_rec.candidacy_id, v_rec.stance);
              END LOOP;
      end if;

      -- independent expenditure - ballot
      if cast(v_json->>'has_independent_expenditures_ballot' as boolean) then
          FOR v_rec IN (
              select
                  case when cast(s->>'id' as int) = v_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end source_type,
                  cast(s->>'id' as int) source_id,
                  s->>'name' as source_name,
                  cast(c->>'amount' as numeric(16,2)) as cost,
                  c->>'description' as description,
                  c->>'recipient' as vendor,
                  cast(c->>'date' as date) as date,
                  c->>'stance' as stance,
                  c->>'ballot_number' as ballot_number
              from json_array_elements(v_json->'independent_expenditures_ballot') c
              join json_array_elements(v_json->'sources') s on c->>'source' = s->>'id'
          )
              LOOP
                  INSERT INTO l2_transaction (source_target_type, source_target_id, source_name, report_id, amount, transaction_type)
                  VALUES(v_rec.source_type, v_rec.source_id, v_rec.source_name, p_report_id, v_rec.cost, 'independent_expenditures_ballot_expense')
                  returning l2_transaction_id into v_transaction_id;

                  INSERT INTO l2_expense(l2_transaction_id, description, date, vendor)
                  VALUES (v_transaction_id, v_rec.description, v_rec.date, v_rec.vendor);

                  INSERT INTO l2_independent_expenditure(l2_transaction_id, ballot_number, stance)
                  VALUES (v_transaction_id, v_rec.ballot_number, v_rec.stance);
              END LOOP;
      end if;
      -- lobbying activities
      perform l2_save_lobbying_activity(p_report_id);
    END
$$;
