create or replace function l2_legacy_transactions(p_json json) returns void
    language plpgsql as
$$
declare
    v_report_id int := cast(p_json->>'nid' as int);
    v_firm_info record;
    n_compensation_rec record;
    v_source_target_id int;
    v_source_name text;
    n_sub_contract_lob_rec record;
    n_personal_expense_rec record;
    n_advertising_rec record;
    n_advertising_political_ads_rec record;
    v_expense_info record;
    n_non_itemized_entertainment_expense_rec record;
    n_itemized_entertainment_expense_rec record;
    v_l2_transaction_id int;
    n_itemized_contribution_rec record;
    n_non_itemized_contribution_rec record;
    n_other_expense_rec record;
    n_itemized_pac_contribution_rec record;
    n_itemized_no_pac_contribution_rec record;
    v_occasion_type text;
    begin
    --due to the possibility of an amendment, we first delete the existing transactions
    delete from l2_transaction where report_id = v_report_id;

    --grab the firm info for use throughout the file
    select * into v_firm_info from lobbyist_firm where lobbyist_firm_id
                                                           = cast(p_json->'og_group_ref'->'und'->0->>'target_id' as int);

    --If any compensation was reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_compensation') = 'object' then
        for n_compensation_rec in select j as n_json from json_array_elements(p_json->'field_compensation'->'und') j
            loop
                --grab the contract title from the most recent submission version
                v_source_target_id :=  cast(n_compensation_rec.n_json->'field_employer'->'und'->0->>'target_id' as int);
                select user_data->>'title' into v_source_name from lobbyist_contract_submission
                                              where lobbyist_contract_id = v_source_target_id and superseded_id is null;

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          'lobbyist_contract',
                          v_source_target_id,
                          v_source_name,
                          v_report_id,
                          cast(n_compensation_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'compensation'
                      );
            end loop;
    end if;

    --If any personal expenses were reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_personal_expenses') = 'object' then
        for n_personal_expense_rec in select j as n_json from json_array_elements(p_json->'field_personal_expenses'->'und') j
            loop
                v_source_target_id :=  cast(n_personal_expense_rec.n_json->'field_employer'->'und'->0->>'target_id' as int);

                --grab the contract title from the most recent submission version
                select user_data->>'title' into v_source_name from lobbyist_contract_submission
                where lobbyist_contract_id = v_source_target_id and superseded_id is null;

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                    --if v_source_name is null then it's the firm as the source
                    case when v_source_target_id = v_firm_info.lobbyist_firm_id
                        then 'lobbyist_firm' else 'lobbyist_contract' end,
                    v_source_target_id,
                    coalesce(v_source_name, v_firm_info.name),
                    v_report_id,
                    cast(n_personal_expense_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                    'personal_expense'
                      );
            end loop;
    end if;

    --if any sum of non-reimbursed personal expenses were reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_non_reimbursed_amount_') = 'object' then

        insert into l2_transaction(
            source_target_type,
            source_target_id,
            source_name,
            report_id,
            amount,
            transaction_type)
        values(
                  'lobbyist_firm',
                  v_firm_info.lobbyist_firm_id,
                  v_firm_info.name,
                  v_report_id,
                  cast(p_json->'field_non_reimbursed_amount_'->'und'->0->>'value' as numeric),
                  'non_reimbursed_personal_expenses'
              );
    end if;

    --If any sub-contracted lobbyist expenses were reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_sub_lobbyist_comp') = 'object' then
        for n_sub_contract_lob_rec in select j as n_json from json_array_elements(p_json->'field_sub_lobbyist_comp'->'und') j
            loop
                --grab the contract title from the most recent submission version
                v_source_target_id :=  cast(n_sub_contract_lob_rec.n_json->'field_sub_lobbyist'->'und'->0->>'target_id' as int);
                select concat(lc2.name, ' (', f.name, ')') into v_source_name from lobbyist_contract lc
                    left join lobbyist_client lc2 on lc2.lobbyist_client_id = lc.lobbyist_client_id
                    left join lobbyist_firm f on f.lobbyist_firm_id = lc.lobbyist_firm_id
                where lobbyist_contract_id = v_source_target_id;

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          'lobbyist_contract',
                          v_source_target_id,
                          v_source_name,
                          v_report_id,
                          cast(n_sub_contract_lob_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'sub_lobbyist_payment'
                      );
            end loop;
    end if;

    --If any advertising expenses were reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_advertising') = 'object' then
        for n_advertising_rec in select j as n_json from json_array_elements(p_json->'field_advertising'->'und') j
            loop
                v_source_target_id :=  cast(n_advertising_rec.n_json->'field_employer'->'und'->0->>'target_id' as int);

                --grab the contract title from the most recent submission version
                select user_data->>'title' into v_source_name from lobbyist_contract_submission
                where lobbyist_contract_id = v_source_target_id and superseded_id is null;

                select lc2.* into v_expense_info from lobbyist_contract lc
                    left join lobbyist_client lc2 on lc2.lobbyist_client_id = lc.lobbyist_client_id
                     where lc.lobbyist_contract_id = v_source_target_id;

                --if there is not a contract, then just reassign to the firm_info
                if v_expense_info.name is null then
                    v_expense_info := v_firm_info;
                end if;

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          --if v_source_name is null then it's the firm as the source
                          case when v_source_target_id = v_firm_info.lobbyist_firm_id
                              then 'lobbyist_firm' else 'lobbyist_contract' end,
                          v_source_target_id,
                          coalesce(v_source_name, v_firm_info.name),
                          v_report_id,
                          cast(n_advertising_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'advertising_expense'
                      )
                returning l2_transaction_id into v_l2_transaction_id;

                insert into l2_expense(
                    l2_transaction_id,
                    address,
                    city,
                    state,
                    postcode,
                    vendor)
                VALUES(
                          v_l2_transaction_id,
                          v_expense_info.address_1,
                          v_expense_info.city,
                          v_expense_info.state,
                          v_expense_info.postcode,
                          v_expense_info.name
                      );
            end loop;
    end if;

    --If any advertising expenses Political ads, public, relations, polling were reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_political_ads') = 'object' then
        for n_advertising_political_ads_rec in select j as n_json from json_array_elements(p_json->'field_political_ads'->'und') j
            loop
                v_source_target_id :=  cast(n_advertising_political_ads_rec.n_json->'field_employer'->'und'->0->>'target_id' as int);

                --grab the contract title from the most recent submission version
                select user_data->>'title' into v_source_name from lobbyist_contract_submission
                where lobbyist_contract_id = v_source_target_id and superseded_id is null;

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          --if v_source_name is null then it's the firm as the source
                          case when v_source_target_id = v_firm_info.lobbyist_firm_id
                                   then 'lobbyist_firm' else 'lobbyist_contract' end,
                          v_source_target_id,
                          coalesce(v_source_name, v_firm_info.name),
                          v_report_id,
                          cast(n_advertising_political_ads_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'ad_expense'
                      )
                returning l2_transaction_id into v_l2_transaction_id;

                insert into l2_expense(
                    l2_transaction_id,
                    date,
                    description,
                    vendor)
                VALUES(
                          v_l2_transaction_id,
                          cast(n_advertising_political_ads_rec.n_json->'field_created_at'->'und'->0->>'value' as date),
                          n_advertising_political_ads_rec.n_json->'field_short_description'->'und'->0->>'value',
                          n_advertising_political_ads_rec.n_json->'field_person_s_entertained'->'und'->0->>'value'
                      );
            end loop;
    end if;

    --If any non-itemized entertainment expenses were reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_non_item_ent') = 'object' then
        for n_non_itemized_entertainment_expense_rec in select j as n_json from json_array_elements(p_json->'field_non_item_ent'->'und') j
            loop
                v_source_target_id :=  cast(n_non_itemized_entertainment_expense_rec.n_json->'field_employer'->'und'->0->>'target_id' as int);
                --grab the contract title from the most recent submission version
                select user_data->>'title' into v_source_name from lobbyist_contract_submission where lobbyist_contract_id = v_source_target_id and superseded_id is null;

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          case when v_source_target_id = v_firm_info.lobbyist_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end,
                          v_source_target_id,
                          coalesce(v_source_name, v_firm_info.name),
                          v_report_id,
                          cast(n_non_itemized_entertainment_expense_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'non_itemized_entertainment_expenses'
                      );
            end loop;
    end if;

    -- If any itemized entertainment expenses were reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_entertainment') = 'object' then
        for n_itemized_entertainment_expense_rec in select j as n_json from json_array_elements(p_json->'field_entertainment'->'und') j
            loop
                v_source_target_id :=  cast(n_itemized_entertainment_expense_rec.n_json->'field_employer'->'und'->0->>'target_id' as int);
                select user_data->>'title' into v_source_name from lobbyist_contract_submission where lobbyist_contract_id = v_source_target_id and superseded_id is null;
                v_occasion_type = cast(n_itemized_entertainment_expense_rec.n_json->'field_type_of_occasion'->'und'->0->>'value' as text);

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          case when v_source_target_id = v_firm_info.lobbyist_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end,
                          v_source_target_id,
                          coalesce(v_source_name, v_firm_info.name),
                          v_report_id,
                          cast(n_itemized_entertainment_expense_rec.n_json->'field_cost'->'und'->0->>'value' as numeric),
                          'itemized_entertainment_expense'
                      ) returning l2_transaction_id into v_l2_transaction_id;

                insert into l2_entertainment(
                    l2_transaction_id,
                    occasion_type,
                    place,
                    participants)
                values (
                          v_l2_transaction_id,
                          case when v_occasion_type = '1' then 'Entertainment'
                               when v_occasion_type = '2' then 'Reception'
                               when v_occasion_type = '3' then 'Travel,lodging and subsistence'
                               when v_occasion_type = '4' then 'Enrollment and course fees'
                          end,
                          n_itemized_entertainment_expense_rec.n_json->'field_place'->'und'->0->>'value',
                          n_itemized_entertainment_expense_rec.n_json->'field_participant_listing'->'und'->0->>'value'
                      );

                insert into l2_expense(
                    l2_transaction_id,
                    date,
                    description,
                    vendor)
                values (
                           v_l2_transaction_id,
                           cast(n_itemized_entertainment_expense_rec.n_json->'field_occasion_date'->'und'->0->>'value' as date),
                           n_itemized_entertainment_expense_rec.n_json->'field_occasion_description'->'und'->0->>'value',
                           coalesce(v_source_name, v_firm_info.name)
                       );
            end loop;
    end if;

    --If any itemized contributions was reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_contributions') = 'object' then
        for n_itemized_contribution_rec in select j as n_json from json_array_elements(p_json->'field_contributions'->'und') j
            loop
                --grab the contract title from the most recent submission version
                v_source_target_id :=  cast(n_itemized_contribution_rec.n_json->'field_employer'->'und'->0->>'target_id' as int);
                select user_data->>'title' into v_source_name from lobbyist_contract_submission
                where lobbyist_contract_id = v_source_target_id and superseded_id is null;

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          case when v_source_target_id = v_firm_info.lobbyist_firm_id
                                   then 'lobbyist_firm' else 'lobbyist_contract' end,
                          v_source_target_id,
                          coalesce(v_source_name, v_firm_info.name),
                          v_report_id,
                          cast(n_itemized_contribution_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'itemized_contribution'
                      ) returning l2_transaction_id into v_l2_transaction_id;

                insert into l2_expense(
                    l2_transaction_id,
                    vendor,
                    date)
                values (
                           v_l2_transaction_id,
                           n_itemized_contribution_rec.n_json->'field_person_s_entertained'->'und'->0->>'value',
                           cast(n_itemized_contribution_rec.n_json->'field_created_at'->'und'->0->>'value' as date)
                       );
            end loop;
    end if;

    --If any non itemized contributions was reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_employer_transmit') = 'object' then
        for n_non_itemized_contribution_rec in select j as n_json from json_array_elements(p_json->'field_employer_transmit'->'und') j
            loop
                --grab the contract title from the most recent submission version
                v_source_target_id :=  cast(n_non_itemized_contribution_rec.n_json->'field_employer'->'und'->0->>'target_id' as int);
                select user_data->>'title' into v_source_name from lobbyist_contract_submission
                where lobbyist_contract_id = v_source_target_id and superseded_id is null;

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          case when v_source_target_id = v_firm_info.lobbyist_firm_id
                                   then 'lobbyist_firm' else 'lobbyist_contract' end,
                          v_source_target_id,
                          coalesce(v_source_name, v_firm_info.name),
                          v_report_id,
                          cast(n_non_itemized_contribution_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'non_itemized_contributions'
                      );
            end loop;
    end if;

    -- If any Other expenses details were reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_other') = 'object' then
        for n_other_expense_rec in select j as n_json from json_array_elements(p_json->'field_other'->'und') j
            loop
                v_source_target_id :=  cast(n_other_expense_rec.n_json->'field_employer'->'und'->0->>'target_id' as int);
                select user_data->>'title' into v_source_name from lobbyist_contract_submission where lobbyist_contract_id = v_source_target_id and superseded_id is null;

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          case when v_source_target_id = v_firm_info.lobbyist_firm_id then 'lobbyist_firm' else 'lobbyist_contract' end,
                          v_source_target_id,
                          coalesce(v_source_name, v_firm_info.name),
                          v_report_id,
                          cast(n_other_expense_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'other_expense'
                      ) returning l2_transaction_id into v_l2_transaction_id;

                insert into l2_expense(
                    l2_transaction_id,
                    address,
                    city,
                    state,
                    postcode,
                    vendor,
                    date)
                values (
                          v_l2_transaction_id,
                          (n_other_expense_rec.n_json->'field_recipient_address'->'und'->0->>'thoroughfare') || ' ' || (n_other_expense_rec.n_json->'field_recipient_address'->'und'->0->>'premise'),
                          n_other_expense_rec.n_json->'field_recipient_address'->'und'->0->>'locality',
                          n_other_expense_rec.n_json->'field_recipient_address'->'und'->0->>'administrative_area',
                          n_other_expense_rec.n_json->'field_recipient_address'->'und'->0->>'postal_code',
                          n_other_expense_rec.n_json->'field_person_s_entertained'->'und'->0->>'value',
                          cast(n_other_expense_rec.n_json->'field_created_at'->'und'->0->>'value' as date)
                      );
            end loop;
    end if;

    --If any itemized contributions by employer's pac was reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_cont_emp_pac') = 'object' then
        for n_itemized_pac_contribution_rec in select j as n_json from json_array_elements(p_json->'field_cont_emp_pac'->'und') j
            loop
                v_source_target_id :=  cast(n_itemized_pac_contribution_rec.n_json->'field_employer'->'und'->0->>'target_id' as int);

                insert into l2_transaction(
                    source_target_type,
                    source_target_id,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          'legacy_committee',
                          -- In this case we attached the employer's/ firm's target_id as it's the only tie we have with legacy data
                          v_source_target_id,
                          n_itemized_pac_contribution_rec.n_json->'field_pac_name'->'und'->0->>'value',
                          v_report_id,
                          cast(n_itemized_pac_contribution_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'itemized_contribution'
                      ) returning l2_transaction_id into v_l2_transaction_id;

                insert into l2_expense(
                    l2_transaction_id,
                    vendor,
                    date)
                values (
                           v_l2_transaction_id,
                           n_itemized_pac_contribution_rec.n_json->'field_person_s_entertained'->'und'->0->>'value',
                           cast(n_itemized_pac_contribution_rec.n_json->'field_occasion_date'->'und'->0->>'value' as date)
                       );
            end loop;
    end if;

    --If any other itemized contributions were reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_cont_no_pac') = 'object' then
        for n_itemized_no_pac_contribution_rec in select j as n_json from json_array_elements(p_json->'field_cont_no_pac'->'und') j
            loop

                insert into l2_transaction(
                    source_target_type,
                    source_name,
                    report_id,
                    amount,
                    transaction_type)
                values(
                          'other',
                          n_itemized_no_pac_contribution_rec.n_json->'field_name_of_contributor'->'und'->0->>'value',
                          v_report_id,
                          cast(n_itemized_no_pac_contribution_rec.n_json->'field_amount'->'und'->0->>'value' as numeric),
                          'itemized_contribution'
                      ) returning l2_transaction_id into v_l2_transaction_id;

                insert into l2_expense(
                    l2_transaction_id,
                    vendor,
                    date)
                values (
                           v_l2_transaction_id,
                           n_itemized_no_pac_contribution_rec.n_json->'field_person_s_entertained'->'und'->0->>'value',
                           cast(n_itemized_no_pac_contribution_rec.n_json->'field_occasion_date'->'und'->0->>'value' as date)
                       );
            end loop;
    end if;

    --if any sum of non-itemized contributions were reported this will appear as an object, otherwise an empty array
    if json_typeof(p_json->'field_aggregate_total_of_all_non') = 'object' then

        insert into l2_transaction(
            source_target_type,
            report_id,
            amount,
            transaction_type)
        values(
                  'other',
                  v_report_id,
                  cast(p_json->'field_aggregate_total_of_all_non'->'und'->0->>'value' as numeric),
                  'non_itemized_contributions'
              );
    end if;
end
$$