DROP FUNCTION IF EXISTS lobbyist_public_agency_access_verification(text,text,integer,integer,json);
create or replace function lobbyist_public_agency_access_verification(user_uid text, user_realm text, filer_request_id int, agency_id int, json_data json) returns json
    language plpgsql as
$$
DECLARE
    new_lobbyist_id int;
    filer_request_record record;
    pdc_user_name text;
    access_users json;
BEGIN
    --grab the pdc_user_name from the uid and realm of the admin user
    select p.user_name into pdc_user_name from pdc_user p where p.uid = user_uid and p.realm = user_realm;

    --grab the filer_request record
    select f.* into filer_request_record from filer_request f where f.id = filer_request_id;

    --either create or update lobbyist_public_agency record based on the id being passed
    if agency_id is null then
        insert into lobbyist_public_agency(name, address, city, state, zip, county, contact, phone, email, head, verified)
        select json_data->>'name', json_data->>'address', json_data->>'city', json_data->>'state', json_data->>'zip', json_data->>'county', json_data->>'contact', json_data->>'phone', json_data->>'email', json_data->>'head', true
        returning id into new_lobbyist_id;
    else
        update lobbyist_public_agency
        set name = json_data->>'name',
            address = json_data->>'address',
            city = json_data->>'city',
            state = json_data->>'state',
            zip = json_data->>'zip',
            county = json_data->>'county',
            contact = json_data->>'contact',
            phone = json_data->>'phone',
            email = json_data->>'email',
            head = json_data->>'head',
            verified = true
        where id = agency_id;
    end if;

    --write record to filer_request_log
    insert into filer_request_log(user_name, target_type, target_id)
    values(pdc_user_name, filer_request_record.target_type, coalesce(new_lobbyist_id, agency_id));

    --write record into pdc_user_authorization insert or update
    insert into pdc_user_authorization (realm, uid, target_type, target_id, allow_modify, allow_submit, granted_date, expiration_date, modified)
    values(filer_request_record.realm, filer_request_record.uid, filer_request_record.target_type,
           coalesce(new_lobbyist_id, agency_id), true, true, now(), null, now())
    on conflict (realm, uid, target_type, target_id) do
        update set
           modified = case when excluded.expiration_date is distinct from(pdc_user_authorization.expiration_date) then now()
                      else pdc_user_authorization.modified end,
           expiration_date = case
                             when excluded.expiration_date is null and pdc_user_authorization.expiration_date is not null then null
                             when excluded.expiration_date is not null then excluded.expiration_date
                             else pdc_user_authorization.expiration_date end;

    --emails from other authorized users
    select json_agg(puc.target) as email into access_users
   from pdc_user_authorization pua
             left join pdc_user_contact puc on puc.uid = pua.uid and puc.realm = pua.realm
    where pua.target_id = agency_id
      and pua.target_type = 'lobbyist_public_agency'
      and puc.protocol = 'email'
      and (pua.uid != filer_request_record.uid or pua.realm != filer_request_record.realm)
      and (pua.expiration_date IS NULL OR pua.expiration_date > CURRENT_TIMESTAMP);

    --delete the record from the filer_request table
    delete from filer_request fr where fr.id = filer_request_record.id;

    return access_users;
END;
$$
