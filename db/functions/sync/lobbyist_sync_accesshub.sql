CREATE OR REPLACE FUNCTION lobbyist_sync_accesshub(p_json JSON )returns bool language plpgsql
as $$
  declare v_node_type text;
      v_publish_status integer;
      n_rec record;
  begin
    for n_rec in select j as n_json from json_array_elements(p_json) j loop
        v_node_type := n_rec.n_json->>'type';
        v_publish_status := cast(n_rec.n_json->>'status' as int);
        if v_publish_status = 1 then
            if v_node_type = 'lobbyist_firm' then
              perform lobbyist_sync_firm(n_rec.n_json);
            elsif v_node_type = 'lobbyist_employer' then
              perform lobbyist_sync_employer(n_rec.n_json);
            elsif v_node_type = 'firm_emp_relationship' then
              perform lobbyist_sync_contract(n_rec.n_json);
            elsif v_node_type = 'l2_filing' then
              perform lobbyist_sync_l2(n_rec.n_json);
              perform l2_legacy_transactions(n_rec.n_json);
            elsif v_node_type = 'l3_filing' then
              perform lobbyist_sync_l3(n_rec.n_json);
            else
              raise  'Unknown type %', v_node_type;
            end if;
        else
            if v_node_type = 'l2_filing' then
                perform lobbyist_sync_draft_l2(n_rec.n_json);
            elsif v_node_type = 'l3_filing' then
                perform lobbyist_sync_draft_l3(n_rec.n_json);
            elsif v_node_type = 'firm_emp_relationship' then
                perform lobbyist_sync_draft_contract(n_rec.n_json);
            end if;
        end if;
    end loop;
    return true;
  end
$$;