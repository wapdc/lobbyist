CREATE OR REPLACE FUNCTION lobbyist_sync_l2(p_json JSON) RETURNS VOID language plpgsql as
$$
DECLARE
    v_report_id INTEGER;
    v_vid INTEGER;
    v_period_start DATE;
    v_period_end DATE;
    v_firm_id INTEGER;
    e_report RECORD;
    v_submitted_at timestamptz;
    v_draft_id int;
BEGIN
    v_report_id := cast(p_json->>'nid' as INTEGER);
    v_vid := cast(p_json->>'vid' as integer);
    v_period_start := CAST(p_json->'field_filing_period_date'->'und'->0->>'value' as text):: date;
    v_period_end := v_period_start + interval '1 MONTH - 1 DAY';
    v_firm_id := cast(p_json->'og_group_ref'->'und'->0->>'target_id' as integer);
    v_submitted_at := to_timestamp(CAST(p_json->>'revision_timestamp' as numeric));

    SELECT draft_id into v_draft_id from private.draft_document where report_key = v_period_start::text
                                                                  and report_type = 'L2-Legacy'
                                                                  and target_type = 'lobbyist_firm'
                                                                  and target_id = v_firm_id;
    SELECT lobbyist_firm_id into v_firm_id from lobbyist_firm where lobbyist_firm_id= v_firm_id;

    if v_firm_id is not null then

        SELECT r.report_id, s.submission_id, s.submitted_at  INTO e_report FROM l2 r
            left join l2_submission s on r.current_submission_id = s.submission_id
            where r.report_id=v_report_id;

        if e_report.report_id is null then
            INSERT INTO l2(report_id, lobbyist_firm_id, current_submission_id, drupal_nid, period_start, period_end)
            VALUES(v_report_id, v_firm_id, v_vid, v_report_id, v_period_start, v_period_end);
        else
            IF e_report.submitted_at < v_submitted_at THEN
              update l2_submission set superseded_id = v_vid where submission_id = e_report.submission_id;
              update l2 set period_start=v_period_start, period_end = v_period_end, current_submission_id = v_vid
                where report_id = v_report_id;
            end if;
        end if;

        -- Insert the base submission
        INSERT INTO l2_submission(
            submission_id, report_id,
            version, submitted_at, user_data
        ) VALUES (
                     CAST(p_json->>'vid' as int),
                     CAST(p_json->>'nid' as int),
                     '1.0',
                     to_timestamp(cast(p_json->>'revision_timestamp' as numeric)),
                                  p_json
                 ) ON CONFLICT (submission_id) DO UPDATE set submitted_at= excluded.submitted_at, user_data=excluded.user_data;
        if v_draft_id is not null then
            delete from private.draft_document
            where draft_id = v_draft_id;
        end if;
    end if;
    --due to the desire to have tid the appropriate value, and sources to be included in json for the public view.
    perform update_legacy_report_data(CAST(p_json->>'vid' as int));
END
$$;
