CREATE OR REPLACE FUNCTION lobbyist_sync_draft_l3(p_json JSON) RETURNS VOID language plpgsql as
$$
DECLARE
    v_report_year text;
    v_client_id INTEGER;
    v_updated_at timestamptz;
    v_user RECORD;
    v_existing_draft int;
    v_save_count int;
BEGIN

    v_report_year := substr(CAST(p_json->'field_filing_period_year'->'und'->0->>'value' as text),1,4);
    v_client_id := cast(p_json->'og_group_ref'->'und'->0->>'target_id' as integer);
    v_updated_at := to_timestamp(CAST(p_json->>'revision_timestamp' as numeric));
    SELECT lobbyist_client_id into v_client_id from lobbyist_client
                           where lobbyist_client_id = cast(p_json->'og_group_ref'->'und'->0->>'target_id' as integer);

    if v_client_id is not null then
            select * into v_user from pdc_user where accesshub_uid = cast(p_json->>'revision_uid' as integer);
            select draft_id, save_count into v_existing_draft, v_save_count from private.draft_document
                                                where target_type = 'lobbyist_client' and target_id = v_client_id
                                                  and report_key = v_report_year and report_type = 'L3-Legacy';

            if v_existing_draft is not null then
                update private.draft_document
                set save_count = v_save_count + 1,
                    uid = v_user.uid,
                    realm = v_user.realm,
                    user_data = p_json,
                    updated_at = v_updated_at
                where draft_id = v_existing_draft;
            else
                insert into private.draft_document(target_type, target_id, save_count, user_data, uid, realm, report_type, report_key, updated_at)
                values('lobbyist_client', v_client_id, 0, p_json, v_user.uid, v_user.realm, 'L3-Legacy', v_report_year, v_updated_at);
            end if;
        end if;
END
$$;
