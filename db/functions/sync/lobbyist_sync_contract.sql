CREATE OR REPLACE FUNCTION lobbyist_sync_contract(p_json JSON) RETURNS VOID language plpgsql as
$$
DECLARE
    v_contract_id INTEGER;
    v_vid INTEGER;
    v_submitted_at timestamptz;
    v_lobbyist_firm_id INTEGER;
    v_lobbyist_client_id INTEGER;
    v_lobbyist_contractor_id INTEGER;
    v_subcontractor TEXT;
    e_reg RECORD;
    j_periods JSON;
    j_exempts JSON;
BEGIN
    v_contract_id := cast(p_json->>'nid' as int);
    v_submitted_at := to_timestamp(CAST(p_json->>'revision_timestamp' as numeric));
    v_vid := CAST(p_json->>'vid' AS INTEGER);
    select lobbyist_client_id INTO v_lobbyist_client_id
        from lobbyist_client where lobbyist_client_id = CAST(p_json->'field_employer_entity_reference'->'und'->0->>'target_id' as int);
    select lobbyist_firm_id INTO v_lobbyist_firm_id
       from lobbyist_firm where lobbyist_firm_id = cast(p_json->'field_firm_entity_reference'->'und'->0->>'target_id' as int);
    select lobbyist_firm_id into v_lobbyist_contractor_id
      from lobbyist_firm where lobbyist_firm_id = cast(p_json->'field_firm_that_is_employing_you'->'und'->0->>'target_id' as int);

    v_subcontractor := cast(p_json->'field_employer_or_subcontracting'->'und'->0->>'value' as text);
    if v_subcontractor = 'subcontractor' then
        delete from private.draft_document  where report_type = 'L-Contract-Legacy'
          and target_type = 'lobbyist_firm' and target_id= v_lobbyist_contractor_id
          and report_key = v_contract_id::text;
    else
        delete from private.draft_document  where report_type = 'L-Contract-Legacy'
                                              and target_type = 'lobbyist_client' and target_id= v_lobbyist_client_id
                                              and report_key = v_contract_id::text;
    end if;

    IF v_lobbyist_client_id is not null and v_lobbyist_firm_id is not null then
        -- Find existing registration
        SELECT e.lobbyist_contract_id, les.submitted_at, les.submission_id  into e_reg
        from lobbyist_contract e
                 left join lobbyist_contract_submission les on e.current_submission_id = les.submission_id
        where e.lobbyist_contract_id = v_contract_id;

        IF e_reg.lobbyist_contract_id is nulL THEN
            insert into lobbyist_contract(lobbyist_contract_id, lobbyist_firm_id, lobbyist_client_id, current_submission_id, drupal_nid, contractor_id)
              values (v_contract_id, v_lobbyist_firm_id, v_lobbyist_client_id, v_vid, v_contract_id, v_lobbyist_contractor_id);

        else
            if e_reg.submitted_at < v_submitted_at or e_reg.submitted_at is null THEN
                update lobbyist_client_submission
                set superseded_id = cast(p_json->>'vid' as int)
                where submission_id = e_reg.submission_id;

                UPDATE lobbyist_contract set
                     current_submission_id = cast(p_json->>'vid' as integer),
                     lobbyist_firm_id = v_lobbyist_firm_id,
                     lobbyist_client_id= v_lobbyist_client_id,
                     contractor_id = v_lobbyist_contractor_id
                where lobbyist_contract_id = v_contract_id;

            end if;
        end if;

        -- Insert reporting periods
        j_periods := p_json->'field_select_your_filing_periods'->'und';

        -- Make sure we have an exempt list
        j_exempts := COALESCE(p_json->'field_exemption_periods'->'und', '[]'::JSON);

        INSERT INTO lobbyist_reporting_periods (lobbyist_client_id, lobbyist_firm_id, period_start, period_end, lobbyist_contract_id, exempt)
          SELECT distinct v_lobbyist_client_id, v_lobbyist_firm_id, rp.period_start, rp.period_end, v_contract_id,
                          case when ep.exempt_period is not null then true else false end
                          FROM (SELECT
                           cast(j->>'value' as text)::date as period_start,
                           date_trunc('month',cast(j->>'value' as text)::date) + interval '1 month - 1 day' as period_end
           from json_array_elements(j_periods) j) rp
              LEFT JOIN (select cast(je->>'value' as text)::date as exempt_period
                    from json_array_elements(j_exempts) je) ep on ep.exempt_period=rp.period_start
        on conflict(period_start, period_end, lobbyist_contract_id) do update
        set lobbyist_contract_id = excluded.lobbyist_contract_id, exempt = excluded.exempt;

        delete from lobbyist_reporting_periods
          where lobbyist_contract_id = v_contract_id
             and period_start not in (select cast(j->'value' as text)::date from json_array_elements(j_periods) j);

        perform l1_legacy_contract_update(v_contract_id);

        -- Insert the base submission
        INSERT INTO lobbyist_contract_submission(
            submission_id, lobbyist_contract_id,
            version, submitted_at, user_data
        ) VALUES (
                     CAST(p_json->>'vid' as int),
                     CAST(p_json->>'nid' as int),
                     '1.0',
                     to_timestamp(cast(p_json->>'revision_timestamp' as numeric)),
                                  p_json
                 ) ON CONFLICT (submission_id) DO UPDATE set submitted_at= excluded.submitted_at, user_data=excluded.user_data;
    end if;
    perform update_l1_legacy_report_data(cast(p_json->>'vid' as int));
END
$$;
