CREATE OR REPLACE FUNCTION lobbyist_sync_deletions(p_json JSON )returns bool language plpgsql
as $$
declare v_node_type text;
        n_rec record;
begin
  for n_rec in select j as n_json from json_array_elements(p_json) j loop
      v_node_type := n_rec.n_json->>'type';
        if v_node_type = 'lobbyist_firm' then
          delete from lobbyist_firm_submission where lobbyist_firm_id in (select lobbyist_firm_id from lobbyist_firm where drupal_nid = (n_rec.n_json->>'nid')::int);
          delete from lobbyist_agent where lobbyist_firm_id in (select lobbyist_firm_id from lobbyist_firm where drupal_nid = (n_rec.n_json->>'nid')::int);
          delete from l2_submission where report_id in (select report_id from l2 where lobbyist_firm_id in
                                    (select lobbyist_firm_id from lobbyist_firm where drupal_nid = (n_rec.n_json->>'nid')::int));
          delete from l2 where lobbyist_firm_id = (select lobbyist_firm_id from lobbyist_firm where drupal_nid = (n_rec.n_json->>'nid')::int);
          delete from lobbyist_contract where lobbyist_firm_id = (select lobbyist_firm_id from lobbyist_firm where drupal_nid = (n_rec.n_json->>'nid')::int);
          delete from lobbyist_firm where drupal_nid = (n_rec.n_json->>'nid')::int;
          delete from pdc_user_authorization where pdc_user_authorization.target_type = 'lobbyist_firm'
                                                            and wapdc.public.pdc_user_authorization.target_id = (n_rec.n_json->>'nid')::int;
          delete from collection_member where target_id = (select lobbyist_firm_id from lobbyist_firm where drupal_nid = (n_rec.n_json->>'nid')::int)
                                          and collection_id in (select collection_id from collection where target = 'lobbyist_firm');
        elsif v_node_type = 'lobbyist_employer' then
          delete from lobbyist_contract where lobbyist_client_id in (select lobbyist_client_id from lobbyist_client where drupal_nid = (n_rec.n_json->>'nid')::int);
          delete from l3_submission where report_id in (select report_id from l3 where lobbyist_client_id in
                                                                                           (select lobbyist_client_id from lobbyist_client where drupal_nid = (n_rec.n_json->>'nid')::int));
          delete from l3 where lobbyist_client_id = (select lobbyist_client_id from lobbyist_client where drupal_nid = (n_rec.n_json->>'nid')::int);
          delete from collection_member where target_id = (select lobbyist_client_id from lobbyist_client where drupal_nid = (n_rec.n_json->>'nid')::int)
                                          and collection_id in (select collection_id from collection where target = 'lobbyist_client');
          delete from lobbyist_client_submission lcs where lobbyist_client_id = (n_rec.n_json->>'nid')::int;
          delete from pdc_user_authorization where pdc_user_authorization.target_type = 'lobbyist_client'
            and pdc_user_authorization.target_id = (n_rec.n_json->>'nid')::int;
          delete from lobbyist_client where drupal_nid = (n_rec.n_json->>'nid')::int;
        elsif v_node_type = 'firm_emp_relationship' then
            if(cast(n_rec.n_json->>'status' as integer) = 0) then
              delete from private.draft_document
                    where target_type = 'lobbyist_client'
                      and target_id = cast(n_rec.n_json->'field_employer_entity_reference'->'und'->0->>'target_id' as integer)
                      and report_type = 'L-Contract-Legacy'
                      and report_key = cast(n_rec.n_json->>'nid' as text);
            else
              delete from lobbyist_contract_submission where lobbyist_contract_id in (select lobbyist_contract_id from lobbyist_contract where drupal_nid = (n_rec.n_json->>'nid')::int);
              delete from lobbyist_contract where drupal_nid = (n_rec.n_json->>'nid')::int;
            end if;
        elsif v_node_type = 'l2_filing' then
            if(cast(n_rec.n_json->>'status' as integer) = 0) then
                delete from private.draft_document
                where target_type = 'lobbyist_firm'
                  and target_id = cast(n_rec.n_json->'og_group_ref'->'und'->0->>'target_id' as integer)
                  and report_type = 'L2-Legacy'
                  and report_key = cast(n_rec.n_json->'field_filing_period_date'->'und'->0->>'value' as date)::text;
            else
                delete from l2_submission where report_id in (select report_id from l2 where drupal_nid = (n_rec.n_json->>'nid')::int);
                delete from l2 where drupal_nid = (n_rec.n_json->>'nid')::int;
            end if;
        elsif v_node_type = 'l3_filing' then
            if(cast(n_rec.n_json->>'status' as integer) = 0) then
                delete from private.draft_document
                    where target_type = 'lobbyist_client'
                    and target_id = cast(n_rec.n_json->'og_group_ref'->'und'->0->>'target_id' as integer)
                    and report_type = 'L3-Legacy'
                    and report_key = substr(CAST(n_rec.n_json->'field_filing_period_year'->'und'->0->>'value' as text),1,4);
            else
                delete from l3_submission where report_id in (select report_id from l3 where drupal_nid = (n_rec.n_json->>'nid')::int);
                delete from l3 where drupal_nid = (n_rec.n_json->>'nid')::int;
            end if;
        else
          raise  'Unknown type %', v_node_type;
        end if;
    end loop;
  return true;
end
$$;