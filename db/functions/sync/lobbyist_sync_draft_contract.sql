CREATE OR REPLACE FUNCTION lobbyist_sync_draft_contract(p_json JSON) RETURNS VOID language plpgsql as
$$
DECLARE
    v_drupal_nid text;
    v_client_id INTEGER;
    v_subcontractor TEXT;
    v_updated_at timestamptz;
    v_user RECORD;
    v_target_type text;
    v_existing_draft int;
    v_save_count int;
BEGIN
    v_drupal_nid := cast(p_json->>'nid' as text);
    v_subcontractor := cast(p_json->'field_employer_or_subcontracting'->'und'->0->>'value' as text);
    v_updated_at := to_timestamp(CAST(p_json->>'revision_timestamp' as numeric));
    -- The client id will be used to store the pointer to the firm/client so that we can find certification lookups based on who need to approve it
    if v_subcontractor = 'subcontractor' then
        -- Contract is certified by the firm that you are contracting with
        SELECT lobbyist_firm_id into v_client_id from lobbyist_firm
        where lobbyist_firm_id = cast(p_json->'field_firm_that_is_employing_you'->'und'->0->>'target_id' as integer);
        v_target_type =  'lobbyist_firm';
    else
        -- Contract is certified by the client
        SELECT lobbyist_client_id into v_client_id from lobbyist_client
        where lobbyist_client_id = cast(p_json->'field_employer_entity_reference'->'und'->0->>'target_id' as integer);
        v_target_type = 'lobbyist_client';
    end if;

    if v_client_id is not null then
        select * into v_user from pdc_user where accesshub_uid = cast(p_json->>'revision_uid' as integer);
        select draft_id, save_count into v_existing_draft, v_save_count from private.draft_document
        where target_type = v_target_type and target_id = v_client_id
          and report_key = v_drupal_nid and report_type = 'L-Contract-Legacy';

        if v_existing_draft is not null then
            update private.draft_document
            set save_count = v_save_count + 1,
                uid = v_user.uid,
                realm = v_user.realm,
                user_data = p_json,
                updated_at = v_updated_at
            where draft_id = v_existing_draft;
        else
            insert into private.draft_document(target_type, target_id, save_count, user_data, uid, realm, report_type, report_key, updated_at)
            values(v_target_type, v_client_id, 0, p_json, v_user.uid, v_user.realm, 'L-Contract-Legacy', v_drupal_nid, v_updated_at);
        end if;
    end if;
END
$$;
