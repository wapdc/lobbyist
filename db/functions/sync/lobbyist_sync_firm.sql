CREATE OR REPLACE FUNCTION lobbyist_sync_firm(p_json JSON) RETURNS VOID language plpgsql as
$$
DECLARE
   v_firm_id INTEGER;
   v_vid INTEGER;
   v_submitted_at timestamptz;
   e_reg RECORD;
   a_json JSON;
   ta_json JSON;
   v_email text;
   v_phone text;
   v_address text;
   v_entity_id INTEGER;
   v_superceded_id INTEGER;
   ag_rec RECORD;
   matching_agent RECORD;
   v_starting timestamptz;
   v_ending timestamptz;
   v_debug text;


BEGIN
   v_firm_id := CAST(p_json->>'nid' AS INTEGER);
   v_submitted_at := to_timestamp(CAST(p_json->>'revision_timestamp' as numeric));
   v_vid := CAST(p_json->>'vid' AS INTEGER);
   a_json := p_json->'field_address'->'und'->0;
   ta_json := p_json->'field_temporary_thurston_addr'->'und'->0;
   v_email := p_json->'field_email'->'und'->0->>'email';
   v_phone := p_json->'field_firm_phone_number'->'und'->0->>'value';
   v_address := trim(COALESCE(a_json->>'thoroughfare','') || ' ' || COALESCE(a_json->>'premise',''));
   v_address := case when v_address <> '' then v_address end;



   -- Find exiting registration
   SELECT f.lobbyist_firm_id, lfs.submitted_at, lfs.submission_id, f.entity_id  into e_reg
          from lobbyist_firm f
            left join lobbyist_firm_submission lfs on f.current_submission_id = lfs.submission_id
          where f.lobbyist_firm_id = v_firm_id;


   IF e_reg.lobbyist_firm_id is nulL THEN
       INSERT INTO entity (name, address, city, state, postcode, email, phone, is_person)
         VALUES (p_json->>'title', v_address, a_json->>'locality',
                 a_json->>'administrative_area', a_json->>'postal_code', v_email, v_phone, false)
       returning entity_id into v_entity_id;
       INSERT INTO lobbyist_firm(
           lobbyist_firm_id,
           drupal_nid,
           current_submission_id,
           name,
           entity_id,
           email,
           address_1,
           address_2,
           city,
           state,
           postcode,
           country,
           phone,
           phone_alternate,
           temp_phone,
           temp_address_1,
           temp_address_2,
           temp_city,
           temp_state,
           temp_postcode,
           temp_country,
           updated_at
       )
       VALUES (
                  v_firm_id,
                  v_firm_id,
                  cast(p_json->>'vid' as integer),
                 p_json->>'title',
                  v_entity_id,
                  v_email,
                 a_json->>'thoroughfare',
                 a_json->>'premise',
                 a_json->>'locality',
                 a_json->>'administrative_area',
                 a_json->>'postal_code',
                 a_json->>'country',
                 p_json->'field_firm_phone_number'->'und'->0->>'value',
                 p_json->'field_mobile_number'->'und'->0->>'value',
                 p_json->'field_temporary_thurston_phone_'->'und'->0->>'value',
                 ta_json->>'thoroughfare',
                 ta_json->>'premise',
                 ta_json->>'locality',
                 ta_json->>'administrative_area',
                 ta_json->>'postal_code',
                 ta_json->>'country',
                 now()
              );
   else
     -- Update firm record but only if this record is newer.
     if e_reg.submitted_at < v_submitted_at or e_reg.submitted_at is null  THEN
       update lobbyist_firm_submission
           set superseded_id = cast(p_json->>'vid' as int)
           where submission_id = e_reg.submission_id;
       UPDATE lobbyist_firm set
            current_submission_id = cast(p_json->>'vid' as integer),
            name = p_json->>'title',
            email = v_email,
            address_1 = a_json->>'thoroughfare',
            address_2 = a_json->>'premise',
            city = a_json->>'locality',
            state = a_json->>'administrative_area',
            postcode = a_json->>'postal_code',
            country = a_json->>'country',
            phone = p_json->'field_firm_phone_number'->'und'->0->>'value',
            phone_alternate = p_json->'field_mobile_number'->'und'->0->>'value',
            temp_address_1 = ta_json->>'thoroughfare',
            temp_address_2 = ta_json->>'premise',
            temp_city = ta_json->>'locality',
            temp_state = ta_json->>'administrative_area',
            temp_postcode = ta_json->>'postal_code',
            temp_country = ta_json->>'country',
            updated_at = now()
          where lobbyist_firm_id = v_firm_id;

       UPDATE entity e set
          name=v.name,
          email = COALESCE(v.email, e.email),
          phone = COALESCE(v.phone, e.phone),
          address = case when v_address is not null then v_address else e.address end,
          city = case when v_address is not null then v.city else e.city end,
          state = case when v_address is not null then v.state else e.state end,
          postcode = case when v_address is not null then v.postcode else e.postcode end
       FROM (SELECT * from lobbyist_firm where lobbyist_firm_id=v_firm_id) v
       WHERE e.entity_id = v.entity_id;

     end if;
   end if;


   -- Insert the base submission
   SELECT MIN(submission_id) into v_superceded_id from lobbyist_firm_submission
     WHERE lobbyist_firm_id = v_firm_id and submitted_at >= v_submitted_at
     and submission_id > cast(p_json->>'vid' as integer);
   INSERT INTO lobbyist_firm_submission(
        submission_id, lobbyist_firm_id,
        version, submitted_at, user_data, superseded_id
   ) VALUES (
     CAST(p_json->>'vid' as int),
             CAST(p_json->>'nid' as int),
             '1.0',
             to_timestamp(cast(p_json->>'revision_timestamp' as text)::numeric),
             p_json,
             v_superceded_id
   ) ON CONFLICT (submission_id) DO UPDATE set superseded_id = excluded.superseded_id, submitted_at= excluded.submitted_at, user_data=excluded.user_data;

   -- Determine next and previous record

   FOR ag_rec IN (SELECT
                      replace(replace(trim(COALESCE((fname->>'title'),'') || COALESCE(' ' || (fname->>'given'),'') || COALESCE(' ' || (fname->>'middle'),'') || COALESCE(' ' || (fname->>'family'),'') || COALESCE(' ' || (fname->>'generational'),'')), '  ', ' '),'  ',' ') as aname,
                     year_first_employed,
                     bio,
                     picture->>'uri' as picture_uri,
                     to_timestamp(cast(picture->>'timestamp' as numeric)) as picture_updated,
                     cast(substr(certification_date,1,10) as date) as training_certified
                     FROM (
                       SELECT
                           ag_json->'field_firm_agent_name'->'und'->0 as fname,
                           ag_json->'field_agent_picture'->'und'->0 as picture,
                           ag_json->'field_agent_bio'->'und'->0->>'value' as bio,
                           cast(substr(ag_json->'field_year_first_employed'->'und'->0->>'value',1,4) as integer) as year_first_employed,
                           ag_json->'field_lob_cert_date'->'und'->0->>'value' as certification_date
                       FROM json_array_elements(p_json->'field_lob_firm_agents'->'und') ag_json) v) LOOP

     v_starting := null;
     v_ending := null;
     -- Find out if there is an overlapping record
     SELECT a.* into matching_agent FROM lobbyist_agent a
       WHERE lobbyist_firm_id = v_firm_id
         and a.name = ag_rec.aname
         and v_submitted_at >= a.starting and ((v_submitted_at < a.ending) or a.ending is null);
     -- If there is an overlapping record
     IF matching_agent.starting is not null then
       -- If the profile the same update the overlapping record
       IF v_submitted_at = matching_agent.starting or
          (
           matching_agent.year_first_employed = ag_rec.year_first_employed
           -- and matching_agent.bio = ag_rec.bio
           and matching_agent.agent_picture = ag_rec.picture_uri
           and matching_agent.training_certified = ag_rec.training_certified
           and matching_agent.picture_updated = ag_rec.picture_updated
           ) then
           v_starting = matching_agent.starting;
           v_ending = matching_agent.ending;
           v_debug = 'overlap so update';
       else
           -- We need to update the matching records end date to be this one
           -- and trigger an insert
           v_debug='overlap insert';
           update lobbyist_agent set ending = v_submitted_at where
              lobbyist_firm_id = v_firm_id and name = ag_rec.aname and starting = matching_agent.starting;
           v_starting = v_submitted_at;
           v_ending = matching_agent.ending;
       end if;
     else
       -- Find out if there is a record after this one
       SELECT a.* into matching_agent FROM lobbyist_agent a
         WHERE lobbyist_firm_id = v_firm_id
           and a.name = ag_rec.aname
           and a.starting = (select min(starting) from lobbyist_agent la2 where la2.lobbyist_firm_id = a.lobbyist_firm_id
                             and a.name = la2.name and la2.starting>v_submitted_at
                             );
       -- if there is one
       if matching_agent.starting is not null then
         -- if the record is the same update, changing the start date to be this one
         if  (matching_agent.bio = ag_rec.bio and matching_agent.year_first_employed = ag_rec.year_first_employed
             and matching_agent.agent_picture = ag_rec.picture_uri and matching_agent.training_certified = ag_rec.training_certified
             and matching_agent.picture_updated = ag_rec.picture_updated) then
             update lobbyist_agent set starting = v_submitted_at where
                     lobbyist_firm_id = v_firm_id and name = ag_rec.aname and starting = matching_agent.starting;
             v_starting = v_submitted_at;
             v_ending = matching_agent.ending;
             v_debug = 'later record update';
         else
             -- insert a new one with an end date matching the start date of the next record
             v_starting = v_submitted_at;
             v_ending = matching_agent.starting;
             v_debug = 'New record with a new end date';
         end if;
       else
         v_starting = v_submitted_at;
         v_ending := null;
         v_debug := 'no match';
       end if;
     end if;

     INSERT INTO lobbyist_agent(lobbyist_firm_id, name, starting, ending, year_first_employed, bio, agent_picture, picture_updated, training_certified)
       VALUES (v_firm_id,
               ag_rec.aname,
               v_starting,
               v_ending,
               ag_rec.year_first_employed,
               ag_rec.bio,
               ag_rec.picture_uri,
               ag_rec.picture_updated,
               ag_rec.training_certified
               )
     ON CONFLICT(lobbyist_firm_id, name, starting)
       DO UPDATE SET ending=excluded.ending,
                     year_first_employed=excluded.year_first_employed,
                     bio=excluded.bio,
                     agent_picture = excluded.agent_picture,
                     picture_updated = excluded.picture_updated,
                     training_certified = excluded.training_certified;
   end loop;

   -- Finally mark any agent who are overlapping but not in the list as inactivated.
    UPDATE lobbyist_agent set ending=v_submitted_at WHERE
       lobbyist_firm_id = v_firm_id and v_submitted_at >= starting and ((v_submitted_at < ending) or ending is null)
       and name not in (SELECT
                            replace(replace(trim(COALESCE((fname->>'title'),'') || COALESCE(' ' || (fname->>'given'),'') || COALESCE(' ' || (fname->>'middle'),'') || COALESCE(' ' || (fname->>'family'),'') || COALESCE(' ' || (fname->>'generational'),'')), '  ', ' '),'  ',' ') as aname

                        FROM (
                                 SELECT
                                                 ag_json->'field_firm_agent_name'->'und'->0 as fname
                                 FROM json_array_elements(p_json->'field_lob_firm_agents'->'und') ag_json) v);
END
$$;
