CREATE OR REPLACE FUNCTION lobbyist_sync_l3(p_json JSON) RETURNS VOID language plpgsql as
$$
DECLARE
    v_report_id integer;
    v_vid integer;
    v_report_year integer;
    v_client_id integer;
    e_report RECORD;
    v_submitted_at timestamptz;
    v_draft_id integer;
    v_submission_id integer;
BEGIN
    v_report_id := cast(p_json->>'nid' as INTEGER);
    v_vid := cast(p_json->>'vid' as integer);
    v_report_year := substr(cast(p_json->'field_filing_period_year'->'und'->0->>'value' as text),1,4):: integer;
    v_client_id := cast(p_json->'og_group_ref'->'und'->0->>'target_id' as integer);
    v_submitted_at := to_timestamp(cast(p_json->>'revision_timestamp' as numeric));
    v_submission_id := cast(p_json->>'vid' as int);
    SELECT lobbyist_client_id into v_client_id from lobbyist_client where lobbyist_client_id = v_client_id;
    SELECT r.report_id, s.submission_id, s.submitted_at  INTO e_report FROM l3 r
                                                left join l3_submission s on r.current_submission_id = s.submission_id
                                                where r.report_id=v_report_id;
    SELECT draft_id into v_draft_id from private.draft_document where report_key = v_report_year::text
                                                                  and report_type = 'L3-Legacy'
                                                                  and target_type = 'lobbyist_client'
                                                                  and target_id = v_client_id;

    if v_client_id is not null then

        if e_report.report_id is null then
            INSERT INTO l3(report_id, lobbyist_client_id, current_submission_id, drupal_nid, report_year)
            VALUES(v_report_id, v_client_id, v_vid, v_report_id, v_report_year);
        else
            IF e_report.submitted_at < v_submitted_at THEN
                update l3_submission set superseded_id = v_vid where submission_id = e_report.submission_id;
                update l3 set report_year = v_report_year, current_submission_id = v_vid
                where report_id = v_report_id;
            end if;
        end if;

        -- Insert the base submission
        INSERT INTO l3_submission(
            submission_id, report_id,
            version, submitted_at, user_data
        ) VALUES (
                     v_submission_id,
                     CAST(p_json->>'nid' as int),
                     '1.0',
                     to_timestamp(cast(p_json->>'revision_timestamp' as numeric)),
                     p_json
                 ) ON CONFLICT (submission_id) DO UPDATE set submitted_at = excluded.submitted_at, user_data = excluded.user_data;

        perform l3_legacy_transactions(p_json);
        perform update_l3_legacy_report_data(v_submission_id);

        if v_draft_id is not null then
            delete from private.draft_document
            where draft_id = v_draft_id;
        end if;
    end if;
END
$$;
