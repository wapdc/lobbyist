CREATE OR REPLACE FUNCTION lobbyist_sync_employer(p_json JSON) RETURNS VOID language plpgsql as
$$
DECLARE
  v_client_id INTEGER;
  v_vid INTEGER;
  v_submitted_at timestamptz;
  v_entity_id INTEGER;
  e_reg RECORD;
  a_json JSON;
  v_email TEXT;
  v_phone TEXT;
  v_address TEXT;
BEGIN
    v_client_id := cast(p_json->>'nid' as int);
    v_submitted_at := to_timestamp(CAST(p_json->>'revision_timestamp' as numeric));
    v_vid := CAST(p_json->>'vid' AS INTEGER);
    a_json := p_json->'field_address'->'und'->0;
    v_email := p_json->'field_email'->'und'->0->>'email';
    v_phone := p_json->'field_phone'->'und'->0->>'value';
    v_address := trim(COALESCE(a_json->>'thoroughfare','') || ' ' || COALESCE(a_json->>'premise',''));
    v_address := case when v_address <> '' then v_address end;


    -- Find existing registration
    SELECT c.lobbyist_client_id, lcs.submitted_at, lcs.submission_id, c.entity_id  into e_reg
    from lobbyist_client c
             left join lobbyist_client_submission lcs on c.current_submission_id = lcs.submission_id
    where c.lobbyist_client_id = v_client_id;

    IF e_reg.lobbyist_client_id is nulL THEN

        -- check to see if existing matching entity in tmp_lobbyist_employer CROSSWALK table
        select c.person_id, c.committee_id into v_entity_id from tmp_lobbyist_entity_crosswalk tle join committee c on tle.committee_id = c.committee_id
        where tle.drupal_nid = v_client_id;

        -- Check for mapping for existing entities.
        if v_entity_id is null then
            INSERT INTO entity (name, address, city, state, postcode, email, phone, is_person)
            VALUES (p_json->>'title', v_address, a_json->>'locality',
                    a_json->>'administrative_area', a_json->>'postal_code', v_email, v_phone, false)
            returning entity_id into v_entity_id;
        end if;

        INSERT INTO lobbyist_client (
          lobbyist_client_id, entity_id, drupal_nid, name, email, address_1, address_2, city, state, postcode, country,
          phone, description, current_submission_id
        )
        VALUES(
          v_client_id, v_entity_id, v_client_id, p_json->>'title', v_email, a_json->>'thoroughfare',
          a_json->>'premise', a_json->>'locality', a_json->>'administrative_area',
          a_json->>'postal_code', a_json->>'country', v_phone, p_json->'field_employer_s_occupation_busi'->'und'->0->>'value',
          v_vid
        );
    else
        if e_reg.submitted_at < v_submitted_at or e_reg.submitted_at is null THEN
            update lobbyist_client_submission
            set superseded_id = cast(p_json->>'vid' as int)
            where submission_id = e_reg.submission_id;

            UPDATE lobbyist_client set
                                     current_submission_id = v_vid,
                                     name = p_json->>'title',
                                     email = v_email,
                                     address_1 = a_json->>'thoroughfare',
                                     address_2 = a_json->>'premise',
                                     city = a_json->>'locality',
                                     state = a_json->>'administrative_area',
                                     postcode = a_json->>'postal_code',
                                     country = a_json->>'country',
                                     phone = v_phone,
                                     description = p_json->'field_employer_s_occupation_busi'->'und'->0->>'value',
                                     updated_at = now()
            where lobbyist_client_id = v_client_id;

            UPDATE entity e set
                                name=v.name,
                                email = COALESCE(v.email, e.email),
                                phone = COALESCE(v.phone, e.phone),
                                address = case when v_address is not null then v_address else e.address end,
                                city = case when v_address is not null then v.city else e.city end,
                                state = case when v_address is not null then v.state else e.state end,
                                postcode = case when v_address is not null then v.postcode else e.postcode end
            FROM (SELECT * from lobbyist_client where lobbyist_client_id=v_client_id) v
            WHERE e.entity_id = v.entity_id;

        end if;
    end if;

    -- Insert the base submission
    INSERT INTO lobbyist_client_submission(
        submission_id, lobbyist_client_id,
        version, submitted_at, user_data
    ) VALUES (
                 CAST(p_json->>'vid' as int),
                 CAST(p_json->>'nid' as int),
                 '1.0',
                 to_timestamp(cast(p_json->>'revision_timestamp' as text)::numeric),
                              p_json
             ) ON CONFLICT (submission_id) DO UPDATE set submitted_at= excluded.submitted_at, user_data=excluded.user_data;
END
$$;