/**
* This function will add or update a record into the pdc_user_authorization table to establish a link to lobbyist_public_agency table by target_id.
* The id and expiration could look like:
  example for inserting/updating
  * [{id: 99999}]
  example for unlinking
  * [{id: 99999, expiration_date: '2020/02/02'}]
* @param text pdc_user_realm
* @param text pdc_user_uid
* @param json id and expiration date(optional) A json array of id(public_agency_id) and expiration_date if you plan to expire/unlink the authorization.
*/

drop function if exists lobbyist_public_agency_add_update_authorization(pdc_user_realm text, pdc_user_uid text, json_data json);
create or replace function lobbyist_public_agency_add_update_authorization(pdc_user_realm text, pdc_user_uid text, json_data json) returns void
    language plpgsql as
    $$
    BEGIN
        insert into pdc_user_authorization (realm, uid, target_type, target_id, allow_modify,
                                            allow_submit, granted_date, expiration_date, modified)
        Select pdc_user_realm, pdc_user_uid, 'lobbyist_public_agency', cast(j->>'id' as int) as id, true, true, now(), cast(j->>'expiration_date' as timestamp) as expiration, now() from json_array_elements(json_data) j
        on conflict (realm, uid, target_type, target_id) do
            update set
                       modified = case when excluded.expiration_date is distinct from(pdc_user_authorization.expiration_date) then now()
                                       else pdc_user_authorization.modified end,
                       expiration_date = case
                                             when excluded.expiration_date is null and pdc_user_authorization.expiration_date is not null then null
                                             when excluded.expiration_date is not null then excluded.expiration_date
                                             else pdc_user_authorization.expiration_date end;
    END;
$$