create or replace function update_legacy_report_data(p_submission_id int) returns void
    language plpgsql as
$$
    DECLARE
        p_report record;
        p_json json;
        v_json text;
        n_lobbyist_rec record;
        taxonomy_value text;
        firm_name text;
        n_sources_rec record;
        sources jsonb;
        subcontracts jsonb;
        contract_lookup jsonb;
        search_string text;

    BEGIN
        select * into p_report from l2 join l2_submission s on s.report_id= l2.report_id where s.submission_id = p_submission_id;
        select user_data into p_json from l2_submission where submission_id = p_submission_id;
        v_json := p_json::text;
        select name into firm_name from lobbyist_firm where lobbyist_firm_id = (p_json->'og_group_ref'->'und'->0->>'target_id')::int;
        select jsonb_build_object('sources', json_agg(jsonb_build_object('id', cast(p_json->'og_group_ref'->'und'->0->>'target_id' as int),
                                                                         'name', firm_name)))::jsonb into sources;

        for n_sources_rec in
            SELECT
                distinct lc.lobbyist_contract_id,
                         case when lc.contractor_id is not null then c.name || '(' || lsc.name || ')'
                              else c.name end as name
            from lobbyist_client c join lobbyist_reporting_periods rp on rp.lobbyist_client_id=c.lobbyist_client_id
                                   join lobbyist_contract lc on lc.lobbyist_contract_id=rp.lobbyist_contract_id
                                   left join lobbyist_firm lsc on lsc.lobbyist_firm_id=lc.contractor_id
            WHERE rp.lobbyist_firm_id=p_report.lobbyist_firm_id and rp.period_start=cast(p_report.period_start as date)
            loop
                select jsonb_insert(sources, '{sources,0}', jsonb_build_object('id', n_sources_rec.lobbyist_contract_id, 'name', n_sources_rec.name)) into sources;
            end loop;

        select clients::jsonb || contracts::jsonb
         from (select json_agg(json_build_object('id', lc.lobbyist_contract_id, 'name',
                                                 case when lc.contractor_id is null then l.name else concat(l.name, ' (', f.name, ')') end)) as contracts,
                      json_agg(json_build_object('id', lc.lobbyist_client_id, 'name', l.name)) as clients
               from lobbyist_contract lc
                   join lobbyist_contract_submission s on s.lobbyist_contract_id = lc.lobbyist_contract_id
                        left join lobbyist_client l on lc.lobbyist_client_id = l.lobbyist_client_id
                        left join lobbyist_firm f on f.lobbyist_firm_id = lc.contractor_id
               where lc.lobbyist_firm_id = p_report.lobbyist_firm_id
               and s.submitted_at <= p_report.submitted_at) c into contract_lookup;

        if contract_lookup is not null then
            select jsonb_build_object('contract_lookup', contract_lookup) into contract_lookup;
        else
            select jsonb_build_object('contract_lookup', array[]::text[]) into contract_lookup;
        end if;

        select json_agg(json_build_object('id', lc.lobbyist_contract_id, 'name', lf.name))
         from lobbyist_contract lc
                  left join lobbyist_firm lf on lf.lobbyist_firm_id = lc.lobbyist_firm_id
         where lc.contractor_id = p_report.lobbyist_firm_id into subcontracts;

        if subcontracts is not null then
            select jsonb_build_object('subcontracts', subcontracts) into subcontracts;
        else
            select jsonb_build_object('subcontracts', array[]::text[]) into subcontracts;
        end if;

        if json_typeof(p_json->'field_sub_mat') = 'object' then
            for n_lobbyist_rec in select j as n_json from json_array_elements(p_json->'field_sub_mat'->'und') j
                loop
                    if n_lobbyist_rec.n_json->'field_mat_of_prop'->'und'->0->>'tid' is not null then
                        search_string := concat('"tid": ', '"', n_lobbyist_rec.n_json->'field_mat_of_prop'->'und'->0->>'tid', '"');
                        select name into taxonomy_value from accesshub_taxonomy_term_data where tid::text = n_lobbyist_rec.n_json->'field_mat_of_prop'->'und'->0->>'tid';
                        v_json := replace(v_json::text, search_string, concat('"name":', '"', taxonomy_value, '"'));
                    end if;
                end loop;
        end if;

        update l2_submission
        set user_data = v_json::jsonb || sources::jsonb || subcontracts::jsonb || contract_lookup::jsonb
        where submission_id = p_report.submission_id;
    END
$$