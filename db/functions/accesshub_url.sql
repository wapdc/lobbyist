create or replace function accesshub_url() returns character varying
    stable
    language sql
as $$
select wapdc_setting('accesshub_url');
$$;

alter function accesshub_url() owner to wapdc;