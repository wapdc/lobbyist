create or replace function l1_legacy_contract_update(p_contract_id int) returns void
    language plpgsql as
$$
    DECLARE
    v_period_start DATE;
    v_period_end DATE;
    v_contract_period_start DATE;
    v_contract_period_end DATE;
    v_start_year INT;
    v_end_year INT;

    BEGIN
       SELECT  min(period_start), max(period_end) into v_period_start, v_period_end FROM lobbyist_reporting_periods where lobbyist_contract_id = p_contract_id;

       IF (v_period_start is not null)
       THEN
       v_start_year = extract(year from v_period_start);
       v_end_year = extract(year from v_period_end);
       v_contract_period_start :=
       CASE WHEN v_start_year%2 = 0 THEN CONCAT(cast(v_start_year - 1 AS text), '-01-01')
         ELSE CONCAT(cast(v_start_year AS text), '-01-01')
       END;
       v_contract_period_end :=
       CASE WHEN v_end_year%2 = 0 THEN CONCAT(cast(v_end_year AS text), '-12-31')
            ELSE CONCAT(cast(v_end_year + 1 AS text), '-12-31')
           END;
       ELSE
           v_contract_period_start = null;
           v_contract_period_end = null;
       END IF;
    UPDATE lobbyist_contract set period_start = v_contract_period_start, period_end = v_contract_period_end
       WHERE lobbyist_contract_id = p_contract_id;

    END
$$