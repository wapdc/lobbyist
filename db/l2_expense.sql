CREATE TABLE IF NOT EXISTS l2_expense
(
    l2_transaction_id int references l2_transaction(l2_transaction_id) on delete cascade,
    address text,
    city text,
    stat text,
    postcode text,
    expense_type text,
    is_ad bool,
    description text,
    vendor text
);