CREATE TABLE IF NOT EXISTS filer_request
(
    filer_request serial primary key,
    uid text,
    realm text,
    possible_target_id int,
    submitted_at timestamptz not null default now(),
    payload json
);