create table if not exists l3_lobbyist
(
    l3_transaction_id int references l3_transaction(l3_transaction_id) on delete cascade,
    lobbyist_contract_id int
);