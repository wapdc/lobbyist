create table if not exists l3_compensation
(
    l3_compensation_id serial primary key,
    report_id int references l3(report_id) on delete cascade,
    person_name text,
    org_name text,
    relationship text,
    compensation text,
    description text
);