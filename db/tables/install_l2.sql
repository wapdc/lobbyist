alter table l2
    drop column if exists report_year;
alter table l2
    drop column if exists report_month;
alter table l2
    add if not exists period_start date not null;
alter table l2
    add if not exists period_end date not null;
alter table l2
    drop column if exists first_filed_at;