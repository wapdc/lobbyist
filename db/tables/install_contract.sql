alter table lobbyist_contract
    add column subcontractor_id integer;
alter table lobbyist_contract add foreign key (subcontractor_id) references lobbyist_employer(lobbyist_employer_id);

create table lobbyist_reporting_periods(
   lobbyist_client_id integer not null,
   lobbyist_firm_id integer not null,
   period_start date not null,
   period_end date not null,
   lobbyist_contract_id integer,
   primary key (lobbyist_firm_id, lobbyist_client_id, period_start)
);
alter table lobbyist_contract rename column lobbyist_employer_id to lobbyist_client_id;
alter table lobbyist_employer rename to lobbyist_client;
alter table lobbyist_client rename column lobbyist_employer_id to lobbyist_client_id;
alter table lobbyist_employer_submission rename to lobbyist_client_submission;
alter table lobbyist_client_submission rename column lobbyist_employer_id to lobbyist_client_id;

create view lobbyist_employer as
  select lobbyist_client_id as lobbyist_employer_id,
         entity_id,
         drupal_nid,
         name,
         email,
         address_1,
         address_2,
         city,
         state,
         postcode,
         country,
         phone,
         description,
         updated_at,
         current_submission_id
  from lobbyist_client;
