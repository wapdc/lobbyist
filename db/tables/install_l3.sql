alter table l3
    drop column if exists report_month;
alter table l3 rename column lobbyist_employer_id to lobbyist_client_id;