CREATE TABLE IF NOT EXISTS lobbyist_agent  (
  lobbyist_firm_id INTEGER NOT NULL,
  name varchar(255) NOT NULL,
  starting TIMESTAMPTZ NOT NULL,
  ending TIMESTAMPTZ,
  year_first_employed INTEGER,
  bio TEXT,
  agent_picture TEXT,
  picture_updated TIMESTAMPTZ,
  training_certified DATE,
  PRIMARY KEY (lobbyist_firm_id, name, starting)
);