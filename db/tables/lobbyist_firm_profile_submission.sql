create table if not exists public.lobbyist_firm_profile_submission (
 submission_id integer primary key not null default nextval('lobbyist_submission_seq'::regclass),
 superseded_id integer,
 profile_id integer,
 version text not null,
 submitted_at timestamp with time zone not null default now(),
 user_data json not null,
 drupal_vid integer,
 foreign key (profile_id) references public.lobbyist_firm_profile(profile_id)
);
create index lobbyist_firm_profile_submission_submitted_index on lobbyist_firm_profile_submission using btree (submitted_at);