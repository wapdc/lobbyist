create table public.lobbyist_client_funders
(
    profile_id integer
        constraint lobbyist_client_funders_lobbyist_client_profile_profile_id_fk
            references public.lobbyist_client_profile
            on delete cascade,
    name       text,
    address    text,
    city       text,
    state      text,
    postcode   text
);
