create table public.lobbyist_firm_profile (
  profile_id integer primary key not null default nextval('lobbyist_report_seq'::regclass),
  lobbyist_firm_id integer not null,
  period_start date not null,
  period_end date not null,
  name text,
  email text,
  address text,
  city text,
  state text,
  postcode text,
  country text,
  phone text,
  phone_alternate text,
  temp_phone text,
  temp_address text,
  temp_city text,
  temp_state text,
  temp_postcode text,
  temp_country text,
  updated_at timestamp with time zone not null default now(),
  current_submission_id integer
);
alter table public.lobbyist_firm_profile
    add constraint lobbyist_firm_profile_lobbyist_firm_lobbyist_firm_id_fk
        foreign key (lobbyist_firm_id) references public.lobbyist_firm;
alter table public.lobbyist_firm_profile
    add constraint lobbyist_firm_profile_uk
        unique (lobbyist_firm_id, period_start);