ALTER TABLE lobbyist_employer ADD COLUMN  IF NOT EXISTS current_submission_id INTEGER;
ALTER TABLE lobbyist_firm ADD COLUMN IF NOT EXISTS current_submission_id INTEGER;

-- employer submission
CREATE TABLE IF NOT EXISTS lobbyist_firm_submission(
  submission_id INTEGER PRIMARY KEY,
  superseded_id INTEGER,
  lobbyist_firm_id INTEGER,
  version TEXT NOT NULL,
  submitted_at TIMESTAMPTZ NOT NULL default now(),
  user_data JSON NOT NULL,
  FOREIGN KEY(lobbyist_firm_id) REFERENCES lobbyist_firm(lobbyist_firm_id)
);

CREATE INDEX IF NOT EXISTS lobbyist_firm_submission_submitted_index ON lobbyist_firm_submission(submitted_at);

-- Firm submission
CREATE TABLE IF NOT EXISTS lobbyist_employer_submission(
  submission_id INTEGER PRIMARY KEY,
  superseded_id INTEGER,
  lobbyist_employer_id INTEGER,
  version TEXT NOT NULL,
  submitted_at TIMESTAMPTZ NOT NULL default now(),
  user_data JSON NOT NULL,
  FOREIGN KEY(lobbyist_employer_id) REFERENCES lobbyist_employer(lobbyist_employer_id)
);

CREATE INDEX IF NOT EXISTS lobbyist_employer_submission_submitted_index ON lobbyist_employer_submission(submitted_at);

-- Lobbyist contract
CREATE TABLE IF NOT EXISTS lobbyist_contract(
    lobbyist_contract_id INTEGER PRIMARY KEY,
    lobbyist_firm_id INTEGER NOT NULL,
    lobbyist_employer_id INTEGER NOT NULL,
    current_submission_id INTEGER,
    drupal_nid INTEGER,
    FOREIGN KEY(lobbyist_firm_id) REFERENCES lobbyist_firm(lobbyist_firm_id) ON DELETE CASCADE,
    FOREIGN KEY(lobbyist_employer_id) REFERENCES lobbyist_employer(lobbyist_employer_id) ON DELETE CASCADE
);

CREATE INDEX lobbyist_contact_accesshub_index ON lobbyist_contract(drupal_nid);

CREATE TABLE IF NOT EXISTS lobbyist_contract_submission(
   submission_id INTEGER PRIMARY KEY,
   superseded_id INTEGER,
   lobbyist_contract_id INTEGER NOT NULL,
   version TEXT NOT NULL,
   submitted_at TIMESTAMPTZ NOT NULL DEFAULT now(),
   user_data JSON NOT NULL,
   FOREIGN KEY(lobbyist_contract_id) REFERENCES lobbyist_contract(lobbyist_contract_id)
);

-- L2 Report
CREATE TABLE IF NOT EXISTS l2(
  report_id INTEGER PRIMARY KEY,
  lobbyist_firm_id INTEGER NOT NULL,
  report_year INTEGER NOT NULL,
  report_month INTEGER NOT NULL,
  current_submission_id INTEGER,
  first_filed_at TIMESTAMPTZ,
  drupal_nid INTEGER,
  FOREIGN KEY(lobbyist_firm_id) REFERENCES lobbyist_firm(lobbyist_firm_id) ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS l2_accesshub_index ON l2(drupal_nid);

CREATE TABLE IF NOT EXISTS l2_submission(
  submission_id INTEGER primary key,
  superseded_id INTEGER,
  report_id INTEGER NOT NULL,
  version TEXT NOT NULL,
  submitted_at TIMESTAMPTZ NOT NULL DEFAULT now(),
  user_data JSON NOT NULL,
  FOREIGN KEY(report_id) REFERENCES l2(report_id)
);

CREATE INDEX IF NOT EXISTS l2_submission_submitted_index ON l2_submission(submitted_at);

CREATE TABLE IF NOT EXISTS l3(
    report_id INTEGER PRIMARY KEY,
    lobbyist_employer_id INTEGER NOT NULL,
    report_year INTEGER NOT NULL,
    report_month INTEGER NOT NULL,
    current_submission_id INTEGER,
    first_filed_at TIMESTAMPTZ,
    drupal_nid INTEGER,
    FOREIGN KEY(lobbyist_employer_id) REFERENCES lobbyist_employer(lobbyist_employer_id) ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS l3_accesshub_index ON l3(drupal_nid);


CREATE TABLE IF NOT EXISTS l3_submission(
    submission_id INTEGER PRIMARY KEY,
    superseded_id INTEGER,
    report_id INTEGER NOT NULL,
    version TEXT NOT NULL,
    submitted_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    user_data JSON NOT NULL,
    FOREIGN KEY(report_id) REFERENCES l3(report_id)
);

CREATE INDEX IF NOT EXISTS l3_submission_submitted_index ON l3_submission(submitted_at);

