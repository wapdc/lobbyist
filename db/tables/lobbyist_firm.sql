CREATE TABLE IF NOT EXISTS lobbyist_firm
(
    lobbyist_firm_id serial primary key,
    entity_id integer,
    drupal_nid integer,
    name text,
    email text,
    address_1 text,
    address_2 text,
    city text,
    state text,
    postcode text,
    country text,
    phone text,
    phone_alternate text,
    temp_phone text,
    temp_address_1 text,
    temp_address_2 text,
    temp_city text,
    temp_state text,
    temp_postcode text,
    temp_country text,
    updated_at timestamptz not null default now()
    );