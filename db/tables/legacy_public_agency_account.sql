DROP TABLE IF EXISTS legacy_public_agency_account;

CREATE TABLE IF NOT EXISTS legacy_public_agency_account
(
    username text primary key,
    password text,
    l5filer_id integer
);