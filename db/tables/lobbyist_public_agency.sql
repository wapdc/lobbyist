create table if not exists lobbyist_public_agency (
    id serial primary key,
    l5filer_id int unique not null,
    name text not null,
    address text not null,
    city text not null,
    state text  not null,
    zip text not null,
    county text not null,
    contact text,
    phone text,
    email text,
    head text,
    account text,
    status int
);