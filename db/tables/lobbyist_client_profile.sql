create table if not exists public.lobbyist_client_profile (
    profile_id integer primary key not null default nextval('lobbyist_report_seq'::regclass),
    lobbyist_client_id integer not null,
    period_start date not null,
    period_end date not null,
    name text,
    email text,
    address text,
    city text,
    state text,
    postcode text,
    country text,
    phone text,
    description text,
    updated_at timestamp with time zone not null default now(),
    current_submission_id integer,
    has_committee boolean not null default false,
    committee_id integer,
    committee_name text,
    has_members_funders boolean not null default false ,
    drupal_nid integer
);
alter table public.lobbyist_client_profile
    add constraint lobbyist_client_profile_lobbyist_client_lobbyist_client_id_fk
        foreign key (lobbyist_client_id) references public.lobbyist_client;
alter table public.lobbyist_client_profile
    add constraint lobbyist_client_profile_uk
        unique (lobbyist_client_id, period_start);



