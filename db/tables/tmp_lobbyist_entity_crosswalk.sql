CREATE TABLE IF NOT EXISTS tmp_lobbyist_entity_crosswalk
(
    drupal_nid integer,
    entity_id integer,
    name text,
    pac text
)