create table if not exists l2_lobbying_activity (
    report_id int not null,
    source_target_id int not null,
    source_target_type text not null,
    activity jsonb not null
);

alter table l2_lobbying_activity
    add constraint l2_lobbying_activity_l2_report_id_fk
        foreign key (report_id) references l2;
