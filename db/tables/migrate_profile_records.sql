do language plpgsql
$$
  begin
  -- insert the base lobbyist profile records based on reporting periods that are currently in the table.
  with periods as (select ps::date as start_date, (ps + interval '2 years' - interval'1 day')::date as end_date  from generate_series('2001-01-01'::date, '2026-01-01'::date, interval '2 years') ps)
  insert into public.lobbyist_firm_profile(lobbyist_firm_id, name, period_start, period_end)
     select f.lobbyist_firm_id, f.name, profiles.start_date, profiles.end_date from (select rp.lobbyist_firm_id, periods.start_date, periods.end_date  from periods
    join lobbyist_reporting_periods rp on rp.period_start between periods.start_date and periods.end_date
    group by rp.lobbyist_firm_id, periods.start_date,  periods.end_date) profiles
  join lobbyist_firm f on f.lobbyist_firm_id=profiles.lobbyist_firm_id;

  -- Populate the submission table with the submission as it was before the reporting period started.
  insert into lobbyist_firm_profile_submission(profile_id, version, submitted_at, user_data, drupal_vid)
  select distinct on (lfp.profile_id) lfp.profile_id,  '1.0', s.submitted_at, s.user_data, s.submission_id
  from lobbyist_firm_profile lfp
    join lobbyist_firm_submission s on s.lobbyist_firm_id=lfp.lobbyist_firm_id and s.submitted_at < lfp.period_start
    order by lfp.profile_id, s.submitted_at desc;

  -- Add any submission that happened from october to october in the prior biennium.
  insert into lobbyist_firm_profile_submission(profile_id, version, submitted_at, user_data, drupal_vid)
  select lfp.profile_id, '1.0' as version, lfs.submitted_at, lfs.user_data, lfs.submission_id from lobbyist_firm_profile lfp
      join lobbyist_firm_submission lfs on lfs.lobbyist_firm_id=lfp.lobbyist_firm_id and lfs.submitted_at between (lfp.period_start - interval '3 months') and (lfp.period_end - interval '3 months')
      left join lobbyist_firm_profile_submission pse on lfp.profile_id = pse.profile_id and pse.drupal_vid=lfs.submission_id
      where pse.submission_id is null
      order by lfp.period_start desc, lfs.submitted_at desc;

  -- Populate the reporting history via superseded.
  update lobbyist_firm_profile_submission set
     superseded_id = v.superseded_id
  from
  (select x.submission_id, x.superseded_id from (select s.submission_id, s.profile_id, lead(s.submission_id) over (partition by s.profile_id order by s.submitted_at, s.drupal_vid) as superseded_id, s.submitted_at
    from lobbyist_firm_profile_submission s
     order by profile_id, submitted_at) x where x.superseded_id is not null) v
  where v.submission_id=lobbyist_firm_profile_submission.submission_id;

  -- Set the current submission id on the profile.
  update lobbyist_firm_profile set
     current_submission_id = x.submission_id,
     name = x.name,
     address = coalesce(x.address1 || ', ' || x.address2,  x.address1) ,
     city = x.city,
     state = x.state,
     postcode = x.postcode,
     phone = x.phone,
     phone_alternate = x.phone_alternate,
     temp_phone = x.temp_phone,
     temp_address = x.temp_address,
     temp_country = x.temp_country,
     temp_city = x.temp_city,
     temp_state = x.temp_state,
     temp_postcode = x.temp_postcode
  from (select
            profile_id,
            submission_id,
            user_data->>'title' as name,
            nullif(user_data->'field_address'->'und'->0->>'country','') as country,
            nullif(user_data->'field_address'->'und'->0->>'thoroughfare','') as address1,
            nullif(user_data->'field_address'->'und'->0->>'premise','') as address2,
            nullif(user_data->'field_address'->'und'->0->>'locality','') as city,
            nullif(user_data->'field_address'->'und'->0->>'administrative_area','') as state,
            nullif(user_data->'field_address'->'und'->0->>'postal_code','') as postcode,
            nullif(user_data->'field_email'->'und'->0->>'email','') as email,
            nullif(user_data->'field_firm_phone_number'->'und'->0->>'value','') as phone,
            nullif(user_data->'field_mobile_number'->'und'->0->>'value','') as phone_alternate,
            nullif(user_data->'field_temporary_thurston_phone_'->'und'->0->>'value','') as temp_phone,
            nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'country','') as temp_country,
            nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'thoroughfare','') as temp_address,
            nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'locality','') as temp_city,
            nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'administrative_area','') as temp_state,
            nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'postal_code','') as temp_postcode
    from lobbyist_firm_profile_submission where superseded_id is null  ) x
    where x.profile_id=lobbyist_firm_profile.profile_id;

  -- convert the firm submission into the new format
  update lobbyist_firm_profile_submission
    set version = '2.0',
        user_data = converted
  from (select s.submission_id, json_build_object(
    'lobbyist_firm_id', p.lobbyist_firm_id,
    'name',   user_data->>'title',
    'address' , json_build_object(
        'country', nullif(user_data->'field_address'->'und'->0->>'country',''),
        'address', coalesce(nullif(user_data->'field_address'->'und'->0->>'thoroughfare','') || ', ' || nullif(user_data->'field_address'->'und'->0->>'premise','')
                   ,nullif(user_data->'field_address'->'und'->0->>'thoroughfare','')),
        'city', nullif(user_data->'field_address'->'und'->0->>'locality',''),
        'state', nullif(user_data->'field_address'->'und'->0->>'administrative_area',''),
        'postcode', nullif(user_data->'field_address'->'und'->0->>'postal_code','')
    ),
    'email',  nullif(user_data->'field_email'->'und'->0->>'email',''),
    'phone', nullif(user_data->'field_firm_phone_number'->'und'->0->>'value',''),
    'phone_alt', nullif(user_data->'field_mobile_number'->'und'->0->>'value',''),
    'temp_address', case when nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'thoroughfare','') is not null then
        json_build_object(
            'country', nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'country',''),
            'address', coalesce(nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'thoroughfare','') || ', ' || nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'premise',''),
                                nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'thoroughfare','')),
            'city', nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'locality', ''),
            'state', nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'administrative_area',''),
            'postcode', nullif(user_data->'field_temporary_thurston_addr'->'und'->0->>'postal_code','')
        )
        end,
    'temp_phone', nullif(user_data->'field_temporary_thurston_phone_'->'und'->0->>'value','')
    ) converted from lobbyist_firm_profile_submission s join lobbyist_firm_profile p on s.profile_id = p.profile_id) x
  where x.submission_id = lobbyist_firm_profile_submission.submission_id;

  -- insert the base lobbyist client profile records based on reporting periods that are currently in the table.
  with periods as (select ps::date as start_date, (ps + interval '2 years' - interval'1 day')::date as end_date  from generate_series('2001-01-01'::date, '2026-01-01'::date, interval '2 years') ps)
  insert into public.lobbyist_client_profile(lobbyist_client_id, name, period_start, period_end)
  select c.lobbyist_client_id, c.name, profiles.start_date, profiles.end_date
    from (select rp.lobbyist_client_id, periods.start_date, periods.end_date  from periods
        join lobbyist_reporting_periods rp on rp.period_start between periods.start_date and periods.end_date
        group by rp.lobbyist_client_id, periods.start_date,  periods.end_date) profiles
        join lobbyist_client c on c.lobbyist_client_id=profiles.lobbyist_client_id;

  -- Populate the submission table with the submission as it was before the reporting period started.
  insert into lobbyist_client_profile_submission(profile_id, version, submitted_at, user_data, drupal_vid)
  select distinct on (lfp.profile_id) lfp.profile_id,  '1.0', s.submitted_at, s.user_data, s.submission_id
  from lobbyist_client_profile lfp
           join lobbyist_client_submission s on s.lobbyist_client_id=lfp.lobbyist_client_id and s.submitted_at < lfp.period_start
  order by lfp.profile_id, s.submitted_at desc;

  -- Add any submission that happened from october to october in the prior biennium.
  insert into lobbyist_client_profile_submission(profile_id, version, submitted_at, user_data, drupal_vid)
  select lfp.profile_id, '1.0' as version, lfs.submitted_at, lfs.user_data, lfs.submission_id from lobbyist_client_profile lfp
                                                                                                       join lobbyist_client_submission lfs on lfs.lobbyist_client_id=lfp.lobbyist_client_id and lfs.submitted_at between (lfp.period_start - interval '3 months') and (lfp.period_end - interval '3 months')
                                                                                                       left join lobbyist_firm_profile_submission pse on lfp.profile_id = pse.profile_id and pse.drupal_vid=lfs.submission_id
  where pse.submission_id is null
  order by lfp.period_start desc, lfs.submitted_at desc;

  -- Populate the reporting history via superseded.
  update lobbyist_client_profile_submission set
      superseded_id = v.superseded_id
  from
      (select x.submission_id, x.superseded_id from (select s.submission_id, s.profile_id, lead(s.submission_id) over (partition by s.profile_id order by s.submitted_at, s.drupal_vid) as superseded_id, s.submitted_at
                                                     from lobbyist_client_profile_submission s
                                                     order by profile_id, submitted_at) x where x.superseded_id is not null) v
  where v.submission_id=lobbyist_client_profile_submission.submission_id;

  -- Set the current submission id on the profile.
  update lobbyist_client_profile set
      current_submission_id = x.submission_id,
         name = x.name,
         address = coalesce(x.address1 || ', ' || x.address2,x.address1),
         city = x.city,
         state = x.state,
         postcode = x.postcode,
         phone=x.phone,
         description = x.description,
         has_committee = case when x.pac_name is not null and x.pac_name <> '' then true else false end,
         committee_name = case when x.pac_name is not null and x.pac_name <> '' then x.pac_name end,
         has_members_funders = case when x.funder_name is not null then true else false end
  from (select profile_id,
               submission_id,
               user_data->>'title' as name,
               nullif(user_data->'field_address'->'und'->0->>'country','') as country,
               nullif(user_data->'field_address'->'und'->0->>'thoroughfare','') as address1,
               nullif(user_data->'field_address'->'und'->0->>'premise','') as address2,
               nullif(user_data->'field_address'->'und'->0->>'locality','') as city,
               nullif(user_data->'field_address'->'und'->0->>'administrative_area','') as state,
               nullif(user_data->'field_address'->'und'->0->>'postal_code','') as postcode,
               nullif(user_data->'field_email'->'und'->0->>'email','') as email,
               nullif(user_data->'field_phone'->'und'->0->>'value','') as phone,
               nullif(user_data->'field_employer_s_occupation_busi'->'und'->0->>'value','') as description,
               nullif(user_data->'field_political_action_committee'->'und'->0->>'value','') as pac_name,
               nullif(user_data->'field_funders'->'und'->0->'field_name_of_person_group_assoc'->'und'->0->>'value','') as funder_name
        from lobbyist_client_profile_submission where superseded_id is null  ) x
  where x.profile_id=lobbyist_client_profile.profile_id;

  update lobbyist_client_profile_submission
    set version = '2.0',
        user_data = x.converted
    from (with funders as (select sa.submission_id, json_agg(json_build_object(
            'name', j->'field_name_of_person_group_assoc'->'und'->0->>'value',
            'country', nullif( j->'field_address'->'und'->0->>'country',''),
            'address', coalesce(nullif( j->'field_address'->'und'->0->>'thoroughfare','') || ', ' || nullif( j->'field_address'->'und'->0->>'premise',''),
                                nullif( j->'field_address'->'und'->0->>'thoroughfare','')),
            'city', nullif( j->'field_address'->'und'->0->>'locality',''),
            'state', nullif( j->'field_address'->'und'->0->>'administrative_area',''),
            'postcode', nullif( j->'field_address'->'und'->0->>'postal_code','')                                     )
        ) funders from (
           select s2.submission_id, json_array_elements(user_data->'field_funders'->'und') as j
           from lobbyist_client_profile_submission s2
           where nullif(s2.user_data->'field_funders'->'und'->0->'field_name_of_person_group_assoc'->'und'->0->>'value','') is not null
        ) sa
       group by sa.submission_id)
    select
              s.submission_id,
              json_build_object(
                      'name', user_data->>'title',
                      'address', json_build_object(
                          'country', nullif(user_data->'field_address'->'und'->0->>'country',''),
                          'address', coalesce(nullif(user_data->'field_address'->'und'->0->>'thoroughfare','') || ', ' || nullif(user_data->'field_address'->'und'->0->>'premise',''),
                                              nullif(user_data->'field_address'->'und'->0->>'thoroughfare','')),
                          'city', nullif(user_data->'field_address'->'und'->0->>'locality',''),
                          'state', nullif(user_data->'field_address'->'und'->0->>'administrative_area',''),
                          'postcode', nullif(user_data->'field_address'->'und'->0->>'postal_code','')
                      ),
                      'email', nullif(user_data->'field_email'->'und'->0->>'email',''),
                      'phone', nullif(user_data->'field_phone'->'und'->0->>'value',''),
                      'description', nullif(user_data->'field_employer_s_occupation_busi'->'und'->0->>'value',''),
                      'pac_name', nullif(user_data->'field_political_action_committee'->'und'->0->>'value',''),
                      'has_members_funders', case when f.funders is not null then true else false end,
                      'members_funders', f.funders
                )
              converted
              from lobbyist_client_profile_submission s
                  join lobbyist_client_profile p on p.profile_id=s.profile_id
                  left join funders f on
                     f.submission_id =s.submission_id
              ) x where x.submission_id=lobbyist_client_profile_submission.submission_id;

  end
$$
