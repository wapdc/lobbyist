-- Adds 'Users Linked to Firm' Lobbyist menu item
INSERT INTO menu_links (
    category,
    menu,
    title,
    abstract,
    icon,
    url,
    permission
)
VALUES (
    'Lobbying',
    'staff',
    'Users Linked to Lobbyist Clients',
    'Allows searching of users linked to lobbyist clients based on a variety of criteria.',
    'Search',
    '/lobbyist/admin/client-users',
    'access wapdc data'
)