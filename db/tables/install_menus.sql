update menu_links set count_function='lobbyist_count_access_requests' where url='/lobbyist/admin/access-requests';

-- Add menu items for
insert into menu_links(category, menu, title, abstract,  url, permission)
  values (
          'Lobbying',
          'staff',
          'Lobbyist management dashboard',
          'View common administrative tasks related to lobbyist firms and clients.',
           '/lobbyist/admin/management',
          'access wapdc data');
insert into menu_links(category, menu, title, abstract,  url, permission)
values (
        'Lobbying',
        'staff',
        'Lobbyist firms',
        'Search for lobbyist firms',
        '/lobbyist/admin/lobbyist-firms',
        'access wapdc data'
        );
insert into menu_links(category, menu, title, abstract,  url, permission)
values (
           'Lobbying',
           'staff',
           'Lobbyist clients',
           'Search for lobbyist employers/clients',
           '/lobbyist/admin/lobbyist-clients',
           'access wapdc data'
       );

