CREATE TABLE IF NOT EXISTS lobbyist_employer
(
    lobbyist_employer_id serial primary key,
    entity_id integer,
    drupal_nid integer,
    name text,
    email text,
    address_1 text,
    address_2 text,
    city text,
    state text,
    postcode text,
    country text,
    phone text,
    description text,
    updated_at timestamptz not null default now()
    );