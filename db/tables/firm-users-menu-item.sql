-- Adds 'Users Linked to Firm' Lobbyist menu item
INSERT INTO menu_links (
    category,
    menu,
    title,
    abstract,
    icon,
    url,
    permission
)
VALUES (
    'Lobbying',
    'staff',
    'Users Linked to Lobbyist Firms',
    'Allows searching of users linked to lobbyist firms based on a variety of criteria.',
    'Search',
    '/lobbyist/admin/firm-users',
    'access wapdc data'
)