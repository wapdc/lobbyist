update l2_transaction
set transaction_type = 'ad_expense'
from (select * from l2_submission l2 join l2_transaction t on t.report_id = l2.report_id
               where version = '1.0' and t.transaction_type = 'political_ads_expense') x
where x.l2_transaction_id = l2_transaction.l2_transaction_id;