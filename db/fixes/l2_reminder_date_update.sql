UPDATE reporting_period
SET reminder_date = due_date - INTERVAL '7 days'
WHERE now() + interval '7 days' <= due_date and report_type = 'L2' and reporting_period_type='Monthly';


