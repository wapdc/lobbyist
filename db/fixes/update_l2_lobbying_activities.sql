DO $$
    DECLARE
    BEGIN
        update l2_submission
        set user_data = cast(replace(user_data::text, 'legislative_agency', 'who_lobbied') as json)
        where version = '2.0';

        update l2_lobbying_activity
        set activity = cast(replace(activity::text, 'legislative_agency', 'who_lobbied') as json);

        update private.draft_document
        set user_data = cast(replace(user_data::text, 'legislative_agency', 'who_lobbied') as json)
        where report_type = 'L2';
    END
$$