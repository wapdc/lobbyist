
DO $$
    BEGIN
        perform l2_save_lobbying_activity(report_id) from l2_submission
             where superseded_id is null
               and version = '2.0';
    END
$$ LANGUAGE plpgsql;
