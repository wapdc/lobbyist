DO $$
    DECLARE
        i_firm_record record;
        new_entity_id int;
    BEGIN
        FOR i_firm_record IN SELECT * FROM lobbyist_firm where drupal_nid <> lobbyist_firm.lobbyist_firm_id LOOP
            update pdc_user_authorization set target_id = i_firm_record.drupal_nid where target_id=i_firm_record.lobbyist_firm_id and target_type = 'lobbyist_firm';
            update fund set target_id = i_firm_record.drupal_nid where target_id = i_firm_record.lobbyist_firm_id and target_type='lobbyist_firm';
            update lobbyist_firm set lobbyist_firm_id = i_firm_record.drupal_nid where lobbyist_firm_id = i_firm_record.lobbyist_firm_id;
            end loop;
        FOR i_firm_record IN SELECT * FROM lobbyist_firms_migration where drupal_nid not in (select drupal_nid from lobbyist_firm)
            LOOP
                INSERT INTO entity (name, address, city, state, postcode, country, email, phone, is_person)
                SELECT i_firm_record.name, trim(i_firm_record.address_1 || ' ' || i_firm_record.address_2), i_firm_record.city, i_firm_record.state, i_firm_record.postcode, i_firm_record.country, i_firm_record.email, i_firm_record.phone, false
                RETURNING entity_id INTO new_entity_id;
                --insert the freshly created entity_id into the lobbyist_firm table
                INSERT INTO lobbyist_firm (lobbyist_firm_id, drupal_nid, entity_id, name, email, address_1, address_2, city, state, postcode, country, phone, phone_alternate, temp_phone, temp_address_1, temp_address_2, temp_city, temp_state, temp_postcode, temp_country, updated_at)
                VALUES (i_firm_record.drupal_nid, i_firm_record.drupal_nid, new_entity_id, i_firm_record.name, i_firm_record.email, i_firm_record.address_1, i_firm_record.address_2, i_firm_record.city, i_firm_record.state, i_firm_record.postcode, i_firm_record.country, i_firm_record.phone, i_firm_record.phone_alternate, i_firm_record.temp_phone, i_firm_record.temp_address_1, i_firm_record.temp_address_2, i_firm_record.temp_city, i_firm_record.temp_state, i_firm_record.temp_postcode, i_firm_record.temp_country, i_firm_record.updated_at);
            END LOOP;

    END;
$$ LANGUAGE plpgsql;