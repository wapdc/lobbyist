/**
  Migration script for populating the l2_transactions table with the current collection of l2_submissions.
  Written with superseded being null as we only want to populate the transaction table with the latest submission.
  The filter about version being 1.0 is to only select out legacy if this needs to be run in the future when 2.0 is released.

  ****DISCLAIMER****
  The function takes about 8-10 minutes; as it it creating about 240,000 records. Looking for ways to improve performance.
  Played around with excluding blank reports, about 13,000 rows but it didn't change performance at all.
*/

DO $$
    BEGIN
        perform l2_legacy_transactions(user_data) from l2_submission
             where superseded_id is null
               and version = '1.0';
    END
$$ LANGUAGE plpgsql;