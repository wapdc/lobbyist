do $$
    -- This script updating a numbers of tables relating to lobbyist employers.
    DECLARE
        e_rec                   record;
        v_entity_id             integer;
        v_committee_id          integer;
        BEGIN
            -- Move lobbyist employer records to matched committee records if they were created before the
            -- crosswalk table was populated to avoid duplicate entities.
            for e_rec in (select e.*,c.person_id
                          from lobbyist_employer e
                              left join tmp_lobbyist_entity_crosswalk tlec on e.drupal_nid = tlec.drupal_nid
                              left join committee c on c.committee_id=tlec.committee_id
                          where (c.person_id <> e.entity_id) or (e.drupal_nid <> e.lobbyist_employer_id)
                                     ) loop
                  if e_rec.person_id <> e_rec.entity_id then
                    update wapdc.public.lobbyist_employer set entity_id = e_rec.person_id where lobbyist_employer_id=e_rec.lobbyist_employer_id;
                    -- Remove the entity record created in error by the sync process.
                    delete from entity where entity_id = e_rec.entity_id;
                  end if;
                  if e_rec.lobbyist_employer_id <> e_rec.drupal_nid then
                    update pdc_user_authorization set target_id = e_rec.drupal_nid where target_id = e_rec.lobbyist_employer_id and target_type='lobbyist_employer';
                    update fund set target_id = e_rec.drupal_nid where target_type='lobbyist_employer' and target_id = e_rec.lobbyist_employer_id;
                    update lobbyist_employer set lobbyist_employer_id = e_rec.drupal_nid where lobbyist_employer_id=e_rec.lobbyist_employer_id;
                  end if;
              end loop;
            -- Select all employer records from view table
            for e_rec in (select
                    drupal_nid,
                    name,
                    email,
                    address_1,
                    address_2,
                    city,
                    state,
                    postcode,
                    country,
                    phone,
                    description,
                    pac,
                    updated_at
                    from lobbyist_employers_migration
                    where drupal_nid not in (select drupal_nid from lobbyist_employer)
            )
            LOOP

            -- check to see if existing matching entity in tmp_lobbyist_employer CROSSWALK table
            select c.person_id, c.committee_id into v_entity_id,v_committee_id from tmp_lobbyist_entity_crosswalk tle join committee c on tle.committee_id = c.committee_id
                where tle.drupal_nid = e_rec.drupal_nid;

            IF v_entity_id IS NULL THEN
                -- if not existing matching entity in crosswalk, create new entity
                INSERT INTO entity (name, address, city, state, postcode, country, phone)
                    VALUES (e_rec.name,
                            CONCAT(e_rec.address_1,' ',e_rec.address_2),
                            e_rec.city,
                            e_rec.state,
                            e_rec.postcode,
                            e_rec.country,
                            e_rec.phone) returning entity_id into v_entity_id;
                if v_committee_id is not null then
                    update committee set person_id = v_entity_id where committee_id = v_committee_id;
                end if;
            END IF;

            -- for each employer create a record in the lobbyist_employer table with appropriate entity_id
            insert into lobbyist_employer (lobbyist_employer_id, drupal_nid, entity_id, name, email, address_1, address_2, city, state,
                                           postcode, country, phone, description, updated_at)
            VALUES (e_rec.drupal_nid, e_rec.drupal_nid, v_entity_id, e_rec.name, e_rec.email, e_rec.address_1, e_rec.address_2,
                    e_rec.city, e_rec.state, e_rec.postcode, e_rec.country, e_rec.phone, e_rec.description,
                    e_rec.updated_at);

            END LOOP;
        END
    $$ language plpgsql;