do language plpgsql $$
declare
    v_max_nid BIGINT;
    v_max_vid BIGINT;
begin
    select max(vid), max(nid) into v_max_vid, v_max_nid from accesshub_revisions_2024_04_29;
    delete from l3_submission where submission_id in (select submission_id from l3_submission s left join accesshub_revisions_2024_04_29 a on a.vid = s.submission_id where a.vid is null )
                                and submission_id <= v_max_vid;
    delete from  l2_submission where submission_id in (select submission_id from l2_submission s left join accesshub_revisions_2024_04_29 a on a.vid = s.submission_id where a.vid is null)
                                 and submission_id <= v_max_vid;
    delete from lobbyist_contract_submission
           where submission_id in (select submission_id from lobbyist_contract_submission s left join accesshub_revisions_2024_04_29 a on a.vid=s.submission_id where a.vid is null)
                                               and submission_id <= v_max_vid;
    delete from lobbyist_firm_submission where submission_id in (select submission_id from lobbyist_firm_submission s left join  accesshub_revisions_2024_04_29 a on a.vid=s.submission_id where a.vid is null)
                                           and submission_id <= v_max_vid;
    delete from lobbyist_client_submission where submission_id  in (select  submission_id  from lobbyist_client_submission s left join  accesshub_revisions_2024_04_29 a on a.vid=s.submission_id where a.vid is null)
                                             and submission_id <= v_max_vid;
    delete from l2
           where l2.report_id in (select l2.report_id from l2 left join accesshub_revisions_2024_04_29 a on a.nid=l2.report_id where a.nid is null)
                     and l2.report_id <= v_max_nid;
    delete from l3
           where l3.report_id in (select l3.report_id from l3 left join accesshub_revisions_2024_04_29 a on a.nid=l3.report_id where a.nid is null)
                     and l3.report_id <= v_max_nid;
    delete from lobbyist_contract
           where lobbyist_contract_id  in (select lobbyist_contract_id from lobbyist_contract lc left join accesshub_revisions_2024_04_29 a on a.nid = lc.lobbyist_contract_id where a.nid is null)
                                    and lobbyist_contract_id <= v_max_nid;
    delete from lobbyist_firm where lobbyist_firm_id  in (select lobbyist_firm_id from lobbyist_firm lf left join  accesshub_revisions_2024_04_29 a on a.nid = lf.lobbyist_firm_id where a.nid is null)
                                and lobbyist_firm_id <= v_max_nid;
    delete from lobbyist_client where lobbyist_client_id  in (select lobbyist_client_id from lobbyist_client lc left join  accesshub_revisions_2024_04_29 a on a.nid = lc.lobbyist_client_id where a.nid is null)
                                  and lobbyist_client_id <= v_max_nid;
end;
$$
