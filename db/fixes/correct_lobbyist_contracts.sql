-- Correct superseded is on all reports
update lobbyist_contract_submission set superseded_id = u.superseded_id from (select lc2.submission_id, lc2.superseded_id from (select
    lcs.submission_id,
    lead(lcs.submission_id) over (partition by lc.lobbyist_contract_id order by submitted_at) as superseded_id,
    row_number() over (partition by lc.lobbyist_contract_id order by submitted_at)
from lobbyist_contract lc join lobbyist_contract_submission lcs on lcs.lobbyist_contract_id=lc.lobbyist_contract_id) lc2
where lc2.superseded_id is not null) u
where lobbyist_contract_submission.submission_id = u.submission_id;

-- Set the current submission to the maximum one for the contract.
update lobbyist_contract set current_submission_id = u.current_submission_id from (select lc.lobbyist_contract_id, max(lcs.submission_id) as current_submission_id
  from lobbyist_contract lc join lobbyist_contract_submission lcs on lcs.lobbyist_contract_id=lc.lobbyist_contract_id
group by lc.lobbyist_contract_id having max(lcs.submission_id)<> max(lc.current_submission_id)) u
where u.lobbyist_contract_id = lobbyist_contract.lobbyist_contract_id;

-- Set the subcontractor on all records that have them.
update lobbyist_contract set contractor_id = u.contractor_id from (select lc.lobbyist_contract_id,
       cast(lcs.user_data->'field_firm_that_is_employing_you'->'und'->0->>'target_id' as int) as contractor_id
   from lobbyist_contract lc join lobbyist_contract_submission lcs on lc.current_submission_id=lcs.submission_id
   where  cast(lcs.user_data->'field_firm_that_is_employing_you'->'und'->0->>'target_id' as int) is not null) u
where u.lobbyist_contract_id = lobbyist_contract.lobbyist_contract_id




