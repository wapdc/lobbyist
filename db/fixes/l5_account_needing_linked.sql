select @@servername; --zeus
use Lobbying;

select *
from (select u.username, u.l5filer_id from aspnetdb..l5_users u where username like '%@%'
      union select f.account, f.l5filer_id from l5filers f where f.account like '%@%'
      union select f.email, f.l5filer_id from l5filers f where f.email like '%@%') e
where exists (select 1 from l5reports r where r.l5filer_id = e.l5filer_id and r.postl5 > '2021-01-01')
  and not exists (select 1
                  from (select l5filer_id
                        from rds.wapdc.[public].pdc_user_authorization az
                                 join rds.wapdc.[public].lobbyist_public_agency l
                                      on az.target_type = 'lobbyist_public_agency' and az.target_id = l.id) d
                  where d.l5filer_id = e.l5filer_id);