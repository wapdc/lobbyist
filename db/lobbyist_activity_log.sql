CREATE TABLE IF NOT EXISTS lobbyist_activity_log
(
    id                     serial primary key,
    lobbyist_firm_id       int,
    lobbyist_client_id     int,
    lobbyist_contractor_id int,
    action                 text,
    message                text,
    updated_at             timestamptz not null default now()
);
