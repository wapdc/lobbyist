create table if not exists l3_independent_expenditure
(
    l3_transaction_id int references l3_transaction(l3_transaction_id) on delete cascade,
    candidacy_id int,
    stance text,
    ballot_number text
);