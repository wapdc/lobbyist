create sequence lobbyist_submission_seq
    as integer
    start with 2000000;

alter sequence lobbyist_submission_seq owner to wapdc;

alter sequence lobbyist_submission_seq owned by l2_submission.submission_id;