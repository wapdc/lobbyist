create sequence lobbyist_report_seq
    as integer
    start with 1000000;

alter sequence lobbyist_report_seq owner to wapdc;

alter sequence lobbyist_report_seq owned by l2.report_id;