create table if not exists l3_contribution
(
    l3_transaction_id int references l3_transaction(l3_transaction_id) on delete cascade,
    committee_id int,
    candidacy_id int
);