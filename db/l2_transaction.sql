CREATE TABLE IF NOT EXISTS l2_transaction
(
    l2_transaction_id serial primary key,
    source_target_type text,
    source_target_id int,
    source_name text,
    report_id int references l2(report_id) on delete cascade,
    amount numeric,
    transaction_type text
);