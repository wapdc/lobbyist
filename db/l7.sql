create table if not exists l7 (
  report_id serial primary key,
  current_submission_id integer,
  target_id integer,
  target_type text
);
