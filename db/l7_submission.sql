create table if not exists l7_submission (
  submission_id serial primary key,
  superseded_id integer,
  report_id integer not null,
  version text not null,
  submitted_at timestamptz not null default now(),
  user_data json not null,
  foreign key(report_id) references l7(report_id)
);
