-- Drop view od_lobbyist_registration_summary
CREATE OR REPLACE VIEW od_lobbyist_registration_summary AS
select lc.lobbyist_contract_id::text || '-' || y::text as id,
       lc.lobbyist_contract_id as report_number,
       f.lobbyist_firm_id as firm_id,
       f.name as firm_name,
       accesshub_url() || '/node/' || lc.lobbyist_firm_id as lobbyist_url,
       c.lobbyist_client_id as employer_id,
       c.name as employer_name,
       accesshub_url() || '/node/' || c.lobbyist_client_id as employer_url,
       y as employment_year,
       fc.lobbyist_firm_id as contractor_id,
       fc.name as contractor_name,
       r.reports::text as employment_period
from generate_series(extract(year from now())::int - 17, extract(year from now())::int,1) y
join lobbyist_contract lc on y between extract(year from period_start) and extract(year from period_end)
join lobbyist_client c on c.lobbyist_client_id=lc.lobbyist_client_id
join lobbyist_firm f on f.lobbyist_firm_id=lc.lobbyist_firm_id
left join lobbyist_firm fc on fc.lobbyist_firm_id=lc.contractor_id
left join (select rp.lobbyist_contract_id, extract(year from rp.period_start) yr, json_agg(json_build_object('year', extract(year from rp.period_start),
                                                                      'month', extract(month from rp.period_start),
                                                                      'period', extract(year from rp.period_start) || '-' || lpad(extract(month from rp.period_start)::text,2,'0'::Text),
                                                                      'exempt', rp.exempt)) as reports from lobbyist_reporting_periods rp
                group by rp.lobbyist_contract_id, extract(year from period_start))  r
  on r.lobbyist_contract_id=lc.lobbyist_contract_id and r.yr=y
where y>=2016
