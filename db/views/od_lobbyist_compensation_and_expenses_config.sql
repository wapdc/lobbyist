-- note that the double cast below makes sure the data is stored efficiently in the table (that is, without cr/lf and white space)
with stage_specific
         as (select s.value as stage,
                    case when s.value = 'live' then true else false end              as is_enabled,
                    case when s.value = 'live' then '9nnw-c693' else 'XXXX-XXXX' end as dataset_id
             from wapdc_settings s
             where property = 'stage'),
     data
         as (
         select
             'lobbyist_compensation_and_expenses_by_source',
             stage,
             is_enabled,
             cast(cast(concat(
                     $$
{
 "changes": [
   {
     "schema": "public",
     "table": "l2_submission",
     "changes": [
       "insert",
       "update"
     ],
     "columns": [
       "report_id"
     ],
     "event": "updateL2ReportData"
   },
   {
     "schema": "public",
     "table": "l2",
     "changes": [
       "delete"
     ],
     "columns": [
       "report_id"
     ],
     "event": "deleteL2ReportData"
   }
 ],
 "events": {
   "updateL2ReportData": {
     "type": "replace",
     "source": "select * from od_lobbyist_compensation_and_expenses where report_number=$1",
     "destination": "select id where report_number=$1",
     "criteria": [
       "report_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "deleteL2ReportData": {
     "type": "delete",
     "source": "select id where report_number=$1",
     "criteria": [
       "report_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   }
 }
}
$$
                       ) as json) as text)
         from stage_specific
     )
insert into open_data_config
select * from data
on conflict(job_name,stage)
    do update set config=excluded.config, is_enabled=excluded.is_enabled;