drop view od_l7;
create or replace view od_l7 as
select
  s.submission_id as id,
  s.report_id as report_number,
  s.superseded_id as amended_by,
  sa.submission_id as amends,
  open_data_date_format(s.submitted_at) as receipt_date,
  rf.lobbyist_firm_id as firm_id,
  rc.lobbyist_client_id as client_id,
  case
    when r.target_type = 'lobbyist_firm' then 'Firm'
    when r.target_type = 'lobbyist_client' then 'Client'
  end as lobbyist_type,
  s.user_data->'lobbyist'->>'name' as lobbyist_name,
  s.user_data->'lobbyist'->>'address' as lobbyist_address,
  s.user_data->'lobbyist'->>'city' as lobbyist_city,
  s.user_data->'lobbyist'->>'state' as lobbyist_state,
  s.user_data->'lobbyist'->>'postcode' as lobbyist_postcode,
  s.user_data->>'employee_name' as employee_name,
  s.user_data->'lobbyist_employment'->>'compensation' as lobbyist_employment_compensation,
  s.user_data->'lobbyist_employment'->>'description' as lobbyist_employment_description,
  s.user_data->'public_employment'->>'description' as public_employment_description,
  s.user_data::text as user_data,
  apollo_url() || '/lobbyist/#/public/l7-report/' || s.submission_id as pdc_report_url
from l7_submission s
  join l7 r on s.report_id = r.report_id
  left join lobbyist_firm rf on rf.lobbyist_firm_id = r.target_id
  left join lobbyist_client rc on rc.lobbyist_client_id = r.target_id
  left join l7_submission sa on sa.superseded_id = s.submission_id
;
