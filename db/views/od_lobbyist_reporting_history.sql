drop view if exists od_lobbyist_reporting_history;
create or replace view od_lobbyist_reporting_history as
    select * from od_l2_reporting_history
    union
    select * from od_l3_reporting_history
    union
    select * from od_lobbyist_contract_history
    union
    select * from od_lobbyist_firm_history
    union
    select * from od_lobbyist_client_history
    order by year desc, receipt_date desc;

