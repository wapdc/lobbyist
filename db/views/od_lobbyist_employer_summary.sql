drop view if exists od_lobbyist_employer_summary;
create or replace view od_lobbyist_employer_summary as

select concat(lc.lobbyist_client_id, '-', sums.reporting_year) as id,
       sums.reporting_year as year,
       lc.name as employer_name,
       lc.email as employer_email,
       lc.phone as employer_phone,
       case when lc.address_2 is null
                then lc.address_1
            else concat(lc.address_1, ' ', lc.address_2)
           end as employer_address,
       lc.city as employer_city,
       lc.state as employer_state,
       lc.postcode as employer_zip,
       lc.country as employer_country,
       sums.lobbyist_reported_compensation_total as compensation,
       sums.lobbyist_reported_expenses_total as expenditures,
       sums.ballot_proposition as ballot_prop ,
       sums.entertainment_expense as entertain ,
       sums.payment_to_vendor as vendor ,
       sums.witnesses_retained_for_lobbying_services as expert_retain ,
       sums.costs_for_promotional_materials as inform_material,
       sums.grassroots_expenses as lobbying_comm,
       sums.ie_candidate as ie_in_support ,
       sums.itemized_expense as itemized_exp ,
       sums.non_itemized_contributions as agg_contrib,
       sums.itemized_contribution as political,
       sums.other_expense as other_l3_exp,
       sums.corrected_compensation as corr_compensation ,
       sums.corrected_expense as corr_expend ,
       sums.total_expenses as total_exp ,
       sums.report_id as l3_nid ,
       lc.lobbyist_client_id as employer_nid,
       apollo_url() || '/lobbyist/public/-/#/public/l3-report/' || l3.current_submission_id as url
from (
         select
             lobbyist_client_id,
             report_year as reporting_year,
             r.report_id,
             sum(case when transaction_type = 'lobbyist_reported_compensation_total' then amount else 0 end) as lobbyist_reported_compensation_total,
             sum(case when transaction_type = 'lobbyist_reported_expense_total' then amount else 0 end) as lobbyist_reported_expenses_total,
             sum(case when transaction_type = 'ie_ballot_proposition' then amount else 0 end) as ballot_proposition,
             sum(case when transaction_type = 'payments_for_registered_lobbyists' then amount else 0 end) as payment_to_vendor,
             sum(case when transaction_type = 'witnesses_retained_for_lobbying_services' then amount else 0 end) as witnesses_retained_for_lobbying_services,
             sum(case when transaction_type = 'costs_for_promotional_materials' then amount else 0 end) as costs_for_promotional_materials,
             sum(case when transaction_type = 'grassroots_expenses_for_lobbying_communications' then amount else 0 end) as grassroots_expenses,
             sum(case when transaction_type = 'ie_candidate' then amount else 0 end) as ie_candidate,
             sum(case when transaction_type = 'itemized_expense' then amount else 0 end) as itemized_expense,
             sum(case when transaction_type = 'other_expense' then amount else 0 end) as other_expense,
             sum(case when transaction_type = 'itemized_contribution' then amount else 0 end) as itemized_contribution,
             sum(case when transaction_type = 'non_itemized_political_contribution_amount' then amount else 0 end) as non_itemized_contributions,
             sum(case when transaction_type = 'entertainment_expense' then amount else 0 end) as entertainment_expense,
             sum(case when transaction_type = 'lobbyist_compensation_total' then amount else 0 end) as corrected_compensation,
             sum(case when transaction_type = 'lobbyist_expense_total' then amount else 0 end) as  corrected_expense,
             sum(case when transaction_type in ('lobbyist_reported_expense_total','non_itemized_political_contribution_amount','ballot_proposition','entertainment_expense','payments_for_registered_lobbyists'
                ,'witnesses_retained_for_lobbying_services','costs_for_promotional_materials','grassroots_expenses_for_lobbying_communications',
                'lobbyist_reported_compensation_total','ie_candidate','itemized_expense','other_expense','itemized_contribution') then amount else 0 end) as total_expenses
         from (
                  select distinct on (lrp.lobbyist_client_id, extract(year from lrp.period_start))
                      l3.report_id, lrp.lobbyist_client_id, extract(year from lrp.period_start) as report_year
                  from lobbyist_reporting_periods lrp
                           left join l3 on lrp.lobbyist_client_id = l3.lobbyist_client_id and l3.report_year = extract(year from lrp.period_start)
               ) r
             left join l3_transaction t on r.report_id = t.report_id
         group by lobbyist_client_id, reporting_year, r.report_id
     ) sums
         left join lobbyist_client lc on sums.lobbyist_client_id = lc.lobbyist_client_id
         left join l3 on sums.report_id = l3.report_id