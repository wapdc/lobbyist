-- note that the double cast below makes sure the data is stored efficiently in the table (that is, without cr/lf and white space)
with stage_specific
         as (select s.value as stage,
                    case when s.value = 'live' then true else false end              as is_enabled,
                    case when s.value = 'live' then 'xhn7-64im' else 'XXXX-XXXX' end as dataset_id
             from wapdc_settings s
             where property = 'stage'),
     data
         as (
         select
             'lobbyist_registration_summary',
             stage,
             is_enabled,
             cast(cast(concat(
                     $$
{
 "changes": [
   {
     "schema": "public",
     "table": "lobbyist_contract",
     "changes": [
       "insert",
       "update"
     ],
     "columns": [
       "lobbyist_contract_id"
     ],
     "event": "updateLobbyistRegistrationData"
   },
  {
     "schema": "public",
     "table": "lobbyist_contract",
     "changes": [
       "delete"
     ],
     "columns": [
       "lobbyist_contract_id"
     ],
     "event": "deleteLobbyistRegistrationData"
   }
 ],
 "events": {
   "updateLobbyistRegistrationData": {
     "type": "upsert",
     "source": "select * from od_lobbyist_registration_summary where report_number=$1",
     "criteria": [
       "lobbyist_contract_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
  "deleteLobbyistRegistrationData": {
     "type": "delete",
     "source": "select id where report_number=$1",
     "criteria": [
       "lobbyist_contract_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   }
 }
}
$$
                       ) as json) as text)
         from stage_specific
     )
insert into open_data_config
select * from data
on conflict(job_name,stage)
    do update set config=excluded.config, is_enabled=excluded.is_enabled;
