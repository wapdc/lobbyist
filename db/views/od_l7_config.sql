-- note that the double cast below makes sure the data is stored efficiently in the table (that is, without cr/lf and white space)
insert into open_data_config(job_name, stage, is_enabled, config)
values (
        'l7_reports',
        (select value from wapdc_settings where property='stage'),
        true,
        CAST(CAST(
$$
{
  "changes": [
    {
      "schema": "public",
      "table": "l7_submission",
      "changes": [
        "insert",
        "update"
      ],
      "columns": [
        "submission_id"
      ],
      "event": "updateReportData"
    },
    {
      "schema": "public",
      "table": "l7",
      "changes": [
        "delete"
      ],
      "columns": [
        "submission_id"
      ],
      "event": "deleteReportData"
    }
  ],
  "events": {
    "updateReportData": {
      "type": "upsert",
      "source": "select * from od_l7 where id=$1",
      "criteria": [
        "submission_id"
      ],
      "dataset_id": "ef7g-tyg8"
    },
    "deleteReportData": {
      "type": "delete",
      "source": "select id where id=$1",
      "criteria": [
        "submission_id"
      ],
      "dataset_id": "ef7g-tyg8"
    }
  }
}
$$
        as JSONB) as TEXT))
        on conflict(job_name,stage)
do update set config=excluded.config;
