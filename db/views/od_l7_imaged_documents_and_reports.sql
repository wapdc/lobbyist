drop view od_l7_imaged_documents_and_reports;
create or replace view od_l7_imaged_documents_and_reports as
select
  s.submission_id::text || '.l7_submission' as id,
  s.submission_id as report_number,
  case
    when s.superseded_id > 0 then 'L7 AMENDED'
    else 'L7'
  end as origin,
  case
    when s.superseded_id > 0 then 'Amended Form L7, employment submissions'
    else 'Form L7, employment submissions'
  end as document_description,
  coalesce(rf.lobbyist_firm_id, rc.lobbyist_client_id) as filer_id,
  null as type,
  s.user_data->'lobbyist'->>'name' as filer_name,
  null as office,
  null as legislative_district,
  null as party,
  null as election_year,
  open_data_date_format(s.submitted_at) as receipt_date,
  open_data_date_format(s.submitted_at) as processed_date,
  'Electronic' as filing_method,
  null as report_from,
  null as report_to,
  apollo_url() || '/lobbyist/#/public/l7-report/' || s.submission_id as url

from l7_submission s
  join l7 r on s.report_id = r.report_id
  left join lobbyist_firm rf on rf.lobbyist_firm_id = r.target_id
  left join lobbyist_client rc on rc.lobbyist_client_id = r.target_id
  left join l7_submission sa on sa.superseded_id = s.submission_id
;
