drop view if exists od_l2_imaged_documents_and_reports;
create or replace view od_l2_imaged_documents_and_reports as
select l2s.submission_id::text || '.lobbyist'::text as id,
       l2.report_id::text as report_number,
       case
           when l2s.superseded_id > 0 then 'L2 AMENDED'
           else 'L2'
           end as origin,
       case
           when l2s.superseded_id > 0 then 'Amended Form L2, lobbyist monthly report'
           else 'Form L2, lobbyist monthly report'
           end as document_description,
       l2.lobbyist_firm_id as filer_id,
       'LOBBYISTS' as type,
       lf.name as filer_name,
       '' as office,
       '' as legislative_district,
       '' as party,
       date_part('year', l2.period_start) as election_year,
       open_data_date_format(l2s.submitted_at) as receipt_date,
       open_data_date_format(l2s.submitted_at) as processed_date,
       'Electronic' as filing_method,
       l2.period_start as report_from,
       l2.period_end as report_to,
       apollo_url() || '/lobbyist/#/public/l2-report/' || l2s.submission_id as url
from l2
         join l2_submission l2s on l2.report_id = l2s.report_id
         left join lobbyist_firm lf on l2.lobbyist_firm_id = lf.lobbyist_firm_id