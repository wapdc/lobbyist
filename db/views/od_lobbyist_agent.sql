drop view if exists od_lobbyist_agent;
create or replace view od_lobbyist_agent as
with client_firm_data as (
    select
        year::int as year,
        string_agg(client_name, ', ') as client_names,
        lobbyist_firm_id,firm_name,firm_email,firm_address,firm_phone
    from (
             select
                 lc.lobbyist_firm_id,
                 c.name as client_name,
                 date_part('year', lrp.period_start) as year,
                 lf.name as firm_name,
                 lf.email as firm_email,
                 concat_ws(' ', lf.address_1, lf.city, lf.state, lf.postcode) as firm_address,
                 lf.phone as firm_phone
             from
                 lobbyist_contract lc
                     join lobbyist_client c on lc.lobbyist_client_id = c.lobbyist_client_id
                     join lobbyist_firm lf on lc.lobbyist_firm_id = lf.lobbyist_firm_id
                     left join lobbyist_reporting_periods lrp on lrp.lobbyist_client_id = c.lobbyist_client_id
                     and lrp.lobbyist_firm_id = lc.lobbyist_firm_id
             where
                 lc.contractor_id is null
             group by
                 lc.lobbyist_firm_id, c.name, date_part('year', lrp.period_start),lf.name,lf.email,lf.phone,lf.address_1,lf.city,lf.state,lf.postcode
         ) as cd
    group by year, lobbyist_firm_id, firm_name,firm_email,firm_address,firm_phone
),
     agent_info as (
         select
             la.lobbyist_firm_id as firm_id,
             la.name as agent_name,
             min(la.bio) as bio,
             date_part('year', lrp.period_start) as year,
             coalesce(
                     min(
                             case
                                 when la.agent_picture is not null then
                                     concat(accesshub_url(), '/sites/default/files/styles/thumbnail/public/', replace(la.agent_picture, 'public://', ''))
                                 else ''
                                 end
                     ), '' ) as agent_pic_url,
             cast(min(
                 /* Only consider valid training dates based on the biennium that we are in */
                     case
                        when mod(cast( date_part('year', lrp.period_start) as int), 2) = 1
                             and la.training_certified >= to_date(concat(cast( date_part('year', lrp.period_start) as int) - 1, '-10-01'), 'YYYY-MM-DD')
                             and la.training_certified < to_date(concat(cast( date_part('year', lrp.period_start) as int) + 1, '-10-01'), 'YYYY-MM-DD')
                         then la.training_certified
                         when mod(cast(date_part('year', lrp.period_start) as int), 2) = 0
                            AND la.training_certified >= to_date(concat(cast( date_part('year', lrp.period_start) as int) - 2, '-10-01'), 'YYYY-MM-DD')
                             AND la.training_certified < to_date(concat(cast( date_part('year', lrp.period_start) as int), '-10-01'), 'YYYY-MM-DD')
                         THEN la.training_certified
                 END) as date) as training_certified
         from
             lobbyist_agent la
                 join lobbyist_reporting_periods lrp on la.lobbyist_firm_id = lrp.lobbyist_firm_id
                 where date_part('year', lrp.period_start) >= date_part('year', la.starting)
                 and (la.ending is null or date_part('year', lrp.period_start) <= date_part('year', la.ending))
         group by
             la.lobbyist_firm_id, la.name, date_part('year', lrp.period_start)
     )
select
    md5(concat( coalesce(ai.agent_name,''),' ', yn.lobbyist_firm_id, yn.year) ) as id,
    yn.lobbyist_firm_id as filer_id,
    yn.firm_name as lobbyist_firm_name,
    yn.firm_phone as lobbyist_phone,
    yn.firm_email as lobbyist_email,
    yn.firm_address as lobbyist_address,
    yn.client_names as employers,
    coalesce(ai.agent_name,'') as agent_name,
    coalesce(ai.bio,'') as agent_bio,
    coalesce(ai.agent_pic_url,'') as agent_pic_url,
    yn.year as employment_year,
    accesshub_url() || '/node/' || yn.lobbyist_firm_id as lobbyist_firm_url,
    coalesce(cast(ai.training_certified as text),'') as training_certified
from client_firm_data yn
         left join agent_info ai on ai.firm_id = yn.lobbyist_firm_id and ai.year = yn.year
order by filer_id,employment_year;

