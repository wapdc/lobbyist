-- note that the double cast below makes sure the data is stored efficiently in the table (that is, without cr/lf and white space)
with stage_specific
         as (select s.value as stage,
                    case when s.value = 'live' then true else false end              as is_enabled,
                    case when s.value = 'live' then 'biux-xiwe' else 'XXXX-XXXX' end as dataset_id
             from wapdc_settings s
             where property = 'stage'),
     data
         as (
         select
             'lobbyist_employer_summary',
             stage,
             is_enabled,
             cast(cast(concat(
                     $$
{
 "changes": [
   {
     "schema": "public",
     "table": "l3",
     "changes": [
       "insert",
       "update",
       "delete"
     ],
     "columns": [
       "lobbyist_client_id",
       "report_year"
     ],
     "event": "updateLobbyistEmployerTotals"
   },
   {
      "schema": "public",
      "table": "lobbyist_client",
      "changes": [
        "insert",
        "update"
      ],
      "columns": [
        "lobbyist_client_id"
      ],
      "event": "updateLobbyistEmployerData"
    },
    {
      "schema": "public",
      "table": "lobbyist_client",
      "changes": [
        "delete"
      ],
      "columns": [
        "lobbyist_client_id"
      ],
      "event": "deleteLobbyistEmployerData"
    }
 ],
 "events": {
   "updateLobbyistEmployerTotals": {
     "type": "upsert",
     "source": "select * from od_lobbyist_employer_summary where employer_nid=$1 and year = $2",
     "criteria": [
       "lobbyist_client_id",
        "report_year"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "updateLobbyistEmployerData": {
      "type": "upsert",
      "source": "select * from od_lobbyist_employer_summary where employer_nid=$1",
      "criteria": [
        "lobbyist_client_id"
      ],
      "dataset_id": "$$,dataset_id,$$"
    },
    "deleteLobbyistEmployerData": {
      "type": "delete",
      "source": "select id where employer_nid=$1",
      "criteria": [
        "lobbyist_client_id"
      ],
      "dataset_id": "$$,dataset_id,$$"
    }
 }
}
$$
                       ) as json) as text)
         from stage_specific
     )
insert into open_data_config
select * from data
on conflict(job_name,stage)
    do update set config=excluded.config, is_enabled=excluded.is_enabled;
