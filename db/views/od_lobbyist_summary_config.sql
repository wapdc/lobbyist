-- note that the double cast below makes sure the data is stored efficiently in the table (that is, without cr/lf and white space)
with stage_specific
         as (select s.value as stage,
                    case when s.value = 'live' then true else false end              as is_enabled,
                    case when s.value = 'live' then 'c4ag-3cmj' else 'XXXX-XXXX' end as dataset_id
             from wapdc_settings s
             where property = 'stage'),
     data
         as (
         select
             'lobbyist_summary',
             stage,
             is_enabled,
             cast(cast(concat(
                     $$
{
 "changes": [
   {
     "schema": "public",
     "table": "l2",
     "changes": [
       "insert",
       "update",
       "delete"
     ],
     "columns": [
       "lobbyist_firm_id",
       "period_start"
     ],
     "event": "updateLobbyistTotals"
   },
   {
      "schema": "public",
      "table": "lobbyist_firm",
      "changes": [
        "insert",
        "update"
      ],
      "columns": [
        "lobbyist_firm_id"
      ],
      "event": "updateLobbyistData"
    },
    {
      "schema": "public",
      "table": "lobbyist_firm",
      "changes": [
        "delete"
      ],
      "columns": [
        "lobbyist_firm_id"
      ],
      "event": "deleteLobbyistData"
    }
 ],
 "events": {
   "updateLobbyistTotals": {
     "type": "upsert",
     "source": "select * from od_lobbyist_summary where filer_id=$1 and year = extract('year' from $2::date)",
     "criteria": [
       "lobbyist_firm_id",
        "period_start"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "updateLobbyistData": {
      "type": "upsert",
      "source": "select * from od_lobbyist_summary where filer_id=$1",
      "criteria": [
        "lobbyist_firm_id"
      ],
      "dataset_id": "$$,dataset_id,$$"
    },
    "deleteLobbyistData": {
      "type": "delete",
      "source": "select id where filer_id=$1",
      "criteria": [
        "lobbyist_firm_id"
      ],
      "dataset_id": "$$,dataset_id,$$"
    }
 }
}
$$
                       ) as json) as text)
         from stage_specific
     )
insert into open_data_config
select * from data
on conflict(job_name,stage)
    do update set config=excluded.config, is_enabled=excluded.is_enabled;