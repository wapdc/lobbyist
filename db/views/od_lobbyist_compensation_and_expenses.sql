--drop view if exists od_lobbyist_compensation_and_expenses;
create or replace view od_lobbyist_compensation_and_expenses as
select distinct concat(l2.report_id, '-', coalesce(x.source_target_id, s.lobbyist_contract_id)) as id,
        l2.report_id as report_number,
        'FE' as origin,
        l2.lobbyist_firm_id as filer_id,
        lf.name as filer_name,
        case when x.source_target_type = 'lobbyist_firm' or  l2.lobbyist_firm_id = coalesce(x.source_target_id, s.lobbyist_contract_id)  then 'Lobbyist'
            when coalesce(x.source_target_type, 'lobbyist_contract') = 'lobbyist_contract' or s.lobbyist_contract_id is not null then 'Employer'
            end as type,
        coalesce(x.source_target_id, s.lobbyist_contract_id) as funding_source_id,
        coalesce(x.source_name, lclient.name,concat(lf.name, ' (Self)')) as funding_source_name,
        z.submitted_at as first_filed_at,
        to_char(l2.period_start,'MM/YYYY') as filing_period,
        to_char(l2s.submitted_at,'MM/DD/YYYY') as receipt_date,
        coalesce(cast(lclient.lobbyist_client_id as text),'')  as employer_id,
        coalesce(lclient.name,'') as employer_name,
        coalesce(x.compensation,0) as compensation,
        coalesce(-1.0 * st.sub_lobbyist_compensation,0) as sub_lobbyist_compensation,
        coalesce(x.compensation,0) - coalesce(st.sub_lobbyist_compensation,0) as net_compensation,
        coalesce(x.personal_expenses,0) as personal_expenses,
        coalesce(x.entertainment,0) as entertainment,
        coalesce(x.contributions,0) as contributions,
        coalesce(x.advertising,0) as advertising,
        coalesce(x.political_ads,0) as political_ads,
        coalesce(x.independent_expenditures_candidate,0) as independent_expenditures_candidate,
        coalesce(x.independent_expenditures_ballot,0) as independent_expenditures_ballot,
        coalesce(x.other,0) as other,
        coalesce(x.direct_lobbying_expenses ,0)as direct_expenses,
        coalesce(x.indirect_lobbying_expenses,0) as indirect_expenses,
        coalesce(x.contributions + x.political_ads + x.independent_expenditures_candidate + x.independent_expenditures_ballot,0) as contributions_total,
        coalesce(x.total_expenses,0) as total_expenses,
        coalesce(x.total_expenses_and_compensation,0) - coalesce(st.sub_lobbyist_compensation,0) as net_total,
       case when l2.lobbyist_firm_id = coalesce(x.source_target_id, s.lobbyist_contract_id) then '' else cast(s.lobbyist_contract_id as text) end as employment_registration_id,
       case when (l2.lobbyist_firm_id = coalesce(x.source_target_id, s.lobbyist_contract_id) and s.subcontractor is not null) then ''
            when s.subcontractor is not null then 'Subcontractor'
            else 'Direct' end as employment_type,
        coalesce(cast(s.subcontractor as text),'') as contractor_id,
        coalesce((select name from lobbyist_firm where lobbyist_firm_id = s.subcontractor),'') as contractor_name,
        apollo_url() || '/lobbyist/public/-/#/public/l2-report/' || l2s.submission_id as url
from l2
         join l2_submission l2s on l2.report_id = l2s.report_id and l2s.superseded_id is null
         join lobbyist_firm lf on l2.lobbyist_firm_id = lf.lobbyist_firm_id
         join
         (select lc.lobbyist_firm_id, lc.lobbyist_contract_id,lc.lobbyist_client_id as lobbyist_client_id,lc.contractor_id as subcontractor ,lrp.period_start, lrp.period_end from lobbyist_contract lc
            join lobbyist_reporting_periods lrp on lrp.lobbyist_contract_id = lc.lobbyist_contract_id
          union all
          select lc2.lobbyist_firm_id, lc2.lobbyist_firm_id,null::int ,null::int , lrp2.period_start, lrp2.period_end from lobbyist_contract lc2
            join lobbyist_reporting_periods lrp2 on lrp2.lobbyist_contract_id = lc2.lobbyist_contract_id
         ) s
         on l2.lobbyist_firm_id = s.lobbyist_firm_id and l2.period_start = s.period_start and l2.period_end = s.period_end
         left join lobbyist_client lclient on s.lobbyist_client_id = lclient.lobbyist_client_id
        left join(
    select
        source_target_id,
        source_target_type,
        source_name,
        report_id,
        sum(case when transaction_type = 'compensation' then amount else 0 end) as compensation,
        sum(case when transaction_type = 'personal_expense' then amount else 0 end) as personal_expenses,
        sum(case when transaction_type in ('non_itemized_entertainment_expenses', 'itemized_entertainment_expense')  then amount else 0 end) as entertainment,
        sum(case when transaction_type in ('itemized_contribution', 'non_itemized_contributions','in_kind_expense', 'ad_expense') then amount else 0 end) as contributions,
        sum(case when transaction_type = 'advertising_expense' then amount else 0 end) as advertising,
        sum(case when transaction_type = 'ad_expense' then amount else 0 end) as political_ads,
        sum(case when transaction_type = 'independent_expenditures_candidate_expense' then amount else 0 end) as independent_expenditures_candidate,
        sum(case when transaction_type = 'independent_expenditures_ballot_expense' then amount else 0 end) as independent_expenditures_ballot,
        sum(case when transaction_type = 'other_expense' then amount else 0 end) as other,
        sum(case when transaction_type in ('consulting_expense','printing_expense','public_relations_expense') then amount else 0 end) as direct_lobbying_expenses,
        sum(case when transaction_type in ('indirect_expense') then amount else 0 end) as indirect_lobbying_expenses,
        sum(case when transaction_type <> 'compensation' then amount else 0 end) as total_expenses,
        sum(amount) as total_expenses_and_compensation
    from
        l2_transaction
    where transaction_type <> 'sub_lobbyist_payment'
    group by
        report_id,source_target_id,source_target_type,source_name
) x on x.report_id = l2.report_id and x.source_target_id = s.lobbyist_contract_id
left join (
    select
      t2.report_id,
      l2d.lobbyist_contract_id,
      sum(t2.amount) as sub_lobbyist_compensation
    from l2_transaction t2
             join l2 on l2.report_id=t2.report_id
             join lobbyist_contract l2sc on t2.source_target_type='lobbyist_contract' and t2.source_target_id = l2sc.lobbyist_contract_id
             join lobbyist_reporting_periods rps on rps.lobbyist_contract_id=l2sc.lobbyist_contract_id and l2.period_start = rps.period_start
             join lobbyist_reporting_periods rpd on rpd.lobbyist_firm_id=l2sc.contractor_id and l2.period_start=rpd.period_start
             join lobbyist_contract l2d on l2d.lobbyist_contract_id=rpd.lobbyist_contract_id and l2d.lobbyist_client_id=l2sc.lobbyist_client_id
    where t2.transaction_type='sub_lobbyist_payment'
      and l2d.contractor_id is null
    group by t2.report_id, l2d.lobbyist_contract_id
) st  on st.report_id = l2.report_id and x.source_target_type='lobbyist_contract' and x.source_target_id = st.lobbyist_contract_id
left join (
    select distinct on (report_id) *
    from l2_submission
    order by report_id, submitted_at
) z on z.report_id = l2.report_id;
