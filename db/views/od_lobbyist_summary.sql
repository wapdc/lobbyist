drop view if exists od_lobbyist_summary;
create or replace view od_lobbyist_summary as
select concat(lf.lobbyist_firm_id, '-', sums.reporting_year) as id,
       sums.reporting_year as year,
       lf.lobbyist_firm_id as filer_id,
       lf.name as lobbyist_name,
       lf.email as lobbyist_email,
       lf.phone as lobbyist_phone,
       case when lf.address_2 is null
                then lf.address_1
            else concat(lf.address_1, ' ', lf.address_2)
           end as firm_address,
       lf.city as firm_city,
       lf.state as firm_state,
       lf.postcode as firm_zip,
       lf.country as firm_country,
       lf.temp_phone,
       case when lf.temp_address_2 is null
                then lf.temp_address_1
            else concat(lf.temp_address_1, ' ', lf.temp_address_2)
           end as temp_address,
       lf.temp_city,
       lf.temp_state,
       lf.temp_postcode as temp_zip,
       lf.temp_country,
       sums.compensation as compensation,
       -sums.sub_lobbyist_compensation as sub_lobbyist_compensation,
       sums.compensation - sums.sub_lobbyist_compensation as net_compensation,
       sums.personal_expenses as personal_expenses,
       sums.entertainment as entertainment,
       sums.contributions as contributions,
       sums.pac_contributions as pac_contributions,
       sums.advertising as advertising,
       sums.political_ads as political_ads,
       sums.independent_expenditures_candidate as independent_expenditures_candidate,
       sums.independent_expenditures_ballot as independent_expenditures_ballot,
       sums.other as other,
       sums.direct_lobbying_expenses,
       sums.indirect_lobbying_expenses,
       sums.total_expenses as total_expenses,
       sums.net_total as net_total,
       sums.use_categories,
       ly.periods
from lobbyist_firm lf
      join
        (select lp.lobbyist_firm_id,
                extract(year from lp.period_start) reporting_year,
                json_agg(json_build_object(
                  'year', extract(year from lp.period_start),
                  'month', extract(month from lp.period_start),
                  'period', extract(year from lp.period_start) || '-' || lpad(extract(month from lp.period_start)::text,2,'0'::Text),
                  'exempt', lp.exempt
                ) order by lp.period_start) periods  from
            (select lc.lobbyist_firm_id, rp.period_start, bool_and(rp.exempt) exempt
            from lobbyist_contract lc
                    join lobbyist_reporting_periods rp on rp.lobbyist_contract_id=lc.lobbyist_contract_id
            group by lc.lobbyist_firm_id, rp.period_start) lp
        group by lp.lobbyist_firm_id, extract(year from lp.period_start)
        ) ly on ly.lobbyist_firm_id = lf.lobbyist_firm_id

       left join (
         select
             lobbyist_firm_id,
             extract(year from l2.period_start) as reporting_year,
             sum(case when transaction_type = 'compensation' then amount else 0 end) as compensation,
             sum(case when transaction_type = 'sub_lobbyist_payment' then amount else 0 end) as sub_lobbyist_compensation,
             sum(case when transaction_type = 'personal_expense' then amount else 0 end) as personal_expenses,
             sum(case when transaction_type in ('non_itemized_entertainment_expenses', 'itemized_entertainment_expense')  then amount else 0 end) as entertainment,
             sum(case when transaction_type = 'itemized_contribution' and source_target_type in ('legacy_committee', 'committee') then 0
                      when transaction_type in ('itemized_contribution', 'non_itemized_contributions','in_kind_expense') then amount else 0 end) as contributions,
             sum(case when transaction_type = 'itemized_contribution' and source_target_type = 'legacy_committee' then amount else 0 end) as pac_contributions,
             sum(case when transaction_type = 'advertising_expense' then amount else 0 end) as advertising,
             sum(case when transaction_type = 'ad_expense' then amount else 0 end) as political_ads,
             sum(case when transaction_type = 'independent_expenditures_candidate_expense' then amount else 0 end) as independent_expenditures_candidate,
             sum(case when transaction_type = 'independent_expenditures_ballot_expense' then amount else 0 end) as independent_expenditures_ballot,
             sum(case when transaction_type = 'other_expense' then amount else 0 end) as other,
             sum(case when transaction_type in ('consulting_expense','printing_expense','public_relations_expense') then amount else 0 end) as direct_lobbying_expenses,
             sum(case when transaction_type in ('indirect_expense') then amount else 0 end) as indirect_lobbying_expenses,
             --exclude the pac_contributions, and non_reimbursed_personal_expenses from total_expenses and net_total
             sum(case when transaction_type = 'itemized_contribution' and source_target_type in ('legacy_committee', 'committee') then 0
                      when transaction_type not in ('compensation', 'sub_lobbyist_payment', 'non_reimbursed_personal_expenses') then amount
                 else 0 end) as total_expenses,
             sum(case when transaction_type = 'itemized_contribution' and source_target_type in ('legacy_committee', 'committee') then 0
                      when transaction_type not in ('non_reimbursed_personal_expenses', 'sub_lobbyist_payment') then amount
                      when transaction_type = 'sub_lobbyist_payment' then -amount
                 else 0 end) as net_total,
             case when max(l2s.version) >= '2' and min(l2s.version) >= '2' then 'new'
                  when max(l2s.version) >= '2' then 'mixed'
                  else 'legacy' end use_categories
         from l2 left join l2_transaction t on l2.report_id = t.report_id
           left join l2_submission l2s on l2s.submission_id=l2.current_submission_id
         group by lobbyist_firm_id, extract(year from l2.period_start)
     ) sums on sums.reporting_year = ly.reporting_year and sums.lobbyist_firm_id = lf.lobbyist_firm_id;
