drop view if exists od_l2_reporting_history cascade;
create or replace view od_l2_reporting_history as
select case when l2s.superseded_id > 0 then 'L2 AMENDED' else 'L2' end as origin,
       l2s.submission_id as id,
       l2s2.submission_id as amends_report,
       l2s.superseded_id as amended_by_report,
       lf.name as filer_name,
       'Firm' as filer_type,
       lf.entity_id as entity_id,
       l2.lobbyist_firm_id as firm_id,
       null::int as client_id,
       cast(date_part('year', l2.period_start) as int) as year,
       open_data_date_format(l2s.submitted_at) as receipt_date,
       'Electronic' as filing_method,
       cast(l2.period_start as text) as report_from,
       cast(l2.period_end as text) as report_through,
       apollo_url() || '/lobbyist/public/-/#/public/l2-report/' || l2s.submission_id as url,
       cast(l2s.user_data as text) as report_data,
       cast(l2s.version as text) as version
from l2
         join l2_submission l2s on l2.report_id = l2s.report_id
         left join l2_submission l2s2 on l2s2.superseded_id = l2s.submission_id
         left join lobbyist_firm lf on l2.lobbyist_firm_id = lf.lobbyist_firm_id
where l2s.submitted_at >= (now()- INTERVAL '17 years')