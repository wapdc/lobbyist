drop view if exists od_lobbyist_contract_history cascade;
create or replace view od_lobbyist_contract_history as
select case when lcs.superseded_id > 0 then 'L1 - Lobbyist Contract AMENDED' else 'L1 - Lobbyist Contract' end as origin,
       lcs.submission_id as id,
       lcs2.submission_id as amends_report,
       lcs.superseded_id as amended_by_report,
       f.name as filer_name,
       'Firm' as filer_type,
       f.entity_id as entity_id,
       lc.lobbyist_firm_id as firm_id,
       lc.lobbyist_client_id as client_id,
       cast(date_part('year', lcs.submitted_at) as int) as year,
       open_data_date_format(lcs.submitted_at) as receipt_date,
       'Electronic' as filing_method,
       cast(lc.period_start as text) as report_from,
       cast(lc.period_end as text) as report_through,
       apollo_url() || '/lobbyist/public/-/#/public/l1-report/' || lcs.submission_id as url,
       cast(lcs.user_data as text) as report_data,
       cast(lcs.version as text) as version
from lobbyist_contract lc
         join lobbyist_contract_submission lcs on lc.lobbyist_contract_id = lcs.lobbyist_contract_id
         left join lobbyist_contract_submission lcs2 on lcs2.superseded_id = lcs.submission_id
         left join lobbyist_client lclient on lclient.lobbyist_client_id = lc.lobbyist_client_id
         left join lobbyist_firm f on lc.lobbyist_firm_id = f.lobbyist_firm_id
where lcs.submitted_at >= (now()- INTERVAL '17 years')