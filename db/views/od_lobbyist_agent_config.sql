-- note that the double cast below makes sure the data is stored efficiently in the table (that is, without cr/lf and white space)
with stage_specific
         as (select s.value as stage,
                    case when s.value = 'live' then true else false end              as is_enabled,
                    case when s.value = 'live' then 'bp5b-jrti' else 'XXXX-XXXX' end as dataset_id
             from wapdc_settings s
             where property = 'stage'),
     data
         as (
         select
             'lobbyist_agents',
             stage,
             is_enabled,
             cast(cast(concat(
                     $$
{
 "changes": [
   {
     "schema": "public",
     "table": "lobbyist_firm",
     "changes": [
       "insert",
       "update"
     ],
     "columns": [
       "lobbyist_firm_id"
     ],
     "event": "replaceLobbyistFirmData"
   },
    {
      "schema": "public",
      "table": "lobbyist_firm",
      "changes": [
        "delete"
      ],
      "columns": [
        "lobbyist_firm_id"
      ],
      "event": "deleteLobbyistFirmData"
    }
 ],
 "events": {
   "replaceLobbyistFirmData": {
     "type": "replace",
     "source": "select * from od_lobbyist_agent where filer_id=$1",
     "destination": "select id where filer_id=$1",
     "criteria": [
       "lobbyist_firm_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "deleteLobbyistFirmData": {
     "type": "delete",
     "source": "select id where filer_id=$1",
     "criteria": [
       "lobbyist_firm_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   }
 }
}
$$
                       ) as json) as text)
         from stage_specific
     )
insert into open_data_config
select * from data
on conflict(job_name,stage)
    do update set config=excluded.config, is_enabled=excluded.is_enabled;