drop view if exists od_lobbyist_client_history cascade;
create or replace view od_lobbyist_client_history as
select case when lcs.superseded_id > 0 then 'L1 - Lobbyist Client AMENDED' else 'L1 - Lobbyist Client' end as origin,
       lcs.submission_id as id,
       lcs2.submission_id as amends_report,
       lcs.superseded_id as amended_by_report,
       lc.name as filer_name,
       'Client' as filer_type,
       lc.entity_id as entity_id,
       null::int as firm_id,
       lc.lobbyist_client_id as client_id,
       cast(date_part('year', lcs.submitted_at) as int) as year,
       open_data_date_format(lcs.submitted_at) as receipt_date,
       'Electronic' as filing_method,
       null as report_from,
       null as report_through,
       '' as url,
       cast(lcs.user_data as text) as report_data,
       cast(lcs.version as text) as version
from lobbyist_client lc
         join lobbyist_client_submission lcs on lc.lobbyist_client_id = lcs.lobbyist_client_id
         left join lobbyist_client_submission lcs2 on lcs2.superseded_id = lcs.submission_id
where lcs.submitted_at >= (now()- INTERVAL '17 years')