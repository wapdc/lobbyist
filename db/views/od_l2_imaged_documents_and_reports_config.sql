-- note that the double cast below makes sure the data is stored efficiently in the table (that is, without cr/lf and white space)
with stage_specific
         as (select s.value as stage,
                    false as is_enabled,
                    case when s.value = 'live' then 'j78t-andi' else 'XXXX-XXXX' end as dataset_id
             from wapdc_settings s
             where property = 'stage'),
     data
         as (
         select
             'l2_imaged_documents_and_reports',
             stage,
             is_enabled,
             cast(cast(concat(
                     $$
{
 "changes": [
   {
     "schema": "public",
     "table": "l2_submission",
     "changes": [
       "insert",
       "update"
     ],
     "columns": [
       "report_id"
     ],
     "event": "updateL2Document"
   },
   {
     "schema": "public",
     "table": "l2_submission",
     "changes": [
       "delete"
     ],
     "columns": [
       "submission_id"
     ],
     "event": "deleteL2ReportData"
   }
 ],
 "events": {
   "updateL2Document": {
     "type": "upsert",
     "source": "select * from od_l2_imaged_documents_and_reports where report_number=$1",
     "criteria": [
       "report_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "deleteReportData": {
     "type": "delete",
     "source": "select id where id=concat($1, '.lobbyist')",
     "criteria": [
       "submission_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   }
 }
}
$$
                       ) as jsonb) as text)
         from stage_specific
     )
insert into open_data_config
select * from data
on conflict(job_name,stage)
    do update set config=excluded.config, is_enabled=excluded.is_enabled;