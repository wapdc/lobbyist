-- note that the double cast below makes sure the data is stored efficiently in the table (that is, without cr/lf and white space)
with stage_specific
         as (select s.value as stage,
                    case when s.value = 'live' then true else false end              as is_enabled,
                    case when s.value = 'live' then 'nuwx-ay5h' else 'XXXX-XXXX' end as dataset_id
             from wapdc_settings s
             where property = 'stage'),
     data
         as (
         select
             'lobbyist_reporting_history',
             stage,
             is_enabled,
             cast(cast(concat(
                     $$
{
 "changes": [
   {
     "schema": "public",
     "table": "l2_submission",
     "changes": [
       "insert",
       "update"
     ],
     "columns": [
       "submission_id"
     ],
     "event": "updateL2ReportData"
   },
   {
     "schema": "public",
     "table": "l2_submission",
     "changes": [
       "delete"
     ],
     "columns": [
       "submission_id"
     ],
     "event": "deleteL2ReportData"
   },
   {
     "schema": "public",
     "table": "l3_submission",
     "changes": [
       "insert",
       "update"
     ],
     "columns": [
       "submission_id"
     ],
     "event": "updateL3ReportData"
   },
   {
     "schema": "public",
     "table": "l3_submission",
     "changes": [
       "delete"
     ],
     "columns": [
       "submission_id"
     ],
     "event": "deleteL3ReportData"
   },
   {
     "schema": "public",
     "table": "lobbyist_client_submission",
     "changes": [
       "insert",
       "update"
     ],
     "columns": [
       "submission_id"
     ],
     "event": "updateLobbyistClientData"
   },
   {
     "schema": "public",
     "table": "lobbyist_client",
     "changes": [
       "delete"
     ],
     "columns": [
       "lobbyist_client_id"
     ],
     "event": "deleteLobbyistClientData"
   },
   {
     "schema": "public",
     "table": "lobbyist_firm_submission",
     "changes": [
       "insert",
       "update"
     ],
     "columns": [
       "submission_id"
     ],
     "event": "updateLobbyistFirmData"
   },
   {
     "schema": "public",
     "table": "lobbyist_firm",
     "changes": [
       "delete"
     ],
     "columns": [
       "lobbyist_firm_id"
     ],
     "event": "deleteLobbyistFirmData"
   },
   {
     "schema": "public",
     "table": "lobbyist_contract_submission",
     "changes": [
       "insert",
       "update"
     ],
     "columns": [
       "submission_id"
     ],
     "event": "updateLobbyistContractData"
   },
   {
     "schema": "public",
     "table": "lobbyist_contract_submission",
     "changes": [
       "delete"
     ],
     "columns": [
       "submission_id"
     ],
     "event": "deleteLobbyistContractData"
   }
 ],
 "events": {
   "updateL2ReportData": {
     "type": "upsert",
     "source": "select * from od_lobbyist_reporting_history where id=$1",
     "criteria": [
       "submission_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "deleteL2ReportData": {
     "type": "delete",
     "source": "select id where id=$1",
     "criteria": [
       "submission_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "updateL3ReportData": {
     "type": "upsert",
     "source": "select * from od_lobbyist_reporting_history where id=$1",
     "criteria": [
       "submission_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "deleteL3ReportData": {
     "type": "delete",
     "source": "select id where id=$1",
     "criteria": [
       "submission_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "updateLobbyistClientData": {
     "type": "upsert",
     "source": "select * from od_lobbyist_reporting_history where id=$1",
     "criteria": [
       "submission_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "deleteLobbyistClientData": {
     "type": "delete",
     "source": "select id where client_id=$1",
     "criteria": [
       "lobbyist_client_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "updateLobbyistFirmData": {
     "type": "upsert",
     "source": "select * from od_lobbyist_reporting_history where id=$1",
     "criteria": [
       "submission_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "deleteLobbyistFirmData": {
     "type": "delete",
     "source": "select id where firm_id=$1",
     "criteria": [
       "lobbyist_firm_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "updateLobbyistContractData": {
     "type": "upsert",
     "source": "select * from od_lobbyist_reporting_history where id=$1",
     "criteria": [
       "submission_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   },
   "deleteLobbyistContractData": {
     "type": "delete",
     "source": "select id where id=$1",
     "criteria": [
       "submission_id"
     ],
     "dataset_id": "$$,dataset_id,$$"
   }
 }
}
$$
                       ) as json) as text)
         from stage_specific
     )
insert into open_data_config
select * from data
on conflict(job_name,stage)
    do update set config=excluded.config, is_enabled=excluded.is_enabled;