-- drop view if exists od_l3_reporting_history cascade;
create or replace view od_l3_reporting_history as
select case when s.superseded_id > 0 then 'L3 AMENDED' else 'L3' end as origin,
    s.submission_id as id,
    l3s.submission_id as amends_report,
    s.superseded_id as amended_by_report,
    rc.name as filer_name,
    'Client' as filer_type,
    rc.entity_id as entity_id,
    null::int as firm_id,
    r.lobbyist_client_id as client_id,
    r.report_year as year,
    open_data_date_format(s.submitted_at) as receipt_date,
    'Electronic' as filing_method,
    concat(cast(r.report_year as text), '-01-01') as report_from,
    concat(cast(r.report_year as text), '-12-31') as report_through,
    apollo_url() || '/lobbyist/public/-/#/public/l3-report/' || s.submission_id as url,
    cast(s.user_data as text) as report_data,
    cast(s.version as text) as version
from l3_submission s
     join l3 r on s.report_id = r.report_id
     left join lobbyist_client rc on rc.lobbyist_client_id = r.lobbyist_client_id
     left join l3_submission l3s on l3s.superseded_id = s.submission_id
where s.submitted_at >= (now()- INTERVAL '17 years')
