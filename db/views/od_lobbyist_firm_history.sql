drop view if exists od_lobbyist_firm_history cascade;
create or replace view od_lobbyist_firm_history as
select case when lfs.superseded_id > 0 then 'L1 - Lobbyist Firm AMENDED' else 'L1 - Lobbyist Firm' end as origin,
       lfs.submission_id as id,
       lfs2.submission_id as amends_report,
       lfs.superseded_id as amended_by_report,
       lf.name as filer_name,
       'Firm' as filer_type,
       lf.entity_id as entity_id,
       lf.lobbyist_firm_id as firm_id,
       null::int as client_id,
       cast(date_part('year', lfs.submitted_at) as int) as year,
       open_data_date_format(lfs.submitted_at) as receipt_date,
       'Electronic' as filing_method,
       null as report_from,
       null as report_through,
       '' as url,
       cast(lfs.user_data as text) as report_data,
       cast(lfs.version as text) as version
from lobbyist_firm lf
         join lobbyist_firm_submission lfs on lf.lobbyist_firm_id = lfs.lobbyist_firm_id
         left join lobbyist_firm_submission lfs2 on lfs2.superseded_id = lfs.submission_id
where lfs.submitted_at >= (now()- INTERVAL '17 years')