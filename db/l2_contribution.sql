CREATE TABLE IF NOT EXISTS l2_contribution
(
    l2_transaction_id int references l2_transaction(l2_transaction_id) on delete cascade,
    recipient_id int,
    recipient_target text
);