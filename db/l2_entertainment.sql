CREATE TABLE IF NOT EXISTS l2_entertainment
(
    l2_transaction_id int references l2_transaction(l2_transaction_id) on delete cascade,
    occasion_type text,
    place text,
    participants text
);