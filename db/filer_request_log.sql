CREATE TABLE IF NOT EXISTS filer_request_log
(
    id serial primary key,
    user_name text,
    target_type text,
    target_id int,
    updated_at timestamptz not null default now()
);