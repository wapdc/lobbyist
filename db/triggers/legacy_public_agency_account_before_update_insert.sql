CREATE OR REPLACE FUNCTION legacy_public_agency_account_before_update_insert()
    RETURNS TRIGGER AS
    $$
        BEGIN

            NEW.password := (select public.crypt(NEW.password, gen_salt('bf')));

            RETURN NEW;
        END;

    $$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS legacy_public_agency_account_before_update_insert_trigger ON legacy_public_agency_account;

CREATE TRIGGER legacy_public_agency_account_before_update_insert_trigger
    BEFORE INSERT OR UPDATE OF password ON legacy_public_agency_account
    FOR EACH ROW EXECUTE FUNCTION legacy_public_agency_account_before_update_insert();