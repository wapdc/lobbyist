create table if not exists l3_transaction
(
    l3_transaction_id serial primary key,
    report_id int references l3(report_id) on delete cascade,
    name text,
    title text,
    amount numeric,
    transaction_type text,
    date date,
    description text
);