# Election expenses

> - Source of funds is a list of your Employer(s) and Self.
> - Amount cannot be a negative value.
> - Totals are calculated automatically.
> - Total count (e.g. 1 of 2) is a count of the number of Entertainment expenses added.
> - Non-itemized expenses use the same form as Personal expenses. 
> - Changing a **Yes** to **No** will discard all information for that section.

## Recording a issue

When the results of testing uncovers problems record the issue on the spreadsheet. Open the spreadsheet, start with your initials, the test number, finally write the out problem describing the problem.

# Questionnaire 

- 5:01: Verify that clicking on **None of these apply** changes all the selections to **No**.
- 5:02: Choose **Yes** and enter Verify you are able to 
- 5:03: Verify that the step above removed all the information you entered by visiting each of the section.

##  Small Contributions

This section has fields already tested earlier. 

- 5.04: Verify that you cannot add multiple entries when a field such as Amount or Source of funds are left empty.
- 5:05: Verify you cannot enter an amount exceeding $100.
- 5.06: Validate that Totals are accurately updated when changing Amounts for one or more sources.

## Monetary Contributions

- Select a **Date** not in the same month and try to add another expense. Can you?
- Does the dropdown in the sources field have the correct values to pick from?
- Pick a **Candidate**, does the dropdown have the correct values to pick from?
- Add another for a PAC, does the dropdown have the correct values? Pick one. 
- Navigate to different records or add some. Is anything missing or broken?
- Choose a PAC contribution, and change, pick a Committee. What did it save?

##  Import

In this section you will test the import functionality. Start by downloading the sample CSV file, add the appropriate information to the correct column on the spreadsheet.

-5.07: Verify that when you click on the **Import** button a import dialog to appears. 
-5.08: Verify that clicking on the link **download the sample CSV** downloads a sample file to your computer. 

Prepare the spreadsheet by adding the required data paying attention to dates and names. You will notice there are different sources and recipient types. import a few with these different types.

- 5.09: Return to the import dialog. Verify that you can select a source and Import/upload the prepared CSV file.
- 5.10: Verify that name matching works and when the wrong name is chosen it is left blank.
- 5.11: Test navigation between different records and verify that value changes are properly saved from the import.
- 5.12: Check data accuracy by comparing source names and amounts for each imported record.

## In-kind Contributions

- 5.13: Verify that you cannot add multiple entries when a field such as Candidate or PAC are left empty.
- 5.14: Verify that when choosing Candidate the dropdown contain a list of candidates only
- 5.15: Verify that when choosing PAC the dropdown contain a list of PACs only.
- 5.16: Verify that when you cannot add another in-kind contributions when the **Yes or No** answer to advertising is left empty.
- 5.17: Verify that when you answer **Yes** to advertising **Vendor** name cannot be empty.

Fill out the form in each of the different ways possible and try to submit testing leaving fields empty.

## Political Ads

Fill out the form in each of the different ways possible and try to submit testing leaving fields empty.

## Contributing PACs

Fill out the form in each of the different ways possible and try to submit testing leaving fields empty.


> Complete the test by filling out the form and navigating away from the form to **Election expenses**.
