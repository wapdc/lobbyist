# Getting setup for testing


> Testers - Do not use your staff account for testing or viewing reports.
> 
> Reason: Your account has elevated privileges for which allow you to get 
around data that isn't available for the public to see.

## Creating a lobbyist account in Accesshub
1. On Apollo Demo, log in with your test account via a different browser or incognito window.
   - https://demo-apollo-d8.pantheonsite.io/user/login
   - You should have received an email regarding your test account. If you have not, or are having difficulties please reach out to the IT team.
2. Navigate to the Lobbying tab in Apollo.
   - First Time: 
     - Select New Profile on the Single sign-on activation.
     - Fill out the form with your information for creation of drupal account in Accesshub.
     - You'll be redirected to Apollo Lobbyist Dashboard
   - Subsequent Times:
     - Just login and navigate to lobbyist tab.

## Creating a lobbyist firm (2 or more preferred)
1. On the lobbyist dashboard under lobbyists I file for select '+ Register'.
2. Skip to step 3 and check box then Submit and fill out the form describing the firm.
   - I recommend only the required fields, and/or answering 'no' as much as possible.
3. You'll be redirected to the lobbyist firm dashboard, return to lobbyist main dashboard and continue.
4. Repeat the steps to make at least one other firm to use as a subcontracted lobbyist.

## Creating a lobbyist client (3 or more preferred)
1. On the lobbyist dashboard under Clients/Employers I file for select '+ Register'.
2. Skip to step 3 and check box then Submit and fill out the form describing the firm.
   - I recommend only the required fields, and/or answering 'no' as much as possible.
3. You'll be redirected to the lobbyist firm dashboard, return to lobbyist main dashboard and continue.
4. Repeat the steps to make at least two other clients to use as a multiple sources while filling out the L2.

## Creating an employment contract (FER)
1. On the lobbyist dashboard select the firm you want to create a contract for.
2. Under client/employment contracts select '+ Employment/Contract Registration'.
3. You must choose whether you are registering the subcontracted lobbyist or the lobbyist hired directly by the client.
4. Fill out the form describing the contract desired for the firm and submit to PDC.
   - I recommend only the required fields, and/or answering 'no' as much as possible.
   - Take note of the months selected so that you can alternate filing periods with a different client to test source propagation while filling out the L2.
5. Following submission you will be viewing the contract you just filled out. Select the home button at the top of the page to be redirected to the lobbyist dashboard in Apollo.
6. Special note:
   - You will need to certify the contracts for direct client relationships before setting up a subcontracted lobbyist because the system needs to know about that contractors employer/client.

## Certify an employment contract
1. On the lobbyist dashboard select the client, or firm if you created a subcontracted lobbyist, that is employing the lobbyist for the contract just submitted.
2. On the firm or client dashboard under contracts to certified, select the contract you just created.
3. Certify and submit the form and you'll be redirected back to the firm or client dashboard you were on prior.

## Filing L2 reports
> After setting up all the contracts you will be ready to file reports. 

1. On the lobbyist dashboard, navigate to the firm you are wanting to file for.
2. On the lobbyist firm dashboard select the reporting period you would like to create an L2 for.
   - Special note:
     - Start the testing with a past reporting period as we want to be able to test the copy forward functionality later in the test plan.
