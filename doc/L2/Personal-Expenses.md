# Personal Expenses

## Notable personal expense issues

- 3.01: Unable to add a negative value for personal expense amount.
- 3.02: Source of funds dropdown includes all expected options (Self and Employer(s)).
- 3.03: Total count of personal expenses updating correctly when adding new rows.
- 3.04: Use the tab key and tab to the **Delete button** icon. Pressing the return key on your keyboard and remove the personal expense entry.
- 3.05: Non-reimbursable personal expenses field doesn't accept negative values.
- 3.06: Totals calculating automatically when entering or modifying expense amounts.
- 3.07: Confirm the process for Adding Personal Expense data and its effectiveness using the tab and Enter key to delete a record
- 3.08: Confirm the process for Deleting Personal Expense data and its effectiveness using the tab and Enter key to delete a record.
- 3.09: Test navigation between different records and verify that value changes are properly saved.

## Reviewing

When reviewing consider the following:

- 3.10: Verify the accuracy of Source of Funds: Ensure that all listed sources of funds correspond to the Sources(s).
- 3.11: Check the validity of Amounts: Verify that all entered amounts are positive values and accurately reflect the amount received.
- 3.12: Calculation of Totals: Double-check that the automatically calculated totals are correct and match the sum of individual entries.
- 3.13: Verify the completeness of Information: Make sure all the columns in the preview are filled out for each entry.