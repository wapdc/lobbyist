# Compensation

## Notable Compensation Issues

- 1.01: Verify that you cannot add multiple compensation entries when a field such as Amount or Source of funds are left empty.
- 1.02: Check if negative amounts can be entered and how they are handled.
- 1.03: Ensure the dropdown in the sources field contains the correct values for Employer(s).
- 1.04: Test adding more than 3 Compensations and observe any issues when deleting entries, especially the first, middle and last ones.
- 1.05: There are multiple ways of discarding **All of Compensation**. Confirm the process for discarding all of the Compensation data and its effectiveness by choosing "No" to the Compensation question about do you have any or Delete and start over button.
- 1.06: Validate that Totals are accurately updated when changing Amounts for one or more sources.
- 1.07: Test navigation between different records and verify that value changes are properly saved.
- 1.08: Check data accuracy by comparing source names and amounts for each record.
- 1.09: Ensure the Preview function accurately displays Compensation information accurately.
