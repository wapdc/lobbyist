> - Source of funds is a list of your Employer(s).
> - Amount cannot be a negative value.
> - Totals are calculated automatically.
> - Total count (e.g. 1 of 2) is a count of the number of Compensations added.
> - Changing a **Yes** to **No** will discard all the information for that section.

## Recording a issue

When the results of testing uncovers problems record the issue on the spreadsheet. Open the spreadsheet, start with your initials, the test number, finally write the out problem describing the problem.

- 7:01: Verify that you can Add at least two **Indirect expenses**. 
- 7:02: Fill out the form in each of the different ways possible and try to submit by leaving fields empty. All fields are required.

All of the fields in this section have already been tested. Completely fill out the form in each of the different ways possible.

## Reviewing

When reviewing consider the following:

- 7.03: Verify the accuracy of Source of Funds: Ensure that all listed sources of funds correspond to the Sources(s).
- 7.04: Check the validity of Amounts: Verify that all entered amounts are positive values and accurately reflect the amount received.
- 7.05: Calculation of Totals: Double-check that the automatically calculated totals are correct and match the sum of individual entries.
- 7.06: Verify the completeness of Information: Make sure all the columns in the preview are filled out for each entry.