> Noteworthy: A number of fields are capable of more than one entry. Use the enter key to store the value.
> - Source of funds is a list of your Employer(s).
> - Amount cannot be a negative value.
> - Totals are calculated automatically.
> - Total count (e.g. 1 of 2) is a count of the number of Compensations added.
> - Changing a **Yes** to **No** will discard all the information for that section.

- 8.01: Verify that the **Subject matter** dropdown lists all the subject matters correctly and that none are missing.
- 8.02: Verify that you cannot leave the **Source of funds** empty.
- 8.03: Verify that you can pick more than one **Source of funds** by choosing more than one.
- 8:04: Verify that you can Add more than one subject area record with different amounts, multiple subject matters, Issues, bills/initiatives and legislative agencies.
- 8:05: Use the **Tab key** to navigate to the **Issue** field. Verify that you can add more than one.
- 8:06: Use the **Tab key** to navigate to the **Legislative agency** field. Verify that you can add more than one.
- 8:07: Use the **Tab key** to navigate to the **Bill/Init numbers** field. Verify that you can add more than one.
- 8:08: Validate that Totals are accurately updated when changing Amounts for one or more sources.
- 8.09: Test navigation between different records and verify that value changes are properly saving.
- 8.10: Check data accuracy by comparing source names for each record.
- 8.11: Verify adding or deleting a **Subject matter** saves the changes.
- 8.12: Verify that adding or removing items also update the table.
- 8:13: Completely fill out the form in each of the different ways possible and then go to the next section.


---
## Notes: Instructions for entering multiple values

- Subject-matter: A field capable of multiple entries from a prepopulated list.
  - Issue: Multi-value input field that allows freeform text.
    - Different values are separated via the <enter> key
      - i.e. financial, environment, education
  - Bill/initiative numbers: Multi-value input field that expects specific values indicating initiative, house bills, senate bills, etc.
    - Different values are separated via the <enter> key
      - i.e. SB-5123, HB-1323, I-1740
  - Legislative Agency: Multi-value input field expecting a list of specific agencies
      - Different values are separated via the <enter> key
          - i.e. Department of Licensing, Secretary of State

- Fill out one or more records with a variety of different attributes in each of the noteworthy sections.

## Review report

When reviewing consider the following:

- 1.10: Verify the accuracy of Source of Funds: Ensure that all listed sources of funds correspond to the Sources(s).
- 1.11: Check the validity of Amounts: Verify that all entered amounts are positive values and accurately reflect the amount received.
- 1.12: Calculation of Totals: Double-check that the automatically calculated totals are correct and match the sum of individual entries.
- 1.13: Verify the completeness of Information: Make sure all the columns in the preview are filled out for each entry.
- 1.14:Validate that all the data being entered shows up completely and correctly as data entered.
- 1.15:This last bit can be employed in tandem with the previous steps if it is easier to compare the preview with data entry as each section is being populated.
- 1.16:After testing of this section is finished, submit the report.
