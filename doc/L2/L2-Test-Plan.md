# Lobbyist Monthly Reporting (L2) Test Plan

---

## Initial Setup

### [Getting setup for testing](Testing-Setup.md)

---

## Recording an issue

When the results of testing uncover problems record the issue on the spreadsheet. Open the spreadsheet, start with your initials, the test number, finally write the out problem describing the problem.

---

## Cross-browser testing

> Different browsers have different capabilities and drawbacks, it is important to open and test the application in a variety of 
> available browsers such as: Google Chrome, Microsoft Edge, Mozilla FireFox, and Apple Safari as well as in smaller windows or formats.

- Main points of focus:
  - Web page rendering
    - Confirm that the form loads appropriately and consistently across each browser paying close attention to any inconsistencies.
      - i.e. no gray or white screens, and partially loaded forms.
    - Examples of what to verify and look out for:
      - Verify that the drop-down fields propagate the correct data, and function as desired.
      - Verify the wizard structure works and looks consistent.
      - Verify that the pop-up dialog boxes load in the proper location and are functional.
      - Verify that each component inside the form's rules apply appropriately.
        - i.e. date picker allows dates within a reporting period, positive monetary amounts, required fields, etc.
  - Import functionality in Monetary contribution
    - Verify that the sample file with the desired format is downloadable.
    - After the import, verify the data brought in from each works as advertised.
      - Try a couple of times with different sources.
  - Accessibility
    - Verify that tabular navigation works as desired paying close attention to tab order.
      - i.e. if you push tab 3 times it should go to the 3rd input not from 1, 5, 2 or some other crazy order.
      
---

## Important baselines to test cases

- Dates: 
  - All validation on the dates should be within the reporting period in question. 
    - i.e. September 2024 should only accept dates from September 1, 2024 - September 30, 2024.

- Amounts: 
  - All validation in the application requires a greater than 0 amount unless otherwise requested.

- Source of funds: 
  - Populated with various different field depending on the transaction types as well as valid contracts contained within reporting periods. 
    - i.e. Lobbyist A has a contract with Client B for February, and April 2024. Therefore, you expect Client B to be available as a source in February, and April only in 2024.
  - Note: **When filing as a subcontracted lobbyist**, the typical client selection for source is the contract for the client.
    - i.e. Subcontracted lobbyist A is hired by Lobbyist firm B on behalf of Lobbyist client C is expressed in the menu as: 
      Lobbyist client C (Lobbyist Firm B).

- Deleting rows of data: 
  - These should have confirmation dialog if they contain data and should warn you that you are about to remove data.

---

# Starting the test

## Lobbyist Dashboard

## Firm Dashboard

L2 Monthly reports are displayed in a table with a number of columns. Columns important to testing are:

**Report Id**: Smashing the report id opens the preview L2 screen. It is also used for amending the report, viewing the reports history and printing.

**Action**: Things you can do with a report.
- **Start** - You can start a new report. 
- **Edit** - You can edit a report you started which is a **Draft**.

**Status**:
- **Overdue**: This report is late.
- **Started**: This report is a Draft
- **Filed**: This report is Filed and can be amended, printed, or history viewed.
- **Amending**: This report is being amended. (A date filed timestamp also is shown)

### Start a Monthly Report (L2)

From the Firm Dashboard SMASH **Start** to start a DRAFT Monthly L2.

### Report Sections

Navigate through each section of the report filling out all required fields. Each section is listed below:

 - 1.0 [Compensation](Compensation.md)
 - 2.0 [Subcontractor payments](Subcontractor-payments.md)
 - 3.0 [Personal Expenses](Personal-Expenses.md)
 - 4.0 [Entertainment Expenses](Entertainment-expenses.md)
 - 5.0 [Election Expenses](Election-expenses.md)
 - 6.0 [Direct Lobbying](Direct-lobbying.md)
 - 7.0 [Indirect/Grass Roots Lobbying](Indirect-grass-roots-lobbying.md)
 - 8.0 [Lobbying Subject Areas](Lobbying-subject-areas.md)

---

- Once all sections are complete, review the entire report for accuracy.
- Submit the report when you're satisfied with its contents.
- Review the submitted report (For use with testing of amendment)

## Make a draft (saved) report

- 9.01: Verify that you can open a Saved report. From the Firm Dashboard, locate the saved report and click on **Edit** button to open.
- 9.02: Ensure that all previously entered data is accurately displayed when opening a saved report.
- 9.03: Check that you can continue editing the saved report from where you left off.
- 9.04: Verify that saving changes in a reopened report works correctly.
- 9.05: Test the "Finish Later" functionality again to ensure you can re-save the report after making changes.

## Submit and preview draft report

- 10.01: In the draft, verify the accuracy of this section to ensure that all listed sources of funds, and other fields correspond to what you've.
- 10.02: Check the validity of Amounts: Verify that all entered amounts are positive values and accurately reflect the amount received.
- 10.03: Calculation of Totals: Double-check that the automatically calculated totals are correct and match the sum of individual entries.
- 10.04: Verify the completeness of Information: Make sure all the columns in the preview are filled out for each entry.
- 10.05: Verify the proper display of Multiple Entries: If there are multiple compensation entries, confirm that they are all correctly recorded and displayed.
- 10.06: Validate that all the data being entered shows up completely and correctly as entered.
- 10.07: Verify that you can submit and certify the report.

## Verify Report Submission

- 11.01: Check that the report status changes to "Filed" from "Start".
- 11.02: From the Firm Dashboard try to access the **Submitted** report
- 11.03: Ensure it's in read-only mode and the **Amend** button is visible.

## Copy forward

- Fields with capability of being brought forward from previous reports:
  - Compensation
  - Personal expenses
  - Sub-Lobbyist compensation

### Tasks

- 12.01: Fill out a report, or use a report that with values in compensation, personal expenses, and subcontractor payments in both the legacy and modern applications.
  - Notable Subjects:
    - That you must have a sub-lobbyist contracted to have the subcontractor payments category visible.
    - In the legacy system, subcontractor payments are inside the 'Other' tab.
- 12.02: Create a report later than the previous reports and copy all three options from the prior month.
- 12.03: Verify that the transactions are in the report.

## Amend the report and submit again.

- 13.01: Open the submitted report and click on the "Amend" button to initiate the amendment process.
- 13.02: Verify you are able to open and view the report submission history.
- 13.03: Check that the submission history accurately records the edit history.
- 13.04: Confirm that you can print the report.
- 13.05: Verify that you can open a Submitted report. From the Firm Dashboard click on the report number of the submitted report to open it.
- 13.06: Verify that a **warning appears** when trying to **Amend** a report that has not been submitted.

## Filing and Amending a Legacy Report

> IT will change your permissions to allow you to file a legacy report to fill one out to test legacy amendments.

- There have been structural changes to several categories on the report and the behavior will differ slightly based on 
  whether the legacy report being amended contains any of these categories.

### Unchanged categories: 

- Personal expenses
- Compensation
- Entertainment expenses

### Visual changes: Lobbying

- Lobbying has an alternate visual display for issue or bill number based on if there is data within report.
  - The modern report separates the collection of these values; whereas the legacy report does not. 
    We have not transformed this data and will preserve the mixed reported value.

### Restructured categories: Contributions, Advertising, and Other

- The new structure is depicted as: 
  - Subcontractor payments
    - Previously reported as Other under subcontracted lobbyist expenses
  - Indirect/Grass roots lobbying
    - Previously reported as Other under Other expenses and has now been split apart.
  - Election Expenses
    - Small Contributions
      - Previously reported as non-itemized contributions
    - Monetary Contributions
      - Previously reported as itemized-contributions, Client PAC contributions, Other contributions.
    - In-kind contributions
    - Political Advertising
      - Previously as advertising under Political ads, public relations, polling, telemarketing, etc. and has now been split apart.
    - Contributing PACs
      - Previously reported as Other PAC Contributions
  - Direct Lobbying
    - Printing and Literature
      - Previously under advertising in Advertising, Printing, Literature
    - Public relations and polling
      - Previously as advertising under Political ads, public relations, polling, telemarketing, etc. and has now been split apart.
    - Consultants and expert witnesses
      - Previously reported as Other under Other expenses and has now been split apart.
    - Other direct lobbying expenses
      - New definition and will contain other expenses that do not fit any other category.

### Tasks

- 14.01: Create a legacy report containing all or some of the categories that have been unchanged: Personal expenses, Compensation, and entertainment expenses.
- 14.02: Open the report to view and amend in the new system via the account that is modern L2 capable.
  - We expect there to not be a warning and for all the data to display and be editable as usual.
- 14.03: Check the Lobbying subject areas tab while in the draft document and confirm that there are separate input values for issues and bill numbers.
- 14.04: Create a legacy report containing lobbying activities with issue or bill numbers filled in.
  - We expect there to not be any warning messages but for the issue and bill number fields to be combined preserving the old structure.
- 14.05: Create a legacy report containing any of the restructured data categories.
  - We expect the values to not populate but instead to throw a warning saying the data is in an old format
    requiring the filer to fill out the form again with new values.


## Confirm Employer Yearly Filing (L3) totals

- 15.01: After filling out one or more L2 reports start an L3 report for the client related to the compensation and expenses filed by the lobbyists.
- 15.02: Verify that the totals generated for each lobbyist and subcontracted lobbyist are correct for compensation and expenses.
- 15.03: It is not necessary to file an L3 report for these tests; but just to confirm the totals are being brought in.
