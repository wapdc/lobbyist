# Direct lobbying

> - Source of funds is a list of your Employer(s) and Self.
> - Amount cannot be a negative value.
> - Totals are calculated automatically.
> - Total count (e.g. 1 of 2) is a count of the number of Entertainment expenses added.
> - Non-itemized expenses use the same form as Personal expenses. 
> - Changing a **Yes** to **No** will discard all information for that section.

## Printing and Literature
Source of funds (Self, Employer)

All of the fields in this section have already been tested. Completely fill out the form in each of the different ways possible.

## Public Relations and polling
Source of funds (Self, Employer)

All of the fields in this section have already been tested. Completely fill out the form in each of the different ways possible.

## Consultants and expert witnesses
Source of funds (Self, Employer)

All of the fields in this section have already been tested except the following fields. 

- 6.01: Verify that you cannot add multiple entries when a field such as Name is left empty.
- 6.02: Verify that you cannot add multiple entries when a field such as Address is left empty.
- 6.03: Verify that you cannot add multiple entries when a field such as City is left empty.
- 6.04: Verify that you cannot add multiple entries when a field such as State is left empty.
- 6.05: Verify that you cannot add multiple entries when a field such as Postcode is left empty.

Finally, completely fill out the form in each of the different ways possible.

## Other direct lobbying expenses
Source of funds (Self, Employer)

- Completely fill out the form in each of the different ways possible and then go to the next section.

## Reviewing

When reviewing consider the following:

- 6.06: Verify the accuracy of Source of Funds: Ensure that all listed sources of funds correspond to the Sources(s).
- 6.07: Check the validity of Amounts: Verify that all entered amounts are positive values and accurately reflect the amount received.
- 6.08: Calculation of Totals: Double-check that the automatically calculated totals are correct and match the sum of individual entries.
- 6.09: Verify the completeness of Information: Make sure all the columns in the preview are filled out for each entry.