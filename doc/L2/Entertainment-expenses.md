# Entertainment expenses

> - Source of funds is a list of your Employer(s).
> - Amount cannot be a negative value.
> - Totals are calculated automatically.
> - Total count (e.g. 1 of 2) is a count of the number of Entertainment expenses added.
> - Non-itemized expenses use the same form as Personal expenses.
> - Changing a **Yes** to **No** will discard all the information for that section.

## Using the keyboard

- Tab (to navigate to fields in a specific order)
- Enter (to select the text in focus)
- Arrow up & down (to navigate to sub-items in a list)

## Recording a issue

When the results of testing uncover problems record the issue on the spreadsheet. Open the spreadsheet, start with your initials, the test number, finally write the out problem describing the problem.

## Non-Itemized
> Notes: Non-itemized expenses use the same form as Personal expenses in this test use the tab and enter keys to navigate through the fields.

### Notable non-itemized entertainment expenses

- 4.01: Verify that you can select a name from the **Source of funds** using the keyboard to tab, arrow keys to navigate the selection, enter selects the source.
- 4.02: Verify the tab order is correct and what you expect to happen when you tab.
- 4.03: Verify that you can file multiple Non-itemized entertainment expenses using the keyboard.
- 4.04: Complete the test by filling out the form and navigating away from the form to **Itemized**.

## Itemized

### Notable Itemized entertainment expenses

- 4.05: Verify that you can select a name from the **Source of funds** using the keyboard to tab, arrow keys to navigate the selection, enter selects the source.
- 4.06: Verify the tab order is correct and what you expect to happen when you tab.
- 4.07: Verify that you can file multiple Itemized entertainment expenses using the keyboard.
- 4.08: Test adding a date not within the filing period to make sure it is rejected.
- 4.09: Confirm that the **Totals** are correctly updating when deleting and adding rows.
- 4.10: Test navigation between different records and verify that value changes are properly saved.- 4.09: Complete the test by filling out the form and navigating away from the form to **Election expenses**.
- 4.11: Verify that leaving a field empty prevents adding new records or navigating to a different section.
- 4.12: Ensure the multiple entries are possible in the **Participant listing**.
  - Grab an Excel, Word, or csv file and copy the contents and paste it into the field.
  - Manually add a few participants before and/or after the pasting of outside data.
  - Verify that each method works and the formatting is consistent for how you copied it.
- 4.13: Complete the test by filling out the form and navigating away from the form to **Election expenses**.

## Reviewing

When reviewing consider the following:

- 4.14: Verify the accuracy of Source of Funds: Ensure that all listed sources of funds correspond to the Sources(s).
- 4.15: Check the validity of Amounts: Verify that all entered amounts are positive values and accurately reflect the amount received.
- 4.16: Calculation of Totals: Double-check that the automatically calculated totals are correct and match the sum of individual entries.
- 4.17: Verify the completeness of Information: Make sure all the columns in the preview are filled out for each entry.