# Subcontractor payments

> This section is only visible for the reporting periods relevant for a firm employing a subcontractor.
> Inversely, this should not be visible for the subcontractor.


## Tasks
- 2.01: Take note of the months the subcontracted lobbyist are contracted for.
- 2.02: Select one or more reporting periods that are within the contracted months for that lobbyist and verify the ability to fill out subcontractor payments.
- 2.03: Select one or more reporting periods that are not contracted months for that lobbyist and verify the inability to fill out subcontractor payments.
- 2.04: Fill out one or more subcontractor payments and preview the report.
- 2.05: Verify that the report contains all the values entered in the form and submit the report.
- 2.06: View/edit the contract for the subcontractor and verify the inability to remove the month that has a filed L2 with subcontractor payments.
- 2.07: Modify the contract for the subcontractor and verify the changes are reflected in the corresponding reporting periods.
  - i.e. Modify the contract to remove August 2024, and add September 2024 then in the new application confirm the inability to fill out 
    subcontractor payments in August and the ability to fill them out in September.
