# Lobbyist Monthly Reporting (L3) Test Plan
## Initial Setup

After filling out one or more L2 reports start an L3 report for the client related to the compensation and expenses filed by the lobbyists.

Note: This test requires that you setup your firms just like L2 Testing. You should already have a testing account if you have previously done this for the L2.
### [Getting setup for testing](../L2/Testing-Setup.md)

---

## Recording an issue

Record problems found through testing in the spreadsheet. Enter your initials, the test number, and a description of the problem.

---

## Cross-browser testing

> [!NOTE]
> Different browsers have different capabilities and drawbacks, it is important to open and test the application in a variety of available browsers such as: Google Chrome, Microsoft Edge, Mozilla Firefox, and Apple Safari as well as in smaller windows or formats.

- Main points of focus:
  - Web page rendering
    - Confirm that the form loads appropriately and consistently across each browser paying close attention to any inconsistencies.
      - i.e. no gray or white screens, and partially loaded forms.
    - Examples of what to verify and look out for:
      - Verify that the drop-down fields propagate the correct data, and function as desired.
      - Verify the wizard structure works and looks consistent.
      - Verify that the pop-up dialog boxes load in the proper location and are functional.
      - Verify that each component inside the form's rules apply appropriately.
        - i.e. date picker allows dates within a reporting period, positive monetary amounts, required fields, etc.
  - Accessibility
    - Verify that tabular navigation works as desired paying close attention to tab order.
      - i.e. if you push tab 3 times it should go to the 3rd input not from 1, 5, 2 or some other crazy order.

---

## Important baselines to test cases

- Tabbing: Try a combination of using the keyboard and the mouse to select and enter data.
- Dates: 
  - All validation on the dates should be within the reporting period in question. 
    - i.e. 2024 should only accept dates in 2024.
- Amounts: 
  - All validation in the application requires a greater than 0 amount unless otherwise requested.
- Source of funds (Candidate or Committee): 
  - Make sure that these contain valid Candidates and Committees and that the field makes sense
- Deleting rows of data: 
  - These should warn you with a confirmation dialog that you are about to remove data.

---

# Starting the test

## Lobbyist Dashboard

---
## Client Dashboard

L3 Annual reports are displayed with a number of columns. Columns important to testing are:

**Report Id**: Smashing the report id opens the preview L3 screen. It is also used for amending the report, viewing the reports history and printing.

**Action**: Things you can do with a report.
- **Start** - You can start a new report. 
- **Edit** - You can edit a report you started which is a **Draft**. Also appears when you click **Amend**

**Status**:
- **Overdue**: This report is late.
- **Started**: This report is a Draft
- **Filed**: This report has been Filed and can be amended, printed, or have its history viewed.
- **Amending**: This report is currently being amended. (A "date filed" timestamp is shown)

### Start a Annual Report (L3)

From the Client/Employer Dashboard SMASH **Start** to start a DRAFT Annual L3.

### Report Sections

Navigate through each section of the report filling out all required fields. Each section is listed below:

 - 1.0 [Lobbyists](review-compensation-expenses-l3.md)
 - 2.0 [Election expenses](Election-expenses-l3.md)
 - 3.0 [Lobbying Expenses](Lobbying-Expenses-l3.md)
 - 4.0 [Other compensation](Other-compensation-l3.md)

---

- Once all sections are complete, review the entire report for accuracy.
- Submit the report when you're satisfied with its contents.
- Review the submitted report (For use of testing the amendment)

## Make a draft (saved) report

- 5.01: Verify that you can open a Saved report. From the Client Dashboard, locate the saved report and click on the **Edit** button to open.
- 5.02: Ensure that all previously entered data is accurately displayed when opening a saved report.
- 5.03: Check that you can continue editing the saved report from where you left off.
- 5.04: Verify that saving changes in a reopened report works correctly.
- 5.05: Test the "Finish Later" functionality again to ensure you can re-save the report after making changes.

## Submit and preview draft report

- 6.01: Verify that you can preview the report.
- 6.02: In the draft preview, verify the accuracy of this section to ensure that all listed sources of funds (candidate or committee) and other fields correspond to what you have.
- 6.03: Check the validity of Amounts: Verify that all entered amounts are positive values and accurately reflect the amount received.
- 6.04: Calculation of Totals: Double-check that the automatically calculated totals are correct and match the sum of individual entries.
- 6.05: Verify the completeness of Information: Make sure all the columns in the preview are filled out for each entry.
- 6.06: Verify the proper display of Multiple Entries: If there are multiple compensation entries, confirm that they are all correctly recorded and displayed.
- 6.07: Verify that you can submit the report.
- 6.08: Verify that the entered data shows up as complete and correct in the report view after clicking on its "Report id".

## Verify Report Submission

- 7.01: Check that the report status has changed from "Start" to "Filed".
- 7.02: From the Dashboard try to access the **Submitted** report
- 7.03: Ensure it's in read-only mode and the **Amend** button is visible.

## Amend the report and submit again.

- 8.01: Open the submitted report and click on the "Amend" button to initiate the amendment process.
- 8.02: Verify you are able to open and view the report submission history.
- 8.03: Check that the submission history accurately records the edit history.
- 8.04: Confirm that you can print the report and that the print preview looks correct.

## Filing and Amending a Legacy Report

> IT will change your permissions to allow you to file a legacy report in order to fill one out and test legacy amendments.

- There have been structural changes to several categories on the report and the behavior will differ slightly based on 
  whether the legacy report that is being amended contains any of these categories.

### Restructured categories

- The new structure is depicted as: 
  - Lobbyists
    - Previously reported as compensation and expenses summary
  - Election expenses
    - Itemized contributions
    - PAC contributions
    - In-kind contributions
    - Independent Expenditures for candidates
    - Independent Expenditures for ballot measures
  - Lobbying Expenses
    - Entertainment expenditures
    - Itemized expenses
    - Other expenditures
  - Other compensation
    - Employment compensation
    - Professional services

### Tasks

- 9.01: Create a legacy report containing all or some of the categories that have been unchanged: Personal expenses, Compensation, and Entertainment expenses.
- 9.02: Open the report to view and amend in the new system via the account that is modern L3 capable.
  - We expect there to not be a warning and for all the data to display and be editable as usual.
- 9.03: Check the Lobbying subject areas tab while in the draft document and confirm that there are separate input values for issues and bill numbers.
- 9.03: Create a legacy report containing lobbying activities with issue or bill numbers filled in.
  - We do not expect there to be any warning messages except for combining the issue and bill number fields to preserve the old structure.
- 9.04: Create a legacy report containing any of the restructured data categories.
  - We expect the values to not populate but instead throw a warning message saying that the data is in an old format. It requires the filer to fill out the form again with new values.


## Confirm Employer Yearly Filing (L3) totals

- 10.01: Verify that the totals generated for each lobbyist and subcontracted lobbyist are correct for compensation and expenses.
- 10.02: It is not necessary to file L3 reports for these tests. Just confirm that the totals are being brought in.
