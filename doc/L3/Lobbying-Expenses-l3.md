## Lobbying Expenses

# Questionnaire 

- 3:01: Choosing **Yes** and then **No** should prompt a dialog asking if you want to clear the data. Saying **No** should remove the data and remove the subsection from the menu list on the right-hand side of the screen
- 3:02: Have **Yes** for all of the questions
- 3:03: For the four Amounts listed, see if you are able to input an invalid amount. If not, then provide valid amounts and continue.

### Entertainment expenditures

- 3:04 Fill out all fields except for the Title. Try to create a new entertainment expense and see that it won't let you. 
- 3:05 Ensure that the Title selection has all the correct values.
	- State Official
	- State Official's family member
	- Legislator
	- Legislator's family member
	- State employee
	- State employee's family member
- 3:06: Pick a Title and verify its selected and create another entertainment expense with a different **Name**.

### Itemized expenses

- 3:07: Verify that you cannot add multiple entries when a field such as "Amount" or "Recipient" is left empty.
- 3:08: Validate that Totals are accurately updated when changing Amounts for one or more sources.
- 3:09: Navigate to different records or add some. Is anything missing or broken?

### Other expenditures

- 3:10: Verify that you cannot add multiple entries when a field such as "Amount" or "Recipient" is left empty.
- 3:11: Validate that Totals are accurately updated when changing Amounts for one or more sources.
- 3:12: Navigate to different records or add some. Is anything missing or broken?
