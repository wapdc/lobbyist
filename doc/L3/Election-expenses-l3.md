## Election expenses

Noteworthy: Use the **NEXT** button to navigate to the next sections. This will ensure that the validation fires. Some sections require a **Total** amount to be entered. 

> - Source of funds is a list of your Employer(s) and Self.
> - Amount cannot be a negative value.
> - Totals are calculated automatically.
> - Total count (e.g. 1 of 2) is a count of the number of Entertainment expenses added.
> - Non-itemized expenses use the same form as Personal expenses. 
> - Changing a **Yes** to **No** will discard all information for that section.

## Recording a issue

Record problems found through testing in the spreadsheet. Enter your initials, the test number, and a description of the problem.

# Questionnaire 

- 2:01: Choosing **Yes** and then **No** should prompt a dialog asking if you want to clear the data. Saying **No** should remove the data and remove the subsection from the menu list on the right-hand side of the screen
- 2:02: Have **Yes** for all of the questions and choose a positive amount for the "Total of all non-itemized political contributions"

##  Itemized Contributions

- 2:03: Verify that you cannot add multiple entries when a field such as Amount or Source of funds is left empty.
- 2:04: Validate that Totals are accurately updated when changing Amounts for one or more sources.
- 2:05: Pick a **Candidate**, does the dropdown have the correct values to pick from?
- 2:06: Add another for a PAC, does the dropdown have the correct values? Pick one. 
- 2:07: Navigate to different records or add some. Is anything missing or broken?

### PAC contributions

- 2:08: Does the dropdown for **Select committee** have the correct values to pick from?
- 2:09: Navigate to different records or add some. Is anything missing or broken?

### In-kind contributions

- 2:10: Verify that you cannot add multiple entries when a field such as the Amount or Date is left empty.
- 2:11: Validate that Totals are accurately updated when changing Amounts for one or more sources.
- 2:12: Pick a **Candidate**, does the dropdown have the correct values to pick from?
- 2:13: Add another for a PAC, does the dropdown have the correct values? Pick one. 
- 2:14: Navigate to different records or add some. Is anything missing or broken?
- 2:15: Choose a PAC contribution, and change, pick a Committee. What did it save?

### Independent Expenditures for candidates

- 2:16: Verify that you cannot add multiple entries when a field such as description, or "for or against" is left empty.

### Independent Expenditures for ballot measures

- 2:17: Verify that you cannot add multiple entries when a field such as description, or "for or against" is left empty.
