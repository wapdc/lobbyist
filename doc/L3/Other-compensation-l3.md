## Other compensation

## Employment compensation

- 4:01: Verify that **Relationship** dropdown has all the correct values.
- 4:02: Verify that the **Amount** dropdown has the correct values. 
- 4:03: Verify that you cannot add multiple entries when a field such as Amount or Relationship is left empty.
- 4:04: Navigate to different records or add more. Is anything missing or broken? Did it save the right stuff?
- 4:05: Have the relationship be a non-family member type and see that the "Name of (type)" input appears 
- 4:06: Have the relationship be a family member type and see that the "Name of (type)" input appears as well as the "Name of family member" input appears. These should both be required fields


## Professional services

- 4:07: Verify that the **Amount** dropdown has the correct values. 
- 4:08: Verify that you cannot add multiple entries when a field such as Amount or "Firm name" is left empty.
- 4:09: Navigate to different records or add more. Is anything missing or broken? Did it save the right stuff?
