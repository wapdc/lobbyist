## Notable Compensation expenses Issues

- 1.01: Check the accuracy of the **Lobbyist** reported amounts.
- 1.02: Click on the amount and check to see if the category amounts total up to the total amount.
- 1.03: Verify that smashing on the **next button** when first starting prevents you.
- 1.04: Enter a negative or incorrect **Compensation** amount to verify the system informs you the amount isn't correct:
- 1.05: Verify that smashing on the "**Use Lobbyist Reported Compensation**" button fills in the correct amount(s).
- 1.06: Verify that smashing on the "**Use Lobbyist Reported Expenses**" button fills in the correct amount(s).
