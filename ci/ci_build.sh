#!/usr/bin/env bash
set -e
#export variables for gitlab runners as not in lambda
export AWS_REGION=us-west-2
export AWS_ACCESS_KEY_ID="$COMMERCIAL_AWS_ACCESS_KEY_ID"
export AWS_SECRET_ACCESS_KEY="$COMMERCIAL_AWS_SECRET_ACCESS_KEY"
export AWS_PAGER=""
export PATH=/app/bin:$PATH
if [[ -f "/app/bin/ci_build_environment.sh" ]] ; then
  . ci_build_environment.sh
fi

echo "PGHOST: $PGHOST"

cd aws/

node --version
npm install
npm run build:rds

#echo 'Successfully ran tests'
