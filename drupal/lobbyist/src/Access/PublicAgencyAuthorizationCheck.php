<?php

namespace Drupal\lobbyist\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use WAPDC\Core\UserManager;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\Util\Environment;

/**
 * An implementation of AccessInterface to check that a pdc_user
 * has access to a public_agency.
 *
 * Class PublicAgencyAuthorizationCheck
 *
 * @package Drupal\lobbyist\Access
 */
class PublicAgencyAuthorizationCheck implements AccessInterface {

    protected $realm = 'apollo';

    public function access(AccountInterface $account, $agency_id) {

        $has_access = false;

        // This access check is too early for the regular environment
        // so we need to do it here before doing the access check.
        $dir = \Drupal::service('file_system')->realpath("private://");
        $env = $_ENV['PANTHEON_ENVIRONMENT'] ?? 'ddev';
        $file = "$dir/environment.$env.yml";
        Environment::loadFromYml($file);

        if (isset($_SESSION['SAW_USER_GUID'])) {
            $this->realm = 'saml_saw';
            $uid = $_SESSION['SAW_USER_GUID'];
        }
        else {
            $uid = $account->id();
        }
        $userManager = new UserManager(CoreDataService::service()
            ->getDataManager(), $this->realm);
        $user = $userManager->getRegisteredUser($uid);

        $db = CoreDataService::service()->getDataManager()->db;

        $has_access = $db->fetchAssociative("select * from pdc_user_authorization ua left join lobbyist_public_agency pa on pa.id = ua.target_id 
                                                where pa.id = :id and ua.realm = :realm and ua.uid = :uid and ua.target_type = 'lobbyist_public_agency'
                                                and (ua.expiration_date >= now() OR ua.expiration_date is null)",
                                                ['id' => $agency_id, 'uid' => $user->uid, 'realm' => $user->realm]);
        return AccessResult::allowedIf($has_access);
    }
}