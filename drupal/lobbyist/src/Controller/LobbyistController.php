<?php

namespace Drupal\lobbyist\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use WAPDC\Core\TokenProcessor;
use WAPDC\Core\CoreDataService;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Controller for Lobbyist module
 */
class LobbyistController extends LobbyistControllerBase {

  protected string $token;
  protected string $url;

  /**
   * Fully qualified path to single page application being served.
   *
   * @return string
   */
  protected function getApplicationDirectory(): string {
    // Get the module handler service from the service container.
    $moduleHandler = \Drupal::service('module_handler');
    $modulePath = $moduleHandler->getModule('lobbyist')->getPath();
    return $modulePath . '/ui';
  }

  /**
   * Return mime type of a given file.
   *
   * @param string $filename
   * @return string
   */
  protected function getMimeType(string $filename): string {
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    $mimeTypes = [
      'txt' => 'text/plain',
      'htm' => 'text/html',
      'html' => 'text/html',
      'php' => 'text/html',
      'css' => 'text/css',
      'js' => 'application/javascript',
      'json' => 'application/json',
      'xml' => 'application/xml',
      'swf' => 'application/x-shockwave-flash',
      'flv' => 'video/x-flv',

      // fonts
      'eot' => 'application/vnd.ms-fontobject',
      'ttf' => 'application/font-sfnt',
      'otf' => 'application/font-sfnt',
      'woff' => 'application/font-woff',
      'woff2' => 'application/font-woff2',

      // images
      'png' => 'image/png',
      'jpe' => 'image/jpeg',
      'jpeg' => 'image/jpeg',
      'jpg' => 'image/jpeg',
      'gif' => 'image/gif',
      'bmp' => 'image/bmp',
      'ico' => 'image/vnd.microsoft.icon',
      'tiff' => 'image/tiff',
      'tif' => 'image/tiff',
      'svg' => 'image/svg+xml',
      'svgz' => 'image/svg+xml',

      // archives
      'zip' => 'application/zip',
      'rar' => 'application/x-rar-compressed',
      'exe' => 'application/x-msdownload',
      'msi' => 'application/x-msdownload',
      'cab' => 'application/vnd.ms-cab-compressed',

      // audio/video
      'mp3' => 'audio/mpeg',
      'qt' => 'video/quicktime',
      'mov' => 'video/quicktime',

      // adobe
      'pdf' => 'application/pdf',
      'psd' => 'image/vnd.adobe.photoshop',
      'ai' => 'application/postscript',
      'eps' => 'application/postscript',
      'ps' => 'application/postscript',

      // ms office
      'doc' => 'application/msword',
      'rtf' => 'application/rtf',
      'xls' => 'application/vnd.ms-excel',
      'ppt' => 'application/vnd.ms-powerpoint',
      'docx' => 'application/msword',
      'xlsx' => 'application/vnd.ms-excel',
      'pptx' => 'application/vnd.ms-powerpoint',

      // open office
      'odt' => 'application/vnd.oasis.opendocument.text',
      'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    ];

    if (array_key_exists($ext, $mimeTypes)) {
      return $mimeTypes[$ext];
    } else {
      $guesser = new FileinfoMimeTypeGuesser();
      if ($guesser->isSupported()) {
        return $guesser->guess($filename) ?: 'application/octet-stream';
      }
    }

    return 'application/octet-stream';
  }


  /**
   * Serves the requested asset from the application directory.
   *
   * @param string $directory The directory of the requested asset.
   * @param string $asset The filename of the requested asset.
   *
   * @return Response The response containing the asset content.
   */
  public function serve(string $directory = "", string $asset = ""): Response {
    $response = new Response();
    $dir = $this->getApplicationDirectory();

    // Determine the requested file.
    if ($asset) {
      $file = $dir . '/' . $directory . '/' . $asset;
    } elseif ($directory === 'favicon.ico') {
      $file = $dir . '/favicon.ico';
    } else {
      $file = $dir . '/index.html';
    }

    // If the file exists, get its contents and set the response.
    if (file_exists($file)) {
      $contents = file_get_contents($file);
      if ($contents !== false) {
        $response->setContent($contents);
        $response->headers->set('Content-Type', $this->getMimeType($file));
      }
    } else {
      \Drupal::logger('lobbyist')->notice('File does not exist: ' . $file);
      // Handle SPA routes
      // We fall back to serving index.html when the requested asset doesn't exist.
      $indexFile = $dir . '/index.html';
      if (file_exists($indexFile)) {
        $contents = file_get_contents($indexFile);
        if ($contents !== false) {
          $response->setContent($contents);
          $response->headers->set('Content-Type', $this->getMimeType($indexFile));
        }
      } else {
        $response->setContent('File not found.');
        $response->setStatusCode(Response::HTTP_NOT_FOUND);
      }
    }

    return $response;
  }

  /**
   * Redirect to accesshub based on current user credentials.
   * @return TrustedRedirectResponse|Response
   */
  public function redirectToAccessHub() {
    $token = $this->getSSOToken();
    if ($token) {
      $url = $this->url . "/lobbyist/login?token=" . urlencode($token);
      if (!empty($_GET['page'])) {
        $url .= "&destination=" . urlencode($_GET['page']);
      }
      return new TrustedRedirectResponse($url);
    }
    else return new TrustedRedirectResponse("/lobbyist/#/activate");
  }

  /**
   * Redirect to accesshub checks to build url redirect and set accesshub uid.
   * @return string | null
   */
  private function getSSOToken() {
    $accesshub_uid = null;
    // Engineering rule: double maximum response time then next order of magnitude.
    $token_expires = 2400;

    // Retrieve accesshub id for current PDC user
    $pdc_user = $this->getCurrentUser();
    $dataManager = CoreDataService::service()->getDataManager();
    $data = $dataManager->db->fetchAssociative(
      "select u.accesshub_uid, s2.value as url FROM pdc_user u join wapdc_settings s1 on s1.property='stage' join wapdc.public.wapdc_settings s2 
                on s2.property='accesshub_url' and s2.stage=s1.value
                where realm=:realm and uid=:uid",
      ['realm' => $pdc_user->realm, 'uid' => $pdc_user->uid]
    );

    if ($data && $data['accesshub_uid']) {
      $this->url = $data['url'];
      $accesshub_uid = $data['accesshub_uid'];
    } else {
      //call Accesshub to see if pdc_users user_name is the email address that exists on Accesshub.
      $validFor = 5;
      $claims = [
        'accesshub_user' => $pdc_user->user_name,
        'user_name' => $pdc_user->user_name,
        'realm' => $pdc_user->realm,
      ];
      $token = (new TokenProcessor())->generateToken($claims, $validFor * 60);
      $accesshubUser = $this->getAccessHubUser($token);
      if($accesshubUser && trim(strtolower($accesshubUser->email)) == trim(strtolower($pdc_user->user_name))) {
        $this->url = $data['url'];
        $pdc_user->accesshub_uid = $accesshubUser->uid;
        $accesshub_uid = $accesshubUser->uid;
        $dataManager->db->executeStatement(
          "update pdc_user  
             set accesshub_uid  = :accesshub_uid
             where uid = :uid 
             and realm = :realm",
          [
            'uid' => $pdc_user->uid,
            'realm' => $pdc_user->realm,
            'accesshub_uid' => $accesshub_uid
          ]
        );
      }
    }
    if($accesshub_uid) {
      $tokenProcessor = new TokenProcessor();
      $claims = [
        'uid' => $accesshub_uid
      ];
      $this->token = $tokenProcessor->generateToken($claims, $token_expires);
      return $this->token;
    } else {
      return null;
    }
  }

    public function redirectToPublicAgency($agency_id) {
        try {
            $pdc_user = $this->getCurrentUser();
            $validFor = 5;
            $db = CoreDataService::service()->getDataManager()->db;
            $settings = $db->fetchAssociative(
                "SELECT wapdc_setting('l5_url') as l5_url;"
            );

            $profile = $db->fetchAssociative('select * from lobbyist_public_agency where id = :id',
                ['id' => $agency_id]);

            $claims = [
                'filer_id' => $agency_id,
                'public_agency_id' => $agency_id,
                'username' => $pdc_user->user_name,
                'profile' => $profile
            ];

            $token = (new TokenProcessor())->generateToken($claims, $validFor * 60);

            if ($token) {
                $url = $settings['l5_url'] . "Account/LogOn?jwt=" . $token;

                return new TrustedRedirectResponse($url);
            }
        } catch (\Exception $e) {
            \Drupal::Logger('lobbyist')->error($e->getMessage());
            return new Response("Error redirecting to Public Agency");
        }

    }

}
