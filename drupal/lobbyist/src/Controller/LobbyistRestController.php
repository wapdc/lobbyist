<?php
namespace Drupal\lobbyist\Controller;
use Doctrine\ORM\Exception\ORMException;
use \Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\Model\User;
use WAPDC\Core\SendGrid\Drupal8CMS;
use WAPDC\Core\SendGrid\Mail;
use WAPDC\Core\TokenProcessor;
use WAPDC\Core\UserManager;
use WAPDC\Core\FloodManager;
use \stdClass;

class LobbyistRestController extends LobbyistControllerBase {

  protected function verifyAccessHubUsernamePassword($token){
    $dm = CoreDataService::service()->getDataManager();
    $accesshub = $dm->db->fetchOne(
      "SELECT s1.value FROM wapdc_settings s1 
         JOIN wapdc_settings s2 on s2.property = 'stage' and s1.stage = s2.value where s1.property='accesshub_url'");
    $url = "$accesshub/lobbyist/verify-user-by-username-password";

    $this->ch = curl_init();

    $post_data = $token;

    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($this->ch, CURLOPT_URL, $url);
    curl_setopt($this->ch, CURLOPT_HEADER, TRUE);
    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);

    $server_output = curl_exec($this->ch);

    $header_size = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);
    $error = curl_error($this->ch);

    if($error) $this->errors[] = $error;
    $response_status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
    if($response_status === 200){
      $data = json_decode(substr($server_output, $header_size));
      $data->success = true;
      return $data;
    }else{
      $data = new stdClass();
      $data->success = false;
      $data->response_code = $response_status;
      return $data;
    }
  }

  /**
   * Request an emailed access token from the core library.
   * @param Request $request
   * @return JsonResponse
   * @throws \Doctrine\ORM\ORMException
   */
  public function requestAccessHubSSO(Request $request) {
    // Lifetime of token in minutes
    $response = new stdClass();
    $validFor = 5;
    $dm = CoreDataService::service()->getDataManager();
    $fm = new FloodManager($dm);

    try {
      //Check flood beforehand before requesting another email token
      if($fm->floodIsAllowed('Accesshub_SSO_email_request', $request->getClientIp(), 10, 3600))
      {
        // register flood event for email being sent
        $fm->floodRegisterEvent('Accesshub_SSO_email_request', $request->getClientIp());

        // Parse data containing the email address to the user.
        $this->prepareRequest($request);
        $pdc_user = $this->getCurrentUser();

        $claims = [
          'accesshub_user' => $this->request_data->email ?? $pdc_user->user_name,
          'user_name' => $pdc_user->user_name,
          'realm' => $pdc_user->realm,
        ];
        $token = (new TokenProcessor())->generateToken($claims, $validFor * 60);

        $drupal_user = $this->currentUser();

        $user = $this->getAccessHubUser($token);
        if ($user) {
          $test_mode_email = $drupal_user->getEmail();
          $mailer = new Mail(new Drupal8CMS());
          $mailer->setTestModeEmail($test_mode_email);
          $userManager = new UserManager( CoreDataService::service()->getDataManager(), $pdc_user->realm);
          $userManager->requestAccessHubSSO($pdc_user->uid, $user->email, $user->uid, $mailer);
        }
        $response->success = true;
        return new JsonResponse($response);
      }else {
        return new JsonResponse("Too many email request made, please try again at another time.", 429);
      }
    }
    catch ( \Exception $e) {
      \Drupal::logger('lobbyist', $e->getMessage());
      return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function verifySsoToken(Request $request) {
    $this->prepareRequest($request);
    $pdcUser = $this->getCurrentUser(false);
    $response = new stdClass();
    try {
      $dm = CoreDataService::service()->getDataManager();
      $fm = new FloodManager($dm);
      // check for too many attempts
      if($fm->floodIsAllowed('Accesshub_SSO_email_verify', $request->getClientIp(), 5, 900)){
        // register flood event
        $fm->floodRegisterEvent('Accesshub_SSO_email_verify', $request->getClientIp(), 900);
        $rows = $dm->db->executeStatement(
          "update pdc_user  
             set accesshub_uid  = cast(verification_payload->>'accesshub_uid' as integer) 
           WHERE uid=:uid 
             and realm=:realm 
             and verification_expiration >= now() 
             and verification_code=:verification_code",
          [
            'uid' => $pdcUser->uid,
            'realm' => $pdcUser->realm,
            'verification_code' => $this->request_data->verification_code  ?? NULL,
          ]
        );
        $response->success = $rows == 1;
        if(!$response->success){
          $response->message = 'invalid verification code';
        }else{
          //clear request and verify events
          $fm->floodClearEvent('Accesshub_SSO_email_request', $request->getClientIp());
          $fm->floodClearEvent('Accesshub_SSO_email_verify', $request->getClientIp());
        }
        return new JsonResponse($response);
      }else {
        return new JsonResponse('Too many attempts made, please try again at a later time', 429);
      }

    }
    catch ( \Exception $e) {
      \Drupal::logger('lobbyist', $e->getMessage());
      return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function createAccesshubAccount(Request $request){
    $validFor = 5;
    $dataManager = CoreDataService::service()->getDataManager();
    try{
      $this->prepareRequest($request);
      $pdcUser = $this->getCurrentUser(false);

      $claims = [
        'user_name' => $pdcUser->user_name
      ];

      $token = (new TokenProcessor())->generateToken($claims, $validFor * 60);

      //accesshub call
      $accesshubUser = $this->createAccesshubUser($token);

      if($accesshubUser && $accesshubUser->uid){
        $rows = $dataManager->db->executeStatement(
          "update pdc_user
           set accesshub_uid = :accesshub_uid
           where uid = :uid
           and realm = :realm",[
             'uid' => $pdcUser->uid,
            'realm' => $pdcUser->realm,
            'accesshub_uid' => $accesshubUser->uid
          ]
        );
        $response = new stdClass();
        $response->success = $rows == 1;
        return new JsonResponse($response);
      }else{
        return null;
      }
    }catch(\Exception $e){
      \Drupal::logger('lobbyist', $e->getMessage());
      return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  protected function createAccesshubUser($token){
    $dm = CoreDataService::service()->getDataManager();
    $accesshub = $dm->db->fetchOne(
      "SELECT s1.value FROM wapdc_settings s1 
         JOIN wapdc_settings s2 on s2.property = 'stage' and s1.stage = s2.value where s1.property='accesshub_url'");
    $url = "$accesshub/lobbyist/create-user";

    // Make request
    $this->ch = curl_init();

    $post_data = $token;

    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($this->ch, CURLOPT_URL, $url);
    curl_setopt($this->ch, CURLOPT_HEADER, TRUE);
    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);

    $server_output = curl_exec($this->ch);

    $header_size = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);
    $error = curl_error($this->ch);
    if ($error) $this->errors[] = $error;
    $response_status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
    if($response_status === 200){
      $data = substr($server_output, $header_size);
      $user = json_decode($data);
      return $user;
    }else {
      return null;
    }
  }

  /**
   * @throws ORMException
   */
  public function loginByAccesshubUsernamePassword(Request $request){
    $this->prepareRequest($request);
    $dataManager = CoreDataService::service()->getDataManager();
    $pdcUser = $this->getCurrentUser(false);

    $validFor = 2;
    try {
      $claims = [
        'username' => $this->request_data->username ?? null,
        'password' => $this->request_data->password ?? null,
        'uid' => $pdcUser->uid
      ];

      $token = (new TokenProcessor())->generateToken($claims, $validFor * 60);
      $result = $this->verifyAccessHubUsernamePassword($token);

      if($result->success && $result->uid){
          $rows = $dataManager->db->executeStatement(
            "update pdc_user
           set accesshub_uid = :accesshub_uid
           where uid = :uid
           and realm = :realm", [
              'uid' => $pdcUser->uid,
              'realm' => $pdcUser->realm,
              'accesshub_uid' => $result->uid
            ]
          );
          $response = new stdClass();
          $response->success = $rows == 1;
          return new JsonResponse($response);
      }elseif(!$result->success && $result->response_code === 401) {
        return new JsonResponse("Unrecognized username or password", $result->response_code);
      }elseif (!$result->success && $result->response_code === 429){
        return new JsonResponse("Too many attempts made, please try again at a later time", $result->response_code);
      }else{
        return new JsonResponse($result->response_code);
      }
    }
    catch ( \Exception $e) {
      \Drupal::logger('lobbyist', $e->getMessage());
      return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Returns a list of pending public agencies that the lobbyist has submitted for verification. W
   * @return JsonResponse
   */
  public function getUserPendingPublicAgenciesAsRest(): JsonResponse
  {
    $response = new stdClass();
    try {
      /** @var User $pdc_user */
      $pending_agencies = $this->getUserPendingPublicAgencies();

      $response->pending_agencies = $pending_agencies;
      $response->success = true;
      $httpStatus = Response::HTTP_OK;
      return new JsonResponse($response, $httpStatus);
    } catch (Exception $e) {
      \Drupal::logger('lobbyist', $e->getMessage());
      return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Returns a list of public agencies that are registered for lobbying that the current user has access to. When
   * $agency_id is specified, only the agency matching the id is returned but the response is still an array of
   * agencies.
   * @param int|null $agency_id
   * @return JsonResponse
   */
  public function getUserPublicAgenciesAsRest(int $agency_id=null)
  {
    $response = new stdClass();

    try {
      /** @var User $pdc_user */
      $agencies = $this->getUserPublicAgencies($agency_id);

      $response->public_agencies = $agencies;
      $response->success = true;
      $httpStatus = Response::HTTP_OK;
      return new JsonResponse($response, $httpStatus);
    } catch (Exception $e) {
      \Drupal::logger('lobbyist', $e->getMessage());
      return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * @return array[]
   * @throws \Doctrine\DBAL\Exception
   * @throws ORMException
   * @throws \Doctrine\ORM\ORMException
   */
  public function getUserPendingPublicAgencies(): array
  {
    $pdc_user = $this->getCurrentUser(true);
    $db = CoreDataService::service()->getDataManager()->db;
    $results = $db->fetchAllAssociative(
      "select
       *
       from filer_request where
            target_type = 'lobbyist_public_agency'
           and realm = :realm and uid = :uid",
      ['realm' => $pdc_user->realm, 'uid' => $pdc_user->uid]) ?: [];
    return $results;
  }

  /**
   * Returns a list of public agencies that are registered for lobbying that the current user has access to. When
   * $agency_id is specified, only the agency matching the id is returned but the response is still an array of
   * agencies.
   * @param int|null $agency_id
   * @return array[]
   * @throws \Doctrine\DBAL\Exception
   * @throws \Doctrine\ORM\ORMException
   */
  public function getUserPublicAgencies(int $agency_id = null): array
  {
    $pdc_user = $this->getCurrentUser(true);
    $db = CoreDataService::service()->getDataManager()->db;
      return $db->fetchAllAssociative(
        "select a.* from lobbyist_public_agency a
             join pdc_user_authorization az on a.id = az.target_id and az.target_type = 'lobbyist_public_agency'
             where az.realm = :realm and az.uid = :uid and (a.id = :agency_id::int or :agency_id::int is null)
             and (az.expiration_date > now() OR az.expiration_date IS NULL)",
        ['realm' => $pdc_user->realm, 'uid' => $pdc_user->uid, 'agency_id' => $agency_id]) ?: [];
  }

  public function autoGrantAgencies() {
      $response = new stdClass();
      $pdc_user = $this->getCurrentUser();
      $db = CoreDataService::service()->getDataManager()->db;

      try {
          $public_agency_ids = $db->fetchAllAssociative(
              "select distinct l.id
                    from lobbyist_public_agency l
                    join legacy_public_agency_account a
                    on l.id = a.l5filer_id
                where :username::text is not null
                    and (lower(:username) = lower(l.email)
                    or lower(:username) = lower(l.account)
                    or lower(:username) = lower(a.username))",
              [
                  'username'=> $pdc_user->user_name,
              ]
          );

          if($public_agency_ids) {
              $db->executeStatement(
                  "SELECT public.lobbyist_public_agency_add_update_authorization(:realm, :uid, :payload)",
                  [
                      'realm' => $pdc_user->realm,
                      'uid' => $pdc_user->uid,
                      'payload' => json_encode($public_agency_ids)
                  ]
              );
          }
          $response->success = true;
          $response->public_agencies = $this->getUserPublicAgencies();
          $httpStatus = Response::HTTP_OK;
      } catch (Exception $e) {
        \Drupal::logger('lobbyist', $e->getMessage());
        return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
      }
      return new JsonResponse($response, $httpStatus);
  }

  public function loginByPublicAgencyUsernamePassword(Request $request)
  {
      $this->prepareRequest($request);
      $dataManager = CoreDataService::service()->getDataManager();
      $floodManager = new FloodManager($dataManager);
      $pdcUser = $this->getCurrentUser(false);
      try {
          if($floodManager->floodIsAllowed('lobbyist_public_agency_usernamepassword_login', $request->getClientIp(), 5, 900)){
              //register flood event
              $floodManager->floodRegisterEvent('lobbyist_public_agency_usernamepassword_login', $request->getClientIp(), 900);
              //query legacy_public_agency_account and check hashed password against given
              $id = $dataManager->db->fetchAllAssociative(
                  "SELECT lpa.id FROM legacy_public_agency_account lpaa
                    LEFT JOIN lobbyist_public_agency lpa ON lpaa.l5filer_id = lpa.id
                    where lpaa.password = public.crypt(:request_password, lpaa.password) and lpaa.username = :username",
                  ['request_password' => $this->request_data->password, 'username' => $this->request_data->username]
              );

              if($id){
                  $response = new stdClass();
                      $rows = $dataManager->db->executeStatement(
                          "SELECT * FROM lobbyist_public_agency_add_update_authorization(:realm, :uid, :json)",
                          [
                              'realm' => $pdcUser->realm,
                              'uid' => $pdcUser->uid,
                              'json' => json_encode($id)
                          ]
                      );

                  $response->success = $rows == 1;
                  $response->public_agencies = $this->getUserPublicAgencies();
                  $floodManager->floodClearEvent('lobbyist_public_agency_usernamepassword_login', $request->getClientIp());
                  return new JsonResponse($response);
              }else{
                  return new JsonResponse("Unrecognized username or password", 401);
              }
          }else{
              return new JsonResponse("Too many attempts made, please try again at a later time", 429);
          }
      }
      catch ( \Exception $e) {
        \Drupal::logger('lobbyist', $e->getMessage());
        return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
      }
  }
  /**
   * Request an emailed access token from the core library.
   * @param Request $request
   * @return JsonResponse
   */
  public function requestPublicAgencySSO(Request $request)
  {
    $response = new stdClass();
    $dm = CoreDataService::service()->getDataManager();
    $fm = new FloodManager($dm);

    try {
      //Check flood beforehand before requesting another email token
      if ($fm->floodIsAllowed('Public_Agency_SSO_email_request', $request->getClientIp(), 10, 3600)) {
        // register flood event for email being sent
        $fm->floodRegisterEvent('Public_Agency_SSO_email_request', $request->getClientIp());

        // Parse data containing the email address to the user.
        $this->prepareRequest($request);
        $pdc_user = $this->getCurrentUser();
        $drupal_user = $this->currentUser();
        $pub_ids = $dm->db->fetchAllAssociative(
            "select l.id from lobbyist_public_agency l
             where l.id in (select a.l5filer_id from legacy_public_agency_account a 
                            where lower(:user_input) = lower(a.username))
             or (:user_input::text is not null and (lower(:user_input) = lower(l.email)
             or lower(:user_input) = lower(l.account)))",
            [
              'user_input'=> $this->request_data->email,
            ]
        );

        if ($pub_ids) {
          $test_mode_email = $drupal_user->getEmail();
          $mailer = new Mail(new Drupal8CMS());
          $mailer->setTestModeEmail($test_mode_email);
          $userManager = new UserManager(CoreDataService::service()->getDataManager(), $pdc_user->realm);
          // set up payload for email...
          $payload = new \stdClass();
          $payload->realm = $pdc_user->realm;
          $payload->uid = $pdc_user->uid;
          $payload->email = $this->request_data->email;
          $template_id = 'd-c988f353b04b42e9b6fe6d4858120707';
          $userManager->sendVerificationCode($pdc_user->uid, $payload, $mailer, $this->request_data->email, $template_id);
        }
        $response->success = true;
        $response->pub_ids = $pub_ids;
        return new JsonResponse($response);
      } else {
        return new JsonResponse("Too many email request made, please try again at another time.", 429);
      }
    } catch (\Exception $e) {
      \Drupal::logger('lobbyist', $e->getMessage());
      return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function verifyPublicAgencySSO(Request $request) {
    $this->prepareRequest($request);
    $pdcUser = $this->getCurrentUser(false);
    $response = new stdClass();
    try {
      $dm = CoreDataService::service()->getDataManager();
      $fm = new FloodManager($dm);
      // check for too many attempts
      if($fm->floodIsAllowed('Public_Agency_SSO_email_verify', $request->getClientIp(), 5, 900)){
        // register flood event
        $fm->floodRegisterEvent('Public_Agency_SSO_email_verify', $request->getClientIp(), 900);
        $valid = $dm->db->fetchOne(
          "select count(*) FROM pdc_user
           WHERE uid=:uid 
             and realm=:realm 
             and verification_expiration >= now()
             and verification_code=:verification_code",
          [
            'uid' => $pdcUser->uid,
            'realm' => $pdcUser->realm,
            'verification_code' => intval($this->request_data->verification_code) ?? NULL,
          ]
        );
        if($valid){
          //loop through pub_ids and insert into pdc_user_auth for each pub_id....
          $rows = $dm->db->executeStatement(
            "SELECT public.lobbyist_public_agency_add_update_authorization(:realm, :uid, :payload)",
            [
              'realm' => $pdcUser->realm,
              'uid' => $pdcUser->uid,
              'payload' => json_encode($this->request_data->pub_ids)
            ]
          );
          $response->success = $rows >= 1;
          $response->public_agencies = $this->getUserPublicAgencies();

          //clear request and verify events
          $fm->floodClearEvent('Public_Agency_SSO_email_request', $request->getClientIp());
          $fm->floodClearEvent('Public_Agency_SSO_email_verify', $request->getClientIp());

          return new JsonResponse($response);
        }else{
          $response->message = 'invalid verification code';
        }
        return new JsonResponse($response);
      }else {
        return new JsonResponse('Too many attempts made, please try again at a later time', 429);
      }

    }
    catch ( \Exception $e) {
      \Drupal::logger('lobbyist', $e->getMessage());
      return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
  public function updatePublicAgencyProfile(Request $request){
    $this->prepareRequest($request);
    $dataManager = CoreDataService::service()->getDataManager();
    $agency_id = $this->request_data->agencyId;
    $data = $this->request_data->payload;
    try{
      if($agency_id){
        $response = new stdClass();
        $rows = $dataManager->db->executeStatement(
          "UPDATE lobbyist_public_agency SET  
                                                    name = :agency_name, 
                                                    address = :address, 
                                                    city = :city, 
                                                    state = 'WA', 
                                                    zip = :zip,
                                                    county = :county,
                                                    phone = :phone,
                                                    contact = :contact,
                                                    email = :email,
                                                    head = :head
                where id = :id",
          [
            "id" => $agency_id,
            "agency_name" => $data->name,
            "address" => $data->address,
            "city" => $data->city,
            "zip" => $data->zip,
            "county" => $data->county,
            "phone" => $data->phone,
            "contact" => $data->contact,
            "email" => $data->email,
            "head"  => $data->head]
        );

        $response->success = $rows == 1;
        $response->public_agencies = $this->getUserPublicAgencies();
        return new JsonResponse($response);
      }else{
        return new JsonResponse('No agency found');
      }
    }
    catch (\Exception $e){
      \Drupal::logger('lobbyist', $e->getMessage());
      return new JsonResponse("Something went wrong, try again at a later time", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
}