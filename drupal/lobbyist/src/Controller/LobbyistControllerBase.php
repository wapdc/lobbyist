<?php

namespace Drupal\lobbyist\Controller;

use Drupal\wapdc_core\Controller\CoreControllerBase;
use WAPDC\Core\CoreDataService;

/**
 * Common controller for lobbyist functions
 */
class LobbyistControllerBase extends CoreControllerBase {

  protected function getAccessHubUser($token) {
    $dm = CoreDataService::service()->getDataManager();
    $accesshub = $dm->db->fetchOne(
      "SELECT s1.value FROM wapdc_settings s1 
         JOIN wapdc_settings s2 on s2.property = 'stage' and s1.stage = s2.value where s1.property='accesshub_url'");
    $url = "$accesshub/lobbyist/user-lookup";

    // Make the request
    $this->ch = curl_init();

    $post_data = $token;

    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($this->ch, CURLOPT_URL, $url);
    curl_setopt($this->ch, CURLOPT_HEADER, TRUE);
    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);

    if (empty($_ENV['PANTHEON_ENVIRONMENT'])) {
      curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
    }

    $server_output = curl_exec($this->ch);

    $header_size = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);
    $error = curl_error($this->ch);
    if ($error) $this->errors[] = $error;
    $response_status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
    if ($response_status === 200) {
      $data = substr($server_output, $header_size);
      $user = json_decode($data);
      return $user;
    }
    else {
      return null;
    }
  }
}
