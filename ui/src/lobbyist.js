/**
 * Application level information store.
 *
 * When using, be sure to await the useApplication() call so that the appInfo and user properties are set.
 */
import {useApplication, useGateway} from '@wapdc/pdc-ui/application'


export async function  useLobbyist() {
  const lobbyistApi = await useGateway('lobbyist_aws_gateway', 3010)
  const {user} = await useApplication()
  return {lobbyistApi, user}
}
