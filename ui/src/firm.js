import { useRoute } from "vue-router";
import { reactive } from 'vue'
import {useLobbyist} from "src/lobbyist";

const firm = reactive({});
const firmReports = reactive({});
let lobbyistApi;

export async function useFirm() {
  lobbyistApi =  (await (useLobbyist())).lobbyistApi

  async function getFirm(firm_id) {
    const result = await lobbyistApi.get(`firm/${ firm_id }`);
    if (result) {
      Object.assign(firm, result.firm)
    }
  }

  async function getReports(){
    const result = await lobbyistApi.get(`firm/${ firm.lobbyist_firm_id }/reports`);
    if (result) {
      Object.assign(firmReports, result)
    }
  }

  return { getFirm, firm, getReports, firmReports };
}
