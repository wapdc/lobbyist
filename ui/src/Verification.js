
import {useLobbyist} from "src/lobbyist";

export async function useVerification () {
  const {lobbyistApi} = await useLobbyist()
  async function getAccessRequests() {
    return await lobbyistApi.get("admin/access-requests")
  }

  async function getAccessRequest(request_id) {
    return await lobbyistApi.get(`admin/access-request/${request_id}`)
  }

  async function verifyAccessRequest(filer_request_id, email, lobbyist_id, payload) {
    return await lobbyistApi.post("admin/verify-agency", {filer_request_id, email, lobbyist_id, payload})
  }

  async function deleteAccessRequest(request_id) {
    return await lobbyistApi.delete(`admin/access-request/${request_id}`)
  }

  async function updateAccessRequest(request_id, user_name = null, action_date = null) {
    return await lobbyistApi.post("admin/access-request/update", {request_id, user_name, action_date})
  }

  return {getAccessRequests, getAccessRequest, verifyAccessRequest, deleteAccessRequest, updateAccessRequest}
}
