import {reactive, ref} from "vue";
import {useLobbyist} from "src/lobbyist";
import {useFirm} from "src/firm";
import {formatDate} from "@wapdc/pdc-ui/util"
import Papa from "papaparse";
import rules from "@wapdc/pdc-ui/rules"
import Moment from "moment"

let lobbyistApi;
let saveCount = 0
let periodStart;
let firm
let savedL2

const emptyL2 = {
  "report_type": "L2",
  "sources": [],
  "has_compensation": null,
  "compensation": [],
  "has_sub_lobbyist_payments": null,
  "sub_lobbyist_payments": [],
  "has_personal_expenses": null,
  "personal_expenses": [],
  "has_contributions_small": null,
  "contributions_small": [],
  "has_contributions": null,
  "contributions": [],
  "has_contributions_in_kind": null,
  "contributions_in_kind": [],
  "has_independent_expenditures_candidate": null,
  "independent_expenditures_candidate": [],
  "has_independent_expenditures_ballot": null,
  "independent_expenditures_ballot": [],
  "has_contributing_pacs": null,
  "contributing_pacs": [],
  "has_entertainment": null,
  "entertainment": [],
  "has_entertainment_small": null,
  "entertainment_small": [],
  "has_printing": null,
  "printing": [],
  "has_public_relations": null,
  "public_relations": [],
  "has_consulting": null,
  "consulting": [],
  "has_expenses_other": null,
  "expenses_other": [],
  "has_expenses_indirect": null,
  "expenses_indirect": [],
  "has_lobbying": null,
  "lobbying": [],
  "needL6": null,
  "certification": {
    signature: null,
    text: null,
    name: null,
    user_name: null
  },
  "period_start": null,
  "period_end": null
}
const validations = ref([]);
const l2 = reactive(emptyL2)
const subjectList = ref([])
const committees = ref([])
const candidates = ref([])
const hasSubcontractors = ref(false)

function clientName(id, row) {
  if (row.source_name) return row.source_name;
  if (id) {
    const found_source = l2.sources.find(source => source.id === parseInt(id))
    return found_source ? found_source.name : id
  }
  return ""
}

function subcontractName(id, row) {
  if (id) {

    const c = l2.subcontractors.find((e) => {
      return e.lobbyist_contract_id === parseInt(id)
    })
    return c ? c.name : row.name ?? id
  } else {
    return row.name ?? ""
  }
}

/**
 * The lobbyistAPI is a predefined object capable of managing state, fetches
 *   draft data for a specific period & updates the l2 application state with the data.
 * periodStartDate and periodEndDates - used for p-date-picker min & max date filtering.
 *
 * @param period_start
 */
async function getDraft(period_start) {
  const result = await lobbyistApi.get(`firm/${firm.lobbyist_firm_id}/l2/${period_start}/draft`)
  if (result) {
    periodStart = period_start
    saveCount = result.draft.save_count
    Object.assign(l2, result.draft.user_data)
    savedL2 = JSON.stringify(l2)
    validations.value = result.validations
    committees.value = result.committees
    candidates.value = result.candidates
    subjectList.value = result.subjectList
    hasSubcontractors.value = l2.subcontractors?.length > 0
  }
}

const submissions = reactive([])

/**
 *
 * @param firm_id
 * @param report_id
 * @returns {Promise<void>}
 */
async function getL2History(firm_id, report_id) {
  const response_submissions = await lobbyistApi.get(`/firm/${firm_id}/report/${report_id}/history`)
  const result = response_submissions.map(response_submission => {
    response_submission.submitted_at = formatDate(response_submission.submitted_at)
    return response_submission
  })
  Object.assign(submissions, result)
}

/**
 * Saves the current draft L2.  Note that it only calls the save function if the L2
 * has changed from when it was last saved/loaded.
 */
function saveDraft() {
  const payload = JSON.stringify(l2)
  // Compare the payload
  if (payload !== savedL2) {
    lobbyistApi.put(`firm/${firm.lobbyist_firm_id}/l2/${periodStart}/draft`, {
      save_count: saveCount,
      user_data: l2
    }).then(result => {
      if (result.save_count) {
        saveCount = result.save_count
        savedL2 = payload
        validations.value = result.validations
      }
    })
  }
}

/**
 * Date validation function based loosely on logic contained in PDatePicker global component.
 * It was rewritten to not rely on vue component technology.
 * @param date
 * @param row
 * @param field
 * @return {boolean|string}
 */
function validDate(date, row, field) {

  if (date) {

    let m;
    const minDateString = (new Moment(l2.period_start, 'YYYY-MM-DD')).format('MM/DD/YYYY')
    const maxDateString = (new Moment(l2.period_end, 'YYYY-MM-DD')).format('MM/DD/YYYY')
    let isSpokenDate = (new RegExp("^(((January?|March?|May?|July?|August?|October?|December?) 31)|((January?|March?|May?|April?|July?|June?|August?|October?|September?|November?|December?) (0?[1-9]|([12]\\d)|30))|(February? (0?[1-9]|1\\d|2[0-8]|(29(?=, ((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))))), ((1[6-9]|[2-9]\\d)\\d{2})").test(date));

    if (isSpokenDate) {
      m = Moment(date, 'MMMM DD, YYYY');
    } else if (date.includes('-')) {
      m = Moment(date, 'YYYY-MM-DD')
    } else {
      m = (new Moment(date, 'MM/DD/YYYY'));
    }

    if (!m.isValid()) {
      return "invalid (must be MM/DD/YYYY or YYYY-MM-DD format)"
    } else {
      date = m.format('YYYY-MM-DD')
      if (date < l2.period_start || date > l2.period_end) {
        return "must be between " + minDateString + " and " + maxDateString
      } else {
        row[field] = date
        return true
      }
    }
  } else {
    return "Required"
  }
}

function validType(v, row, field) {
  if (v && (v.toLowerCase() === 'candidate' || v.toLowerCase() === 'candidacy')) {
    row[field] = 'candidacy'
    return true
  } else if (v && (v.toLowerCase() === 'pac' || v.toLowerCase() === 'committee')) {
    row[field] = 'committee'
    return true
  } else {
    return 'Invalid type of contribution (valid values are "pac" or "candidate")'
  }
}

/**
 * Validates a single row of a contributions record.
 * @param row An object containing a single contribution
 * @param rowNumber The row number in the file that needs validating
 * @param errors Errors array to store
 */
function validContribution(row, rowNumber, errors) {
  // Define validations that need to be applied in similar syntax to how it is used in form validation.  This is
  // done so that common validation logic used in forms can be copied into this routine from the controls that provide
  // the form validation.
  const columns = [
    {field: 'name', rules: [rules.required]},
    {field: 'amount', rules: [rules.required, rules.positiveAmount]},
    {field: 'type', rules: [rules.requireString, validType]},
    {field: 'date', rules: [validDate]}
  ]

  /**
   * Helper function to add errors to the response.
   * @param row
   * @param message
   * @return boolean Indicates whether the row was valid.
   */
  function addError(row, message) {
    const error = {row, message}
    errors.push(error)
  }

  // Validate each filed defined in columns
  let valid = true
  columns.forEach(column => {
    column.rules.forEach(rule => {
      const result = rule(row[column.field], row, column.field)
      if ((typeof result) === "string") {
        addError(rowNumber, column.field + " " + result.toLowerCase())
        valid = false;
      }
    })
  })

  return valid;
}

/**
 * Import the contributions file to the contributions data.
 * @param file
 * @param defaults Object specifying the default values for items in the import
 * @return {Promise<unknown>}
 */
export async function importContributions(file, defaults) {
  return new Promise(resolve => {
    const response = {errors: [], contributions: [], valid: true}
    let rowNumber = 1
    Papa.parse(file, {
      header: true,
      skipEmptyLines: true,
      // reformat headers so they don't have to be specified with _ and the same case
      transformHeader: (header) => {
        return header.toLowerCase().replace(' ', '');
      },
      chunk: (chunk) => {
        chunk.data.forEach(r => {
          rowNumber++;
          // check to make sure the contribution is valid
          if (!validContribution(r, rowNumber, response.errors)) {
            response.valid = false
          } else {
            // Manually push onto the array, so we don't get extra rows
            response.contributions.push(Object.assign(structuredClone(defaults), {
              import_recipient: r.name,
              recipient_type: r.type,
              amount: r.amount,
              date: r.date,
            }))
          }
        })
      },
      chunkSize: 5000,
      complete: () => {
        resolve(response)
      }
    });
  })
}

const sampleExport = [
  {date: '07/11/2024', amount: 1555.55, recipient: 'Charlie Jones', recipient_type: 'candidate'},
  {date: '07/12/2024', amount: 1234.45, recipient: 'Acme PAC', recipient_type: 'committee'}
]

export function exportContributions(contributions = sampleExport) {
  const data = contributions.map(c => (
    {
      amount: c.amount,
      date: c.date,
      name: c.recipient,
      type: c.recipient_type
    }
  ))
  const csv = Papa.unparse(data);
  const filename = 'Contributions';
  const blob = new Blob([csv], {type: 'text/csv;charset=utf-8;'});

  const link = document.createElement("a");
  if (link.download !== undefined) { // feature detection
    // Browsers that support HTML5 download attribute
    const url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", filename);
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}

export const entertainmentDescription = {
  itemizeDisclaimer: {
    prompt: "Itemize all of the following expenditures that were incurred by lobbyist or lobbyist client/employer(s) for legislators, state officials, state employees and members of their immediate families.",
    list: [
      "Entertainment expenditures exceeding $100 per occasion (including lobbyist’s expense) for meals, beverages, tickets, passes, or for other forms of entertainment.",
      "Receptions. See WAC 390-20-020A, L-2 Reporting Guide, to determine if per person cost is required.",
      "Travel, lodging and subsistence expenses in connection with a speech, presentation, appearance, trade mission, seminar or educational program.",
      "Enrollment and course fees in connection with a seminar or educational program."
    ],
  },
  electedOfficialDisclaimer: {
    prompt: "Lobbyists must provide an elected official with a copy of the L-2 or Memo Report if the lobbyist reports:",
    list: [
      " Spending on one occasion over $100 for food or beverages for the official and/or his or her family member(s).",
      " Providing travel, lodging, subsistence expenses or enrollment or course fees for the official and, if permitted, the official’s family."
    ]
  }
}
function validSource(sources, source) {
  return sources.some(s => s.id === source)
}

export function nullInvalidSource(sources, section) {
  return section.forEach((s) => {
    if(!validSource(sources, s.source)) {
      s.source = null
    }
  })
}

export async function useL2() {
  lobbyistApi = (await useLobbyist()).lobbyistApi
  firm = (await useFirm()).firm
  return {
    l2, validations, getDraft, saveDraft, clientName, submissions, getL2History,
    subjectList, committees, candidates, hasSubcontractors, subcontractName, nullInvalidSource
  }
}
