import {reactive, ref} from "vue";
import {useLobbyist} from "src/lobbyist"
import {useClient} from "src/client"
import {formatDate} from "@wapdc/pdc-ui/util"

let lobbyistApi
let client
let saveCount = 0
let reportYear
let savedL3
const submissions = reactive([])
const emptyL3 = {
  period_start: null,
  period_end: null,
  entertainment_expenditures: [],
  itemized_expenditures: [],
  other_expenditures: [],
  has_entertainment_expenditures: null,
  has_itemized_expenditures: null,
  has_other_expenditures: null,
  payments_for_registered_lobbyists: null,
  witnesses_retained_for_lobbying_services: null,
  costs_for_promotional_materials: null,
  grassroots_expenses_for_lobbying_communications: null,
  non_itemized_political_contribution_amount: null,
  has_itemized_contributions: null,
  itemized_contributions: [],
  has_pac_contributions: null,
  pac_contributions: [],
  statewide_candidacies: [],
  has_candidate_independent_expenditures: null,
  candidate_independent_expenditures: [],
  has_ballot_proposition_expenditures: null,
  ballot_proposition_expenditures: [],
  has_employment_compensation: null,
  employment_compensation: [],
  has_professional_services_compensation: null,
  professional_services_compensation: [],
  has_in_kind_contributions: null,
  in_kind_contributions: [],
  certification: {}
}
const l3 = reactive(structuredClone(emptyL3))
const validations = ref([])
const committees = ref([])
const candidates = ref([])
const l2_summary = ref([])
const statewide_candidacies = ref([])
const other_compensation_amount_ranges  = [
    { value: '', text: 'None'},
    { value: '0-29999', text: 'less than $30,000'},
    { value: '30000-59999', text: '$30,000 to $59,999'},
    { value: '60000-99999', text: '$60,000 to $99,999'},
    { value: '100000-199999', text: '$100,000 to $199,999'},
    { value: '200000-499999', text: '$200,000 to $499,999'},
    { value: '500000-749999', text: '$500,000 to $749,999'},
    { value: '750000-999999', text: '$750,000 to $999,999'},
    { value: '1000000-', text: '$1,000,000 or more' },
  ]

/**
 *
 * @param client_id
 * @param report_id
 * @returns {Promise<void>}
 */
async function getL3History(client_id, report_id) {
  const response_submissions = await lobbyistApi.get(`/client/${client_id}/report/${report_id}/history`)
  const result = response_submissions.map(response_submission => {
    response_submission.submitted_at = formatDate(response_submission.submitted_at)
    return response_submission
  })
  Object.assign(submissions, result)
}

/**
 * Retrieves the current draft based on the year.
 * @param year
 * @return {Promise<void>}
 */
async function getDraft(year) {
  const result = await lobbyistApi.get(`client/${client.lobbyist_client_id}/l3/${year}/draft`)
  if (result) {
    reportYear = year
    saveCount = result.draft.save_count
    Object.assign(l3, result.draft.user_data)
    savedL3 = JSON.stringify(l3)
    validations.value = result.validations
    committees.value = result.committees
    candidates.value = result.candidates
    l2_summary.value = result.l2_summary
    statewide_candidacies.value = result.statewide_candidacies
  }
}

/**
 * Saves the current draft L3.  Note that it only calls the save function if the L3
 * has changed from when it was last saved/loaded.
 */
function saveDraft() {
  const payLoad = JSON.stringify(l3)
  // Compare the payload
  if (payLoad !== savedL3) {
    lobbyistApi.put(`client/${client.lobbyist_client_id}/l3/${reportYear}/draft`, {
      save_count: saveCount,
      user_data: l3
    }).then(result => {
      if (result.save_count) {
        saveCount = result.save_count
        savedL3 = payLoad
        validations.value = result.validations
      }
    })
  }
}

export async function useL3() {
  lobbyistApi = (await useLobbyist()).lobbyistApi
  client = (await useClient()).client
  return {
    l3,
    getL3History,
    submissions,
    getDraft,
    saveDraft,
    validations,
    committees,
    candidates,
    l2_summary,
    statewide_candidacies,
    other_compensation_amount_ranges
  }
}
