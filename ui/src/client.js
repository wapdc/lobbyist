import { reactive } from 'vue';
import {useLobbyist} from "src/lobbyist"

const client = reactive({});
const clientReports = reactive({})


export async function useClient() {
  const {lobbyistApi} = await useLobbyist()

  async function loadClient(client_id) {
    const result = await lobbyistApi.get(`client/${client_id}`)
    if (result) {
      Object.assign(client, result)
    }
  }

  async function loadClientReports(client_id) {
    const result = await lobbyistApi.get(`client/${client_id}/reports`)
    if (result) {
      Object.assign(clientReports, result)
    }
  }

  return { client, loadClient, clientReports, loadClientReports }
}
