import axios from 'axios'
import { Notify } from 'quasar'

const datasets = {
  L7: {
    live: 'ef7g-tyg8',
    demo: 'ef7g-tyg8'
  },
Lobbyist_reporting_history: {
  live: 'nuwx-ay5h',
  demo: 'nuwx-ay5h'
}
}
let stage = 'live'
if ((location.host.search('demo') > 0) || location.host.includes('.lndo.site') || location.host.includes('localhost')) {
  stage = 'demo'
}
const socrata_api = axios.create({baseURL: 'https://data.wa.gov/resource'})

export const getRecords = async (report_type, soql) => {
  const dataset_id = `${datasets[report_type][stage]}.json`
  const params = {
    $query: soql
  }
  try {
    const result = await socrata_api.get(dataset_id, {
      headers: {
        'content-type': 'application/json',
      },
      params
    })
    return (result) ? result : []
  }
  catch (error) {
    console.error('Error fetching axios request', error)
    Notify.create({
      position: 'top',
      type: 'negative',
      message: 'Unable to retrieve data from https://data.wa.gov',
    })
    return []
  }
}
