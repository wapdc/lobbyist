import moment from 'moment'
import { Notify } from 'quasar'

export function formatMoney(amount, defaultValue='$0.00') {
  try {
    if(typeof amount !== "undefined" && amount && !isNaN(amount)) {
      return addParenthesis(format(amount))
    } else {
      return defaultValue
    }
  } catch(e) {
    console.error(e);
  }
}

function addParenthesis(val) {
  const temp = val.replace('$', '')
  if( temp < 0 ){
    return "$(" + temp + ")"
  } else {
    return val
  }
}

export function formatName(name) {
  let formattedName = '';
  if (name.title) {
    formattedName += name.title;
  }
  if (name.given) {
    formattedName += (formattedName !=='') ? ' ' + name.given : name.given;
  }
  if (name.middle) {
    formattedName += ' ' + name.middle;
  }
  if (name.family) {
    formattedName += ' ' + name.family;
  }
  if (name.generational) {
    formattedName += ' ' + name.generational;
  }
  return formattedName;
}



const {format} = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  maximumFractionDigits: 2
});

export function today() {
  return moment(new Date()).format('YYYY-MM-DD');
}

export function formatType(lobbyistType) {
  switch(lobbyistType) {
    case 'lobbyist_public_agency':
      return 'Public Agency'
    case 'lobbyist_client':
      return 'Client'
    case 'lobbyist_firm':
      return 'Firm'
    case 'lobbyist_agent':
      return 'Agent'
  }
}

/* Takes in a date and returns the Month and Year */
export function getMonthYear(dateString) {
  const date = new Date(dateString);
  return date.toLocaleDateString(
    'en-US',
    { year: 'numeric', month: 'long' , timeZone: 'UTC'}
  );
}

export function formatPhone(phoneNumber) {
  if (!phoneNumber) return;
  // Match against extension formats like 'ext1234', 'ext 1234', 'x1234', 'x 1234'
  const extMatch = phoneNumber.match(/(ext|x)\s*(\d+)/i);
  let extension = "";

  if (extMatch) {
    // Standardize the extension to 'x1234' format
    extension = ` x${extMatch[2]}`;
    // Remove the original extension from the phone number string
    phoneNumber = phoneNumber.replace(extMatch[0], '');
  }

  // Remove non-digit characters
  const cleaned = ('' + phoneNumber).replace(/\D/g, '');

  // Match the remaining phone number against the pattern
  const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);

  if (match) {
    return `(${match[1]}) ${match[2]}-${match[3]}${extension}`;
  }
  return phoneNumber;
}

export function notifyErrorWithReason(reason) {
  Notify.create({
    position: "top",
    type: "negative",
    message: reason,
  })
}

export function formatMultiValue(value) {
  if (value && Array.isArray(value)) {
    return value.join(', ')
  }
  return value
}

export const validationStyle = {fontSize: '1.5em', color: '#9f2d2d', width: '5%'};

