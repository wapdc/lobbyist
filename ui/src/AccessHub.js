import {useApollo, useApplication} from "@wapdc/pdc-ui/application";
import {Loading} from "quasar";

let apollo_url
let accesshub_url
let apollo
function accessHubUrl(path) {

  const safePath = encodeURI(path);
  return `${apollo_url}/accesshub/login?page=${safePath}`
}

/**
 * Request access to email
 * @param email
 * @returns {Promise<*|boolean>}
 */
async function requestSsoActivation(email) {
  let data
  try {
    const result = await apollo.post('sso-request', {email: email})
    data = {success: result.success}
  } catch (e) {
    data = {success: false, message: e.response.data, status: e.response.status}
  }
  return data
}

async function verifyActivationCode(verification_code, email){
  Loading.show()
  let data
  try {
    const result = await apollo.post('sso-verify-token', {verification_code, email})
    //When token code is invalid
    if(!result.success){
      if (uiBlocking) {
        Loading.hide()
      }
    }
    data = result
  }
  catch (e) {
    data = {success: false, message: e.response.data, status: e.response.status}
  }
  Loading.hide()
  return data
}

/**
 * Request sso with username password
 */
async function loginSsoActivation(username, password){
  Loading.show()
  let data = {}
  try{
    const result = await apollo.post('sso-verify-login', {username, password})
    if(result.success){
      data = result
    }
  }
  catch (e){
    Loading.hide()
    data = {success: false, message: e.response.data, status: e.response.status}
  }
  return data
}

async function createAccesshubUser(){
  Loading.show()
  try {
    const result = await apollo.post('sso-create-account', {})
    return result.success
  } catch(e){
    alert("Unknown error trying to create lobbyist account")
    Loading.hide()
  }
}

export async function useAccessHub() {
  const {getSetting} = await useApplication()
  apollo_url = getSetting('apollo_url')
  apollo = useApollo('service/lobbyist')



  const devMode = (location.host.search('localhost') >= 0) || (location.host.search('127.0.0.1') >= 0) ||
    (location.host.search('lndo.site') >=0)
  accesshub_url = getSetting('accesshub_url')

  if (devMode && !accesshub_url) {
    // Assume ports that start with 818 (e.g. 8180-8189 will connect to demo environment)
    // so that devs with no lando work.
    accesshub_url = window.localStorage.accesshub_url ?? 'https://accesshub.lndo.site'
  }
  return {accessHubUrl, accesshub_url, createAccesshubUser, loginSsoActivation, verifyActivationCode, requestSsoActivation}
}
