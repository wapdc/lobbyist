import { reactive } from "vue"
import moment from 'moment'
import { useClient } from './client.js'
import { useFirm } from './firm.js'
import {useLobbyist} from "src/lobbyist";

const fixed_l7_report = {
  report_type: 'L7',
  version: '1.0',
  report_id: null,
  lobbyist: {
    name: null,
    target_type: null,
    target_id: null,
    address: null,
    city: null,
    state: null,
    postcode: null,
  },
  employee_name: null,
  lobbyist_employment: {
    compensation: null,
    description: null,
  },
  public_employment: {
    description: null,
  },
  certification: {
    name: null,
    statement: null,
    text: null,
  }
}

function jsonClone(object) {
  return JSON.parse(JSON.stringify(object))
}

const l7_report = reactive(jsonClone(fixed_l7_report))

async function loadL7Submission(target_type, target_id, submission_id, store_to_session_storage = true) {
  if(submission_id){
    const {lobbyistApi} = await useLobbyist()
    const data = await lobbyistApi.get(`/${target_type}/${target_id}/submission/${submission_id}`)
    if(data){
      Object.assign(l7_report, data.user_data)
      l7_report.report_id = data.report_id
      l7_report.submitted_at = data.submitted_at
      l7_report.superseded_id = data.superseded_id
      if (store_to_session_storage) {
        sessionStorage.setItem('l7_report', JSON.stringify(l7_report));
      }
    }
  }
}

async function prepareL7Amendment(data) {
  if (data) {
    Object.assign(l7_report, data.user_data)
    l7_report.certification.name = null
    l7_report.certification.statement = null
    l7_report.certification.text = null
    l7_report.report_id = data.report_id
    delete (l7_report.submitted_at)
    sessionStorage.setItem('l7_report', JSON.stringify(l7_report))
  }
}
async function loadL7SubmissionFromSocrata(data) {
  data = data[0]
  Object.assign(l7_report, JSON.parse(data.user_data))
  l7_report.submitted_at = new moment(data.receipt_date, 'YYYY-MM-DD')
  l7_report.superseded_id = data.amended_by
  l7_report.view_type = 'public'
}

export const useL7 = () => {
  const saveL7ToSessionStorage = () => {
    sessionStorage.setItem('l7_report', JSON.stringify(l7_report))
  }

  const retrieveL7FromSessionStorage = () => {
    const session_storage_report = JSON.parse(sessionStorage.getItem('l7_report'))
    Object.assign(l7_report, fixed_l7_report, session_storage_report)
  }

  const clearL7Certification = () => {
    l7_report.certification.statement = null
  }

  const clearL7 = () => {
    Object.assign(l7_report, jsonClone(fixed_l7_report))
    saveL7ToSessionStorage()
  }

  async function loadLobbyistIntoL7(target_type) {
    let target = {}
    switch(target_type) {
      case 'firm': target = (await useFirm()).firm; break;
      case 'client': target = (await useClient()).client; break;
    }
    const target_id = (target_type === 'client') ? target.lobbyist_client_id : target.lobbyist_firm_id

    l7_report.lobbyist.name = target.name
    l7_report.lobbyist.target_type = 'lobbyist_' + target_type
    l7_report.lobbyist.target_id = target_id
    l7_report.lobbyist.address = target.address_1
    l7_report.lobbyist.city = target.city
    l7_report.lobbyist.state = target.state
    l7_report.lobbyist.postcode = target.postcode
  }

  return {
    prepareL7Amendment,
    loadL7Submission,
    loadL7SubmissionFromSocrata,
    l7_report,
    saveL7ToSessionStorage,
    retrieveL7FromSessionStorage,
    clearL7Certification,
    loadLobbyistIntoL7,
    clearL7,
  }
}
