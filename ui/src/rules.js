export default {
  required: value => !!value || 'Required',

  requireString: value => ! (! value || ! value.trim()) || 'Required',

  validState: value => (value || '').length <= 2 || '2 characters only',

  positiveAmount: value => {
    // determine the maximum possible number for a numeric(16,2)
    const maxNumber = 9999999999999999;

    // when v is not a valid decimal number
    if (isNaN(value)) return 'Invalid number'

    // convert the number to a float
    const floatNum = parseFloat(value)

    if (floatNum > maxNumber) return 'Invalid amount'
    if (floatNum.toString().split(".")[1]?.length > 2) return 'Dollar amounts cannot contain fractional cents'
    if (value === '0.0' || value === '0') return 'Amount must be greater than 0'

    return !value || parseFloat(value) >= 0 || 'Negative amounts are not allowed'
  },

  validAddressLength: value => value.length < 40 || 'Address must be 40 characters or less',

  validEmail: value => ! value || /.+@.+\..+/.test(value) || 'Invalid email',

  validCertificationStatement: value => (value && value.toUpperCase().trim() === 'I CERTIFY') || "Invalid certification",

  states: [
    "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC",
    "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA",
    "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE",
    "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC",
    "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"
  ]
}
