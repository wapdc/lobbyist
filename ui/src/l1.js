import {reactive, ref} from "vue"
import {useLobbyist} from "src/lobbyist"
import {useFirm} from "src/firm"
import {useClient} from "src/client";

let lobbyistApi
let draftId
let saveCount = 0
let firm
let client
let savedL1

const emptyL1 = {
  period_start: null,
  period_end: null,
  firm: {},
  client: {},
  certification: {},
  approval: {},
  have_any_reimbursements: false,
  compensation: {
    amount: null,
    period: null,
    other: null,
    description: []
  },
  areas_of_interest: [],
  period_year_months: [],
  expenses:  {
    amount: null,
    period: null,
    incidentals: null,
    pay_directly: null,
    explanation: null
  },
}

const validations = ref([])
const l1 = reactive(emptyL1)
const has_exemptions = ref(null)
const has_been_approved = ref(null)

function setHasExemptions() {
  has_exemptions.value = Boolean(l1.period_year_months.find(period_year_month => period_year_month.exempt === true))
}

async function getFirmDraft(draft_id) {
  draftId=draft_id
  const result = await lobbyistApi.get(`firm/${firm.lobbyist_firm_id}/l1/draft/${draftId}`)
  if (result) {
    saveCount = result.draft.save_count
    Object.assign(l1, result.draft.user_data)
    savedL1 = JSON.stringify(l1)
    validations.value = result.validations
    has_been_approved.value = !!result.draft.approval_target_id
    setHasExemptions()
  }
}

async function getClientDraft(draft_id) {
  draftId=draft_id
  const result = await lobbyistApi.get(`client/${client.lobbyist_client_id}/l1/draft/${draftId}`)
  if (result) {
    saveCount = result.draft.save_count
    Object.assign(l1, result.draft.user_data)
    savedL1 = JSON.stringify(l1)
  }
}

async function getContractorDraft(draft_id) {
  draftId=draft_id
   const result = await lobbyistApi.get(`firm/${firm.lobbyist_firm_id}/l1/draft/${draftId}/contractor`)
  if (result) {
    saveCount = result.draft.save_count
    Object.assign(l1, result.draft.user_data)
    savedL1 = JSON.stringify(l1)
    validations.value = result.validations
    setHasExemptions()
  }
}

async function saveClientDraft() {
  const payload = JSON.stringify(l1)
  if (payload !== savedL1) {
    lobbyistApi.put(`client/${client.lobbyist_client_id}/l1/draft/${draftId}`, {
      save_count: saveCount,
      user_data: l1
    }).then(result => {
      if (result.save_count) {
        saveCount = result.save_count
        savedL1 = payload
        validations.value = result.validations
        setHasExemptions()
      }
    })
  }
}

async function saveDraft() {
  const payload = JSON.stringify(l1)
  if (payload !== savedL1) {
    lobbyistApi.put(`firm/${firm.lobbyist_firm_id}/l1/draft/${draftId}`, {
      save_count: saveCount,
      user_data: l1
    }).then(result => {
      if (result.save_count) {
        saveCount = result.save_count
        savedL1 = payload
        validations.value = result.validations
        setHasExemptions()
      }
    })
  }
}

async function saveContractorDraft() {
  const payload = JSON.stringify(l1)
  if (payload !== savedL1) {
    lobbyistApi.put(`firm/${firm.lobbyist_firm_id}/l1/contractor-draft/${draftId}`, {
      save_count: saveCount,
      user_data: l1
    }).then(result => {
      if (result.save_count) {
        saveCount = result.save_count
        savedL1 = payload
        validations.value = result.validations
        setHasExemptions()
      }
    })
  }
}

export async function useFirmL1() {
  lobbyistApi = (await useLobbyist()).lobbyistApi
  firm = (await useFirm()).firm
  return { l1, validations, getFirmDraft, saveDraft,getContractorDraft, has_exemptions, has_been_approved, setHasExemptions }
}

export async function useClientL1() {
  lobbyistApi = (await useLobbyist()).lobbyistApi
  client = (await useClient()).client
  return { l1, getClientDraft, saveClientDraft, has_exemptions, setHasExemptions }
}

export async function useContractorL1() {
  lobbyistApi = (await useLobbyist()).lobbyistApi
  firm = (await useFirm()).firm
  return { l1, getContractorDraft, saveContractorDraft, has_exemptions, setHasExemptions }
}
