import MainLayout from "layouts/MainLayout.vue";
import SsoActivation from "pages/SsoActivation.vue";
import PublicAgency from 'pages/PublicAgency.vue';
import PublicAgencyActivate from 'pages/PublicAgencyActivate.vue';
import FirmLayout from "layouts/FirmLayout.vue";
import ClientLayout from "layouts/ClientLayout.vue";
import PublicLayout from "layouts/PublicLayout.vue";
import PublicAgencyVerification from "pages/PublicAgencyVerification.vue";

const routes = [
  {
    path: '/',
    component: MainLayout,
    children: [
      { path: '', component: () => import('pages/LobbyistDashboard.vue') },
      { path: 'activate', component: SsoActivation},
      { path: 'public-agency', name: 'public-agency-dashboard', component: PublicAgency},
      { path: 'public-agency/activate', name: 'public-agency-activate', component: PublicAgencyActivate},
      { path: 'public-agency/request', name: 'public-agency-request', component:() => import ('pages/PublicAgencyRequestAccess.vue')},
      { path: 'public-agency/profile/:id/edit', name: 'profile-edit', component: () => import('pages/PublicAgencyEditProfile.vue')},
      { path: 'public-agency/:id/users', name: 'public-agency-users', component: () => import('pages/PublicAgencyUsers.vue')},
      { path: 'admin/access-request/:request_id', name: 'verify-access-request', component: PublicAgencyVerification},
      { path: 'admin/access-requests', name: 'access-request-list', component: () => import('pages/AccessRequestsList.vue')},
      { path: 'admin/migration', component: () => import('pages/AccessHubMigration.vue') },
      { path: 'admin/management', component: () => import('pages/ManagementDashboard.vue') },
      { path: 'admin/lobbyist-firms', component: () => import('pages/management-dashboard/LobbyistFirms.vue') },
      { path: 'admin/lobbyist-clients', component: () => import('pages/management-dashboard/LobbyistClients.vue') },
      { path: 'admin/lobbyist-contracts', component: () => import('pages/management-dashboard/LobbyistContracts.vue') },
      { path: 'admin/firm-users', component: () => import('pages/admin/FirmUsers.vue')},
      { path: 'admin/client-users', component: () => import('pages/admin/ClientUsers.vue') },
      { path: 'admin/new-reports-by-quarter', component: () => import('pages/admin/NewReportsByQuarter.vue')}
    ]
  },
  {
    path: '/client/:client_id',
    component: ClientLayout,
    children: [
      { path: '', name: 'client-dashboard', component: () => import('pages/ClientDashboard.vue')},
      { path: 'registration/:submission_id', name: 'client-registration', component: () => import('pages/ClientRegistration.vue')},
      { path: 'create-l7', name: 'client-create-l7', component: () => import ('pages/l7/EditL7.vue')},
      { path: 'preview-l7', name: 'client-preview-l7', component: () => import('pages/l7/PreviewL7.vue')},
      { path: 'view-l7/:submission_id', name: 'client-view-l7', component: () => import('pages/l7/ViewL7.vue')},
      { path: 'amend-l7/:report_id', name: 'client-amend-l7', component: () => import ('pages/l7/EditL7.vue')},
      { path: 'history-l7/:submission_id', name: 'client-history-l7', component: () => import ('pages/l7/HistoryL7.vue')},
      { path: 'l3/:submission_id', name: 'client-l3', component: () => import('pages/l3/L3View.vue')},
      { path: 'history-l3/:report_id', name: 'client-history-l3', component: () => import ('pages/l3/HistoryL3.vue')},
      { path: 'l3-draft/:year', name: 'edit-l3', component: () => import('pages/l3/DraftL3.vue')},
      { path: 'l3-draft/:year/preview', name: 'client-create-l3', component: () => import('pages/l3/L3PreviewDraft.vue')},
      { path: 'l1-draft/:draft_id/preview', name: 'client-l1-preview', component: () => import('pages/l1/ClientL1Preview.vue')},
      { path: 'l1-contract/:submission_id', name: 'client-l1-contract', component: () => import('pages/l1/L1View.vue')},
      { path: 'l1-draft/:draft_id', name: 'client-edit-l1', component: () => import('pages/l1/ClientDraft.vue')},
      { path: 'l1-activity-logs', name: 'client-l1-logs', component: () => import('pages/l1/L1ClientLogs.vue')},
    ]
  },
  {
    path: '/firm/:firm_id',
    component: FirmLayout,
    children: [
      { path: '', name: 'firm-dashboard', component: () => import('pages/FirmDashboard.vue') },
      { path: 'registration/:submission_id', name: 'firm-registration', component: () => import('pages/LobbyistFirmRegistration.vue')},
      { path: 'l1-draft/:draft_id', name: 'edit-l1', component: () => import('pages/l1/DraftL1.vue')},
      { path: 'l1-draft/:draft_id/preview', name: 'firm-preview-l1', component: () => import('pages/l1/L1PreviewDraft.vue')},
      { path: 'l2-draft/:period_start', name: 'edit-l2', component: () => import('pages/l2/DraftL2.vue')},
      { path: 'l2-draft/:period_start/preview', name: 'client-create-l2', component: () => import('pages/l2/L2PreviewDraft.vue')},
      { path: 'profile/:submission_id', name: 'firm-profile-view', component: () => import('pages/firm/ViewFirmProfile.vue')},
      { path: 'create-l7', name: 'firm-create-l7', component: () => import ('pages/l7/EditL7.vue')},
      { path: 'preview-l7', name: 'firm-preview-l7', component: () => import('pages/l7/PreviewL7.vue')},
      { path: 'view-l7/:submission_id', name: 'firm-view-l7', component: () => import('pages/l7/ViewL7.vue')},
      { path: 'amend-l7/:report_id', name: 'firm-amend-l7', component: () => import ('pages/l7/EditL7.vue')},
      { path: 'history-l7/:submission_id', name: 'firm-history-l7', component: () => import ('pages/l7/HistoryL7.vue')},
      { path: 'l2/:submission_id', name: 'firm-l2', component: () => import('pages/l2/L2View.vue')},
      { path: 'l1-contract/:submission_id', name: 'firm-l1-contract', component: () => import('pages/l1/L1View.vue')},
      { path: 'history-l2/:report_id', name: 'firm-history-l2', component: () => import ('pages/l2/HistoryL2.vue')},
      { path: 'l1-draft/:draft_id/preview', name: 'firm-l1-preview', component: () => import('pages/l1/L1PreviewDraft.vue')},
      { path: 'l1-contractor-draft/:draft_id', name: 'contractor-edit-l1', component: () => import('pages/l1/ContractorDraft.vue')},
      { path: 'l1-draft/:draft_id/contractor-preview', name: 'l1-contractor-preview', component: () => import('pages/l1/ContractorL1Preview.vue') },
      { path: 'l1-activity-logs', name: 'firm-l1-logs', component: () => import('pages/l1/L1FirmLogs.vue')}
    ]
  },
  {
    path: '/public',
    component: PublicLayout,
    children: [
      { path: 'l7-report/:submission_id', name: 'public-view-l7', component: () => import('pages/l7/PublicViewL7.vue') },
      { path: 'l2-report/:submission_id', name: 'public-view-l2', component: () => import('pages/l2/PublicViewL2.vue') },
      { path: 'l3-report/:submission_id', name: 'public-view-l3', component: () => import('pages/l3/PublicViewL3.vue') },
      { path: 'l1-report/:submission_id', name: 'public-view-l1', component: () => import('pages/l1/PublicViewL1.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
