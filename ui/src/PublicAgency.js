import {ref} from 'vue'
import {useLobbyist} from "src/lobbyist";
import {useApollo} from "@wapdc/pdc-ui/application"

const userPublicAgencies = ref([])
let initialized = false
let lobbyistApi
const apollo = useApollo('service/lobbyist')

async function getUserPublicAgencies(){
  try{
    const result= await apollo.get('public-agency/agencies')
    if(result.success){
      userPublicAgencies.value = result.public_agencies
    }
  } catch (e) {
    alert('Unknown error trying to fetch public agencies')
  }
}

async function autoGrantAgencies(){
  let data
  try {
    const result = await apollo.get('public-agency/auto-grant')
    userPublicAgencies.value = result.public_agencies
    data = result
  } catch (e) {
    data = {success: false, message: e.response.data, status: e.response.status}
  }
  return data
}

const userPendingPublicAgencies = ref([])

async function getUserPendingPublicAgencies(){
  try{
    const result = await apollo.get('public-agency/pending-agencies')
    if(result.success){
      result.pending_agencies.forEach(x => {
        x.payload = JSON.parse(x.payload)
      })
      userPendingPublicAgencies.value = result.pending_agencies
    }
  }catch(e){
    alert('Unknown error trying to fetch pending public agencies')
  }
}

export async function useUserPublicAgencies(){
  lobbyistApi = (await useLobbyist()).lobbyistApi
  if(!initialized){
    const waits = []
    waits.push(await getUserPublicAgencies(false))
    waits.push(await getUserPendingPublicAgencies(false))
    await Promise.all(waits)

    if(!userPublicAgencies.value.length){
      await autoGrantAgencies(false)
    }
    initialized = true
  }

  return{userPublicAgencies, userPendingPublicAgencies}
}


export async function  usePublicAgency() {
  lobbyistApi = (await useLobbyist()).lobbyistApi
  async function updatePublicAgencyProfile(agencyId, payload){
    let data
    try {
      //update agency
      const result = await apollo.post('public-agency/update-profile', {agencyId, payload})
      userPublicAgencies.value = result.public_agencies
      data = result
    } catch (e) {
      data  = {success: false, message: e.response.data, status: e.response.status}
    }
    return data
  }

  async function getPublicAgencies(){
    return await lobbyistApi.get("public-agency/agencies")
  }

  async function getPublicAgency(id){
    let agency = null
    try {
      const result = await apollo.get(`public-agency/agencies/${id}`)
      if(result.success){
        agency = result.public_agencies[0]
      }
    } catch (e) {
      alert('Something went wrong when getting agency')
    }
    return agency
  }

  async function submitPublicAgencyRequest(target_id, payload){
    try{
      const {success} = await lobbyistApi.post("public-agency/request", {target_id, payload})
      if(success){
        await getUserPendingPublicAgencies()
      }
      return success
    } catch (e){
      alert('Something went wrong submitting access request')
      return null
    }
  }

  async function deletePendingPublicAgency(request_id){
    const success = await lobbyistApi.delete(`access-request/${request_id}`)
    if(success){
      await getUserPendingPublicAgencies()
    }
    return {success}
  }

  async function getAuthorizedUsers(agency_id) {
    const {authorized_users} = await lobbyistApi.get(`public-agency/${agency_id}/users`)
    return {authorized_users}
  }

  async function revokeAuthorizedUser(selected_user, agency_id) {
    const {authorized_users} = await lobbyistApi.post("public-agency/revoke",
      {
        uid: selected_user.uid,
        realm: selected_user.realm,
        target_id: agency_id,
        target_type: 'lobbyist_public_agency'
      })

    await getUserPublicAgencies()

    return {authorized_users}
  }

  return { getPublicAgencies, getPublicAgency, updatePublicAgencyProfile, submitPublicAgencyRequest, deletePendingPublicAgency, getAuthorizedUsers, revokeAuthorizedUser}
}

export const useSSO = () => {

  /**
   * Request sso with username password
   */
  async function loginSsoActivation(username, password){
    let data = {}
    try {
      const result = await apollo.post('public-agency/sso-verify-login', {username, password})
      if (result.success) {
        data = {success: result.success, pub_ids: result.pub_ids}
        userPublicAgencies.value = result.public_agencies
      }
    } catch (e) {
      data = {success: false, message: e.result}
    }
    return data
  }

  async function requestSsoActivation(email){
    let data
    try {
      const result = await apollo.post('public-agency/sso-request', {email: email})
      data = {success: result.success, pub_ids: result.pub_ids}
    } catch (e) {
      data = {success: false, message: e.response.data, status: e.response.status}
    }
    return data
  }

  async function verifyActivationCode(verification_code, email, pub_ids){
    let data
    try {
      const result = await apollo.post('public-agency/sso-verify-token', {
        verification_code,
        email,
        pub_ids
      })
      //When token code is invalid
      if (!result.success) {
      } else {
        userPublicAgencies.value = result.public_agencies
      }
      data = result
    } catch (e) {
      data = {success: false, message: e.response.data, status: e.response.status}
    }
    return data
  }

  return {
    loginSsoActivation, requestSsoActivation, verifyActivationCode
  }
}
