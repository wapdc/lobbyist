const fs = require('fs-extra');
const path = require('path');
const os = require('os');

const project_dir_name = 'lobbyist';

const distDir = path.resolve(__dirname, 'dist', 'spa');
const sourceDirApollo = path.resolve(__dirname, '../', 'drupal', project_dir_name);
const targetDirDrupal = path.resolve(__dirname, '../', 'drupal', project_dir_name, 'ui');
const targetDirApollo = path.resolve(os.homedir(), `dev/apollo-d8/web/modules/custom/${project_dir_name}`);

const deployToApollo = process.argv.includes('--deployToApollo');

fs.emptyDir(targetDirDrupal)
  .then(() => fs.copy(distDir, targetDirDrupal, { filter: (src) => src !== targetDirDrupal }))
  .then(() => {
    console.log('\x1b[32m%s\x1b[0m', 'Build files successfully copied to Drupal module directory.\n'); // green console message

    if (deployToApollo){
      return fs.emptyDir(targetDirApollo)
        .then(() => fs.copy(sourceDirApollo, targetDirApollo))
        .then(() => console.log('\x1b[32m%s\x1b[0m', 'Build files successfully deployed to Apollo directory.\n')) // green console message
        .catch((err) => console.error('\x1b[31m%s\x1b[0m', 'An error occurred while copying build files to Apollo D8 directory:', err)); // red console message
    }
  })
  .catch((err) => console.error('\x1b[31m%s\x1b[0m', 'An error occurred while copying build files to Drupal module:', err)); // red console message
